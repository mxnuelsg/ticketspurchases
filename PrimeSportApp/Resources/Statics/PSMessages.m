//
//  PSMessages.m
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSMessages.h"
#import "CRToast.h"


@implementation PSMessages

static PSMessages *sharedInstance = nil;

-(id)init
{
    @synchronized(self)
    {
        return [super init];
    }
}

+(PSMessages *)getModel
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

#pragma mark -
#pragma mark - ALERT VIEW
-(void)showAlertMessage:(NSString *)message withTitle:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"Close"
                                          otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}

#pragma mark -
#pragma mark - ALERT LOGIN PURCHASE PROCESS
-(void)showAlertLoginMessageWithDelay:(NSTimeInterval)interval
{
    [self performSelector:@selector(showAlertForLoginPurchaseProcessTemplate) withObject:nil afterDelay:interval];
}

-(void)showAlertForLoginPurchaseProcessTemplate
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : @"Welcome",
                              kCRToastTextColorKey : [UIColor whiteColor],
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastSubtitleTextKey : [NSString stringWithFormat:@"%@", [PSSessionParameters information].sessionEmail],
                              kCRToastSubtitleTextColorKey : [UIColor whiteColor],
                              kCRToastSubtitleTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastBackgroundColorKey : [UIColor primeSportColorNotificationInfo],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed Login CRToast Message");
                                }];
}

#pragma mark -
#pragma mark - SUCCESS ALERT (Password Changed)
-(void)showAlertForPasswordChanged
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : @"Congrats!",
                              kCRToastTextColorKey : [UIColor whiteColor],
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastSubtitleTextKey : @"Your password has been changed",
                              kCRToastSubtitleTextColorKey : [UIColor whiteColor],
                              kCRToastSubtitleTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastBackgroundColorKey : [UIColor primeSportColorNotificationSuccess],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Password changed CRToast Message");
                                }];
}

#pragma mark -
#pragma mark - SUCCESS ALERT (ContactInfo)
-(void)showAlertForContactInfoChanged
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : @"Congrats!",
                              kCRToastTextColorKey : [UIColor whiteColor],
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastSubtitleTextKey : @"Your account information has been updated",
                              kCRToastSubtitleTextColorKey : [UIColor whiteColor],
                              kCRToastSubtitleTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastBackgroundColorKey : [UIColor primeSportColorNotificationSuccess],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Contact info changed CRToast Message");
                                }];
}

#pragma mark -
#pragma mark - SUCCESS ALERT (Billing info deleted)
-(void)showAlertForBillingDeleted
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : @"Billing Deleted!",
                              kCRToastTextColorKey : [UIColor whiteColor],
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastBackgroundColorKey : [UIColor primeSportColorNotificationError],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Billing deleted CRToast Message");
                                }];
}

#pragma mark -
#pragma mark - SUCCESS ALERT (Billing info Saved)
-(void)showAlertForBillingInfoSaved
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : @"Congrats!",
                              kCRToastTextColorKey : [UIColor whiteColor],
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastSubtitleTextKey : @"Your billing information has been saved",
                              kCRToastSubtitleTextColorKey : [UIColor whiteColor],
                              kCRToastSubtitleTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastBackgroundColorKey : [UIColor primeSportColorNotificationSuccess],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Billing info Saved CRToast Message");
                                }];
}

#pragma mark -
#pragma mark - GENERAL ALERT
-(void)showToastStatusAlertWithMessage:(NSString *)message withBackgroundColor:(UIColor *)bgColor withTextColor:(UIColor *)txtColor withImage:(NSString *)image
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : message,
                              kCRToastTextColorKey : txtColor,
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentLeft),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeStatusBar),
                              kCRToastBackgroundColorKey : bgColor,
                              kCRToastImageKey : [UIImage imageNamed:image],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"General CRToast Message");
                                }];

}

-(void)showToastStatusAlertWithMessage:(NSString *)message withBackgroundColor:(UIColor *)bgColor withTextColor:(UIColor *)txtColor
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : message,
                              kCRToastTextColorKey : txtColor,
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypePush),
                              kCRToastNotificationTypeKey : @(CRToastTypeStatusBar),
                              kCRToastBackgroundColorKey : bgColor,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"General CRToast Message with Image");
                                }];
    
}

#pragma mark -
#pragma mark - REACHABILITY
-(void)showAlertForUnReachableNetwork
{
    //Configuration
    NSDictionary *options = @{
                              kCRToastTextKey : @"Network Error",
                              kCRToastTextColorKey : [UIColor whiteColor],
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:14],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastSubtitleTextKey : @"Check your internet connection",
                              kCRToastSubtitleTextColorKey : [UIColor whiteColor],
                              kCRToastSubtitleTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastBackgroundColorKey : [UIColor primeSportColorNotificationError],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    //Show
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed Reachability CRToast Message");
                                }];
}

#pragma mark -
#pragma mark - ALERT REQUEST ERROR
-(void)showAlertForRequestError
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kErrorTitle
                                                    message:@"An error has occurred during this data request"
                                                   delegate:self
                                          cancelButtonTitle:@"Close"
                                          otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}

@end
