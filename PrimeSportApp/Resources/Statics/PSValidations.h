//
//
//  Created by Manuel Salinas on 29/07/14
//
/* This class contains all possible validations messages */


//Titles
static NSString *const kWarningTitle = @"Warning";
static NSString *const kErrorTitle = @"Error";
static NSString *const kInfoTitle = @"Notification";

//Body
static NSString *const kValidationFirstname         = @"Enter your first name";
static NSString *const kValidationLastname          = @"Enter your last name";
static NSString *const kValidationPhone             = @"Enter your number phone";
static NSString *const kValidationPhoneLength       = @"Your Phone is wrong. You need to enter at least 10 digits";
static NSString *const kValidationAddress           = @"Enter your address";
static NSString *const kValidationCountry           = @"Select your country";
static NSString *const kValidationState             = @"Select your state";
static NSString *const kValidationCity              = @"Enter your city";
static NSString *const kValidationZipCode           = @"Enter your zip code";
static NSString *const kValidationZipCodeLength     = @"Your zip code must have 5 characters";
static NSString *const kValidationConfirmCountry    = @"Confirm your country";
static NSString *const kValidationAlias             = @"Enter an Alias for your billing information";
static NSString *const kValidationBillingInfo       = @"Select billing information to delete";
static NSString *const kValidationCurrentPassword   = @"Enter your current password";
static NSString *const kValidationNewPassword       = @"Enter your new password";
static NSString *const kValidationNewPasswordLength = @"Your password must have at least 6 characters";
static NSString *const kValidationConfirmNewPassword = @"Confirm your new password";
static NSString *const kValidationDifferentNewPasswords = @"Your new password and confirmation are different";
static NSString *const kValidationEmail             = @"Enter your email account";
static NSString *const kValidationConfirmEmail      = @"Confirm your email account";
static NSString *const kValidationEmailWrongFormat  = @"Your email account is not valid";
static NSString *const kValidationDifferentEmail    = @"Your email account and confirm are different";
static NSString *const kValidationPriceRange        = @"Enter a price range to look for";
static NSString *const kValidationQuantity          = @"Enter a quantity to look for";
static NSString *const kValidationEmptyPassword     = @"Password cannot be empty";
static NSString *const kValidationEmptyUsername     = @"Username cannot be empty";
static NSString *const kValidationConfirmPassword   = @"Confirm your password";
static NSString *const kValidationDifferentPasswords = @"Your password and confirm password are different";
static NSString *const kValidationAccountPassword   = @"Enter a password for your account";
static NSString *const kValidationCreditCardType    = @"Select your credit card type";
static NSString *const kValidationCreditCardNumber  = @"Enter your credit card number";
static NSString *const kValidationExpirationCreditCard = @"Expiration date is not correct";
static NSString *const kValidationCreditCardCode    = @"Enter your credit card code";
static NSString *const kValidationCitySelection     = @"Select your city";

static NSString *const kSessionExpired = @"Your session has expired. Please log in again";
