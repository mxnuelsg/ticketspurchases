//
//  PSUtils.h
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSUtils : NSObject

+(NSUInteger)getTotalOfPagesFromTotalRecords:(NSUInteger)records;
+(NSString *)moneyFormat:(NSString *)value withFraction:(BOOL)showFraction;
+(NSAttributedString *)strikethroughStyleForText:(NSString *)text;
+(NSString *)validateWhiteSpaces:(NSString *)text;
+(NSString *)removeAllWhiteSpacesInString:(NSString *)text;
+(BOOL)validateIsMail:(NSString *)stringMail;
+(NSString *)dateTimeFormatFromDate:(NSDate *)dateTime;
+(NSString *)dateFormat:(NSDate *)date;
+(NSString *)timeFormat:(NSDate *)time withSeconds:(BOOL)showSeconds;
+(NSString *)renderStringDateFromDate:(NSDate *)date;
+(void)tabBarInteracionIsEnable:(BOOL)userInteractionEnable;
+(int)getRandomNumberFrom:(int)from to:(int)to;
+(NSString *)monthWordFromNumberMonth:(int)month;
+(NSString *)getDayWeekMonthDayYearFormatFromStringDate:(NSString *)stringDate;
@end
