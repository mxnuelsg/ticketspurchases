//
//  PSWebServiceUtils.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 28/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSWebServiceUtils.h"
#import "PSLocation.h"

@implementation PSWebServiceUtils

/************** WEB SERVICE REQUEST **************/

#pragma mark -
#pragma mark - QUERY FORMAT
+(NSString *)createQueryString:(NSDictionary *)parameters
{
	NSString *queryString = @"?";
    
    NSArray *arrayParametersKeys;
    arrayParametersKeys = [parameters allKeys];
    
    NSUInteger index = 1;
    
    for (NSString *key in arrayParametersKeys)
    {
        if (index == arrayParametersKeys.count)
        {
            NSString *parameterValue = [parameters objectForKey:key];
            queryString = [queryString stringByAppendingFormat:@"%@=%@",key, parameterValue];
        }
        else
        {
            NSString *parameterValue = [parameters objectForKey:key];
            queryString = [queryString stringByAppendingFormat:@"%@=%@&",key, parameterValue];
        }
        index++;
    }
    
    queryString =  [queryString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
	return queryString;
}


+(NSMutableURLRequest *)setHeadersForGET_MethodInRequest:(NSMutableURLRequest *)getRequest
{
    [getRequest setHTTPMethod:@"GET"];
    [getRequest addValue:wsUSERNAME forHTTPHeaderField:@"username"];
    [getRequest addValue:wsPASSWORD forHTTPHeaderField:@"password"];
    [getRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [getRequest addValue:[PSSessionParameters information].sessionToken forHTTPHeaderField:@"AuthorizationToken"];
    
    return getRequest;
}

+(NSMutableURLRequest *)setHeadersForGET_MethodInRequestWithLocation:(NSMutableURLRequest *)getRequest
{
    [getRequest setHTTPMethod:@"GET"];
    [getRequest addValue:wsUSERNAME forHTTPHeaderField:@"username"];
    [getRequest addValue:wsPASSWORD forHTTPHeaderField:@"password"];
    [getRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [getRequest addValue:[PSSessionParameters information].sessionToken forHTTPHeaderField:@"AuthorizationToken"];
    [getRequest addValue:[PSLocation GPS].latitude forHTTPHeaderField:@"Lat"];
    [getRequest addValue:[PSLocation GPS].longitude forHTTPHeaderField:@"Long"];
        
    return getRequest;
}

+(NSMutableURLRequest *)setHeadersForPOST_MethodInRequest:(NSMutableURLRequest *)postRequest withBody:(NSData *)dataBody
{
    [postRequest setHTTPMethod:@"POST"];
    [postRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [postRequest addValue:wsUSERNAME forHTTPHeaderField:@"username"];
    [postRequest addValue:wsPASSWORD forHTTPHeaderField:@"password"];
    [postRequest addValue:[PSSessionParameters information].sessionToken forHTTPHeaderField:@"AuthorizationToken"];
    [postRequest setHTTPBody: dataBody];
    
    return postRequest;
}

@end
