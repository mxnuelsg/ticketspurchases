//
//  PSUtils.m
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
/*******
 Note:
        HH = 24 hours
        hh = 12 hours
 ******/


#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]

#import "PSUtils.h"
#import "PSPrincipalViewController.h"

@implementation PSUtils

#pragma mark -
#pragma mark - PAGES FROM COUNT ARRAY
+(NSUInteger)getTotalOfPagesFromTotalRecords:(NSUInteger)records
{
    NSUInteger totalRecords = records;
    NSUInteger pagesTotal = totalRecords / 10;
    pagesTotal = totalRecords % 10 > 0 ? pagesTotal +1 : pagesTotal;
   
    return pagesTotal;
}

#pragma mark -
#pragma mark - MONEY FORMAT
+(NSString *)moneyFormat:(NSString *)value withFraction:(BOOL)showFraction
{
    if (value.length == 0)
    {
        value = @"0";
    }
    
    double currentValue = [value doubleValue];
    
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    if (showFraction == YES)
    {
        [numberFormat setMaximumFractionDigits:2];
    }
    else
    {
        [numberFormat setMaximumFractionDigits:0];
    }
    
    
    NSString *moneyString = [numberFormat stringFromNumber:[NSNumber numberWithDouble:currentValue]];
    
    return  moneyString;
}

#pragma mark -
#pragma mark - STRIKE THROUGH FORMAT
+(NSAttributedString *)strikethroughStyleForText:(NSString *)text
{
    NSDictionary *attributes = @{ NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]};
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
    return attrText;
    
    /*
     //How to uses
     Label.attributedText = [PSUtils strikethroughStyleForText:text];
    */
}

#pragma mark -
#pragma mark - VALIDATIONS
+(NSString *)validateWhiteSpaces:(NSString *)text
{
    if (text != nil)
    {
        return [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    return @"";
}

+(NSString *)removeAllWhiteSpacesInString:(NSString *)text
{
   text =  [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return text;
}

+(BOOL)validateIsMail:(NSString *)stringMail;
{
    NSString *email = [stringMail lowercaseString];
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    return [self validateValue:email byUsingRegEx:emailRegEx];
}

+ (BOOL)validateValue:(NSObject *)val byUsingRegEx:(NSString *)regEx
{
    NSError *error = nil;
    NSUInteger numberOfMatches = 0;
    
    // Creates an NSRegularExpression instance with the specified regular expression pattern and options.
    NSRegularExpression *regexObj = [NSRegularExpression regularExpressionWithPattern:regEx
                                                                              options:NSRegularExpressionCaseInsensitive
                                                                                error:&error];
    if (error)
    {
        NSLog(@"An error has ocurred while trying to compile the regular expression for email");
    }
    else
    {
        // return more that zero if there the value matches
        numberOfMatches = [regexObj numberOfMatchesInString:(NSString*)val
                                                    options:0
                                                      range:NSMakeRange(0, [(NSString*)val length])];
        if (numberOfMatches == 0)
        {
            //          Pattern did not match string
        }
    }
    
    // if numberOfMatches == zero it means the value doesn't match
	return (numberOfMatches > 0);
}

#pragma mark -
#pragma mark - DATE & TIME FORMAT
+(NSString *)dateTimeFormatFromDate:(NSDate *)dateTime
{
    //Example: 01/01/2014 04:30 pm
    NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
    [dateTimeFormat setAMSymbol:@"am"];
    [dateTimeFormat setPMSymbol:@"pm"];
    [dateTimeFormat setDateFormat:@"dd/MM/yyyy hh:mm a"];
    
    NSString *fechaConFormato = [dateTimeFormat stringFromDate:dateTime];
    
    return fechaConFormato;
}

+(NSString *)dateFormat:(NSDate *)date
{
     //Example: 01/01/2014
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateWithFormat = [dateFormat stringFromDate:date];
    
    return dateWithFormat;
}

+(NSString *)timeFormat:(NSDate *)time withSeconds:(BOOL)showSeconds
{

    NSDateFormatter *timeFormat  = [[NSDateFormatter alloc] init];
    [timeFormat setAMSymbol:@"am"];
    [timeFormat setPMSymbol:@"pm"];

    if (showSeconds == YES)
    {
        //Example: 04:17:34 pm
        [timeFormat setDateFormat:@"hh:mm:ss a"];
    }
    else
    {
        //Example: 04:17 pm
        [timeFormat setDateFormat:@"hh:mm a"];
    }
    
    NSString *timeWithFormat = [timeFormat stringFromDate:time];
    
    return timeWithFormat;
}

+(NSString *)renderStringDateFromDate:(NSDate *)date
{
    //Example: May 12, 2014 (Month day, year)
    NSDateComponents *today = [CURRENT_CALENDAR components:DATE_COMPONENTS
                                                fromDate:date];
    
    NSString *stringMonthYear;
    
    switch (today.month)
    {
        case 1:
            stringMonthYear = [NSString stringWithFormat:@"January %li, %li", (long)today.day , (long)today.year];
            break;
            
        case 2:
            stringMonthYear = [NSString stringWithFormat:@"February %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 3:
            stringMonthYear = [NSString stringWithFormat:@"March %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 4:
            stringMonthYear = [NSString stringWithFormat:@"April %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 5:
            stringMonthYear = [NSString stringWithFormat:@"May %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 6:
            stringMonthYear = [NSString stringWithFormat:@"June %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 7:
            stringMonthYear = [NSString stringWithFormat:@"July %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 8:
            stringMonthYear = [NSString stringWithFormat:@"August %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 9:
            stringMonthYear = [NSString stringWithFormat:@"September %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 10:
            stringMonthYear = [NSString stringWithFormat:@"October %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 11:
            stringMonthYear = [NSString stringWithFormat:@"November %li, %li", (long)today.day, (long)today.year];
            break;
            
        case 12:
            stringMonthYear = [NSString stringWithFormat:@"December %li, %li", (long)today.day, (long)today.year];
            break;
            
        default:
            break;
    }
    return stringMonthYear;
}

+(NSString *)monthWordFromNumberMonth:(int)month
{
    NSString *stringMonth;
    
    switch (month)
    {
        case 1:
            stringMonth = @"January";
            break;
            
        case 2:
            stringMonth = @"February";
            break;
            
        case 3:
            stringMonth =  @"March";
            break;
            
        case 4:
            stringMonth = @"April";
            break;
            
        case 5:
            stringMonth =  @"May";
            break;
            
        case 6:
            stringMonth =  @"June";
            break;
            
        case 7:
            stringMonth =  @"July";
            break;
            
        case 8:
            stringMonth =  @"August";
            break;
            
        case 9:
            stringMonth =  @"September";
            break;
            
        case 10:
            stringMonth =  @"October";
            break;
            
        case 11:
            stringMonth =  @"November";
            break;
            
        case 12:
            stringMonth =  @"December";
            break;
            
        default:
            break;
    }
    return stringMonth;
}

+(NSString *)getDayWeekMonthDayYearFormatFromStringDate:(NSString *)stringDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"]; 
    NSDate *date = [dateFormat dateFromString:stringDate];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"EEEE, MMMM dd, YYYY"];
    stringDate = [dateFormat stringFromDate:date];
    
    return stringDate;
}

#pragma mark -
#pragma mark - UI FUNCTIONS
+(void)tabBarInteracionIsEnable:(BOOL)userInteractionEnable
{
    PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    [(PSPrincipalViewController *)[delegate.window rootViewController] tabBarInteraction:userInteractionEnable];

}

#pragma mark -
#pragma mark - RANDOM NUMBER
+(int)getRandomNumberFrom:(int)from to:(int)to
{
    return (int)from + arc4random() % (to-from+1);
}



@end
