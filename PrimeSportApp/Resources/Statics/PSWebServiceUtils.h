//
//  PSWebServiceUtils.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 28/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSWebServiceUtils : NSObject

+(NSString *)createQueryString:(NSDictionary *)parameters;
+(NSMutableURLRequest *)setHeadersForGET_MethodInRequest:(NSMutableURLRequest *)getRequest;
+(NSMutableURLRequest *)setHeadersForGET_MethodInRequestWithLocation:(NSMutableURLRequest *)getRequest;
+(NSMutableURLRequest *)setHeadersForPOST_MethodInRequest:(NSMutableURLRequest *)postRequest withBody:(NSData *)dataBody;

@end
