//
//  BlurView.h
//
//  Created by Manuel Salinas on 18/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BlurView : UIView

-(void)setBlurColor:(UIColor *)blurColor;
-(void)setBlurAlpha:(CGFloat)alphaValue;

@end
