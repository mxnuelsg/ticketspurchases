//
//  UIView+ParallaxMotion.h
//

#import <UIKit/UIKit.h>

@interface UIView (NGAParallaxMotion)

// Positive values make the view appear to be above the surface
// Negative values are below.
// The unit is in points
@property (nonatomic) CGFloat parallaxIntensity;

@end
