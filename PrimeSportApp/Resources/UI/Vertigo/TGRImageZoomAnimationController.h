
#import <Foundation/Foundation.h>

// Image zoom custom transition.
@interface TGRImageZoomAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

// The image view that will be used as the source (zoom in) or destination
// (zoom out) of the transition.
@property (weak, nonatomic, readonly) UIImageView *referenceImageView;

// Initializes the receiver with the specified reference image view.
- (id)initWithReferenceImageView:(UIImageView *)referenceImageView;

@end
