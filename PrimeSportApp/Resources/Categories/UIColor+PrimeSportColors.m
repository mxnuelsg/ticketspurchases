//
//  UIColor+PrimeSportColors.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 28/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "UIColor+PrimeSportColors.h"

@implementation UIColor (PrimeSportColors)

+(UIColor *)primeSportColorNavigationController
{
    return [UIColor colorWithHexadecimal:@"#102B48" alpha:1.0];
}

+(UIColor *)primeSportColorBackground
{
    return [UIColor colorWithHexadecimal:@"#EDEFF1" alpha:1.0];
}

+(UIColor *)primeSportColorTabBarController
{
    return [UIColor colorWithHexadecimal:@"#3375AD" alpha:1.0];
}

+(UIColor *)primeSportColorToolBar
{
    return [UIColor colorWithHexadecimal:@"#435265" alpha:1.0];
}

+(UIColor *)primeSportColorHeaderSection
{
    return [UIColor colorWithHexadecimal:@"#596A7F" alpha:1.0];
}

+(UIColor *)primeSportColorLineSection
{
    return [UIColor colorWithHexadecimal:@"#1A3E5C" alpha:1.0];
}

+(UIColor *)primeSportColorCellText
{
    return [UIColor colorWithHexadecimal:@"#396595" alpha:1.0];
}

+(UIColor *)primeSportColorCellBlue
{
    return [UIColor colorWithHexadecimal:@"#CCDFFC" alpha:0.4];
}

+(UIColor *)primeSportColorYellow
{
    return [UIColor colorWithHexadecimal:@"#FEDD33" alpha:1];
}

+(UIColor *)primeSportColorRed
{
    return [UIColor colorWithHexadecimal:@"#E85948" alpha:1];
}

+(UIColor *)primeSportColorDoneButton
{
    return [UIColor colorWithHexadecimal:@"#FCEA39" alpha:1.0];
}

+(UIColor *)primeSportColorSelectedCell
{
    return [UIColor colorWithHexadecimal:@"#FEDD33" alpha:0.50];
}

+(UIColor *)primeSportColorNotificationSuccess
{
    return [UIColor colorWithHexadecimal:@"#3EA19C" alpha:1.0];
}

+(UIColor *)primeSportColorNotificationInfo
{
    return [UIColor colorWithHexadecimal:@"#2972a5" alpha:1.0];
}

+(UIColor *)primeSportColorNotificationError
{
    return [UIColor colorWithHexadecimal:@"#D22433" alpha:1.0];
}

+(UIColor *)primeSportColorFacebook
{
    return [UIColor colorWithHexadecimal:@"#3A5998" alpha:1.0];
}

+(UIColor *)primeSportColorTwitter
{
    return [UIColor colorWithHexadecimal:@"#5DA9DD" alpha:1.0];
}

@end
