//
//  UIScrollView+AutoSize_Top.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 04/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (AutoSize_Top)

- (void)autosize;
- (void)scrollToTopAnimated:(BOOL)animated;

@end
