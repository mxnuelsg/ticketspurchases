//
//  NSArray+FilteredSorted.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 19/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (FilteredSorted)

- (NSArray *)filteredArrayUsingPredicateFormat:(NSString *)predicateFormat arguments:(NSArray *)arguments sortedWithKey:(NSString *)key ascending:(BOOL)ascending;
- (NSArray *)filteredArrayUsingPredicateFormat:(NSString *)predicateFormat arguments:(NSArray *)arguments;
- (NSArray *)filteredArrayUsingPredicateFormat:(NSString *)predicateFormat;
- (NSArray *)sortedArrayWithKey:(NSString *)key ascending:(BOOL)ascending;
- (NSArray *)collectUsingKey:(NSString *)key;
- (NSArray *)collapseUsingKey:(NSString *)key;

@end
