//
//  UILabel+Boldify.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)

-(void) boldSubstring: (NSString *) substring;
-(void) boldRange: (NSRange) range;

@end
