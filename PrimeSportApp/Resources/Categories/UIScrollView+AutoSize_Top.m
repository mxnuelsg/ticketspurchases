//
//  UIScrollView+AutoSize_Top.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 04/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "UIScrollView+AutoSize_Top.h"

@implementation UIScrollView (AutoSize_Top)

- (void)scrollToTopAnimated:(BOOL)animated;
{
    [self scrollRectToVisible:CGRectMake(0, 0, self.bounds.size.width, 44) animated:animated];
}

- (void)autosize;
{
    __block CGSize maxSize = CGSizeMake(0, 0);
    [[self subviews] enumerateObjectsUsingBlock:^(UIView *scrollSubview, NSUInteger idx, BOOL *stop) {
        if (scrollSubview.isHidden) return;
        if (CGRectGetMaxY(scrollSubview.frame) > maxSize.height)
            maxSize.height = CGRectGetMaxY(scrollSubview.frame);
        if (CGRectGetMaxX(scrollSubview.frame) > maxSize.width)
            maxSize.width = CGRectGetMaxX(scrollSubview.frame);
    }];
//    NSLog(@"update scrollview content size %@", NSStringFromCGSize(maxSize));
    [self setContentSize:maxSize];
}
@end
