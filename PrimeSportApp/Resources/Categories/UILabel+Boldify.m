//
//  UILabel+Boldify.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "UILabel+Boldify.h"

@implementation UILabel (Boldify)

- (void)boldRange:(NSRange)range
{
    if (![self respondsToSelector:@selector(setAttributedText:)])
    {
        return;
    }
    
    NSMutableAttributedString *attributedText;
    if (!self.attributedText)
    {
        attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    }
    else
    {
        attributedText = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    }
    
    [attributedText setAttributes:@{
                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize],
                                    NSForegroundColorAttributeName:[UIColor colorWithHexadecimal:@"#102B48" alpha:1.0]
                                    } range:range];
    
    self.attributedText = attributedText;
}

- (void)boldSubstring:(NSString*)substring
{
    if(!substring) return;
    NSRange range = [self.text rangeOfString:substring];
    [self boldRange:range];
}


@end
