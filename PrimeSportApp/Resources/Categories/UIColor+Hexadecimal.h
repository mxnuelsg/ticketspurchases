//
//  UIColor+Hexadecimal.h
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hexadecimal)

// hexadecimal - debe ser formato: #FF00CC
// alpha - debe ser en un rango 0.0 - 1.0
+ (UIColor *)colorWithHexadecimal:(NSString *)hexadecimal alpha:(CGFloat)alpha;
@end
