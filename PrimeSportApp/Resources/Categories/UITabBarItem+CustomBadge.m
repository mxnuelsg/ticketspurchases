//
//  UITabBarItem+CustomBadge.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 13/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

static int const kCUSTOM_BADGE_TAG = 99;

#import "UITabBarItem+CustomBadge.h"

@implementation UITabBarItem (CustomBadge)

-(void)setCustomBadgeValue:(NSString *)value
{
    
    UIFont *myAppFont = [UIFont systemFontOfSize:13.0];
    UIColor *myAppFontColor = [UIColor blackColor];
    UIColor *myAppBackColor = [UIColor primeSportColorYellow];
    
    [self setCustomBadgeValue:value withFont:myAppFont andFontColor:myAppFontColor andBackgroundColor:myAppBackColor];
}



-(void) setCustomBadgeValue: (NSString *) value withFont: (UIFont *) font andFontColor: (UIColor *) color andBackgroundColor: (UIColor *) backColor
{
    UIView *v = [self valueForKey:@"view"];
    
    [self setBadgeValue:value];
    
    // REMOVE PREVIOUS IF EXIST
    for(UIView *sv in v.subviews)
    {
        if(sv.tag == kCUSTOM_BADGE_TAG)
        {
            [sv removeFromSuperview];
            continue;
        }
    }
    
    
    for(UIView *sv in v.subviews)
    {
        NSString *str = NSStringFromClass([sv class]);
        
        if([str isEqualToString:@"_UIBadgeView"])
        {
            UILabel *l = [[UILabel alloc] initWithFrame:[sv frame]];
            [l setFont:font];
            [l setText:value];
            [l setBackgroundColor:backColor];
            [l setTextColor:color];
            [l setTextAlignment:NSTextAlignmentCenter];
            
            l.layer.cornerRadius = l.frame.size.height/2;
            l.layer.masksToBounds = YES;
            
            [v addSubview:l];
            [sv setHidden:YES];
            
            l.tag = kCUSTOM_BADGE_TAG;
        }
    }
}

@end
