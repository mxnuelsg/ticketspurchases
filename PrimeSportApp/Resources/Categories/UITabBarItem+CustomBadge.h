//
//  UITabBarItem+CustomBadge.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 13/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarItem (CustomBadge)

-(void)setCustomBadgeValue:(NSString *)value;
-(void)setCustomBadgeValue:(NSString *)value withFont:(UIFont *)font andFontColor:(UIColor *)color andBackgroundColor:(UIColor *) backColor;

@end
