//
//  UIColor+PrimeSportColors.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 28/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PrimeSportColors)

+(UIColor *)primeSportColorNavigationController;
+(UIColor *)primeSportColorBackground;
+(UIColor *)primeSportColorTabBarController;
+(UIColor *)primeSportColorToolBar;
+(UIColor *)primeSportColorHeaderSection;
+(UIColor *)primeSportColorLineSection;
+(UIColor *)primeSportColorCellText;
+(UIColor *)primeSportColorCellBlue;
+(UIColor *)primeSportColorYellow;
+(UIColor *)primeSportColorRed;
+(UIColor *)primeSportColorDoneButton;
+(UIColor *)primeSportColorSelectedCell;
+(UIColor *)primeSportColorNotificationSuccess;
+(UIColor *)primeSportColorNotificationInfo;
+(UIColor *)primeSportColorNotificationError;
+(UIColor *)primeSportColorFacebook;
+(UIColor *)primeSportColorTwitter;

@end
