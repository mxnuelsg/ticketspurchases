//
//  PSLocation.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 09/09/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSLocation : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}

@property (copy, nonatomic) NSString *latitude;
@property (copy, nonatomic) NSString *longitude;

+(PSLocation *)GPS;

-(void)updateLocation;

@end
