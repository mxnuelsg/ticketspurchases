//
//  PSSessionParameters.m
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSSessionParameters.h"

@implementation PSSessionParameters

static PSSessionParameters *sharedInstance = nil;

-(id)init
{
    @synchronized(self)
    {
        [self reset];
        return [super init];
    }
}

#ifndef __clang_analyzer__
+(PSSessionParameters *)information
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}
#endif

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

+ (id) copyWithZone:(NSZone *)zone
{
    return self;
}


#pragma mark -
#pragma mark - PUBLIC METHODS
-(void)setSessionParametersWithToken:(NSString *)token withEmail:(NSString *)email
{
    self.isSessionActive = YES;
    self.sessionToken = token;
    self.sessionEmail = email;
}

-(void)setFacebookSessionParametersWithToken:(NSString *)fbToken withEmail:(NSString *)fbEmail withIdentifier:(NSString *)fbId
{
    self.facebookEmail = fbEmail;
    self.facebookToken = fbToken;
    self.facebookId    = fbId;
}

-(void)reset
{
    NSLog(@"reset session");
    self.sessionToken = [NSString string];
    self.sessionEmail = [NSString string];
    self.isSessionActive = NO;
    
    self.facebookEmail = [NSString string];
    self.facebookToken = [NSString string];
    self.facebookId    = [NSString string];
}

@end
