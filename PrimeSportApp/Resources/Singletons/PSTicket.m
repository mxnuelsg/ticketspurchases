//
//  PSTicket.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSTicket.h"

@implementation PSTicket

static PSTicket *sharedInstance = nil;

-(id)init
{
    @synchronized(self)
    {
        [self reset];
        return [super init];
    }
}

#ifndef __clang_analyzer__
+(PSTicket *)information
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}
#endif

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

+ (id) copyWithZone:(NSZone *)zone
{
    return self;
}


#pragma mark -
#pragma mark - PUBLIC METHODS
-(void)useBillingInfo
{
    self.isUsingBillingInfoInShipping = YES;
    
    self.shippingStorageAddress1      = self.tcktEventShippingInfoAddress1;
    self.shippingStorageAddress2      = self.tcktEventShippingInfoAddress2;
    self.shippingStorageCity          = self.tcktEventShippingInfoCity;
    self.shippingStorageZipCode       = self.tcktEventShippingInfoZipCode;
    self.shippingStorageCountry       = self.tcktEventShippingInfoCountry;
    self.shippingStorageCountryId     = self.tcktEventShippingInfoCountryId;
    self.shippingStorageState         = self.tcktEventShippingInfoState;
    self.shippingStorageStateId       = self.tcktEventShippingInfoStateId;
}

-(void)reset
{
    NSLog(@"reset ticket");
    self.isPurchaseProcessActive            = NO;
    self.tcktEventId                        = [NSString string];
    self.tcktEventTitle                     = [NSString string];
    self.tcktEventVenue                     = [NSString string];
    self.tcktEventDate                      = [NSString string];
    self.tcktEventSection                   = [NSString string];
    self.tcktEventRow                       = [NSString string];
    self.tcktEventQuantity                  = [NSString string];
    self.tcktEventPricePerQuantity          = [NSString string];
    self.tcktEventPrice                     = [NSString string];
    self.tcktEventCouponCode                = [NSString string];
    self.tcktEventShippingMethodId          = [NSString string];
    self.tcktEventShippingMethod            = [NSString string];
    self.tcktEventCreditCardNumber          = [NSString string];
    self.tcktEventCreditCardType            = [NSString string];
    self.tcktEventCreditCardExpirationDate  = [NSString string];
    self.tcktEventCreditCardSecurityCode    = [NSString string];
    self.tcktEventHolderFirstName           = [NSString string];
    self.tcktEventHolderLastName            = [NSString string];
    self.tcktEventHolderPhoneNumber         = [NSString string];
    self.tcktEventHolderMobileNumber        = [NSString string];
    self.tcktEventShippingInfoAddress1      = [NSString string];
    self.tcktEventShippingInfoAddress2      = [NSString string];
    self.tcktEventShippingInfoCountry       = [NSString string];
    self.tcktEventShippingInfoCountryId     = [NSString string];
    self.tcktEventShippingInfoState         = [NSString string];
    self.tcktEventShippingInfoStateId       = [NSString string];
    self.tcktEventShippingInfoCity          = [NSString string];
    self.tcktEventShippingInfoZipCode       = [NSString string];
    self.tcktEventBillingSelected           = [NSString string];
    self.tcktEventBillingSelectedId         = [NSString string];
    
    [self resetShippingInfo];
}

-(void)resetShippingInfo
{
    self.isUsingBillingInfoInShipping = NO;
    
    self.shippingStorageAddress1    = [NSString string];
    self.shippingStorageAddress2    = [NSString string];
    self.shippingStorageCountry     = [NSString string];
    self.shippingStorageCountryId   = [NSString string];
    self.shippingStorageState       = [NSString string];
    self.shippingStorageStateId     = [NSString string];
    self.shippingStorageCity        = [NSString string];
    self.shippingStorageZipCode     = [NSString string];
}
@end
