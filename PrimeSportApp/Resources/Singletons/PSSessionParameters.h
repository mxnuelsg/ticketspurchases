//
//  PSSessionParameters.h
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSSessionParameters : NSObject

@property (copy, nonatomic) NSString *sessionToken;
@property (copy, nonatomic) NSString *sessionEmail;
@property (assign, nonatomic) BOOL isSessionActive;

@property (copy, nonatomic) NSString *facebookEmail;
@property (copy, nonatomic) NSString *facebookToken;
@property (copy, nonatomic) NSString *facebookId;

+(PSSessionParameters *)information;

-(void)setSessionParametersWithToken:(NSString *)token withEmail:(NSString *)email;
-(void)setFacebookSessionParametersWithToken:(NSString *)fbToken withEmail:(NSString *)fbEmail withIdentifier:(NSString *)fbId;
-(void)reset;

@end
