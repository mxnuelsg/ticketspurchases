//
//  PSTicket.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSTicket : NSObject

//Properties
@property (assign, nonatomic) BOOL isPurchaseProcessActive;

@property (copy, nonatomic) NSString *tcktEventId;
@property (copy, nonatomic) NSString *tcktEventTitle;
@property (copy, nonatomic) NSString *tcktEventVenue;
@property (copy, nonatomic) NSString *tcktEventDate;
@property (copy, nonatomic) NSString *tcktEventSection;
@property (copy, nonatomic) NSString *tcktEventRow;
@property (copy, nonatomic) NSString *tcktEventQuantity;
@property (copy, nonatomic) NSString *tcktEventPricePerQuantity;
@property (copy, nonatomic) NSString *tcktEventPrice;
@property (copy, nonatomic) NSString *tcktEventCouponCode;
@property (copy, nonatomic) NSString *tcktEventShippingMethodId;
@property (copy, nonatomic) NSString *tcktEventShippingMethod;
@property (copy, nonatomic) NSString *tcktEventCreditCardNumber;
@property (copy, nonatomic) NSString *tcktEventCreditCardType;
@property (copy, nonatomic) NSString *tcktEventCreditCardExpirationDate;
@property (copy, nonatomic) NSString *tcktEventCreditCardSecurityCode;
@property (copy, nonatomic) NSString *tcktEventHolderFirstName;
@property (copy, nonatomic) NSString *tcktEventHolderLastName;
@property (copy, nonatomic) NSString *tcktEventHolderPhoneNumber;
@property (copy, nonatomic) NSString *tcktEventHolderMobileNumber;
@property (copy, nonatomic) NSString *tcktEventShippingInfoAddress1;
@property (copy, nonatomic) NSString *tcktEventShippingInfoAddress2;
@property (copy, nonatomic) NSString *tcktEventShippingInfoCountry;
@property (copy, nonatomic) NSString *tcktEventShippingInfoCountryId;
@property (copy, nonatomic) NSString *tcktEventShippingInfoState;
@property (copy, nonatomic) NSString *tcktEventShippingInfoStateId;
@property (copy, nonatomic) NSString *tcktEventShippingInfoCity;
@property (copy, nonatomic) NSString *tcktEventShippingInfoZipCode;

@property (copy, nonatomic) NSString *tcktEventBillingSelected;
@property (copy, nonatomic) NSString *tcktEventBillingSelectedId;
@property (assign, nonatomic) BOOL isUsingBillingInfoInShipping;

@property (copy, nonatomic) NSString *shippingStorageAddress1;
@property (copy, nonatomic) NSString *shippingStorageAddress2;
@property (copy, nonatomic) NSString *shippingStorageCountry;
@property (copy, nonatomic) NSString *shippingStorageCountryId;
@property (copy, nonatomic) NSString *shippingStorageState;
@property (copy, nonatomic) NSString *shippingStorageStateId;
@property (copy, nonatomic) NSString *shippingStorageCity;
@property (copy, nonatomic) NSString *shippingStorageZipCode;

+(PSTicket *)information;

-(void)useBillingInfo;
-(void)reset;
-(void)resetShippingInfo;
@end
