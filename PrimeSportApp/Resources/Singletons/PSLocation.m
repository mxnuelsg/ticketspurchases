//
//  PSLocation.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 09/09/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSLocation.h"

@implementation PSLocation

static PSLocation *sharedInstance = nil;

-(id)init
{
    @synchronized(self)
    {
        [self createLocationManager];
        return [super init];
    }
}

#ifndef __clang_analyzer__
+(PSLocation *)GPS
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}
#endif

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

+ (id)copyWithZone:(NSZone *)zone
{
    return self;
}

-(void)createLocationManager
{
    //init properties
    self.latitude  = [NSString string];
    self.longitude = [NSString string];
    
    
    //Get permission to location
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
    }
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

-(void)updateLocation
{
    //Get permission to location
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    [locationManager startUpdatingLocation];
}

#pragma mark -
#pragma mark - CORE LOCATION DELEGATE
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
//    NSLog(@"Location Error: %@",error.description);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *myLocation = [locations lastObject];
    
    if (myLocation != nil)
    {
        self.longitude = [NSString stringWithFormat:@"%.8f", myLocation.coordinate.longitude];
        self.latitude = [NSString stringWithFormat:@"%.8f", myLocation.coordinate.latitude];
    }
    
    [locationManager stopUpdatingLocation];
}

@end
