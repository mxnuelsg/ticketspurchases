//
//  PSEmptyView.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSEmptyView.h"

@implementation PSEmptyView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    //Create Empty State
    UILabel *lblPrincipal = [[UILabel alloc] initWithFrame:CGRectMake(0, /*self.center.y */ self.frame.size.height/2 - 40, self.frame.size.width, 25)];
    lblPrincipal.textAlignment = NSTextAlignmentCenter;
    lblPrincipal.font = [UIFont boldSystemFontOfSize:20];
    lblPrincipal.textColor = [UIColor colorWithHexadecimal:@"#1E578F" alpha:1.0];
    lblPrincipal.text = @"Sorry!";
    
    
    UILabel *lblMessageLine1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lblPrincipal.frame.origin.y + lblPrincipal.frame.size.height, self.frame.size.width, 25)];
    lblMessageLine1.textAlignment = NSTextAlignmentCenter;
    lblMessageLine1.textColor = [UIColor primeSportColorTabBarController];
    lblMessageLine1.font = [UIFont fontWithName:@"Verdana" size:14];
    lblMessageLine1.text = @"No results were found";
    
    
    UILabel *lblMessageLine2 = [[UILabel alloc] initWithFrame:CGRectMake(0, lblMessageLine1.frame.origin.y + lblMessageLine1.frame.size.height, self.frame.size.width, 25)];
    lblMessageLine2.textAlignment = NSTextAlignmentCenter;
    lblMessageLine2.textColor = [UIColor primeSportColorTabBarController];
    lblMessageLine2.font = [UIFont fontWithName:@"Verdana" size:14];
    lblMessageLine2.text = @"Please try again!";
    
    [self addSubview:lblPrincipal];
    [self addSubview:lblMessageLine1];
    [self addSubview:lblMessageLine2];
}

@end
