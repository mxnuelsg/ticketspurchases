//
//  PSBorderLabel.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 06/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSBorderLabel : UILabel

@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, assign) int borderSize;

@end
