//
//  PSBorderLabel.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 06/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSBorderLabel.h"

@implementation PSBorderLabel


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.borderColor = [UIColor blackColor];
        self.borderSize = 1;
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.borderColor = [UIColor blackColor];
        self.borderSize = 1;
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect
{
    CGSize shadowOffset = self.shadowOffset;
    UIColor *textColor  = self.textColor;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c, self.borderSize);
    
    CGContextSetLineJoin(c, kCGLineJoinRound);
    CGContextSetTextDrawingMode(c, kCGTextStroke);
    self.textColor = self.borderColor;
    [super drawTextInRect:rect];
    
    CGContextSetTextDrawingMode(c, kCGTextFill);
    self.textColor = textColor;
    self.shadowOffset = CGSizeMake(0, 0);
    [super drawTextInRect:rect];
    
    self.shadowOffset = shadowOffset;
}

@end
