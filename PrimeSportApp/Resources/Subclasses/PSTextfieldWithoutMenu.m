//
//  PSTextfieldWithoutMenu.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 17/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSTextfieldWithoutMenu.h"

@implementation PSTextfieldWithoutMenu

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    
    if (menuController)
    {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}

@end
