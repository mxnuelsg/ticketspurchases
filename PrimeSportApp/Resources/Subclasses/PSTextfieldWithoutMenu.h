//
//  PSTextfieldWithoutMenu.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 17/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTextfieldWithoutMenu : UITextField

@end
