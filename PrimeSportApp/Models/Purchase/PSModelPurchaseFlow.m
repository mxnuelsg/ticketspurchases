//
//  PSModelPurchaseFlow.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelPurchaseFlow.h"

@implementation PSModelPurchaseFlow

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


#pragma mark -
#pragma mark - ************* SHIPPING PROCESS
-(void)getShippingMethodsWithObject:(PSWebService *)wsObject
{
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForShippingMethods:)])
                                               {
                                                   [self.delegate modelResponseForShippingMethods:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForShippingMethods:)])
                                               {
                                                   [self.delegate modelResponseForShippingMethods:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

-(void)getShippingMethodCountriesWithObject:(PSWebService *)wsObject
{
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               //                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForShippingMethodCountries:)])
                                               {
                                                   [self.delegate modelResponseForShippingMethodCountries:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForShippingMethodCountries:)])
                                               {
                                                   [self.delegate modelResponseForShippingMethodCountries:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

-(void)getZipCodeWithObject:(PSWebService *)wsObject
{
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForZipCode:)])
                                               {
                                                   [self.delegate modelResponseForZipCode:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForZipCode:)])
                                               {
                                                   [self.delegate modelResponseForZipCode:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - ************* BILLING INFOS PROCESS
-(void)getSpecificBillingWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSpecificBilling:)])
                                               {
                                                   [self.delegate modelResponseForSpecificBilling:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSpecificBilling:)])
                                               {
                                                   [self.delegate modelResponseForSpecificBilling:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - ************* CONFIRMATION PROCESS
-(void)postOrderTotalsWithObject:(PSWebService *)wsObject
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
       
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForOrderTotals:)])
                                               {
                                                   [self.delegate modelResponseForOrderTotals:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForOrderTotals:)])
                                               {
                                                   [self.delegate modelResponseForOrderTotals:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - ************* SUBMIT ORDER
-(void)postSubmitOrderWithObject:(PSWebService *)wsObject
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
       
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSubmitOrder:)])
                                               {
                                                   [self.delegate modelResponseForSubmitOrder:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSubmitOrder:)])
                                               {
                                                   [self.delegate modelResponseForSubmitOrder:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

@end
