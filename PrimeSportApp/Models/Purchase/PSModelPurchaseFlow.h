//
//  PSModelPurchaseFlow.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol modelPurchaseFlowDelegate <NSObject>

@optional
-(void)modelResponseForShippingMethods:(NSDictionary *)result;
-(void)modelResponseForShippingMethodCountries:(NSDictionary *)result;
-(void)modelResponseForSpecificBilling:(NSDictionary *)result;
-(void)modelResponseForOrderTotals:(NSDictionary *)result;
-(void)modelResponseForSubmitOrder:(NSDictionary *)result;
-(void)modelResponseForZipCode:(NSDictionary *)result;
@end

@interface PSModelPurchaseFlow : NSObject

@property (assign, nonatomic) id <modelPurchaseFlowDelegate> delegate;

-(void)getShippingMethodsWithObject:(PSWebService *)wsObject;
-(void)getShippingMethodCountriesWithObject:(PSWebService *)wsObject;
-(void)getSpecificBillingWithObject:(PSWebService *)wsObject;
-(void)postOrderTotalsWithObject:(PSWebService *)wsObject;
-(void)postSubmitOrderWithObject:(PSWebService *)wsObject;
-(void)getZipCodeWithObject:(PSWebService *)wsObject;

@end
