//
//  PSModelAccount.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 11/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelAccount.h"

@implementation PSModelAccount

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

#pragma mark -
#pragma mark - CHANGE PASSWORD
-(void)postChangePasswordWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
        
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForChangePassword:)])
                                               {
                                                   [self.delegate modelResponseForChangePassword:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForChangePassword:)])
                                               {
                                                   [self.delegate modelResponseForChangePassword:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - UPCOMINGS & PURCHASED EVENTS
-(void)getTicketsPurchasedWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               TPResultTicketsPurchased  *wsResponse = [[TPResultTicketsPurchased alloc]initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForUpcomingPurchasedEvents:)])
                                               {
                                                   [self.delegate modelResponseForUpcomingPurchasedEvents:wsResponse];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForUpcomingPurchasedEvents:)])
                                               {
                                                   [self.delegate modelResponseForUpcomingPurchasedEvents:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    
    [jsonRequest start];
}

#pragma mark -
#pragma mark - CONTACT INFO
-(void)getContactInfoWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForContactInfo:)])
                                               {
                                                   [self.delegate modelResponseForContactInfo:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForContactInfo:)])
                                               {
                                                   [self.delegate modelResponseForContactInfo:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - COUNTRIES, STATES & CITIES
-(void)getZipCodeWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForZipCode:)])
                                               {
                                                   [self.delegate modelResponseForZipCode:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForZipCode:)])
                                               {
                                                   [self.delegate modelResponseForZipCode:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - SAVE ACCOUNT
-(void)postSaveContactInfoWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
        
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSaveContactInfo:)])
                                               {
                                                   [self.delegate modelResponseForSaveContactInfo:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSaveContactInfo:)])
                                               {
                                                   [self.delegate modelResponseForSaveContactInfo:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - BILLING INFOS PROCESS
-(void)getSpecificBillingWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSpecificBilling:)])
                                               {
                                                   [self.delegate modelResponseForSpecificBilling:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSpecificBilling:)])
                                               {
                                                   [self.delegate modelResponseForSpecificBilling:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

-(void)getDeleteBillingInfoWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForDeleteBillingInfo:)])
                                               {
                                                   [self.delegate modelResponseForDeleteBillingInfo:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForDeleteBillingInfo:)])
                                               {
                                                   [self.delegate modelResponseForDeleteBillingInfo:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

-(void)postSaveBillingInfoWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
       
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSaveBillingInfo:)])
                                               {
                                                   [self.delegate modelResponseForSaveBillingInfo:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSaveBillingInfo:)])
                                               {
                                                   [self.delegate modelResponseForSaveBillingInfo:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

@end
