//
//  TPData.h
//
//  Created by Manuel Salinas on 20/08/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TPData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double page;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSArray *details;
@property (nonatomic, assign) double totalPages;
@property (nonatomic, assign) double count;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
