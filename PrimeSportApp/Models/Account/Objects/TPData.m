//
//  TPData.m
//
//  Created by Manuel Salinas on 20/08/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "TPData.h"
#import "TPDetails.h"


NSString *const kTPDataPage = @"Page";
NSString *const kTPDataEmail = @"Email";
NSString *const kTPDataDetails = @"Details";
NSString *const kTPDataTotalPages = @"TotalPages";
NSString *const kTPDataCount = @"Count";


@interface TPData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TPData

@synthesize page = _page;
@synthesize email = _email;
@synthesize details = _details;
@synthesize totalPages = _totalPages;
@synthesize count = _count;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.page = [[self objectOrNilForKey:kTPDataPage fromDictionary:dict] doubleValue];
            self.email = [self objectOrNilForKey:kTPDataEmail fromDictionary:dict];
    NSObject *receivedTPDetails = [dict objectForKey:kTPDataDetails];
    NSMutableArray *parsedTPDetails = [NSMutableArray array];
    if ([receivedTPDetails isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedTPDetails) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedTPDetails addObject:[TPDetails modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedTPDetails isKindOfClass:[NSDictionary class]]) {
       [parsedTPDetails addObject:[TPDetails modelObjectWithDictionary:(NSDictionary *)receivedTPDetails]];
    }

    self.details = [NSArray arrayWithArray:parsedTPDetails];
            self.totalPages = [[self objectOrNilForKey:kTPDataTotalPages fromDictionary:dict] doubleValue];
            self.count = [[self objectOrNilForKey:kTPDataCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.page] forKey:kTPDataPage];
    [mutableDict setValue:self.email forKey:kTPDataEmail];
    NSMutableArray *tempArrayForDetails = [NSMutableArray array];
    for (NSObject *subArrayObject in self.details) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDetails addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDetails addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDetails] forKey:kTPDataDetails];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalPages] forKey:kTPDataTotalPages];
    [mutableDict setValue:[NSNumber numberWithDouble:self.count] forKey:kTPDataCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.page = [aDecoder decodeDoubleForKey:kTPDataPage];
    self.email = [aDecoder decodeObjectForKey:kTPDataEmail];
    self.details = [aDecoder decodeObjectForKey:kTPDataDetails];
    self.totalPages = [aDecoder decodeDoubleForKey:kTPDataTotalPages];
    self.count = [aDecoder decodeDoubleForKey:kTPDataCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_page forKey:kTPDataPage];
    [aCoder encodeObject:_email forKey:kTPDataEmail];
    [aCoder encodeObject:_details forKey:kTPDataDetails];
    [aCoder encodeDouble:_totalPages forKey:kTPDataTotalPages];
    [aCoder encodeDouble:_count forKey:kTPDataCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    TPData *copy = [[TPData alloc] init];
    
    if (copy) {

        copy.page = self.page;
        copy.email = [self.email copyWithZone:zone];
        copy.details = [self.details copyWithZone:zone];
        copy.totalPages = self.totalPages;
        copy.count = self.count;
    }
    
    return copy;
}


@end
