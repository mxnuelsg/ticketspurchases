//
//  TPTickets.h
//
//  Created by Manuel Salinas on 18/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TPTickets : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double quantity;
@property (nonatomic, assign) double eITicketId;
@property (nonatomic, assign) double handling;
@property (nonatomic, assign) double ticketId;
@property (nonatomic, assign) BOOL buyAsPaperless;
@property (nonatomic, strong) NSString *split;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSString *row;
@property (nonatomic, assign) double price;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *sectionWiseView;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
