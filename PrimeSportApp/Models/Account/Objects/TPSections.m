//
//  TPSections.m
//
//  Created by Manuel Salinas on 18/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "TPSections.h"
#import "TPTickets.h"


NSString *const kTPSectionsTickets = @"Tickets";
NSString *const kTPSectionsName = @"Name";


@interface TPSections ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TPSections

@synthesize tickets = _tickets;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedTPTickets = [dict objectForKey:kTPSectionsTickets];
    NSMutableArray *parsedTPTickets = [NSMutableArray array];
    if ([receivedTPTickets isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedTPTickets) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedTPTickets addObject:[TPTickets modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedTPTickets isKindOfClass:[NSDictionary class]]) {
       [parsedTPTickets addObject:[TPTickets modelObjectWithDictionary:(NSDictionary *)receivedTPTickets]];
    }

    self.tickets = [NSArray arrayWithArray:parsedTPTickets];
            self.name = [self objectOrNilForKey:kTPSectionsName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForTickets = [NSMutableArray array];
    for (NSObject *subArrayObject in self.tickets) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForTickets addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForTickets addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForTickets] forKey:kTPSectionsTickets];
    [mutableDict setValue:self.name forKey:kTPSectionsName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tickets = [aDecoder decodeObjectForKey:kTPSectionsTickets];
    self.name = [aDecoder decodeObjectForKey:kTPSectionsName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tickets forKey:kTPSectionsTickets];
    [aCoder encodeObject:_name forKey:kTPSectionsName];
}

- (id)copyWithZone:(NSZone *)zone
{
    TPSections *copy = [[TPSections alloc] init];
    
    if (copy) {

        copy.tickets = [self.tickets copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
