//
//  TPResultTicketsPurchased.h
//
//  Created by Manuel Salinas on 20/08/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TPData;

@interface TPResultTicketsPurchased : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) TPData *data;
@property (nonatomic, strong) NSString *validationErrors;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) double responseCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
