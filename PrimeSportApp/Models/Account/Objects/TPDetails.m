//
//  TPDetails.m
//
//  Created by Manuel Salinas on 18/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "TPDetails.h"
#import "TPSections.h"


NSString *const kTPDetailsSections = @"Sections";
NSString *const kTPDetailsVenue = @"Venue";
NSString *const kTPDetailsName = @"Name";
NSString *const kTPDetailsDate = @"Date";
NSString *const kTPDetailsLocalDate = @"LocalDate";
NSString *const kTPDetailsEventId = @"EventId";


@interface TPDetails ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TPDetails

@synthesize sections = _sections;
@synthesize venue = _venue;
@synthesize name = _name;
@synthesize date = _date;
@synthesize localDate = _localDate;
@synthesize eventId = _eventId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedTPSections = [dict objectForKey:kTPDetailsSections];
    NSMutableArray *parsedTPSections = [NSMutableArray array];
    if ([receivedTPSections isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedTPSections) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedTPSections addObject:[TPSections modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedTPSections isKindOfClass:[NSDictionary class]]) {
       [parsedTPSections addObject:[TPSections modelObjectWithDictionary:(NSDictionary *)receivedTPSections]];
    }

    self.sections = [NSArray arrayWithArray:parsedTPSections];
            self.venue = [self objectOrNilForKey:kTPDetailsVenue fromDictionary:dict];
            self.name = [self objectOrNilForKey:kTPDetailsName fromDictionary:dict];
            self.date = [self objectOrNilForKey:kTPDetailsDate fromDictionary:dict];
            self.localDate = [self objectOrNilForKey:kTPDetailsLocalDate fromDictionary:dict];
            self.eventId = [[self objectOrNilForKey:kTPDetailsEventId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSections = [NSMutableArray array];
    for (NSObject *subArrayObject in self.sections) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSections addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSections addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSections] forKey:kTPDetailsSections];
    [mutableDict setValue:self.venue forKey:kTPDetailsVenue];
    [mutableDict setValue:self.name forKey:kTPDetailsName];
    [mutableDict setValue:self.date forKey:kTPDetailsDate];
    [mutableDict setValue:self.localDate forKey:kTPDetailsLocalDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eventId] forKey:kTPDetailsEventId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.sections = [aDecoder decodeObjectForKey:kTPDetailsSections];
    self.venue = [aDecoder decodeObjectForKey:kTPDetailsVenue];
    self.name = [aDecoder decodeObjectForKey:kTPDetailsName];
    self.date = [aDecoder decodeObjectForKey:kTPDetailsDate];
    self.localDate = [aDecoder decodeObjectForKey:kTPDetailsLocalDate];
    self.eventId = [aDecoder decodeDoubleForKey:kTPDetailsEventId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_sections forKey:kTPDetailsSections];
    [aCoder encodeObject:_venue forKey:kTPDetailsVenue];
    [aCoder encodeObject:_name forKey:kTPDetailsName];
    [aCoder encodeObject:_date forKey:kTPDetailsDate];
    [aCoder encodeObject:_localDate forKey:kTPDetailsLocalDate];
    [aCoder encodeDouble:_eventId forKey:kTPDetailsEventId];
}

- (id)copyWithZone:(NSZone *)zone
{
    TPDetails *copy = [[TPDetails alloc] init];
    
    if (copy) {

        copy.sections = [self.sections copyWithZone:zone];
        copy.venue = [self.venue copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.date = [self.date copyWithZone:zone];
        copy.localDate = [self.localDate copyWithZone:zone];
        copy.eventId = self.eventId;
    }
    
    return copy;
}


@end
