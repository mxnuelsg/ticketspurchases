//
//  TPDetails.h
//
//  Created by Manuel Salinas on 18/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TPDetails : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSString *venue;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *localDate;
@property (nonatomic, assign) double eventId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
