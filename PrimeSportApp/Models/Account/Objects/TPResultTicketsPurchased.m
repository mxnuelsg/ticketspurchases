//
//  TPResultTicketsPurchased.m
//
//  Created by Manuel Salinas on 20/08/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "TPResultTicketsPurchased.h"
#import "TPData.h"


NSString *const kTPResultTicketsPurchasedData = @"Data";
NSString *const kTPResultTicketsPurchasedValidationErrors = @"ValidationErrors";
NSString *const kTPResultTicketsPurchasedMessage = @"Message";
NSString *const kTPResultTicketsPurchasedResponseCode = @"ResponseCode";


@interface TPResultTicketsPurchased ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TPResultTicketsPurchased

@synthesize data = _data;
@synthesize validationErrors = _validationErrors;
@synthesize message = _message;
@synthesize responseCode = _responseCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.data = [TPData modelObjectWithDictionary:[dict objectForKey:kTPResultTicketsPurchasedData]];
            self.validationErrors = [self objectOrNilForKey:kTPResultTicketsPurchasedValidationErrors fromDictionary:dict];
            self.message = [self objectOrNilForKey:kTPResultTicketsPurchasedMessage fromDictionary:dict];
            self.responseCode = [[self objectOrNilForKey:kTPResultTicketsPurchasedResponseCode fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kTPResultTicketsPurchasedData];
    [mutableDict setValue:self.validationErrors forKey:kTPResultTicketsPurchasedValidationErrors];
    [mutableDict setValue:self.message forKey:kTPResultTicketsPurchasedMessage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.responseCode] forKey:kTPResultTicketsPurchasedResponseCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.data = [aDecoder decodeObjectForKey:kTPResultTicketsPurchasedData];
    self.validationErrors = [aDecoder decodeObjectForKey:kTPResultTicketsPurchasedValidationErrors];
    self.message = [aDecoder decodeObjectForKey:kTPResultTicketsPurchasedMessage];
    self.responseCode = [aDecoder decodeDoubleForKey:kTPResultTicketsPurchasedResponseCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_data forKey:kTPResultTicketsPurchasedData];
    [aCoder encodeObject:_validationErrors forKey:kTPResultTicketsPurchasedValidationErrors];
    [aCoder encodeObject:_message forKey:kTPResultTicketsPurchasedMessage];
    [aCoder encodeDouble:_responseCode forKey:kTPResultTicketsPurchasedResponseCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    TPResultTicketsPurchased *copy = [[TPResultTicketsPurchased alloc] init];
    
    if (copy) {

        copy.data = [self.data copyWithZone:zone];
        copy.validationErrors = [self.validationErrors copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
        copy.responseCode = self.responseCode;
    }
    
    return copy;
}


@end
