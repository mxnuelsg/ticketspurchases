//
//  TPTickets.m
//
//  Created by Manuel Salinas on 18/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "TPTickets.h"


NSString *const kTPTicketsQuantity = @"Quantity";
NSString *const kTPTicketsEITicketId = @"EITicketId";
NSString *const kTPTicketsHandling = @"Handling";
NSString *const kTPTicketsTicketId = @"TicketId";
NSString *const kTPTicketsBuyAsPaperless = @"BuyAsPaperless";
NSString *const kTPTicketsSplit = @"Split";
NSString *const kTPTicketsNotes = @"Notes";
NSString *const kTPTicketsRow = @"Row";
NSString *const kTPTicketsPrice = @"Price";
NSString *const kTPTicketsType = @"Type";
NSString *const kTPTicketsSectionWiseView = @"SectionWiseView";


@interface TPTickets ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation TPTickets

@synthesize quantity = _quantity;
@synthesize eITicketId = _eITicketId;
@synthesize handling = _handling;
@synthesize ticketId = _ticketId;
@synthesize buyAsPaperless = _buyAsPaperless;
@synthesize split = _split;
@synthesize notes = _notes;
@synthesize row = _row;
@synthesize price = _price;
@synthesize type = _type;
@synthesize sectionWiseView = _sectionWiseView;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.quantity = [[self objectOrNilForKey:kTPTicketsQuantity fromDictionary:dict] doubleValue];
            self.eITicketId = [[self objectOrNilForKey:kTPTicketsEITicketId fromDictionary:dict] doubleValue];
            self.handling = [[self objectOrNilForKey:kTPTicketsHandling fromDictionary:dict] doubleValue];
            self.ticketId = [[self objectOrNilForKey:kTPTicketsTicketId fromDictionary:dict] doubleValue];
            self.buyAsPaperless = [[self objectOrNilForKey:kTPTicketsBuyAsPaperless fromDictionary:dict] boolValue];
            self.split = [self objectOrNilForKey:kTPTicketsSplit fromDictionary:dict];
            self.notes = [self objectOrNilForKey:kTPTicketsNotes fromDictionary:dict];
            self.row = [self objectOrNilForKey:kTPTicketsRow fromDictionary:dict];
            self.price = [[self objectOrNilForKey:kTPTicketsPrice fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kTPTicketsType fromDictionary:dict];
            self.sectionWiseView = [self objectOrNilForKey:kTPTicketsSectionWiseView fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.quantity] forKey:kTPTicketsQuantity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eITicketId] forKey:kTPTicketsEITicketId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.handling] forKey:kTPTicketsHandling];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ticketId] forKey:kTPTicketsTicketId];
    [mutableDict setValue:[NSNumber numberWithBool:self.buyAsPaperless] forKey:kTPTicketsBuyAsPaperless];
    [mutableDict setValue:self.split forKey:kTPTicketsSplit];
    [mutableDict setValue:self.notes forKey:kTPTicketsNotes];
    [mutableDict setValue:self.row forKey:kTPTicketsRow];
    [mutableDict setValue:[NSNumber numberWithDouble:self.price] forKey:kTPTicketsPrice];
    [mutableDict setValue:self.type forKey:kTPTicketsType];
    [mutableDict setValue:self.sectionWiseView forKey:kTPTicketsSectionWiseView];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.quantity = [aDecoder decodeDoubleForKey:kTPTicketsQuantity];
    self.eITicketId = [aDecoder decodeDoubleForKey:kTPTicketsEITicketId];
    self.handling = [aDecoder decodeDoubleForKey:kTPTicketsHandling];
    self.ticketId = [aDecoder decodeDoubleForKey:kTPTicketsTicketId];
    self.buyAsPaperless = [aDecoder decodeBoolForKey:kTPTicketsBuyAsPaperless];
    self.split = [aDecoder decodeObjectForKey:kTPTicketsSplit];
    self.notes = [aDecoder decodeObjectForKey:kTPTicketsNotes];
    self.row = [aDecoder decodeObjectForKey:kTPTicketsRow];
    self.price = [aDecoder decodeDoubleForKey:kTPTicketsPrice];
    self.type = [aDecoder decodeObjectForKey:kTPTicketsType];
    self.sectionWiseView = [aDecoder decodeObjectForKey:kTPTicketsSectionWiseView];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_quantity forKey:kTPTicketsQuantity];
    [aCoder encodeDouble:_eITicketId forKey:kTPTicketsEITicketId];
    [aCoder encodeDouble:_handling forKey:kTPTicketsHandling];
    [aCoder encodeDouble:_ticketId forKey:kTPTicketsTicketId];
    [aCoder encodeBool:_buyAsPaperless forKey:kTPTicketsBuyAsPaperless];
    [aCoder encodeObject:_split forKey:kTPTicketsSplit];
    [aCoder encodeObject:_notes forKey:kTPTicketsNotes];
    [aCoder encodeObject:_row forKey:kTPTicketsRow];
    [aCoder encodeDouble:_price forKey:kTPTicketsPrice];
    [aCoder encodeObject:_type forKey:kTPTicketsType];
    [aCoder encodeObject:_sectionWiseView forKey:kTPTicketsSectionWiseView];
}

- (id)copyWithZone:(NSZone *)zone
{
    TPTickets *copy = [[TPTickets alloc] init];
    
    if (copy) {

        copy.quantity = self.quantity;
        copy.eITicketId = self.eITicketId;
        copy.handling = self.handling;
        copy.ticketId = self.ticketId;
        copy.buyAsPaperless = self.buyAsPaperless;
        copy.split = [self.split copyWithZone:zone];
        copy.notes = [self.notes copyWithZone:zone];
        copy.row = [self.row copyWithZone:zone];
        copy.price = self.price;
        copy.type = [self.type copyWithZone:zone];
        copy.sectionWiseView = [self.sectionWiseView copyWithZone:zone];
    }
    
    return copy;
}


@end
