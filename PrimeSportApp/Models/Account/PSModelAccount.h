//
//  PSModelAccount.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 11/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataTicketsPurchasedModels.h"

@protocol modelAccountDelegate <NSObject>

@optional
-(void)modelResponseForChangePassword:(NSDictionary *)result;
-(void)modelResponseForUpcomingPurchasedEvents:(TPResultTicketsPurchased *)result;
-(void)modelResponseForContactInfo:(NSDictionary *)result;
-(void)modelResponseForSaveContactInfo:(NSDictionary *)result;
-(void)modelResponseForSpecificBilling:(NSDictionary *)result;
-(void)modelResponseForDeleteBillingInfo:(NSDictionary *)result;
-(void)modelResponseForSaveBillingInfo:(NSDictionary *)result;
-(void)modelResponseForZipCode:(NSDictionary *)result;
@end

@interface PSModelAccount : NSObject

@property (assign, nonatomic) id <modelAccountDelegate> delegate;

-(void)postChangePasswordWithObject:(PSWebService *)wsObject;
-(void)getTicketsPurchasedWithObject:(PSWebService *)wsObject;
-(void)getContactInfoWithObject:(PSWebService *)wsObject;
-(void)postSaveContactInfoWithObject:(PSWebService *)wsObject;
-(void)getSpecificBillingWithObject:(PSWebService *)wsObject;
-(void)getDeleteBillingInfoWithObject:(PSWebService *)wsObject;
-(void)postSaveBillingInfoWithObject:(PSWebService *)wsObject;
-(void)getZipCodeWithObject:(PSWebService *)wsObject;

@end
