//
//  PSModelLogin.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 29/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelLogin.h"

@implementation PSModelLogin

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

#pragma mark -
#pragma mark - LOGIN
-(void)postLoginCredentialsWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                     options:NSJSONWritingPrettyPrinted
                                                                       error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
       
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    if (![[PSSessionParameters information].facebookToken isBlank])
    {
        [request addValue:[PSSessionParameters information].facebookToken forHTTPHeaderField:@"facebookToken"];
    }
    
    [request allHTTPHeaderFields];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               LGResultLogin  *wsResponse = [[LGResultLogin alloc]initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForLoginWithObject:)])
                                               {
                                                   [self.delegate modelResponseForLoginWithObject:wsResponse];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForLoginWithObject:)])
                                               {
                                                   [self.delegate modelResponseForLoginWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - FORGOT PASSWORD
-(void)postForgotPasswordWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
        
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = (NSDictionary *)JSON;
                                               
                                               NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
                                               NSString *message       = [result objectForKey:@"Message"];
                                               
                                               if (responseCode == 0)
                                               {
                                                   if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForForgotPasswordWithMessage:)])
                                                   {
                                                       [self.delegate modelResponseForForgotPasswordWithMessage:@"The email has been sent to recover your password"];
                                                   }
                                               }
                                               else
                                               {
                                                   if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForForgotPasswordWithMessage:)])
                                                   {
                                                       [self.delegate modelResponseForForgotPasswordWithMessage:message];
                                                   }
                                                   
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForForgotPasswordWithMessage:)])
                                               {
                                                   [self.delegate modelResponseForForgotPasswordWithMessage:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - REGISTER NEW USER    
-(void)postRegisterNewUserWithObject:(PSWebService *)wsObject
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSError *error = nil;
    NSData *serializedDictionary = [NSJSONSerialization dataWithJSONObject:wsObject.wsParameters
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
    
    if(!error)
    {
        NSString *serJSON = [[NSString alloc] initWithData:serializedDictionary encoding:NSUTF8StringEncoding];
       
        if (kLogActivated)
        {
            NSLog(@"Serialized JSON: %@", serJSON);
            NSLog(@"JSON Encoding Success");
        }
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [error localizedDescription]);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForPOST_MethodInRequest:request
                                                          withBody:serializedDictionary];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                              
                                               
                                               LGResultLogin  *wsResponse = [[LGResultLogin alloc]initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForRegisterWithObject:)])
                                               {
                                                   [self.delegate modelResponseForRegisterWithObject:wsResponse];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForRegisterWithObject:)])
                                               {
                                                   [self.delegate modelResponseForRegisterWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - TOKEN STATUS
-(void)getTokenStatusWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               if (kLogActivated) {NSLog(@"Response: %@", JSON);}
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForTokenStatus:)])
                                               {
                                                   [self.delegate modelResponseForTokenStatus:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForTokenStatus:)])
                                               {
                                                   [self.delegate modelResponseForTokenStatus:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    
    [jsonRequest start];
}

@end
