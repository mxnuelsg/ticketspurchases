//
//  PSModelLogin.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 29/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataLoginModels.h"

@protocol modelLoginDelegate <NSObject>

@optional
-(void)modelResponseForLoginWithObject:(LGResultLogin *)result;
-(void)modelResponseForForgotPasswordWithMessage:(NSString *)message;
-(void)modelResponseForRegisterWithObject:(LGResultLogin *)result;
-(void)modelResponseForTokenStatus:(NSDictionary *)result;
@end

@interface PSModelLogin : NSObject

@property (assign, nonatomic) id <modelLoginDelegate> delegate;

-(void)postLoginCredentialsWithObject:(PSWebService *)wsObject;
-(void)postForgotPasswordWithObject:(PSWebService *)wsObject;
-(void)postRegisterNewUserWithObject:(PSWebService *)wsObject;
-(void)getTokenStatusWithObject:(PSWebService *)wsObject;
@end
