//
//  LGResultLogin.m
//
//  Created by Manuel Salinas on 29/05/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "LGResultLogin.h"
#import "LGData.h"


NSString *const kLGResultLoginData = @"Data";
NSString *const kLGResultLoginValidationErrors = @"ValidationErrors";
NSString *const kLGResultLoginMessage = @"Message";
NSString *const kLGResultLoginResponseCode = @"ResponseCode";


@interface LGResultLogin ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LGResultLogin

@synthesize data = _data;
@synthesize validationErrors = _validationErrors;
@synthesize message = _message;
@synthesize responseCode = _responseCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.data = [LGData modelObjectWithDictionary:[dict objectForKey:kLGResultLoginData]];
            self.validationErrors = [self objectOrNilForKey:kLGResultLoginValidationErrors fromDictionary:dict];
            self.message = [self objectOrNilForKey:kLGResultLoginMessage fromDictionary:dict];
            self.responseCode = [[self objectOrNilForKey:kLGResultLoginResponseCode fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLGResultLoginData];
    NSMutableArray *tempArrayForValidationErrors = [NSMutableArray array];
    for (NSObject *subArrayObject in self.validationErrors) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForValidationErrors addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForValidationErrors addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForValidationErrors] forKey:kLGResultLoginValidationErrors];
    [mutableDict setValue:self.message forKey:kLGResultLoginMessage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.responseCode] forKey:kLGResultLoginResponseCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.data = [aDecoder decodeObjectForKey:kLGResultLoginData];
    self.validationErrors = [aDecoder decodeObjectForKey:kLGResultLoginValidationErrors];
    self.message = [aDecoder decodeObjectForKey:kLGResultLoginMessage];
    self.responseCode = [aDecoder decodeDoubleForKey:kLGResultLoginResponseCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_data forKey:kLGResultLoginData];
    [aCoder encodeObject:_validationErrors forKey:kLGResultLoginValidationErrors];
    [aCoder encodeObject:_message forKey:kLGResultLoginMessage];
    [aCoder encodeDouble:_responseCode forKey:kLGResultLoginResponseCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    LGResultLogin *copy = [[LGResultLogin alloc] init];
    
    if (copy) {

        copy.data = [self.data copyWithZone:zone];
        copy.validationErrors = [self.validationErrors copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
        copy.responseCode = self.responseCode;
    }
    
    return copy;
}


@end
