//
//  LGResultLogin.h
//
//  Created by Manuel Salinas on 29/05/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LGData;

@interface LGResultLogin : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) LGData *data;
@property (nonatomic, strong) NSArray *validationErrors;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) double responseCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
