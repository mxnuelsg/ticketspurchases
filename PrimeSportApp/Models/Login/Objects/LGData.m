//
//  LGData.m
//
//  Created by Manuel Salinas on 29/05/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "LGData.h"


NSString *const kLGDataEmail = @"Email";
NSString *const kLGDataAuthenticationToken = @"AuthenticationToken";


@interface LGData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LGData

@synthesize email = _email;
@synthesize authenticationToken = _authenticationToken;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.email = [self objectOrNilForKey:kLGDataEmail fromDictionary:dict];
            self.authenticationToken = [self objectOrNilForKey:kLGDataAuthenticationToken fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.email forKey:kLGDataEmail];
    [mutableDict setValue:self.authenticationToken forKey:kLGDataAuthenticationToken];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.email = [aDecoder decodeObjectForKey:kLGDataEmail];
    self.authenticationToken = [aDecoder decodeObjectForKey:kLGDataAuthenticationToken];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_email forKey:kLGDataEmail];
    [aCoder encodeObject:_authenticationToken forKey:kLGDataAuthenticationToken];
}

- (id)copyWithZone:(NSZone *)zone
{
    LGData *copy = [[LGData alloc] init];
    
    if (copy) {

        copy.email = [self.email copyWithZone:zone];
        copy.authenticationToken = [self.authenticationToken copyWithZone:zone];
    }
    
    return copy;
}


@end
