//
//  PSModelFlow.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 03/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataSubcategoriesModels.h"
#import "DataDynamicSplashModels.h"
#import "DataPerformerModels.h"
#import "DataEventGroupsModels.h"
#import "DataEventTicketsSearchModels.h"

@protocol modelFlowDelegate <NSObject>

@optional
-(void)modelResponseForSubCategoriesWithObject:(SCResultSubCategories *)result;
-(void)modelResponseForEventListWithObject:(ELDResultDynamicSplash *)result;
-(void)modelResponseForEventListPerformerWithObject:(ELPResultPerformer *)result;
-(void)modelResponseForEventGroupsWithObject:(EGResultSplash *)result;
-(void)modelResponseForEventTicketsWithObject:(ETSResultEventTicketsSearch *)result;
-(void)modelResponseForPreferredWithObject:(NSDictionary *)result;
@end

@interface PSModelFlow : NSObject

@property (assign, nonatomic) id <modelFlowDelegate> delegate;

-(void)getSubCategoriesWithObject:(PSWebService *)wsObject;
-(void)getEventListDynamicSplashWithObject:(PSWebService *)wsObject;
-(void)getEventListPerformerWithObject:(PSWebService *)wsObject;
-(void)getEventGroupsWithObject:(PSWebService *)wsObject;
-(void)getEventTicketsWithObject:(PSWebService *)wsObject;
-(void)getPreferredWithObject:(PSWebService *)wsObject;
@end
