//
//  PSModelFlow.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 03/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelFlow.h"

@implementation PSModelFlow

- (instancetype)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

#pragma mark -
#pragma mark - SUB CATEGORIES
-(void)getSubCategoriesWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
     request = [PSWebServiceUtils setHeadersForGET_MethodInRequestWithLocation:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               SCResultSubCategories *result = [[SCResultSubCategories alloc] initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSubCategoriesWithObject:)])
                                               {
                                                   [self.delegate modelResponseForSubCategoriesWithObject:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSubCategoriesWithObject:)])
                                               {
                                                   [self.delegate modelResponseForSubCategoriesWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}


#pragma mark -
#pragma mark - DYNAMIC SPLASH PAGE GROUP
-(void)getEventListDynamicSplashWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequestWithLocation:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               ELDResultDynamicSplash *result = [[ELDResultDynamicSplash alloc] initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventListWithObject:)])
                                               {
                                                    [self.delegate modelResponseForEventListWithObject:result];
                                               }
                                              
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventListWithObject:)])
                                               {
                                                   [self.delegate modelResponseForEventListWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}


#pragma mark -
#pragma mark - PERFORMER  EVENT GROUP
-(void)getEventListPerformerWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequestWithLocation:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               ELPResultPerformer *result = [[ELPResultPerformer alloc] initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventListPerformerWithObject:)])
                                               {
                                                   [self.delegate modelResponseForEventListPerformerWithObject:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventListPerformerWithObject:)])
                                               {
                                                   [self.delegate modelResponseForEventListPerformerWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - SPLASH PAGE GROUP
-(void)getEventGroupsWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequestWithLocation:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               EGResultSplash *result = [[EGResultSplash alloc] initWithDictionary:JSON];
                                          
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventGroupsWithObject:)])
                                               {
                                                   [self.delegate modelResponseForEventGroupsWithObject:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventGroupsWithObject:)])
                                               {
                                                   [self.delegate modelResponseForEventGroupsWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark -  EVENT TICKETS & EVENT TICKETS SEARCH
-(void)getEventTicketsWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               ETSResultEventTicketsSearch *result = [[ETSResultEventTicketsSearch alloc] initWithDictionary:JSON];
                                               
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventTicketsWithObject:)])
                                               {
                                                   [self.delegate modelResponseForEventTicketsWithObject:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForEventTicketsWithObject:)])
                                               {
                                                    [self.delegate modelResponseForEventTicketsWithObject:nil];
                                               }
                                              
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}

#pragma mark -
#pragma mark - PREFERRED EVENTS
-(void)getPreferredWithObject:(PSWebService *)wsObject
{
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               NSLog(@"Response: %@", JSON);
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForPreferredWithObject:)])
                                               {
                                                   [self.delegate modelResponseForPreferredWithObject:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForPreferredWithObject:)])
                                               {
                                                   [self.delegate modelResponseForPreferredWithObject:nil];
                                               }
                                           }];
    [jsonRequest start];
}

@end
