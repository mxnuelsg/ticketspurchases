//
//  SCResultSubCategories.h
//
//  Created by Manuel Salinas on 03/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SCResultSubCategories : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *validationErrors;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) double responseCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
