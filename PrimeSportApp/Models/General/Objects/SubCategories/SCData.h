//
//  SCData.h
//
//  Created by Manuel Salinas on 03/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SCData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *groupType;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double order;
@property (nonatomic, assign) double dataIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
