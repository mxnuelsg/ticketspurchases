//
//  SCResultSubCategories.m
//
//  Created by Manuel Salinas on 03/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SCResultSubCategories.h"
#import "SCData.h"


NSString *const kSCResultSubCategoriesData = @"Data";
NSString *const kSCResultSubCategoriesValidationErrors = @"ValidationErrors";
NSString *const kSCResultSubCategoriesMessage = @"Message";
NSString *const kSCResultSubCategoriesResponseCode = @"ResponseCode";


@interface SCResultSubCategories ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SCResultSubCategories

@synthesize data = _data;
@synthesize validationErrors = _validationErrors;
@synthesize message = _message;
@synthesize responseCode = _responseCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedSCData = [dict objectForKey:kSCResultSubCategoriesData];
    NSMutableArray *parsedSCData = [NSMutableArray array];
    if ([receivedSCData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSCData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSCData addObject:[SCData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSCData isKindOfClass:[NSDictionary class]]) {
       [parsedSCData addObject:[SCData modelObjectWithDictionary:(NSDictionary *)receivedSCData]];
    }

    self.data = [NSArray arrayWithArray:parsedSCData];
            self.validationErrors = [self objectOrNilForKey:kSCResultSubCategoriesValidationErrors fromDictionary:dict];
            self.message = [self objectOrNilForKey:kSCResultSubCategoriesMessage fromDictionary:dict];
            self.responseCode = [[self objectOrNilForKey:kSCResultSubCategoriesResponseCode fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kSCResultSubCategoriesData];
    NSMutableArray *tempArrayForValidationErrors = [NSMutableArray array];
    for (NSObject *subArrayObject in self.validationErrors) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForValidationErrors addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForValidationErrors addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForValidationErrors] forKey:kSCResultSubCategoriesValidationErrors];
    [mutableDict setValue:self.message forKey:kSCResultSubCategoriesMessage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.responseCode] forKey:kSCResultSubCategoriesResponseCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.data = [aDecoder decodeObjectForKey:kSCResultSubCategoriesData];
    self.validationErrors = [aDecoder decodeObjectForKey:kSCResultSubCategoriesValidationErrors];
    self.message = [aDecoder decodeObjectForKey:kSCResultSubCategoriesMessage];
    self.responseCode = [aDecoder decodeDoubleForKey:kSCResultSubCategoriesResponseCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_data forKey:kSCResultSubCategoriesData];
    [aCoder encodeObject:_validationErrors forKey:kSCResultSubCategoriesValidationErrors];
    [aCoder encodeObject:_message forKey:kSCResultSubCategoriesMessage];
    [aCoder encodeDouble:_responseCode forKey:kSCResultSubCategoriesResponseCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    SCResultSubCategories *copy = [[SCResultSubCategories alloc] init];
    
    if (copy) {

        copy.data = [self.data copyWithZone:zone];
        copy.validationErrors = [self.validationErrors copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
        copy.responseCode = self.responseCode;
    }
    
    return copy;
}


@end
