//
//  SCData.m
//
//  Created by Manuel Salinas on 03/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SCData.h"


NSString *const kSCDataUrl = @"Url";
NSString *const kSCDataGroupType = @"groupType";
NSString *const kSCDataName = @"Name";
NSString *const kSCDataOrder = @"Order";
NSString *const kSCDataId = @"Id";


@interface SCData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SCData

@synthesize url = _url;
@synthesize groupType = _groupType;
@synthesize name = _name;
@synthesize order = _order;
@synthesize dataIdentifier = _dataIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.url = [self objectOrNilForKey:kSCDataUrl fromDictionary:dict];
            self.groupType = [self objectOrNilForKey:kSCDataGroupType fromDictionary:dict];
            self.name = [self objectOrNilForKey:kSCDataName fromDictionary:dict];
            self.order = [[self objectOrNilForKey:kSCDataOrder fromDictionary:dict] doubleValue];
            self.dataIdentifier = [[self objectOrNilForKey:kSCDataId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.url forKey:kSCDataUrl];
    [mutableDict setValue:self.groupType forKey:kSCDataGroupType];
    [mutableDict setValue:self.name forKey:kSCDataName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.order] forKey:kSCDataOrder];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kSCDataId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.url = [aDecoder decodeObjectForKey:kSCDataUrl];
    self.groupType = [aDecoder decodeObjectForKey:kSCDataGroupType];
    self.name = [aDecoder decodeObjectForKey:kSCDataName];
    self.order = [aDecoder decodeDoubleForKey:kSCDataOrder];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kSCDataId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_url forKey:kSCDataUrl];
    [aCoder encodeObject:_groupType forKey:kSCDataGroupType];
    [aCoder encodeObject:_name forKey:kSCDataName];
    [aCoder encodeDouble:_order forKey:kSCDataOrder];
    [aCoder encodeDouble:_dataIdentifier forKey:kSCDataId];
}

- (id)copyWithZone:(NSZone *)zone
{
    SCData *copy = [[SCData alloc] init];
    
    if (copy) {

        copy.url = [self.url copyWithZone:zone];
        copy.groupType = [self.groupType copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.order = self.order;
        copy.dataIdentifier = self.dataIdentifier;
    }
    
    return copy;
}


@end
