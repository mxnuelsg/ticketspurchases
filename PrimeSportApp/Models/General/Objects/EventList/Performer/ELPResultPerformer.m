//
//  ELPResultPerformer.m
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ELPResultPerformer.h"
#import "ELPEventList.h"


NSString *const kELPResultPerformerEventList = @"EventList";
NSString *const kELPResultPerformerVenueState = @"VenueState";
NSString *const kELPResultPerformerVenueCP = @"VenueCP";
NSString *const kELPResultPerformerMetaTitle = @"MetaTitle";
NSString *const kELPResultPerformerId = @"Id";
NSString *const kELPResultPerformerType = @"Type";
NSString *const kELPResultPerformerVenueStreet = @"VenueStreet";
NSString *const kELPResultPerformerName = @"Name";
NSString *const kELPResultPerformerVenueCity = @"VenueCity";
NSString *const kELPResultPerformerDescription = @"Description";
NSString *const kELPResultPerformerMetaDescription = @"MetaDescription";


@interface ELPResultPerformer ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ELPResultPerformer

@synthesize eventList = _eventList;
@synthesize venueState = _venueState;
@synthesize venueCP = _venueCP;
@synthesize metaTitle = _metaTitle;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize type = _type;
@synthesize venueStreet = _venueStreet;
@synthesize name = _name;
@synthesize venueCity = _venueCity;
@synthesize Description = _Description;
@synthesize metaDescription = _metaDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedELPEventList = [dict objectForKey:kELPResultPerformerEventList];
    NSMutableArray *parsedELPEventList = [NSMutableArray array];
    if ([receivedELPEventList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedELPEventList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedELPEventList addObject:[ELPEventList modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedELPEventList isKindOfClass:[NSDictionary class]]) {
       [parsedELPEventList addObject:[ELPEventList modelObjectWithDictionary:(NSDictionary *)receivedELPEventList]];
    }

    self.eventList = [NSArray arrayWithArray:parsedELPEventList];
            self.venueState = [self objectOrNilForKey:kELPResultPerformerVenueState fromDictionary:dict];
            self.venueCP = [self objectOrNilForKey:kELPResultPerformerVenueCP fromDictionary:dict];
            self.metaTitle = [self objectOrNilForKey:kELPResultPerformerMetaTitle fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kELPResultPerformerId fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kELPResultPerformerType fromDictionary:dict];
            self.venueStreet = [self objectOrNilForKey:kELPResultPerformerVenueStreet fromDictionary:dict];
            self.name = [self objectOrNilForKey:kELPResultPerformerName fromDictionary:dict];
            self.venueCity = [self objectOrNilForKey:kELPResultPerformerVenueCity fromDictionary:dict];
            self.Description = [self objectOrNilForKey:kELPResultPerformerDescription fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kELPResultPerformerMetaDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForEventList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.eventList) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForEventList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForEventList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForEventList] forKey:kELPResultPerformerEventList];
    [mutableDict setValue:self.venueState forKey:kELPResultPerformerVenueState];
    [mutableDict setValue:self.venueCP forKey:kELPResultPerformerVenueCP];
    [mutableDict setValue:self.metaTitle forKey:kELPResultPerformerMetaTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kELPResultPerformerId];
    [mutableDict setValue:self.type forKey:kELPResultPerformerType];
    [mutableDict setValue:self.venueStreet forKey:kELPResultPerformerVenueStreet];
    [mutableDict setValue:self.name forKey:kELPResultPerformerName];
    [mutableDict setValue:self.venueCity forKey:kELPResultPerformerVenueCity];
    [mutableDict setValue:self.Description forKey:kELPResultPerformerDescription];
    [mutableDict setValue:self.metaDescription forKey:kELPResultPerformerMetaDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.eventList = [aDecoder decodeObjectForKey:kELPResultPerformerEventList];
    self.venueState = [aDecoder decodeObjectForKey:kELPResultPerformerVenueState];
    self.venueCP = [aDecoder decodeObjectForKey:kELPResultPerformerVenueCP];
    self.metaTitle = [aDecoder decodeObjectForKey:kELPResultPerformerMetaTitle];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kELPResultPerformerId];
    self.type = [aDecoder decodeObjectForKey:kELPResultPerformerType];
    self.venueStreet = [aDecoder decodeObjectForKey:kELPResultPerformerVenueStreet];
    self.name = [aDecoder decodeObjectForKey:kELPResultPerformerName];
    self.venueCity = [aDecoder decodeObjectForKey:kELPResultPerformerVenueCity];
    self.Description = [aDecoder decodeObjectForKey:kELPResultPerformerDescription];
    self.metaDescription = [aDecoder decodeObjectForKey:kELPResultPerformerMetaDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_eventList forKey:kELPResultPerformerEventList];
    [aCoder encodeObject:_venueState forKey:kELPResultPerformerVenueState];
    [aCoder encodeObject:_venueCP forKey:kELPResultPerformerVenueCP];
    [aCoder encodeObject:_metaTitle forKey:kELPResultPerformerMetaTitle];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kELPResultPerformerId];
    [aCoder encodeObject:_type forKey:kELPResultPerformerType];
    [aCoder encodeObject:_venueStreet forKey:kELPResultPerformerVenueStreet];
    [aCoder encodeObject:_name forKey:kELPResultPerformerName];
    [aCoder encodeObject:_venueCity forKey:kELPResultPerformerVenueCity];
    [aCoder encodeObject:_Description forKey:kELPResultPerformerDescription];
    [aCoder encodeObject:_metaDescription forKey:kELPResultPerformerMetaDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    ELPResultPerformer *copy = [[ELPResultPerformer alloc] init];
    
    if (copy) {

        copy.eventList = [self.eventList copyWithZone:zone];
        copy.venueState = [self.venueState copyWithZone:zone];
        copy.venueCP = [self.venueCP copyWithZone:zone];
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.type = [self.type copyWithZone:zone];
        copy.venueStreet = [self.venueStreet copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.venueCity = [self.venueCity copyWithZone:zone];
        copy.Description = [self.Description copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
    }
    
    return copy;
}


@end
