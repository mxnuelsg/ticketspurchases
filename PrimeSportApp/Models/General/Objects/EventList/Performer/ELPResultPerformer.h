//
//  ELPResultPerformer.h
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ELPResultPerformer : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *eventList;
@property (nonatomic, strong) NSString *venueState;
@property (nonatomic, strong) NSString *venueCP;
@property (nonatomic, strong) NSString *metaTitle;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *venueStreet;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *venueCity;
@property (nonatomic, strong) NSString *Description;
@property (nonatomic, strong) NSString *metaDescription;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
