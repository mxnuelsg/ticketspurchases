//
//  ELPEventList.m
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ELPEventList.h"


NSString *const kELPEventListName = @"Name";
NSString *const kELPEventListSubTitle = @"SubTitle";
NSString *const kELPEventListSnippetDate = @"SnippetDate";
NSString *const kELPEventListMetaDescription = @"MetaDescription";
NSString *const kELPEventListPerformerId = @"PerformerId";
NSString *const kELPEventListDate = @"Date";
NSString *const kELPEventListPerformer = @"Performer";
NSString *const kELPEventListVenueState = @"VenueState";
NSString *const kELPEventListPerformerType = @"PerformerType";
NSString *const kELPEventListVenueId = @"VenueId";
NSString *const kELPEventListDescription = @"Description";
NSString *const kELPEventListSubcategoryId = @"SubcategoryId";
NSString *const kELPEventListPrimeSportUrl = @"PrimeSportUrl";
NSString *const kELPEventListVenue = @"Venue";
NSString *const kELPEventListLocalDate = @"LocalDate";
NSString *const kELPEventListSectionWiseView = @"SectionWiseView";
NSString *const kELPEventListAvailableTickets = @"AvailableTickets";
NSString *const kELPEventListVenueCity = @"VenueCity";
NSString *const kELPEventListEIProductionId = @"EIProductionId";
NSString *const kELPEventListEventId = @"EventId";
NSString *const kELPEventListMetaTitle = @"MetaTitle";
NSString *const kELPEventListUrlCategoryName = @"UrlCategoryName";


@interface ELPEventList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ELPEventList

@synthesize name = _name;
@synthesize subTitle = _subTitle;
@synthesize snippetDate = _snippetDate;
@synthesize metaDescription = _metaDescription;
@synthesize performerId = _performerId;
@synthesize date = _date;
@synthesize performer = _performer;
@synthesize venueState = _venueState;
@synthesize performerType = _performerType;
@synthesize venueId = _venueId;
@synthesize eventListDescription = _eventListDescription;
@synthesize subcategoryId = _subcategoryId;
@synthesize primeSportUrl = _primeSportUrl;
@synthesize venue = _venue;
@synthesize localDate = _localDate;
@synthesize sectionWiseView = _sectionWiseView;
@synthesize availableTickets = _availableTickets;
@synthesize venueCity = _venueCity;
@synthesize eIProductionId = _eIProductionId;
@synthesize eventId = _eventId;
@synthesize metaTitle = _metaTitle;
@synthesize urlCategoryName = _urlCategoryName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kELPEventListName fromDictionary:dict];
            self.subTitle = [self objectOrNilForKey:kELPEventListSubTitle fromDictionary:dict];
            self.snippetDate = [self objectOrNilForKey:kELPEventListSnippetDate fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kELPEventListMetaDescription fromDictionary:dict];
            self.performerId = [[self objectOrNilForKey:kELPEventListPerformerId fromDictionary:dict] doubleValue];
            self.date = [self objectOrNilForKey:kELPEventListDate fromDictionary:dict];
            self.performer = [self objectOrNilForKey:kELPEventListPerformer fromDictionary:dict];
            self.venueState = [self objectOrNilForKey:kELPEventListVenueState fromDictionary:dict];
            self.performerType = [self objectOrNilForKey:kELPEventListPerformerType fromDictionary:dict];
            self.venueId = [[self objectOrNilForKey:kELPEventListVenueId fromDictionary:dict] doubleValue];
            self.eventListDescription = [self objectOrNilForKey:kELPEventListDescription fromDictionary:dict];
            self.subcategoryId = [[self objectOrNilForKey:kELPEventListSubcategoryId fromDictionary:dict] doubleValue];
            self.primeSportUrl = [self objectOrNilForKey:kELPEventListPrimeSportUrl fromDictionary:dict];
            self.venue = [self objectOrNilForKey:kELPEventListVenue fromDictionary:dict];
            self.localDate = [self objectOrNilForKey:kELPEventListLocalDate fromDictionary:dict];
            self.sectionWiseView = [self objectOrNilForKey:kELPEventListSectionWiseView fromDictionary:dict];
            self.availableTickets = [[self objectOrNilForKey:kELPEventListAvailableTickets fromDictionary:dict] doubleValue];
            self.venueCity = [self objectOrNilForKey:kELPEventListVenueCity fromDictionary:dict];
            self.eIProductionId = [[self objectOrNilForKey:kELPEventListEIProductionId fromDictionary:dict] doubleValue];
            self.eventId = [[self objectOrNilForKey:kELPEventListEventId fromDictionary:dict] doubleValue];
            self.metaTitle = [self objectOrNilForKey:kELPEventListMetaTitle fromDictionary:dict];
            self.urlCategoryName = [self objectOrNilForKey:kELPEventListUrlCategoryName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kELPEventListName];
    [mutableDict setValue:self.subTitle forKey:kELPEventListSubTitle];
    [mutableDict setValue:self.snippetDate forKey:kELPEventListSnippetDate];
    [mutableDict setValue:self.metaDescription forKey:kELPEventListMetaDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.performerId] forKey:kELPEventListPerformerId];
    [mutableDict setValue:self.date forKey:kELPEventListDate];
    [mutableDict setValue:self.performer forKey:kELPEventListPerformer];
    [mutableDict setValue:self.venueState forKey:kELPEventListVenueState];
    [mutableDict setValue:self.performerType forKey:kELPEventListPerformerType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.venueId] forKey:kELPEventListVenueId];
    [mutableDict setValue:self.eventListDescription forKey:kELPEventListDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subcategoryId] forKey:kELPEventListSubcategoryId];
    [mutableDict setValue:self.primeSportUrl forKey:kELPEventListPrimeSportUrl];
    [mutableDict setValue:self.venue forKey:kELPEventListVenue];
    [mutableDict setValue:self.localDate forKey:kELPEventListLocalDate];
    [mutableDict setValue:self.sectionWiseView forKey:kELPEventListSectionWiseView];
    [mutableDict setValue:[NSNumber numberWithDouble:self.availableTickets] forKey:kELPEventListAvailableTickets];
    [mutableDict setValue:self.venueCity forKey:kELPEventListVenueCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eIProductionId] forKey:kELPEventListEIProductionId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eventId] forKey:kELPEventListEventId];
    [mutableDict setValue:self.metaTitle forKey:kELPEventListMetaTitle];
    [mutableDict setValue:self.urlCategoryName forKey:kELPEventListUrlCategoryName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kELPEventListName];
    self.subTitle = [aDecoder decodeObjectForKey:kELPEventListSubTitle];
    self.snippetDate = [aDecoder decodeObjectForKey:kELPEventListSnippetDate];
    self.metaDescription = [aDecoder decodeObjectForKey:kELPEventListMetaDescription];
    self.performerId = [aDecoder decodeDoubleForKey:kELPEventListPerformerId];
    self.date = [aDecoder decodeObjectForKey:kELPEventListDate];
    self.performer = [aDecoder decodeObjectForKey:kELPEventListPerformer];
    self.venueState = [aDecoder decodeObjectForKey:kELPEventListVenueState];
    self.performerType = [aDecoder decodeObjectForKey:kELPEventListPerformerType];
    self.venueId = [aDecoder decodeDoubleForKey:kELPEventListVenueId];
    self.eventListDescription = [aDecoder decodeObjectForKey:kELPEventListDescription];
    self.subcategoryId = [aDecoder decodeDoubleForKey:kELPEventListSubcategoryId];
    self.primeSportUrl = [aDecoder decodeObjectForKey:kELPEventListPrimeSportUrl];
    self.venue = [aDecoder decodeObjectForKey:kELPEventListVenue];
    self.localDate = [aDecoder decodeObjectForKey:kELPEventListLocalDate];
    self.sectionWiseView = [aDecoder decodeObjectForKey:kELPEventListSectionWiseView];
    self.availableTickets = [aDecoder decodeDoubleForKey:kELPEventListAvailableTickets];
    self.venueCity = [aDecoder decodeObjectForKey:kELPEventListVenueCity];
    self.eIProductionId = [aDecoder decodeDoubleForKey:kELPEventListEIProductionId];
    self.eventId = [aDecoder decodeDoubleForKey:kELPEventListEventId];
    self.metaTitle = [aDecoder decodeObjectForKey:kELPEventListMetaTitle];
    self.urlCategoryName = [aDecoder decodeObjectForKey:kELPEventListUrlCategoryName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kELPEventListName];
    [aCoder encodeObject:_subTitle forKey:kELPEventListSubTitle];
    [aCoder encodeObject:_snippetDate forKey:kELPEventListSnippetDate];
    [aCoder encodeObject:_metaDescription forKey:kELPEventListMetaDescription];
    [aCoder encodeDouble:_performerId forKey:kELPEventListPerformerId];
    [aCoder encodeObject:_date forKey:kELPEventListDate];
    [aCoder encodeObject:_performer forKey:kELPEventListPerformer];
    [aCoder encodeObject:_venueState forKey:kELPEventListVenueState];
    [aCoder encodeObject:_performerType forKey:kELPEventListPerformerType];
    [aCoder encodeDouble:_venueId forKey:kELPEventListVenueId];
    [aCoder encodeObject:_eventListDescription forKey:kELPEventListDescription];
    [aCoder encodeDouble:_subcategoryId forKey:kELPEventListSubcategoryId];
    [aCoder encodeObject:_primeSportUrl forKey:kELPEventListPrimeSportUrl];
    [aCoder encodeObject:_venue forKey:kELPEventListVenue];
    [aCoder encodeObject:_localDate forKey:kELPEventListLocalDate];
    [aCoder encodeObject:_sectionWiseView forKey:kELPEventListSectionWiseView];
    [aCoder encodeDouble:_availableTickets forKey:kELPEventListAvailableTickets];
    [aCoder encodeObject:_venueCity forKey:kELPEventListVenueCity];
    [aCoder encodeDouble:_eIProductionId forKey:kELPEventListEIProductionId];
    [aCoder encodeDouble:_eventId forKey:kELPEventListEventId];
    [aCoder encodeObject:_metaTitle forKey:kELPEventListMetaTitle];
    [aCoder encodeObject:_urlCategoryName forKey:kELPEventListUrlCategoryName];
}

- (id)copyWithZone:(NSZone *)zone
{
    ELPEventList *copy = [[ELPEventList alloc] init];
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.subTitle = [self.subTitle copyWithZone:zone];
        copy.snippetDate = [self.snippetDate copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
        copy.performerId = self.performerId;
        copy.date = [self.date copyWithZone:zone];
        copy.performer = [self.performer copyWithZone:zone];
        copy.venueState = [self.venueState copyWithZone:zone];
        copy.performerType = [self.performerType copyWithZone:zone];
        copy.venueId = self.venueId;
        copy.eventListDescription = [self.eventListDescription copyWithZone:zone];
        copy.subcategoryId = self.subcategoryId;
        copy.primeSportUrl = [self.primeSportUrl copyWithZone:zone];
        copy.venue = [self.venue copyWithZone:zone];
        copy.localDate = [self.localDate copyWithZone:zone];
        copy.sectionWiseView = [self.sectionWiseView copyWithZone:zone];
        copy.availableTickets = self.availableTickets;
        copy.venueCity = [self.venueCity copyWithZone:zone];
        copy.eIProductionId = self.eIProductionId;
        copy.eventId = self.eventId;
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
        copy.urlCategoryName = [self.urlCategoryName copyWithZone:zone];
    }
    
    return copy;
}


@end
