//
//  ELDResultDynamicSplash.h
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ELDResultDynamicSplash : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *containers;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *metaTitle;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *metaDescription;
@property (nonatomic, strong) NSString *Description;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
