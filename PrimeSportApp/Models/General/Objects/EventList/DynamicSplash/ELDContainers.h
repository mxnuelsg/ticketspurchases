//
//  ELDContainers.h
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ELDContainers : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *containerTitle;
@property (nonatomic, strong) NSArray *events;
@property (nonatomic, assign) double sort;
@property (nonatomic, strong) NSString *pSDynamicSplashPageUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
