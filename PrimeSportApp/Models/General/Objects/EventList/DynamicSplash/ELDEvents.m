//
//  ELDEvents.m
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ELDEvents.h"


NSString *const kELDEventsName = @"Name";
NSString *const kELDEventsSubTitle = @"SubTitle";
NSString *const kELDEventsSnippetDate = @"SnippetDate";
NSString *const kELDEventsMetaDescription = @"MetaDescription";
NSString *const kELDEventsPerformerId = @"PerformerId";
NSString *const kELDEventsDate = @"Date";
NSString *const kELDEventsPerformer = @"Performer";
NSString *const kELDEventsVenueState = @"VenueState";
NSString *const kELDEventsPerformerType = @"PerformerType";
NSString *const kELDEventsVenueId = @"VenueId";
NSString *const kELDEventsDescription = @"Description";
NSString *const kELDEventsSubcategoryId = @"SubcategoryId";
NSString *const kELDEventsPrimeSportUrl = @"PrimeSportUrl";
NSString *const kELDEventsVenue = @"Venue";
NSString *const kELDEventsLocalDate = @"LocalDate";
NSString *const kELDEventsSectionWiseView = @"SectionWiseView";
NSString *const kELDEventsAvailableTickets = @"AvailableTickets";
NSString *const kELDEventsVenueCity = @"VenueCity";
NSString *const kELDEventsEIProductionId = @"EIProductionId";
NSString *const kELDEventsEventId = @"EventId";
NSString *const kELDEventsMetaTitle = @"MetaTitle";
NSString *const kELDEventsUrlCategoryName = @"UrlCategoryName";


@interface ELDEvents ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ELDEvents

@synthesize name = _name;
@synthesize subTitle = _subTitle;
@synthesize snippetDate = _snippetDate;
@synthesize metaDescription = _metaDescription;
@synthesize performerId = _performerId;
@synthesize date = _date;
@synthesize performer = _performer;
@synthesize venueState = _venueState;
@synthesize performerType = _performerType;
@synthesize venueId = _venueId;
@synthesize eventsDescription = _eventsDescription;
@synthesize subcategoryId = _subcategoryId;
@synthesize primeSportUrl = _primeSportUrl;
@synthesize venue = _venue;
@synthesize localDate = _localDate;
@synthesize sectionWiseView = _sectionWiseView;
@synthesize availableTickets = _availableTickets;
@synthesize venueCity = _venueCity;
@synthesize eIProductionId = _eIProductionId;
@synthesize eventId = _eventId;
@synthesize metaTitle = _metaTitle;
@synthesize urlCategoryName = _urlCategoryName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kELDEventsName fromDictionary:dict];
            self.subTitle = [self objectOrNilForKey:kELDEventsSubTitle fromDictionary:dict];
            self.snippetDate = [self objectOrNilForKey:kELDEventsSnippetDate fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kELDEventsMetaDescription fromDictionary:dict];
            self.performerId = [[self objectOrNilForKey:kELDEventsPerformerId fromDictionary:dict] doubleValue];
            self.date = [self objectOrNilForKey:kELDEventsDate fromDictionary:dict];
            self.performer = [self objectOrNilForKey:kELDEventsPerformer fromDictionary:dict];
            self.venueState = [self objectOrNilForKey:kELDEventsVenueState fromDictionary:dict];
            self.performerType = [self objectOrNilForKey:kELDEventsPerformerType fromDictionary:dict];
            self.venueId = [[self objectOrNilForKey:kELDEventsVenueId fromDictionary:dict] doubleValue];
            self.eventsDescription = [self objectOrNilForKey:kELDEventsDescription fromDictionary:dict];
            self.subcategoryId = [[self objectOrNilForKey:kELDEventsSubcategoryId fromDictionary:dict] doubleValue];
            self.primeSportUrl = [self objectOrNilForKey:kELDEventsPrimeSportUrl fromDictionary:dict];
            self.venue = [self objectOrNilForKey:kELDEventsVenue fromDictionary:dict];
            self.localDate = [self objectOrNilForKey:kELDEventsLocalDate fromDictionary:dict];
            self.sectionWiseView = [self objectOrNilForKey:kELDEventsSectionWiseView fromDictionary:dict];
            self.availableTickets = [[self objectOrNilForKey:kELDEventsAvailableTickets fromDictionary:dict] doubleValue];
            self.venueCity = [self objectOrNilForKey:kELDEventsVenueCity fromDictionary:dict];
            self.eIProductionId = [[self objectOrNilForKey:kELDEventsEIProductionId fromDictionary:dict] doubleValue];
            self.eventId = [[self objectOrNilForKey:kELDEventsEventId fromDictionary:dict] doubleValue];
            self.metaTitle = [self objectOrNilForKey:kELDEventsMetaTitle fromDictionary:dict];
            self.urlCategoryName = [self objectOrNilForKey:kELDEventsUrlCategoryName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kELDEventsName];
    [mutableDict setValue:self.subTitle forKey:kELDEventsSubTitle];
    [mutableDict setValue:self.snippetDate forKey:kELDEventsSnippetDate];
    [mutableDict setValue:self.metaDescription forKey:kELDEventsMetaDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.performerId] forKey:kELDEventsPerformerId];
    [mutableDict setValue:self.date forKey:kELDEventsDate];
    [mutableDict setValue:self.performer forKey:kELDEventsPerformer];
    [mutableDict setValue:self.venueState forKey:kELDEventsVenueState];
    [mutableDict setValue:self.performerType forKey:kELDEventsPerformerType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.venueId] forKey:kELDEventsVenueId];
    [mutableDict setValue:self.eventsDescription forKey:kELDEventsDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subcategoryId] forKey:kELDEventsSubcategoryId];
    [mutableDict setValue:self.primeSportUrl forKey:kELDEventsPrimeSportUrl];
    [mutableDict setValue:self.venue forKey:kELDEventsVenue];
    [mutableDict setValue:self.localDate forKey:kELDEventsLocalDate];
    [mutableDict setValue:self.sectionWiseView forKey:kELDEventsSectionWiseView];
    [mutableDict setValue:[NSNumber numberWithDouble:self.availableTickets] forKey:kELDEventsAvailableTickets];
    [mutableDict setValue:self.venueCity forKey:kELDEventsVenueCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eIProductionId] forKey:kELDEventsEIProductionId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eventId] forKey:kELDEventsEventId];
    [mutableDict setValue:self.metaTitle forKey:kELDEventsMetaTitle];
    [mutableDict setValue:self.urlCategoryName forKey:kELDEventsUrlCategoryName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kELDEventsName];
    self.subTitle = [aDecoder decodeObjectForKey:kELDEventsSubTitle];
    self.snippetDate = [aDecoder decodeObjectForKey:kELDEventsSnippetDate];
    self.metaDescription = [aDecoder decodeObjectForKey:kELDEventsMetaDescription];
    self.performerId = [aDecoder decodeDoubleForKey:kELDEventsPerformerId];
    self.date = [aDecoder decodeObjectForKey:kELDEventsDate];
    self.performer = [aDecoder decodeObjectForKey:kELDEventsPerformer];
    self.venueState = [aDecoder decodeObjectForKey:kELDEventsVenueState];
    self.performerType = [aDecoder decodeObjectForKey:kELDEventsPerformerType];
    self.venueId = [aDecoder decodeDoubleForKey:kELDEventsVenueId];
    self.eventsDescription = [aDecoder decodeObjectForKey:kELDEventsDescription];
    self.subcategoryId = [aDecoder decodeDoubleForKey:kELDEventsSubcategoryId];
    self.primeSportUrl = [aDecoder decodeObjectForKey:kELDEventsPrimeSportUrl];
    self.venue = [aDecoder decodeObjectForKey:kELDEventsVenue];
    self.localDate = [aDecoder decodeObjectForKey:kELDEventsLocalDate];
    self.sectionWiseView = [aDecoder decodeObjectForKey:kELDEventsSectionWiseView];
    self.availableTickets = [aDecoder decodeDoubleForKey:kELDEventsAvailableTickets];
    self.venueCity = [aDecoder decodeObjectForKey:kELDEventsVenueCity];
    self.eIProductionId = [aDecoder decodeDoubleForKey:kELDEventsEIProductionId];
    self.eventId = [aDecoder decodeDoubleForKey:kELDEventsEventId];
    self.metaTitle = [aDecoder decodeObjectForKey:kELDEventsMetaTitle];
    self.urlCategoryName = [aDecoder decodeObjectForKey:kELDEventsUrlCategoryName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kELDEventsName];
    [aCoder encodeObject:_subTitle forKey:kELDEventsSubTitle];
    [aCoder encodeObject:_snippetDate forKey:kELDEventsSnippetDate];
    [aCoder encodeObject:_metaDescription forKey:kELDEventsMetaDescription];
    [aCoder encodeDouble:_performerId forKey:kELDEventsPerformerId];
    [aCoder encodeObject:_date forKey:kELDEventsDate];
    [aCoder encodeObject:_performer forKey:kELDEventsPerformer];
    [aCoder encodeObject:_venueState forKey:kELDEventsVenueState];
    [aCoder encodeObject:_performerType forKey:kELDEventsPerformerType];
    [aCoder encodeDouble:_venueId forKey:kELDEventsVenueId];
    [aCoder encodeObject:_eventsDescription forKey:kELDEventsDescription];
    [aCoder encodeDouble:_subcategoryId forKey:kELDEventsSubcategoryId];
    [aCoder encodeObject:_primeSportUrl forKey:kELDEventsPrimeSportUrl];
    [aCoder encodeObject:_venue forKey:kELDEventsVenue];
    [aCoder encodeObject:_localDate forKey:kELDEventsLocalDate];
    [aCoder encodeObject:_sectionWiseView forKey:kELDEventsSectionWiseView];
    [aCoder encodeDouble:_availableTickets forKey:kELDEventsAvailableTickets];
    [aCoder encodeObject:_venueCity forKey:kELDEventsVenueCity];
    [aCoder encodeDouble:_eIProductionId forKey:kELDEventsEIProductionId];
    [aCoder encodeDouble:_eventId forKey:kELDEventsEventId];
    [aCoder encodeObject:_metaTitle forKey:kELDEventsMetaTitle];
    [aCoder encodeObject:_urlCategoryName forKey:kELDEventsUrlCategoryName];
}

- (id)copyWithZone:(NSZone *)zone
{
    ELDEvents *copy = [[ELDEvents alloc] init];
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.subTitle = [self.subTitle copyWithZone:zone];
        copy.snippetDate = [self.snippetDate copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
        copy.performerId = self.performerId;
        copy.date = [self.date copyWithZone:zone];
        copy.performer = [self.performer copyWithZone:zone];
        copy.venueState = [self.venueState copyWithZone:zone];
        copy.performerType = [self.performerType copyWithZone:zone];
        copy.venueId = self.venueId;
        copy.eventsDescription = [self.eventsDescription copyWithZone:zone];
        copy.subcategoryId = self.subcategoryId;
        copy.primeSportUrl = [self.primeSportUrl copyWithZone:zone];
        copy.venue = [self.venue copyWithZone:zone];
        copy.localDate = [self.localDate copyWithZone:zone];
        copy.sectionWiseView = [self.sectionWiseView copyWithZone:zone];
        copy.availableTickets = self.availableTickets;
        copy.venueCity = [self.venueCity copyWithZone:zone];
        copy.eIProductionId = self.eIProductionId;
        copy.eventId = self.eventId;
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
        copy.urlCategoryName = [self.urlCategoryName copyWithZone:zone];
    }
    
    return copy;
}


@end
