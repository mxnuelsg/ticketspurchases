//
//  ELDResultDynamicSplash.m
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ELDResultDynamicSplash.h"
#import "ELDContainers.h"


NSString *const kELDResultDynamicSplashContainers = @"Containers";
NSString *const kELDResultDynamicSplashId = @"Id";
NSString *const kELDResultDynamicSplashMetaTitle = @"MetaTitle";
NSString *const kELDResultDynamicSplashCode = @"Code";
NSString *const kELDResultDynamicSplashTitle = @"Title";
NSString *const kELDResultDynamicSplashMetaDescription = @"MetaDescription";
NSString *const kELDResultDynamicSplashDescription = @"Description";


@interface ELDResultDynamicSplash ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ELDResultDynamicSplash

@synthesize containers = _containers;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize metaTitle = _metaTitle;
@synthesize code = _code;
@synthesize title = _title;
@synthesize metaDescription = _metaDescription;
@synthesize Description = _Description;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedELDContainers = [dict objectForKey:kELDResultDynamicSplashContainers];
    NSMutableArray *parsedELDContainers = [NSMutableArray array];
    if ([receivedELDContainers isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedELDContainers) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedELDContainers addObject:[ELDContainers modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedELDContainers isKindOfClass:[NSDictionary class]]) {
       [parsedELDContainers addObject:[ELDContainers modelObjectWithDictionary:(NSDictionary *)receivedELDContainers]];
    }

    self.containers = [NSArray arrayWithArray:parsedELDContainers];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kELDResultDynamicSplashId fromDictionary:dict] doubleValue];
            self.metaTitle = [self objectOrNilForKey:kELDResultDynamicSplashMetaTitle fromDictionary:dict];
            self.code = [self objectOrNilForKey:kELDResultDynamicSplashCode fromDictionary:dict];
            self.title = [self objectOrNilForKey:kELDResultDynamicSplashTitle fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kELDResultDynamicSplashMetaDescription fromDictionary:dict];
            self.Description = [self objectOrNilForKey:kELDResultDynamicSplashDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForContainers = [NSMutableArray array];
    for (NSObject *subArrayObject in self.containers) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForContainers addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForContainers addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForContainers] forKey:kELDResultDynamicSplashContainers];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kELDResultDynamicSplashId];
    [mutableDict setValue:self.metaTitle forKey:kELDResultDynamicSplashMetaTitle];
    [mutableDict setValue:self.code forKey:kELDResultDynamicSplashCode];
    [mutableDict setValue:self.title forKey:kELDResultDynamicSplashTitle];
    [mutableDict setValue:self.metaDescription forKey:kELDResultDynamicSplashMetaDescription];
    [mutableDict setValue:self.Description forKey:kELDResultDynamicSplashDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.containers = [aDecoder decodeObjectForKey:kELDResultDynamicSplashContainers];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kELDResultDynamicSplashId];
    self.metaTitle = [aDecoder decodeObjectForKey:kELDResultDynamicSplashMetaTitle];
    self.code = [aDecoder decodeObjectForKey:kELDResultDynamicSplashCode];
    self.title = [aDecoder decodeObjectForKey:kELDResultDynamicSplashTitle];
    self.metaDescription = [aDecoder decodeObjectForKey:kELDResultDynamicSplashMetaDescription];
    self.Description = [aDecoder decodeObjectForKey:kELDResultDynamicSplashDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_containers forKey:kELDResultDynamicSplashContainers];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kELDResultDynamicSplashId];
    [aCoder encodeObject:_metaTitle forKey:kELDResultDynamicSplashMetaTitle];
    [aCoder encodeObject:_code forKey:kELDResultDynamicSplashCode];
    [aCoder encodeObject:_title forKey:kELDResultDynamicSplashTitle];
    [aCoder encodeObject:_metaDescription forKey:kELDResultDynamicSplashMetaDescription];
    [aCoder encodeObject:_Description forKey:kELDResultDynamicSplashDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    ELDResultDynamicSplash *copy = [[ELDResultDynamicSplash alloc] init];
    
    if (copy) {

        copy.containers = [self.containers copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
        copy.code = [self.code copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
        copy.Description = [self.Description copyWithZone:zone];
    }
    
    return copy;
}


@end
