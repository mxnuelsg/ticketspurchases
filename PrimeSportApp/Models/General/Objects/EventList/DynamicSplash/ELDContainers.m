//
//  ELDContainers.m
//
//  Created by Manuel Salinas on 06/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ELDContainers.h"
#import "ELDEvents.h"


NSString *const kELDContainersContainerTitle = @"ContainerTitle";
NSString *const kELDContainersEvents = @"Events";
NSString *const kELDContainersSort = @"Sort";
NSString *const kELDContainersPSDynamicSplashPageUrl = @"PSDynamicSplashPageUrl";


@interface ELDContainers ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ELDContainers

@synthesize containerTitle = _containerTitle;
@synthesize events = _events;
@synthesize sort = _sort;
@synthesize pSDynamicSplashPageUrl = _pSDynamicSplashPageUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.containerTitle = [self objectOrNilForKey:kELDContainersContainerTitle fromDictionary:dict];
    NSObject *receivedELDEvents = [dict objectForKey:kELDContainersEvents];
    NSMutableArray *parsedELDEvents = [NSMutableArray array];
    if ([receivedELDEvents isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedELDEvents) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedELDEvents addObject:[ELDEvents modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedELDEvents isKindOfClass:[NSDictionary class]]) {
       [parsedELDEvents addObject:[ELDEvents modelObjectWithDictionary:(NSDictionary *)receivedELDEvents]];
    }

    self.events = [NSArray arrayWithArray:parsedELDEvents];
            self.sort = [[self objectOrNilForKey:kELDContainersSort fromDictionary:dict] doubleValue];
            self.pSDynamicSplashPageUrl = [self objectOrNilForKey:kELDContainersPSDynamicSplashPageUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.containerTitle forKey:kELDContainersContainerTitle];
    NSMutableArray *tempArrayForEvents = [NSMutableArray array];
    for (NSObject *subArrayObject in self.events) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForEvents addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForEvents addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForEvents] forKey:kELDContainersEvents];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sort] forKey:kELDContainersSort];
    [mutableDict setValue:self.pSDynamicSplashPageUrl forKey:kELDContainersPSDynamicSplashPageUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.containerTitle = [aDecoder decodeObjectForKey:kELDContainersContainerTitle];
    self.events = [aDecoder decodeObjectForKey:kELDContainersEvents];
    self.sort = [aDecoder decodeDoubleForKey:kELDContainersSort];
    self.pSDynamicSplashPageUrl = [aDecoder decodeObjectForKey:kELDContainersPSDynamicSplashPageUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_containerTitle forKey:kELDContainersContainerTitle];
    [aCoder encodeObject:_events forKey:kELDContainersEvents];
    [aCoder encodeDouble:_sort forKey:kELDContainersSort];
    [aCoder encodeObject:_pSDynamicSplashPageUrl forKey:kELDContainersPSDynamicSplashPageUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    ELDContainers *copy = [[ELDContainers alloc] init];
    
    if (copy) {

        copy.containerTitle = [self.containerTitle copyWithZone:zone];
        copy.events = [self.events copyWithZone:zone];
        copy.sort = self.sort;
        copy.pSDynamicSplashPageUrl = [self.pSDynamicSplashPageUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
