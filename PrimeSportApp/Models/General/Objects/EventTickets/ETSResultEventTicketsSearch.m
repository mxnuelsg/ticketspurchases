//
//  ETSResultEventTicketsSearch.m
//
//  Created by Manuel Salinas on 02/09/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ETSResultEventTicketsSearch.h"
#import "ETSTickets.h"


NSString *const kETSResultEventTicketsSearchUrlCategoryName = @"UrlCategoryName";
NSString *const kETSResultEventTicketsSearchEventURL = @"EventUrl";
NSString *const kETSResultEventTicketsSearchLocalDateFrom = @"LocalDateFrom";
NSString *const kETSResultEventTicketsSearchName = @"Name";
NSString *const kETSResultEventTicketsSearchSubTitle = @"SubTitle";
NSString *const kETSResultEventTicketsSearchSnippetDate = @"SnippetDate";
NSString *const kETSResultEventTicketsSearchMetaDescription = @"MetaDescription";
NSString *const kETSResultEventTicketsSearchPerformerId = @"PerformerId";
NSString *const kETSResultEventTicketsSearchDate = @"Date";
NSString *const kETSResultEventTicketsSearchPerformer = @"Performer";
NSString *const kETSResultEventTicketsSearchVenueState = @"VenueState";
NSString *const kETSResultEventTicketsSearchPerformerType = @"PerformerType";
NSString *const kETSResultEventTicketsSearchVenueId = @"VenueId";
NSString *const kETSResultEventTicketsSearchDescription = @"Description";
NSString *const kETSResultEventTicketsSearchSubcategoryId = @"SubcategoryId";
NSString *const kETSResultEventTicketsSearchPrimeSportUrl = @"PrimeSportUrl";
NSString *const kETSResultEventTicketsSearchVenue = @"Venue";
NSString *const kETSResultEventTicketsSearchLocalDate = @"LocalDate";
NSString *const kETSResultEventTicketsSearchSectionWiseView = @"SectionWiseView";
NSString *const kETSResultEventTicketsSearchAvailableTickets = @"AvailableTickets";
NSString *const kETSResultEventTicketsSearchVenueCity = @"VenueCity";
NSString *const kETSResultEventTicketsSearchEIProductionId = @"EIProductionId";
NSString *const kETSResultEventTicketsSearchEventId = @"EventId";
NSString *const kETSResultEventTicketsSearchImageUrl = @"ImageUrl";
NSString *const kETSResultEventTicketsSearchTickets = @"Tickets";
NSString *const kETSResultEventTicketsSearchMetaTitle = @"MetaTitle";


@interface ETSResultEventTicketsSearch ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ETSResultEventTicketsSearch

@synthesize urlCategoryName = _urlCategoryName;
@synthesize eventURL = _eventURL;
@synthesize localDateFrom = _localDateFrom;
@synthesize name = _name;
@synthesize subTitle = _subTitle;
@synthesize snippetDate = _snippetDate;
@synthesize metaDescription = _metaDescription;
@synthesize performerId = _performerId;
@synthesize date = _date;
@synthesize performer = _performer;
@synthesize venueState = _venueState;
@synthesize performerType = _performerType;
@synthesize venueId = _venueId;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;
@synthesize subcategoryId = _subcategoryId;
@synthesize primeSportUrl = _primeSportUrl;
@synthesize venue = _venue;
@synthesize localDate = _localDate;
@synthesize sectionWiseView = _sectionWiseView;
@synthesize availableTickets = _availableTickets;
@synthesize venueCity = _venueCity;
@synthesize eIProductionId = _eIProductionId;
@synthesize eventId = _eventId;
@synthesize imageUrl = _imageUrl;
@synthesize tickets = _tickets;
@synthesize metaTitle = _metaTitle;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.urlCategoryName = [self objectOrNilForKey:kETSResultEventTicketsSearchUrlCategoryName fromDictionary:dict];
            self.eventURL = [self objectOrNilForKey:kETSResultEventTicketsSearchEventURL fromDictionary:dict];
            self.localDateFrom = [self objectOrNilForKey:kETSResultEventTicketsSearchLocalDateFrom fromDictionary:dict];
            self.name = [self objectOrNilForKey:kETSResultEventTicketsSearchName fromDictionary:dict];
            self.subTitle = [self objectOrNilForKey:kETSResultEventTicketsSearchSubTitle fromDictionary:dict];
            self.snippetDate = [self objectOrNilForKey:kETSResultEventTicketsSearchSnippetDate fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kETSResultEventTicketsSearchMetaDescription fromDictionary:dict];
            self.performerId = [[self objectOrNilForKey:kETSResultEventTicketsSearchPerformerId fromDictionary:dict] doubleValue];
            self.date = [self objectOrNilForKey:kETSResultEventTicketsSearchDate fromDictionary:dict];
            self.performer = [self objectOrNilForKey:kETSResultEventTicketsSearchPerformer fromDictionary:dict];
            self.venueState = [self objectOrNilForKey:kETSResultEventTicketsSearchVenueState fromDictionary:dict];
            self.performerType = [self objectOrNilForKey:kETSResultEventTicketsSearchPerformerType fromDictionary:dict];
            self.venueId = [[self objectOrNilForKey:kETSResultEventTicketsSearchVenueId fromDictionary:dict] doubleValue];
            self.internalBaseClassDescription = [self objectOrNilForKey:kETSResultEventTicketsSearchDescription fromDictionary:dict];
            self.subcategoryId = [[self objectOrNilForKey:kETSResultEventTicketsSearchSubcategoryId fromDictionary:dict] doubleValue];
            self.primeSportUrl = [self objectOrNilForKey:kETSResultEventTicketsSearchPrimeSportUrl fromDictionary:dict];
            self.venue = [self objectOrNilForKey:kETSResultEventTicketsSearchVenue fromDictionary:dict];
            self.localDate = [self objectOrNilForKey:kETSResultEventTicketsSearchLocalDate fromDictionary:dict];
            self.sectionWiseView = [self objectOrNilForKey:kETSResultEventTicketsSearchSectionWiseView fromDictionary:dict];
            self.availableTickets = [[self objectOrNilForKey:kETSResultEventTicketsSearchAvailableTickets fromDictionary:dict] doubleValue];
            self.venueCity = [self objectOrNilForKey:kETSResultEventTicketsSearchVenueCity fromDictionary:dict];
            self.eIProductionId = [[self objectOrNilForKey:kETSResultEventTicketsSearchEIProductionId fromDictionary:dict] doubleValue];
            self.eventId = [[self objectOrNilForKey:kETSResultEventTicketsSearchEventId fromDictionary:dict] doubleValue];
            self.imageUrl = [self objectOrNilForKey:kETSResultEventTicketsSearchImageUrl fromDictionary:dict];
    NSObject *receivedETSTickets = [dict objectForKey:kETSResultEventTicketsSearchTickets];
    NSMutableArray *parsedETSTickets = [NSMutableArray array];
    if ([receivedETSTickets isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedETSTickets) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedETSTickets addObject:[ETSTickets modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedETSTickets isKindOfClass:[NSDictionary class]]) {
       [parsedETSTickets addObject:[ETSTickets modelObjectWithDictionary:(NSDictionary *)receivedETSTickets]];
    }

    self.tickets = [NSArray arrayWithArray:parsedETSTickets];
            self.metaTitle = [self objectOrNilForKey:kETSResultEventTicketsSearchMetaTitle fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.urlCategoryName forKey:kETSResultEventTicketsSearchUrlCategoryName];
    [mutableDict setValue:self.eventURL forKey:kETSResultEventTicketsSearchEventURL];
    [mutableDict setValue:self.localDateFrom forKey:kETSResultEventTicketsSearchLocalDateFrom];
    [mutableDict setValue:self.name forKey:kETSResultEventTicketsSearchName];
    [mutableDict setValue:self.subTitle forKey:kETSResultEventTicketsSearchSubTitle];
    [mutableDict setValue:self.snippetDate forKey:kETSResultEventTicketsSearchSnippetDate];
    [mutableDict setValue:self.metaDescription forKey:kETSResultEventTicketsSearchMetaDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.performerId] forKey:kETSResultEventTicketsSearchPerformerId];
    [mutableDict setValue:self.date forKey:kETSResultEventTicketsSearchDate];
    [mutableDict setValue:self.performer forKey:kETSResultEventTicketsSearchPerformer];
    [mutableDict setValue:self.venueState forKey:kETSResultEventTicketsSearchVenueState];
    [mutableDict setValue:self.performerType forKey:kETSResultEventTicketsSearchPerformerType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.venueId] forKey:kETSResultEventTicketsSearchVenueId];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kETSResultEventTicketsSearchDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subcategoryId] forKey:kETSResultEventTicketsSearchSubcategoryId];
    [mutableDict setValue:self.primeSportUrl forKey:kETSResultEventTicketsSearchPrimeSportUrl];
    [mutableDict setValue:self.venue forKey:kETSResultEventTicketsSearchVenue];
    [mutableDict setValue:self.localDate forKey:kETSResultEventTicketsSearchLocalDate];
    [mutableDict setValue:self.sectionWiseView forKey:kETSResultEventTicketsSearchSectionWiseView];
    [mutableDict setValue:[NSNumber numberWithDouble:self.availableTickets] forKey:kETSResultEventTicketsSearchAvailableTickets];
    [mutableDict setValue:self.venueCity forKey:kETSResultEventTicketsSearchVenueCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eIProductionId] forKey:kETSResultEventTicketsSearchEIProductionId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eventId] forKey:kETSResultEventTicketsSearchEventId];
    [mutableDict setValue:self.imageUrl forKey:kETSResultEventTicketsSearchImageUrl];
    NSMutableArray *tempArrayForTickets = [NSMutableArray array];
    for (NSObject *subArrayObject in self.tickets) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForTickets addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForTickets addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForTickets] forKey:kETSResultEventTicketsSearchTickets];
    [mutableDict setValue:self.metaTitle forKey:kETSResultEventTicketsSearchMetaTitle];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.urlCategoryName = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchUrlCategoryName];
    self.eventURL = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchEventURL];
    self.localDateFrom = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchLocalDateFrom];
    self.name = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchName];
    self.subTitle = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchSubTitle];
    self.snippetDate = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchSnippetDate];
    self.metaDescription = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchMetaDescription];
    self.performerId = [aDecoder decodeDoubleForKey:kETSResultEventTicketsSearchPerformerId];
    self.date = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchDate];
    self.performer = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchPerformer];
    self.venueState = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchVenueState];
    self.performerType = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchPerformerType];
    self.venueId = [aDecoder decodeDoubleForKey:kETSResultEventTicketsSearchVenueId];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchDescription];
    self.subcategoryId = [aDecoder decodeDoubleForKey:kETSResultEventTicketsSearchSubcategoryId];
    self.primeSportUrl = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchPrimeSportUrl];
    self.venue = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchVenue];
    self.localDate = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchLocalDate];
    self.sectionWiseView = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchSectionWiseView];
    self.availableTickets = [aDecoder decodeDoubleForKey:kETSResultEventTicketsSearchAvailableTickets];
    self.venueCity = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchVenueCity];
    self.eIProductionId = [aDecoder decodeDoubleForKey:kETSResultEventTicketsSearchEIProductionId];
    self.eventId = [aDecoder decodeDoubleForKey:kETSResultEventTicketsSearchEventId];
    self.imageUrl = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchImageUrl];
    self.tickets = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchTickets];
    self.metaTitle = [aDecoder decodeObjectForKey:kETSResultEventTicketsSearchMetaTitle];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_urlCategoryName forKey:kETSResultEventTicketsSearchUrlCategoryName];
    [aCoder encodeObject:_eventURL forKey:kETSResultEventTicketsSearchEventURL];
    [aCoder encodeObject:_localDateFrom forKey:kETSResultEventTicketsSearchLocalDateFrom];
    [aCoder encodeObject:_name forKey:kETSResultEventTicketsSearchName];
    [aCoder encodeObject:_subTitle forKey:kETSResultEventTicketsSearchSubTitle];
    [aCoder encodeObject:_snippetDate forKey:kETSResultEventTicketsSearchSnippetDate];
    [aCoder encodeObject:_metaDescription forKey:kETSResultEventTicketsSearchMetaDescription];
    [aCoder encodeDouble:_performerId forKey:kETSResultEventTicketsSearchPerformerId];
    [aCoder encodeObject:_date forKey:kETSResultEventTicketsSearchDate];
    [aCoder encodeObject:_performer forKey:kETSResultEventTicketsSearchPerformer];
    [aCoder encodeObject:_venueState forKey:kETSResultEventTicketsSearchVenueState];
    [aCoder encodeObject:_performerType forKey:kETSResultEventTicketsSearchPerformerType];
    [aCoder encodeDouble:_venueId forKey:kETSResultEventTicketsSearchVenueId];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kETSResultEventTicketsSearchDescription];
    [aCoder encodeDouble:_subcategoryId forKey:kETSResultEventTicketsSearchSubcategoryId];
    [aCoder encodeObject:_primeSportUrl forKey:kETSResultEventTicketsSearchPrimeSportUrl];
    [aCoder encodeObject:_venue forKey:kETSResultEventTicketsSearchVenue];
    [aCoder encodeObject:_localDate forKey:kETSResultEventTicketsSearchLocalDate];
    [aCoder encodeObject:_sectionWiseView forKey:kETSResultEventTicketsSearchSectionWiseView];
    [aCoder encodeDouble:_availableTickets forKey:kETSResultEventTicketsSearchAvailableTickets];
    [aCoder encodeObject:_venueCity forKey:kETSResultEventTicketsSearchVenueCity];
    [aCoder encodeDouble:_eIProductionId forKey:kETSResultEventTicketsSearchEIProductionId];
    [aCoder encodeDouble:_eventId forKey:kETSResultEventTicketsSearchEventId];
    [aCoder encodeObject:_imageUrl forKey:kETSResultEventTicketsSearchImageUrl];
    [aCoder encodeObject:_tickets forKey:kETSResultEventTicketsSearchTickets];
    [aCoder encodeObject:_metaTitle forKey:kETSResultEventTicketsSearchMetaTitle];
}

- (id)copyWithZone:(NSZone *)zone
{
    ETSResultEventTicketsSearch *copy = [[ETSResultEventTicketsSearch alloc] init];
    
    if (copy) {

        copy.urlCategoryName = [self.urlCategoryName copyWithZone:zone];
        copy.eventURL = [self.eventURL copyWithZone:zone];
        copy.localDateFrom = [self.localDateFrom copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.subTitle = [self.subTitle copyWithZone:zone];
        copy.snippetDate = [self.snippetDate copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
        copy.performerId = self.performerId;
        copy.date = [self.date copyWithZone:zone];
        copy.performer = [self.performer copyWithZone:zone];
        copy.venueState = [self.venueState copyWithZone:zone];
        copy.performerType = [self.performerType copyWithZone:zone];
        copy.venueId = self.venueId;
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
        copy.subcategoryId = self.subcategoryId;
        copy.primeSportUrl = [self.primeSportUrl copyWithZone:zone];
        copy.venue = [self.venue copyWithZone:zone];
        copy.localDate = [self.localDate copyWithZone:zone];
        copy.sectionWiseView = [self.sectionWiseView copyWithZone:zone];
        copy.availableTickets = self.availableTickets;
        copy.venueCity = [self.venueCity copyWithZone:zone];
        copy.eIProductionId = self.eIProductionId;
        copy.eventId = self.eventId;
        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.tickets = [self.tickets copyWithZone:zone];
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
    }
    
    return copy;
}


@end
