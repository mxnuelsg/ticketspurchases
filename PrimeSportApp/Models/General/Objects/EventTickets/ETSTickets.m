//
//  ETSTickets.m
//
//  Created by Manuel Salinas on 10/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ETSTickets.h"


NSString *const kETSTicketsSection = @"Section";
NSString *const kETSTicketsSectionWiseView = @"SectionWiseView";
NSString *const kETSTicketsHandling = @"Handling";
NSString *const kETSTicketsTicketId = @"TicketId";
NSString *const kETSTicketsSplit = @"Split";
NSString *const kETSTicketsType = @"Type";
NSString *const kETSTicketsNotes = @"Notes";
NSString *const kETSTicketsPrice = @"Price";
NSString *const kETSTicketsEITicketId = @"EITicketId";
NSString *const kETSTicketsQuantity = @"Quantity";
NSString *const kETSTicketsRow = @"Row";


@interface ETSTickets ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ETSTickets

@synthesize section = _section;
@synthesize sectionWiseView = _sectionWiseView;
@synthesize handling = _handling;
@synthesize ticketId = _ticketId;
@synthesize split = _split;
@synthesize type = _type;
@synthesize notes = _notes;
@synthesize price = _price;
@synthesize eITicketId = _eITicketId;
@synthesize quantity = _quantity;
@synthesize row = _row;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.section = [self objectOrNilForKey:kETSTicketsSection fromDictionary:dict];
            self.sectionWiseView = [self objectOrNilForKey:kETSTicketsSectionWiseView fromDictionary:dict];
            self.handling = [[self objectOrNilForKey:kETSTicketsHandling fromDictionary:dict] doubleValue];
            self.ticketId = [[self objectOrNilForKey:kETSTicketsTicketId fromDictionary:dict] doubleValue];
            self.split = [self objectOrNilForKey:kETSTicketsSplit fromDictionary:dict];
            self.type = [self objectOrNilForKey:kETSTicketsType fromDictionary:dict];
            self.notes = [self objectOrNilForKey:kETSTicketsNotes fromDictionary:dict];
            self.price = [[self objectOrNilForKey:kETSTicketsPrice fromDictionary:dict] doubleValue];
            self.eITicketId = [[self objectOrNilForKey:kETSTicketsEITicketId fromDictionary:dict] doubleValue];
            self.quantity = [[self objectOrNilForKey:kETSTicketsQuantity fromDictionary:dict] doubleValue];
            self.row = [self objectOrNilForKey:kETSTicketsRow fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.section forKey:kETSTicketsSection];
    [mutableDict setValue:self.sectionWiseView forKey:kETSTicketsSectionWiseView];
    [mutableDict setValue:[NSNumber numberWithDouble:self.handling] forKey:kETSTicketsHandling];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ticketId] forKey:kETSTicketsTicketId];
    [mutableDict setValue:self.split forKey:kETSTicketsSplit];
    [mutableDict setValue:self.type forKey:kETSTicketsType];
    [mutableDict setValue:self.notes forKey:kETSTicketsNotes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.price] forKey:kETSTicketsPrice];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eITicketId] forKey:kETSTicketsEITicketId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.quantity] forKey:kETSTicketsQuantity];
    [mutableDict setValue:self.row forKey:kETSTicketsRow];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.section = [aDecoder decodeObjectForKey:kETSTicketsSection];
    self.sectionWiseView = [aDecoder decodeObjectForKey:kETSTicketsSectionWiseView];
    self.handling = [aDecoder decodeDoubleForKey:kETSTicketsHandling];
    self.ticketId = [aDecoder decodeDoubleForKey:kETSTicketsTicketId];
    self.split = [aDecoder decodeObjectForKey:kETSTicketsSplit];
    self.type = [aDecoder decodeObjectForKey:kETSTicketsType];
    self.notes = [aDecoder decodeObjectForKey:kETSTicketsNotes];
    self.price = [aDecoder decodeDoubleForKey:kETSTicketsPrice];
    self.eITicketId = [aDecoder decodeDoubleForKey:kETSTicketsEITicketId];
    self.quantity = [aDecoder decodeDoubleForKey:kETSTicketsQuantity];
    self.row = [aDecoder decodeObjectForKey:kETSTicketsRow];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_section forKey:kETSTicketsSection];
    [aCoder encodeObject:_sectionWiseView forKey:kETSTicketsSectionWiseView];
    [aCoder encodeDouble:_handling forKey:kETSTicketsHandling];
    [aCoder encodeDouble:_ticketId forKey:kETSTicketsTicketId];
    [aCoder encodeObject:_split forKey:kETSTicketsSplit];
    [aCoder encodeObject:_type forKey:kETSTicketsType];
    [aCoder encodeObject:_notes forKey:kETSTicketsNotes];
    [aCoder encodeDouble:_price forKey:kETSTicketsPrice];
    [aCoder encodeDouble:_eITicketId forKey:kETSTicketsEITicketId];
    [aCoder encodeDouble:_quantity forKey:kETSTicketsQuantity];
    [aCoder encodeObject:_row forKey:kETSTicketsRow];
}

- (id)copyWithZone:(NSZone *)zone
{
    ETSTickets *copy = [[ETSTickets alloc] init];
    
    if (copy) {

        copy.section = [self.section copyWithZone:zone];
        copy.sectionWiseView = [self.sectionWiseView copyWithZone:zone];
        copy.handling = self.handling;
        copy.ticketId = self.ticketId;
        copy.split = [self.split copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.notes = [self.notes copyWithZone:zone];
        copy.price = self.price;
        copy.eITicketId = self.eITicketId;
        copy.quantity = self.quantity;
        copy.row = [self.row copyWithZone:zone];
    }
    
    return copy;
}


@end
