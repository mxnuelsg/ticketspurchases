//
//  ETSTickets.h
//
//  Created by Manuel Salinas on 10/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ETSTickets : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSString *sectionWiseView;
@property (nonatomic, assign) double handling;
@property (nonatomic, assign) double ticketId;
@property (nonatomic, strong) NSString *split;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, assign) double price;
@property (nonatomic, assign) double eITicketId;
@property (nonatomic, assign) double quantity;
@property (nonatomic, strong) NSString *row;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
