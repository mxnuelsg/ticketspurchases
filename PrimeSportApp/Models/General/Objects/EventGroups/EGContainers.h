//
//  EGContainers.h
//
//  Created by Manuel Salinas on 04/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface EGContainers : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *contaiterElements;
@property (nonatomic, strong) NSString *containerTitle;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
