//
//  EGContainers.m
//
//  Created by Manuel Salinas on 04/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "EGContainers.h"
#import "EGContaiterElements.h"


NSString *const kEGContainersContaiterElements = @"ContaiterElements";
NSString *const kEGContainersContainerTitle = @"ContainerTitle";


@interface EGContainers ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation EGContainers

@synthesize contaiterElements = _contaiterElements;
@synthesize containerTitle = _containerTitle;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedEGContaiterElements = [dict objectForKey:kEGContainersContaiterElements];
    NSMutableArray *parsedEGContaiterElements = [NSMutableArray array];
    if ([receivedEGContaiterElements isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedEGContaiterElements) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedEGContaiterElements addObject:[EGContaiterElements modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedEGContaiterElements isKindOfClass:[NSDictionary class]]) {
       [parsedEGContaiterElements addObject:[EGContaiterElements modelObjectWithDictionary:(NSDictionary *)receivedEGContaiterElements]];
    }

    self.contaiterElements = [NSArray arrayWithArray:parsedEGContaiterElements];
            self.containerTitle = [self objectOrNilForKey:kEGContainersContainerTitle fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForContaiterElements = [NSMutableArray array];
    for (NSObject *subArrayObject in self.contaiterElements) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForContaiterElements addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForContaiterElements addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForContaiterElements] forKey:kEGContainersContaiterElements];
    [mutableDict setValue:self.containerTitle forKey:kEGContainersContainerTitle];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.contaiterElements = [aDecoder decodeObjectForKey:kEGContainersContaiterElements];
    self.containerTitle = [aDecoder decodeObjectForKey:kEGContainersContainerTitle];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_contaiterElements forKey:kEGContainersContaiterElements];
    [aCoder encodeObject:_containerTitle forKey:kEGContainersContainerTitle];
}

- (id)copyWithZone:(NSZone *)zone
{
    EGContainers *copy = [[EGContainers alloc] init];
    
    if (copy) {

        copy.contaiterElements = [self.contaiterElements copyWithZone:zone];
        copy.containerTitle = [self.containerTitle copyWithZone:zone];
    }
    
    return copy;
}


@end
