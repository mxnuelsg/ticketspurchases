//
//  EGResultSplash.m
//
//  Created by Manuel Salinas on 04/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "EGResultSplash.h"
#import "EGContainers.h"


NSString *const kEGResultSplashContainers = @"Containers";
NSString *const kEGResultSplashId = @"Id";
NSString *const kEGResultSplashMetaTitle = @"MetaTitle";
NSString *const kEGResultSplashCode = @"Code";
NSString *const kEGResultSplashTitle = @"Title";
NSString *const kEGResultSplashMetaDescription = @"MetaDescription";
NSString *const kEGResultSplashPSSplashPageUrl = @"PSSplashPageUrl";


@interface EGResultSplash ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation EGResultSplash

@synthesize containers = _containers;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize metaTitle = _metaTitle;
@synthesize code = _code;
@synthesize title = _title;
@synthesize metaDescription = _metaDescription;
@synthesize pSSplashPageUrl = _pSSplashPageUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedEGContainers = [dict objectForKey:kEGResultSplashContainers];
    NSMutableArray *parsedEGContainers = [NSMutableArray array];
    if ([receivedEGContainers isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedEGContainers) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedEGContainers addObject:[EGContainers modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedEGContainers isKindOfClass:[NSDictionary class]]) {
       [parsedEGContainers addObject:[EGContainers modelObjectWithDictionary:(NSDictionary *)receivedEGContainers]];
    }

    self.containers = [NSArray arrayWithArray:parsedEGContainers];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kEGResultSplashId fromDictionary:dict] doubleValue];
            self.metaTitle = [self objectOrNilForKey:kEGResultSplashMetaTitle fromDictionary:dict];
            self.code = [self objectOrNilForKey:kEGResultSplashCode fromDictionary:dict];
            self.title = [self objectOrNilForKey:kEGResultSplashTitle fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kEGResultSplashMetaDescription fromDictionary:dict];
            self.pSSplashPageUrl = [self objectOrNilForKey:kEGResultSplashPSSplashPageUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForContainers = [NSMutableArray array];
    for (NSObject *subArrayObject in self.containers) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForContainers addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForContainers addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForContainers] forKey:kEGResultSplashContainers];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kEGResultSplashId];
    [mutableDict setValue:self.metaTitle forKey:kEGResultSplashMetaTitle];
    [mutableDict setValue:self.code forKey:kEGResultSplashCode];
    [mutableDict setValue:self.title forKey:kEGResultSplashTitle];
    [mutableDict setValue:self.metaDescription forKey:kEGResultSplashMetaDescription];
    [mutableDict setValue:self.pSSplashPageUrl forKey:kEGResultSplashPSSplashPageUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.containers = [aDecoder decodeObjectForKey:kEGResultSplashContainers];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kEGResultSplashId];
    self.metaTitle = [aDecoder decodeObjectForKey:kEGResultSplashMetaTitle];
    self.code = [aDecoder decodeObjectForKey:kEGResultSplashCode];
    self.title = [aDecoder decodeObjectForKey:kEGResultSplashTitle];
    self.metaDescription = [aDecoder decodeObjectForKey:kEGResultSplashMetaDescription];
    self.pSSplashPageUrl = [aDecoder decodeObjectForKey:kEGResultSplashPSSplashPageUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_containers forKey:kEGResultSplashContainers];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kEGResultSplashId];
    [aCoder encodeObject:_metaTitle forKey:kEGResultSplashMetaTitle];
    [aCoder encodeObject:_code forKey:kEGResultSplashCode];
    [aCoder encodeObject:_title forKey:kEGResultSplashTitle];
    [aCoder encodeObject:_metaDescription forKey:kEGResultSplashMetaDescription];
    [aCoder encodeObject:_pSSplashPageUrl forKey:kEGResultSplashPSSplashPageUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    EGResultSplash *copy = [[EGResultSplash alloc] init];
    
    if (copy) {

        copy.containers = [self.containers copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
        copy.code = [self.code copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
        copy.pSSplashPageUrl = [self.pSSplashPageUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
