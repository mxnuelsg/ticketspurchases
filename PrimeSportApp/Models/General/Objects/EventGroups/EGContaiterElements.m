//
//  EGContaiterElements.m
//
//  Created by Manuel Salinas on 04/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "EGContaiterElements.h"


NSString *const kEGContaiterElementsUrl = @"Url";
NSString *const kEGContaiterElementsType = @"Type";
NSString *const kEGContaiterElementsName = @"Name";
NSString *const kEGContaiterElementsId = @"Id";


@interface EGContaiterElements ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation EGContaiterElements

@synthesize url = _url;
@synthesize type = _type;
@synthesize name = _name;
@synthesize contaiterElementsIdentifier = _contaiterElementsIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.url = [self objectOrNilForKey:kEGContaiterElementsUrl fromDictionary:dict];
            self.type = [self objectOrNilForKey:kEGContaiterElementsType fromDictionary:dict];
            self.name = [self objectOrNilForKey:kEGContaiterElementsName fromDictionary:dict];
            self.contaiterElementsIdentifier = [self objectOrNilForKey:kEGContaiterElementsId fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.url forKey:kEGContaiterElementsUrl];
    [mutableDict setValue:self.type forKey:kEGContaiterElementsType];
    [mutableDict setValue:self.name forKey:kEGContaiterElementsName];
    [mutableDict setValue:self.contaiterElementsIdentifier forKey:kEGContaiterElementsId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.url = [aDecoder decodeObjectForKey:kEGContaiterElementsUrl];
    self.type = [aDecoder decodeObjectForKey:kEGContaiterElementsType];
    self.name = [aDecoder decodeObjectForKey:kEGContaiterElementsName];
    self.contaiterElementsIdentifier = [aDecoder decodeObjectForKey:kEGContaiterElementsId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_url forKey:kEGContaiterElementsUrl];
    [aCoder encodeObject:_type forKey:kEGContaiterElementsType];
    [aCoder encodeObject:_name forKey:kEGContaiterElementsName];
    [aCoder encodeObject:_contaiterElementsIdentifier forKey:kEGContaiterElementsId];
}

- (id)copyWithZone:(NSZone *)zone
{
    EGContaiterElements *copy = [[EGContaiterElements alloc] init];
    
    if (copy) {

        copy.url = [self.url copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.contaiterElementsIdentifier = [self.contaiterElementsIdentifier copyWithZone:zone];
    }
    
    return copy;
}


@end
