//
//  PSGlobalData.h
//
//  Created by Manuel Salinas on 12/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.

/**************************************************************************************************
 * Description:
 * This class allows global access to: Macros, Imports, Frameworks, Constants, Etc. *
**************************************************************************************************/

//******************************************* CLASSES **********************************************//
#import "PSAppDelegate.h"
#import "PSPrincipalViewController.h"
#import "PSSessionParameters.h"
#import "PSTicket.h"
#import "PSNavigationController.h"
#import "PSValidations.h"
#import "PSTextfieldWithoutMenu.h"
#import "UIColor+Hexadecimal.h"
#import "UIColor+PrimeSportColors.h"
#import "UIScrollView+AutoSize_Top.h"
#import "UIViewController+PopUp.h"
#import "UITabBarItem+CustomBadge.h"
#import "UILabel+Boldify.h"
#import "PSBorderLabel.h"
#import "NSString+Plus.h"
#import "NSArray+FilteredSorted.h"
#import "UIImageView+AFNetworking.h"
#import "PSUtils.h"
#import "PSWebServiceUtils.h"
#import "PSMessages.h"
#import "AFNetworking.h"
#import "PSWebService.h"
#import "MLPAccessoryBadge.h"
#import "NGAParallaxMotion.h"
#import "PSEmptyView.h"
#import "UITextField+Shake.h"

//******************************************** FRAMEWORKS ******************************************//
#import <QuartzCore/QuartzCore.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>

//******************************************** MACROS **********************************************//

#define iPhone4_or_iPhone5_or_iPad ([[UIScreen mainScreen] bounds].size.height == 568 ? 5 : ([[UIScreen mainScreen] bounds].size.height == 480 ? 4 : 999))

//******************************************** CONSTANTS *******************************************//

//General
static int const kWS_TIMEOUT         = 60;
static int const kZIP_CODE_LIMIT     = 5;
static int const kPHONE_NUMBER_LIMIT = 20;

//Enable  or Disable Console Log
static BOOL const kLogActivated = YES;

//Empty
static NSString *const kEmptyField = @"- - - -";

//Connection
static NSString *const wsPATH     = @"http://primesport.sieenasoftware.com/QryApi";
static NSString *const wsUSERNAME = @"mobile";
static NSString *const wsPASSWORD = @"mobile";

//Constant Methods for Web Service
static NSString *const kGetMainCategories   = @"GetMainCategories";
static NSString *const kGetSiteLinks        = @"GetSiteLinks";
static NSString *const kLogin               = @"Login";
static NSString *const kForgotPassword      = @"ForgotPassword";
static NSString *const kSignUp              = @"SignUp";
static NSString *const kSearchEvents        = @"SearchEvents";
static NSString *const kCategoryEventGroups = @"GetCategoryEventGroups";
static NSString *const kDynamicSplash       = @"GetDynamicSplashPageGroup";
static NSString *const kSplash              = @"GetSplashPageGroup";
static NSString *const kEventGroup          = @"GetEventGroup";
static NSString *const kEventTicketsSearch  = @"SearchTickets";
static NSString *const kShippingMethods     = @"GetShippingMethods";
static NSString *const kAllCountries        = @"GetCountries";
static NSString *const kShippingCountries   = @"GetShippingCountries";
static NSString *const kStates              = @"GetStates";
static NSString *const kOrderTotals         = @"GetOrderTotals";
static NSString *const kSubmitOrder         = @"SubmitOrder";
static NSString *const kBillingInfos        = @"GetBillingInfos";
static NSString *const kSpecificBillingInfo = @"GetBillingInfo";
static NSString *const kChangePassword      = @"ChangePassword";
static NSString *const kTicketsPurchased    = @"GetTicketsPurchasedDetails";
static NSString *const kAccountInfo         = @"GetAccountByEmail";
static NSString *const kCities              = @"GetCities";
static NSString *const kSaveAccount         = @"SaveAccount";
static NSString *const kDeleteBilling       = @"DeleteBillingInfo";
static NSString *const kSaveOrUpdateBilling = @"SaveBillingInfo";
static NSString *const kGetZipCode          = @"GetZipcode";
static NSString *const kVerifyToken         = @"ValidToken";
static NSString *const kPreferred           = @"GetPreferred";
