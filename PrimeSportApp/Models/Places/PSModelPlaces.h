//
//  PSModelPlaces.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol modelPlacesDelegate <NSObject>

@optional
-(void)modelResponseForShippingMethodCountries:(NSDictionary *)result;
-(void)modelResponseForBillings:(NSDictionary *)result;
-(void)modelResponseForCountries:(NSDictionary *)result;
-(void)modelResponseForStates:(NSDictionary *)result;
-(void)modelResponseForCities:(NSDictionary *)result;
@end

@interface PSModelPlaces : NSObject

@property (assign, nonatomic) id <modelPlacesDelegate> delegate;

-(void)getShippingMethodCountriesWithObject:(PSWebService *)wsObject;
-(void)getBillingsWithObject:(PSWebService *)wsObject;
-(void)getCountriesWithObject:(PSWebService *)wsObject;
-(void)getStatesWithObject:(PSWebService *)wsObject;
-(void)getCitiesWithObject:(PSWebService *)wsObject;


@end
