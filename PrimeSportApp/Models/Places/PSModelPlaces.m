//
//  PSModelPlaces.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelPlaces.h"

@implementation PSModelPlaces

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


#pragma mark -
#pragma mark - BILLINGS
-(void)getBillingsWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForBillings:)])
                                               {
                                                   [self.delegate modelResponseForBillings:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForBillings:)])
                                               {
                                                   [self.delegate modelResponseForBillings:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}


#pragma mark -
#pragma mark - COUNTRIES, STATES & CITIES
-(void)getShippingMethodCountriesWithObject:(PSWebService *)wsObject
{
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForShippingMethodCountries:)])
                                               {
                                                   [self.delegate modelResponseForShippingMethodCountries:result];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForShippingMethodCountries:)])
                                               {
                                                   [self.delegate modelResponseForShippingMethodCountries:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                           }];
    [jsonRequest start];
}


-(void)getCountriesWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               //                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForCountries:)])
                                               {
                                                   [self.delegate modelResponseForCountries:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForCountries:)])
                                               {
                                                   [self.delegate modelResponseForCountries:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

-(void)getStatesWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               //                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForStates:)])
                                               {
                                                   [self.delegate modelResponseForStates:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForStates:)])
                                               {
                                                   [self.delegate modelResponseForStates:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

-(void)getCitiesWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);}
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               //                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               NSDictionary *result = JSON;
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForCities:)])
                                               {
                                                   [self.delegate modelResponseForCities:result];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForCities:)])
                                               {
                                                   [self.delegate modelResponseForCities:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}


@end
