//
//  PSModelUpcomingEvents.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 27/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataSitesModels.h"

@protocol modelUpcomingEventsDelegate <NSObject>

@optional
-(void)numberOfSiteLinks:(NSInteger)totalUpcomingEvents;
-(void)modelResponseForSiteLinksWithObject:(SLResultSiteLinks *)result;
@end

@interface PSModelUpcomingEvents : NSObject

@property (assign, nonatomic) id <modelUpcomingEventsDelegate> delegate;

-(void)getNumberOfSiteLinksWithObject:(PSWebService *)wsObject;
-(void)getSiteLinksWithObject:(PSWebService *)wsObject;

@end
