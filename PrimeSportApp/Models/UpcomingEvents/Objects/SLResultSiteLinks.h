//
//  SLResultSiteLinks.h
//
//  Created by Manuel Salinas on 16/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SLResultSiteLinks : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *siteLinks;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
