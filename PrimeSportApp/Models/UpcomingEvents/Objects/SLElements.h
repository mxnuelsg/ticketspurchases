//
//  SLElements.h
//
//  Created by Manuel Salinas on 01/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SLElements : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *elementType;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double elementId;
@property (nonatomic, strong) NSString *dateRange;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
