//
//  SLResultSiteLinks.m
//
//  Created by Manuel Salinas on 16/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SLResultSiteLinks.h"
#import "SLSiteLinks.h"


NSString *const kSLResultSiteLinksSiteLinks = @"SiteLinks";


@interface SLResultSiteLinks ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SLResultSiteLinks

@synthesize siteLinks = _siteLinks;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedSLSiteLinks = [dict objectForKey:kSLResultSiteLinksSiteLinks];
    NSMutableArray *parsedSLSiteLinks = [NSMutableArray array];
    if ([receivedSLSiteLinks isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSLSiteLinks) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSLSiteLinks addObject:[SLSiteLinks modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSLSiteLinks isKindOfClass:[NSDictionary class]]) {
       [parsedSLSiteLinks addObject:[SLSiteLinks modelObjectWithDictionary:(NSDictionary *)receivedSLSiteLinks]];
    }

    self.siteLinks = [NSArray arrayWithArray:parsedSLSiteLinks];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSiteLinks = [NSMutableArray array];
    for (NSObject *subArrayObject in self.siteLinks) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSiteLinks addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSiteLinks addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSiteLinks] forKey:kSLResultSiteLinksSiteLinks];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.siteLinks = [aDecoder decodeObjectForKey:kSLResultSiteLinksSiteLinks];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_siteLinks forKey:kSLResultSiteLinksSiteLinks];
}

- (id)copyWithZone:(NSZone *)zone
{
    SLResultSiteLinks *copy = [[SLResultSiteLinks alloc] init];
    
    if (copy) {

        copy.siteLinks = [self.siteLinks copyWithZone:zone];
    }
    
    return copy;
}


@end
