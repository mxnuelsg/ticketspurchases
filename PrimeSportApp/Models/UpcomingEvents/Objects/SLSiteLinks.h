//
//  SLSiteLinks.h
//
//  Created by Manuel Salinas on 16/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SLSiteLinks : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tabName;
@property (nonatomic, strong) NSArray *elements;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
