//
//  SLSiteLinks.m
//
//  Created by Manuel Salinas on 16/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SLSiteLinks.h"
#import "SLElements.h"


NSString *const kSLSiteLinksTabName = @"TabName";
NSString *const kSLSiteLinksElements = @"Elements";


@interface SLSiteLinks ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SLSiteLinks

@synthesize tabName = _tabName;
@synthesize elements = _elements;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tabName = [self objectOrNilForKey:kSLSiteLinksTabName fromDictionary:dict];
    NSObject *receivedSLElements = [dict objectForKey:kSLSiteLinksElements];
    NSMutableArray *parsedSLElements = [NSMutableArray array];
    if ([receivedSLElements isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSLElements) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSLElements addObject:[SLElements modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSLElements isKindOfClass:[NSDictionary class]]) {
       [parsedSLElements addObject:[SLElements modelObjectWithDictionary:(NSDictionary *)receivedSLElements]];
    }

    self.elements = [NSArray arrayWithArray:parsedSLElements];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tabName forKey:kSLSiteLinksTabName];
    NSMutableArray *tempArrayForElements = [NSMutableArray array];
    for (NSObject *subArrayObject in self.elements) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForElements addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForElements addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForElements] forKey:kSLSiteLinksElements];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tabName = [aDecoder decodeObjectForKey:kSLSiteLinksTabName];
    self.elements = [aDecoder decodeObjectForKey:kSLSiteLinksElements];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tabName forKey:kSLSiteLinksTabName];
    [aCoder encodeObject:_elements forKey:kSLSiteLinksElements];
}

- (id)copyWithZone:(NSZone *)zone
{
    SLSiteLinks *copy = [[SLSiteLinks alloc] init];
    
    if (copy) {

        copy.tabName = [self.tabName copyWithZone:zone];
        copy.elements = [self.elements copyWithZone:zone];
    }
    
    return copy;
}


@end
