//
//  SLElements.m
//
//  Created by Manuel Salinas on 01/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SLElements.h"


NSString *const kSLElementsElementType = @"ElementType";
NSString *const kSLElementsImageURL = @"ImageURL";
NSString *const kSLElementsUrl = @"Url";
NSString *const kSLElementsLocation = @"Location";
NSString *const kSLElementsTitle = @"Title";
NSString *const kSLElementsElementId = @"ElementId";
NSString *const kSLElementsDateRange = @"DateRange";


@interface SLElements ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SLElements

@synthesize elementType = _elementType;
@synthesize imageURL = _imageURL;
@synthesize url = _url;
@synthesize location = _location;
@synthesize title = _title;
@synthesize elementId = _elementId;
@synthesize dateRange = _dateRange;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.elementType = [self objectOrNilForKey:kSLElementsElementType fromDictionary:dict];
            self.imageURL = [self objectOrNilForKey:kSLElementsImageURL fromDictionary:dict];
            self.url = [self objectOrNilForKey:kSLElementsUrl fromDictionary:dict];
            self.location = [self objectOrNilForKey:kSLElementsLocation fromDictionary:dict];
            self.title = [self objectOrNilForKey:kSLElementsTitle fromDictionary:dict];
            self.elementId = [[self objectOrNilForKey:kSLElementsElementId fromDictionary:dict] doubleValue];
            self.dateRange = [self objectOrNilForKey:kSLElementsDateRange fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.elementType forKey:kSLElementsElementType];
    [mutableDict setValue:self.imageURL forKey:kSLElementsImageURL];
    [mutableDict setValue:self.url forKey:kSLElementsUrl];
    [mutableDict setValue:self.location forKey:kSLElementsLocation];
    [mutableDict setValue:self.title forKey:kSLElementsTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.elementId] forKey:kSLElementsElementId];
    [mutableDict setValue:self.dateRange forKey:kSLElementsDateRange];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.elementType = [aDecoder decodeObjectForKey:kSLElementsElementType];
    self.imageURL = [aDecoder decodeObjectForKey:kSLElementsImageURL];
    self.url = [aDecoder decodeObjectForKey:kSLElementsUrl];
    self.location = [aDecoder decodeObjectForKey:kSLElementsLocation];
    self.title = [aDecoder decodeObjectForKey:kSLElementsTitle];
    self.elementId = [aDecoder decodeDoubleForKey:kSLElementsElementId];
    self.dateRange = [aDecoder decodeObjectForKey:kSLElementsDateRange];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_elementType forKey:kSLElementsElementType];
    [aCoder encodeObject:_imageURL forKey:kSLElementsImageURL];
    [aCoder encodeObject:_url forKey:kSLElementsUrl];
    [aCoder encodeObject:_location forKey:kSLElementsLocation];
    [aCoder encodeObject:_title forKey:kSLElementsTitle];
    [aCoder encodeDouble:_elementId forKey:kSLElementsElementId];
    [aCoder encodeObject:_dateRange forKey:kSLElementsDateRange];
}

- (id)copyWithZone:(NSZone *)zone
{
    SLElements *copy = [[SLElements alloc] init];
    
    if (copy) {

        copy.elementType = [self.elementType copyWithZone:zone];
        copy.imageURL = [self.imageURL copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.elementId = self.elementId;
        copy.dateRange = [self.dateRange copyWithZone:zone];
    }
    
    return copy;
}


@end
