//
//  PSModelUpcomingEvents.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 27/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelUpcomingEvents.h"

@implementation PSModelUpcomingEvents

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

-(void)getNumberOfSiteLinksWithObject:(PSWebService *)wsObject
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
                                               SLResultSiteLinks  *wsResponse = [[SLResultSiteLinks alloc]initWithDictionary:JSON];
                                               
                                               //get total of events
                                               NSUInteger total = 0;
                                               for (SLSiteLinks *SL in wsResponse.siteLinks)
                                               {
                                                   for (SLElements *element in SL.elements)
                                                   {
                                                        #pragma unused (element)
                                                       total++;
                                                   }
                                               }
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(numberOfSiteLinks:)])
                                               {
                                                   [self.delegate numberOfSiteLinks:total];
                                               }
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(numberOfSiteLinks:)])
                                               {
                                                   [self.delegate numberOfSiteLinks:0];
                                               }
                                               
                                           }];
    [jsonRequest start];
}

-(void)getSiteLinksWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               SLResultSiteLinks *wsResponse = [[SLResultSiteLinks alloc]initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSiteLinksWithObject:)])
                                               {
                                                    [self.delegate modelResponseForSiteLinksWithObject:wsResponse];
                                               }
                                              
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSiteLinksWithObject:)])
                                               {
                                                   [self.delegate modelResponseForSiteLinksWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

@end
