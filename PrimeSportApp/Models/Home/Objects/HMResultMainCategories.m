//
//  HMResultMainCategories.m
//
//  Created by Manuel Salinas on 27/05/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "HMResultMainCategories.h"
#import "HMData.h"


NSString *const kHMResultMainCategoriesData = @"Data";
NSString *const kHMResultMainCategoriesValidationErrors = @"ValidationErrors";
NSString *const kHMResultMainCategoriesMessage = @"Message";
NSString *const kHMResultMainCategoriesResponseCode = @"ResponseCode";


@interface HMResultMainCategories ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HMResultMainCategories

@synthesize data = _data;
@synthesize validationErrors = _validationErrors;
@synthesize message = _message;
@synthesize responseCode = _responseCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedHMData = [dict objectForKey:kHMResultMainCategoriesData];
    NSMutableArray *parsedHMData = [NSMutableArray array];
    if ([receivedHMData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedHMData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedHMData addObject:[HMData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedHMData isKindOfClass:[NSDictionary class]]) {
       [parsedHMData addObject:[HMData modelObjectWithDictionary:(NSDictionary *)receivedHMData]];
    }

    self.data = [NSArray arrayWithArray:parsedHMData];
            self.validationErrors = [self objectOrNilForKey:kHMResultMainCategoriesValidationErrors fromDictionary:dict];
            self.message = [self objectOrNilForKey:kHMResultMainCategoriesMessage fromDictionary:dict];
            self.responseCode = [[self objectOrNilForKey:kHMResultMainCategoriesResponseCode fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kHMResultMainCategoriesData];
    NSMutableArray *tempArrayForValidationErrors = [NSMutableArray array];
    for (NSObject *subArrayObject in self.validationErrors) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForValidationErrors addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForValidationErrors addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForValidationErrors] forKey:kHMResultMainCategoriesValidationErrors];
    [mutableDict setValue:self.message forKey:kHMResultMainCategoriesMessage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.responseCode] forKey:kHMResultMainCategoriesResponseCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.data = [aDecoder decodeObjectForKey:kHMResultMainCategoriesData];
    self.validationErrors = [aDecoder decodeObjectForKey:kHMResultMainCategoriesValidationErrors];
    self.message = [aDecoder decodeObjectForKey:kHMResultMainCategoriesMessage];
    self.responseCode = [aDecoder decodeDoubleForKey:kHMResultMainCategoriesResponseCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_data forKey:kHMResultMainCategoriesData];
    [aCoder encodeObject:_validationErrors forKey:kHMResultMainCategoriesValidationErrors];
    [aCoder encodeObject:_message forKey:kHMResultMainCategoriesMessage];
    [aCoder encodeDouble:_responseCode forKey:kHMResultMainCategoriesResponseCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    HMResultMainCategories *copy = [[HMResultMainCategories alloc] init];
    
    if (copy) {

        copy.data = [self.data copyWithZone:zone];
        copy.validationErrors = [self.validationErrors copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
        copy.responseCode = self.responseCode;
    }
    
    return copy;
}


@end
