//
//  HMData.m
//
//  Created by Manuel Salinas on 31/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "HMData.h"


NSString *const kHMDataImageUrl = @"ImageUrl";
NSString *const kHMDataName = @"Name";
NSString *const kHMDataOrder = @"Order";
NSString *const kHMDataId = @"Id";


@interface HMData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HMData

@synthesize imageUrl = _imageUrl;
@synthesize name = _name;
@synthesize order = _order;
@synthesize dataIdentifier = _dataIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.imageUrl = [self objectOrNilForKey:kHMDataImageUrl fromDictionary:dict];
            self.name = [self objectOrNilForKey:kHMDataName fromDictionary:dict];
            self.order = [[self objectOrNilForKey:kHMDataOrder fromDictionary:dict] doubleValue];
            self.dataIdentifier = [[self objectOrNilForKey:kHMDataId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.imageUrl forKey:kHMDataImageUrl];
    [mutableDict setValue:self.name forKey:kHMDataName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.order] forKey:kHMDataOrder];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kHMDataId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.imageUrl = [aDecoder decodeObjectForKey:kHMDataImageUrl];
    self.name = [aDecoder decodeObjectForKey:kHMDataName];
    self.order = [aDecoder decodeDoubleForKey:kHMDataOrder];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kHMDataId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_imageUrl forKey:kHMDataImageUrl];
    [aCoder encodeObject:_name forKey:kHMDataName];
    [aCoder encodeDouble:_order forKey:kHMDataOrder];
    [aCoder encodeDouble:_dataIdentifier forKey:kHMDataId];
}

- (id)copyWithZone:(NSZone *)zone
{
    HMData *copy = [[HMData alloc] init];
    
    if (copy) {

        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.order = self.order;
        copy.dataIdentifier = self.dataIdentifier;
    }
    
    return copy;
}


@end
