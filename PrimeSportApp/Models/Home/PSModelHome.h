//
//  PSModelHome.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 27/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHomeModels.h"

@protocol modelHomeDelegate <NSObject>

@optional
-(void)modelResponseForHomeWithObject:(HMResultMainCategories *)result;
@end

@interface PSModelHome : NSObject

@property (assign, nonatomic) id <modelHomeDelegate> delegate;

-(void)getMainCategoriesWithObject:(PSWebService *)wsObject;

@end
