//
//  PSModelHome.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 27/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelHome.h"

@implementation PSModelHome

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

-(void)getMainCategoriesWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",
                                       wsPATH,
                                       wsObject.wsMethod]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequest:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               HMResultMainCategories  *wsResponse = [[HMResultMainCategories alloc]initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForHomeWithObject:)])
                                               {
                                                   [self.delegate modelResponseForHomeWithObject:wsResponse];
                                               }
                                               
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForHomeWithObject:)])
                                               {
                                                   [self.delegate modelResponseForHomeWithObject:nil];
                                               }
                                           }];
    
    [jsonRequest start];
}

@end
