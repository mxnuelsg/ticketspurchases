//
//  SEResultSearch.m
//
//  Created by Manuel Salinas on 02/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SEResultSearch.h"
#import "SEEvents.h"


NSString *const kSEResultSearchEvents = @"Events";
NSString *const kSEResultSearchTotalRecordsCount = @"TotalRecordsCount";


@interface SEResultSearch ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SEResultSearch

@synthesize events = _events;
@synthesize totalRecordsCount = _totalRecordsCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedSEEvents = [dict objectForKey:kSEResultSearchEvents];
    NSMutableArray *parsedSEEvents = [NSMutableArray array];
    if ([receivedSEEvents isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSEEvents) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSEEvents addObject:[SEEvents modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSEEvents isKindOfClass:[NSDictionary class]]) {
       [parsedSEEvents addObject:[SEEvents modelObjectWithDictionary:(NSDictionary *)receivedSEEvents]];
    }

    self.events = [NSArray arrayWithArray:parsedSEEvents];
            self.totalRecordsCount = [[self objectOrNilForKey:kSEResultSearchTotalRecordsCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForEvents = [NSMutableArray array];
    for (NSObject *subArrayObject in self.events) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForEvents addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForEvents addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForEvents] forKey:kSEResultSearchEvents];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalRecordsCount] forKey:kSEResultSearchTotalRecordsCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.events = [aDecoder decodeObjectForKey:kSEResultSearchEvents];
    self.totalRecordsCount = [aDecoder decodeDoubleForKey:kSEResultSearchTotalRecordsCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_events forKey:kSEResultSearchEvents];
    [aCoder encodeDouble:_totalRecordsCount forKey:kSEResultSearchTotalRecordsCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    SEResultSearch *copy = [[SEResultSearch alloc] init];
    
    if (copy) {

        copy.events = [self.events copyWithZone:zone];
        copy.totalRecordsCount = self.totalRecordsCount;
    }
    
    return copy;
}


@end
