//
//  SEEvents.h
//
//  Created by Manuel Salinas on 31/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SEEvents : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *localDateFrom;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *subTitle;
@property (nonatomic, strong) NSString *snippetDate;
@property (nonatomic, strong) NSString *metaDescription;
@property (nonatomic, assign) double performerId;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *performer;
@property (nonatomic, strong) NSString *venueState;
@property (nonatomic, strong) NSString *performerType;
@property (nonatomic, assign) double venueId;
@property (nonatomic, strong) NSString *eventsDescription;
@property (nonatomic, assign) double subcategoryId;
@property (nonatomic, strong) NSString *primeSportUrl;
@property (nonatomic, strong) NSString *venue;
@property (nonatomic, strong) NSString *localDate;
@property (nonatomic, strong) NSString *sectionWiseView;
@property (nonatomic, assign) double availableTickets;
@property (nonatomic, strong) NSString *venueCity;
@property (nonatomic, assign) double eIProductionId;
@property (nonatomic, assign) double eventId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *metaTitle;
@property (nonatomic, strong) NSString *urlCategoryName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
