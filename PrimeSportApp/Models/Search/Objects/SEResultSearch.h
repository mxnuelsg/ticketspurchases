//
//  SEResultSearch.h
//
//  Created by Manuel Salinas on 02/06/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SEResultSearch : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *events;
@property (nonatomic, assign) double totalRecordsCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
