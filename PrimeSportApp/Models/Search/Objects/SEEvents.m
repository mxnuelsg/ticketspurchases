//
//  SEEvents.m
//
//  Created by Manuel Salinas on 31/07/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SEEvents.h"


NSString *const kSEEventsLocalDateFrom = @"LocalDateFrom";
NSString *const kSEEventsName = @"Name";
NSString *const kSEEventsSubTitle = @"SubTitle";
NSString *const kSEEventsSnippetDate = @"SnippetDate";
NSString *const kSEEventsMetaDescription = @"MetaDescription";
NSString *const kSEEventsPerformerId = @"PerformerId";
NSString *const kSEEventsDate = @"Date";
NSString *const kSEEventsPerformer = @"Performer";
NSString *const kSEEventsVenueState = @"VenueState";
NSString *const kSEEventsPerformerType = @"PerformerType";
NSString *const kSEEventsVenueId = @"VenueId";
NSString *const kSEEventsDescription = @"Description";
NSString *const kSEEventsSubcategoryId = @"SubcategoryId";
NSString *const kSEEventsPrimeSportUrl = @"PrimeSportUrl";
NSString *const kSEEventsVenue = @"Venue";
NSString *const kSEEventsLocalDate = @"LocalDate";
NSString *const kSEEventsSectionWiseView = @"SectionWiseView";
NSString *const kSEEventsAvailableTickets = @"AvailableTickets";
NSString *const kSEEventsVenueCity = @"VenueCity";
NSString *const kSEEventsEIProductionId = @"EIProductionId";
NSString *const kSEEventsEventId = @"EventId";
NSString *const kSEEventsImageUrl = @"ImageUrl";
NSString *const kSEEventsMetaTitle = @"MetaTitle";
NSString *const kSEEventsUrlCategoryName = @"UrlCategoryName";


@interface SEEvents ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SEEvents

@synthesize localDateFrom = _localDateFrom;
@synthesize name = _name;
@synthesize subTitle = _subTitle;
@synthesize snippetDate = _snippetDate;
@synthesize metaDescription = _metaDescription;
@synthesize performerId = _performerId;
@synthesize date = _date;
@synthesize performer = _performer;
@synthesize venueState = _venueState;
@synthesize performerType = _performerType;
@synthesize venueId = _venueId;
@synthesize eventsDescription = _eventsDescription;
@synthesize subcategoryId = _subcategoryId;
@synthesize primeSportUrl = _primeSportUrl;
@synthesize venue = _venue;
@synthesize localDate = _localDate;
@synthesize sectionWiseView = _sectionWiseView;
@synthesize availableTickets = _availableTickets;
@synthesize venueCity = _venueCity;
@synthesize eIProductionId = _eIProductionId;
@synthesize eventId = _eventId;
@synthesize imageUrl = _imageUrl;
@synthesize metaTitle = _metaTitle;
@synthesize urlCategoryName = _urlCategoryName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.localDateFrom = [self objectOrNilForKey:kSEEventsLocalDateFrom fromDictionary:dict];
            self.name = [self objectOrNilForKey:kSEEventsName fromDictionary:dict];
            self.subTitle = [self objectOrNilForKey:kSEEventsSubTitle fromDictionary:dict];
            self.snippetDate = [self objectOrNilForKey:kSEEventsSnippetDate fromDictionary:dict];
            self.metaDescription = [self objectOrNilForKey:kSEEventsMetaDescription fromDictionary:dict];
            self.performerId = [[self objectOrNilForKey:kSEEventsPerformerId fromDictionary:dict] doubleValue];
            self.date = [self objectOrNilForKey:kSEEventsDate fromDictionary:dict];
            self.performer = [self objectOrNilForKey:kSEEventsPerformer fromDictionary:dict];
            self.venueState = [self objectOrNilForKey:kSEEventsVenueState fromDictionary:dict];
            self.performerType = [self objectOrNilForKey:kSEEventsPerformerType fromDictionary:dict];
            self.venueId = [[self objectOrNilForKey:kSEEventsVenueId fromDictionary:dict] doubleValue];
            self.eventsDescription = [self objectOrNilForKey:kSEEventsDescription fromDictionary:dict];
            self.subcategoryId = [[self objectOrNilForKey:kSEEventsSubcategoryId fromDictionary:dict] doubleValue];
            self.primeSportUrl = [self objectOrNilForKey:kSEEventsPrimeSportUrl fromDictionary:dict];
            self.venue = [self objectOrNilForKey:kSEEventsVenue fromDictionary:dict];
            self.localDate = [self objectOrNilForKey:kSEEventsLocalDate fromDictionary:dict];
            self.sectionWiseView = [self objectOrNilForKey:kSEEventsSectionWiseView fromDictionary:dict];
            self.availableTickets = [[self objectOrNilForKey:kSEEventsAvailableTickets fromDictionary:dict] doubleValue];
            self.venueCity = [self objectOrNilForKey:kSEEventsVenueCity fromDictionary:dict];
            self.eIProductionId = [[self objectOrNilForKey:kSEEventsEIProductionId fromDictionary:dict] doubleValue];
            self.eventId = [[self objectOrNilForKey:kSEEventsEventId fromDictionary:dict] doubleValue];
            self.imageUrl = [self objectOrNilForKey:kSEEventsImageUrl fromDictionary:dict];
            self.metaTitle = [self objectOrNilForKey:kSEEventsMetaTitle fromDictionary:dict];
            self.urlCategoryName = [self objectOrNilForKey:kSEEventsUrlCategoryName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.localDateFrom forKey:kSEEventsLocalDateFrom];
    [mutableDict setValue:self.name forKey:kSEEventsName];
    [mutableDict setValue:self.subTitle forKey:kSEEventsSubTitle];
    [mutableDict setValue:self.snippetDate forKey:kSEEventsSnippetDate];
    [mutableDict setValue:self.metaDescription forKey:kSEEventsMetaDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.performerId] forKey:kSEEventsPerformerId];
    [mutableDict setValue:self.date forKey:kSEEventsDate];
    [mutableDict setValue:self.performer forKey:kSEEventsPerformer];
    [mutableDict setValue:self.venueState forKey:kSEEventsVenueState];
    [mutableDict setValue:self.performerType forKey:kSEEventsPerformerType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.venueId] forKey:kSEEventsVenueId];
    [mutableDict setValue:self.eventsDescription forKey:kSEEventsDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.subcategoryId] forKey:kSEEventsSubcategoryId];
    [mutableDict setValue:self.primeSportUrl forKey:kSEEventsPrimeSportUrl];
    [mutableDict setValue:self.venue forKey:kSEEventsVenue];
    [mutableDict setValue:self.localDate forKey:kSEEventsLocalDate];
    [mutableDict setValue:self.sectionWiseView forKey:kSEEventsSectionWiseView];
    [mutableDict setValue:[NSNumber numberWithDouble:self.availableTickets] forKey:kSEEventsAvailableTickets];
    [mutableDict setValue:self.venueCity forKey:kSEEventsVenueCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eIProductionId] forKey:kSEEventsEIProductionId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eventId] forKey:kSEEventsEventId];
    [mutableDict setValue:self.imageUrl forKey:kSEEventsImageUrl];
    [mutableDict setValue:self.metaTitle forKey:kSEEventsMetaTitle];
    [mutableDict setValue:self.urlCategoryName forKey:kSEEventsUrlCategoryName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.localDateFrom = [aDecoder decodeObjectForKey:kSEEventsLocalDateFrom];
    self.name = [aDecoder decodeObjectForKey:kSEEventsName];
    self.subTitle = [aDecoder decodeObjectForKey:kSEEventsSubTitle];
    self.snippetDate = [aDecoder decodeObjectForKey:kSEEventsSnippetDate];
    self.metaDescription = [aDecoder decodeObjectForKey:kSEEventsMetaDescription];
    self.performerId = [aDecoder decodeDoubleForKey:kSEEventsPerformerId];
    self.date = [aDecoder decodeObjectForKey:kSEEventsDate];
    self.performer = [aDecoder decodeObjectForKey:kSEEventsPerformer];
    self.venueState = [aDecoder decodeObjectForKey:kSEEventsVenueState];
    self.performerType = [aDecoder decodeObjectForKey:kSEEventsPerformerType];
    self.venueId = [aDecoder decodeDoubleForKey:kSEEventsVenueId];
    self.eventsDescription = [aDecoder decodeObjectForKey:kSEEventsDescription];
    self.subcategoryId = [aDecoder decodeDoubleForKey:kSEEventsSubcategoryId];
    self.primeSportUrl = [aDecoder decodeObjectForKey:kSEEventsPrimeSportUrl];
    self.venue = [aDecoder decodeObjectForKey:kSEEventsVenue];
    self.localDate = [aDecoder decodeObjectForKey:kSEEventsLocalDate];
    self.sectionWiseView = [aDecoder decodeObjectForKey:kSEEventsSectionWiseView];
    self.availableTickets = [aDecoder decodeDoubleForKey:kSEEventsAvailableTickets];
    self.venueCity = [aDecoder decodeObjectForKey:kSEEventsVenueCity];
    self.eIProductionId = [aDecoder decodeDoubleForKey:kSEEventsEIProductionId];
    self.eventId = [aDecoder decodeDoubleForKey:kSEEventsEventId];
    self.imageUrl = [aDecoder decodeObjectForKey:kSEEventsImageUrl];
    self.metaTitle = [aDecoder decodeObjectForKey:kSEEventsMetaTitle];
    self.urlCategoryName = [aDecoder decodeObjectForKey:kSEEventsUrlCategoryName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_localDateFrom forKey:kSEEventsLocalDateFrom];
    [aCoder encodeObject:_name forKey:kSEEventsName];
    [aCoder encodeObject:_subTitle forKey:kSEEventsSubTitle];
    [aCoder encodeObject:_snippetDate forKey:kSEEventsSnippetDate];
    [aCoder encodeObject:_metaDescription forKey:kSEEventsMetaDescription];
    [aCoder encodeDouble:_performerId forKey:kSEEventsPerformerId];
    [aCoder encodeObject:_date forKey:kSEEventsDate];
    [aCoder encodeObject:_performer forKey:kSEEventsPerformer];
    [aCoder encodeObject:_venueState forKey:kSEEventsVenueState];
    [aCoder encodeObject:_performerType forKey:kSEEventsPerformerType];
    [aCoder encodeDouble:_venueId forKey:kSEEventsVenueId];
    [aCoder encodeObject:_eventsDescription forKey:kSEEventsDescription];
    [aCoder encodeDouble:_subcategoryId forKey:kSEEventsSubcategoryId];
    [aCoder encodeObject:_primeSportUrl forKey:kSEEventsPrimeSportUrl];
    [aCoder encodeObject:_venue forKey:kSEEventsVenue];
    [aCoder encodeObject:_localDate forKey:kSEEventsLocalDate];
    [aCoder encodeObject:_sectionWiseView forKey:kSEEventsSectionWiseView];
    [aCoder encodeDouble:_availableTickets forKey:kSEEventsAvailableTickets];
    [aCoder encodeObject:_venueCity forKey:kSEEventsVenueCity];
    [aCoder encodeDouble:_eIProductionId forKey:kSEEventsEIProductionId];
    [aCoder encodeDouble:_eventId forKey:kSEEventsEventId];
    [aCoder encodeObject:_imageUrl forKey:kSEEventsImageUrl];
    [aCoder encodeObject:_metaTitle forKey:kSEEventsMetaTitle];
    [aCoder encodeObject:_urlCategoryName forKey:kSEEventsUrlCategoryName];
}

- (id)copyWithZone:(NSZone *)zone
{
    SEEvents *copy = [[SEEvents alloc] init];
    
    if (copy) {

        copy.localDateFrom = [self.localDateFrom copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.subTitle = [self.subTitle copyWithZone:zone];
        copy.snippetDate = [self.snippetDate copyWithZone:zone];
        copy.metaDescription = [self.metaDescription copyWithZone:zone];
        copy.performerId = self.performerId;
        copy.date = [self.date copyWithZone:zone];
        copy.performer = [self.performer copyWithZone:zone];
        copy.venueState = [self.venueState copyWithZone:zone];
        copy.performerType = [self.performerType copyWithZone:zone];
        copy.venueId = self.venueId;
        copy.eventsDescription = [self.eventsDescription copyWithZone:zone];
        copy.subcategoryId = self.subcategoryId;
        copy.primeSportUrl = [self.primeSportUrl copyWithZone:zone];
        copy.venue = [self.venue copyWithZone:zone];
        copy.localDate = [self.localDate copyWithZone:zone];
        copy.sectionWiseView = [self.sectionWiseView copyWithZone:zone];
        copy.availableTickets = self.availableTickets;
        copy.venueCity = [self.venueCity copyWithZone:zone];
        copy.eIProductionId = self.eIProductionId;
        copy.eventId = self.eventId;
        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.metaTitle = [self.metaTitle copyWithZone:zone];
        copy.urlCategoryName = [self.urlCategoryName copyWithZone:zone];
    }
    
    return copy;
}


@end
