//
//  PSModelSearch.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 02/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataSearchModels.h"

@protocol modelSearchDelegate <NSObject>

@optional
-(void)modelResponseForSearchEventsWithObject:(SEResultSearch *)result;
@end

@interface PSModelSearch : NSObject

@property (assign, nonatomic) id <modelSearchDelegate> delegate;

-(void)getSearchEventsWithObject:(PSWebService *)wsObject;

@end
