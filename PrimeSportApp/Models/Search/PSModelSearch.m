//
//  PSModelSearch.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 02/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModelSearch.h"

@implementation PSModelSearch

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

-(void)getSearchEventsWithObject:(PSWebService *)wsObject
{
    [PSUtils tabBarInteracionIsEnable:NO];
    
    NSString *queryString;
    
    queryString = [PSWebServiceUtils createQueryString:wsObject.wsParameters];
    
    if([queryString isEqualToString:@"?"])
    {
        queryString = [NSString string];
    }

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@",
                                       wsPATH,
                                       wsObject.wsMethod,
                                       queryString]];
    
    if (kLogActivated) {NSLog(@"URL => %@", url);} 
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWS_TIMEOUT];
    
    request = [PSWebServiceUtils setHeadersForGET_MethodInRequestWithLocation:request];
    
    AFJSONRequestOperation *jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                           {
//                                               NSLog(@"Response: %@", JSON);
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               SEResultSearch  *wsResponse = [[SEResultSearch alloc]initWithDictionary:JSON];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSearchEventsWithObject:)])
                                               {
                                                    [self.delegate modelResponseForSearchEventsWithObject:wsResponse];
                                               }
                                              
                                               
                                           }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error,id JSON)
                                           {
                                               [PSUtils tabBarInteracionIsEnable:YES];
                                               
                                               if (self.delegate && [self.delegate respondsToSelector:@selector(modelResponseForSearchEventsWithObject:)])
                                               {
                                                   [self.delegate modelResponseForSearchEventsWithObject:nil];
                                               }
                                               
                                               [[PSMessages getModel] showAlertForRequestError];
                                               
                                           }];
    [jsonRequest start];
}

@end
