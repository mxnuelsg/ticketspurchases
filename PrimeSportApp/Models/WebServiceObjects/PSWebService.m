//
//  PSWebService.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSWebService.h"

@implementation PSWebService

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.wsParameters = [NSDictionary dictionary];
        self.wsMethod     = [NSString string];
    }
    return self;
}

@end
