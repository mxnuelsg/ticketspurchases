//
//  PSWebService.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSWebService : NSObject

@property (strong, nonatomic) NSDictionary *wsParameters;
@property (copy, nonatomic) NSString *wsMethod;

@end
