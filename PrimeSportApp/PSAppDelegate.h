//
//  PSAppDelegate.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSessionParameters.h"
#import "PSTicket.h"
#import <FacebookSDK/FBSession.h>

@interface PSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PSSessionParameters *psSession;
@property (strong, nonatomic) PSTicket *psTicket;

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error;

@end
