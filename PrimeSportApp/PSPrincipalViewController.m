//
//  PSPrincipalViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSPrincipalViewController.h"
#import "PSHomeViewController.h"
#import "PSUpcomingEventsViewController.h"
#import "PSSearchViewController.h"
#import "PSAccountViewController.h"
#import "PSLoginViewController.h"
#import "PSModelUpcomingEvents.h"
#import "PSShippingViewController.h"
#import "PSFinalTicketViewController.h"
#import "PSLocation.h"
#import "MYBlurIntroductionView.h"
#import "PSIntroFavorites.h"

@interface PSPrincipalViewController () <UITabBarControllerDelegate, modelUpcomingEventsDelegate, finalTicketDelegate, MYIntroductionDelegate>
{
    PSSearchViewController *vcSearch;
}
@property (strong, nonatomic) PSLocation *psLocation;
@property (strong, nonatomic) NSDictionary *dictTicketInfo;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) PSNavigationController *navControllerSiteLinks;
@property (strong, nonatomic) PSModelUpcomingEvents *model;

@end

@implementation PSPrincipalViewController

static NSString *const titleTabBarTextColor = @"#FEDD33";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self buildPrimeSportApp];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Principal dealloc");
    self.model.delegate         = nil;
    self.tabBarController       = nil;
    self.navControllerSiteLinks = nil;
    self.dictTicketInfo         = nil;
    self.psLocation             = nil;
}

#pragma mark -
#pragma mark - LOAD APP OR LOAD INTRO
-(void)buildPrimeSportApp
{
    self.psLocation = [[PSLocation alloc] init];
    
    self.model = [[PSModelUpcomingEvents alloc] init];
    self.model.delegate = self;
    
    self.dictTicketInfo = [[NSDictionary alloc]init];
    
    [self loadTabBarController];
//    [self updateUpcomingEvents];
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)updateUpcomingEvents
{
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetSiteLinks;
    
    [self.model getNumberOfSiteLinksWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelUpcomingEvents)
-(void)numberOfSiteLinks:(NSInteger)totalUpcomingEvents
{
    if (totalUpcomingEvents > 0)
    {
        [self.navControllerSiteLinks.tabBarItem setCustomBadgeValue:[NSString stringWithFormat:@"%li", (long)totalUpcomingEvents]];
    }
}

#pragma mark -
#pragma mark - UPDATE BADGE UPCOMING EVENTS
-(void)updateBadgeUpcomingEvents:(NSUInteger)totalUpcomingEvents
{
    if (totalUpcomingEvents == 0)
    {
        [self.navControllerSiteLinks.tabBarItem setCustomBadgeValue:nil];
    }
    else
    {
        [self.navControllerSiteLinks.tabBarItem setCustomBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)totalUpcomingEvents]];
    }
}

#pragma mark -
#pragma mark - TAB BAR CONTROLLER INTERACTION
-(void)tabBarInteraction:(BOOL)interaction
{
    if (interaction == YES)
    {
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        self.tabBarController.tabBar.alpha = 1.0;
        
        for (UITabBarItem *item in self.tabBarController.tabBar.items)
        {
            [item setEnabled:YES];
        }
    }
    else
    {
        self.tabBarController.tabBar.userInteractionEnabled = NO;
        self.tabBarController.tabBar.alpha = 0.9;
        
        for (UITabBarItem *item in self.tabBarController.tabBar.items)
        {
            [item setEnabled:NO];
        }
    }
}

-(void)tabBarSelectedItem:(NSUInteger)itemSelected
{
    [self.tabBarController setSelectedIndex:itemSelected];
}

#pragma mark -
#pragma mark - TAB BAR CONTROLLER
-(void)loadTabBarController
{
    //White Unselected title items
     [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    
    //Init main ViewControllers & NavigationBar to add in TabBarController
    
    /* 3- Search */
    /*PSSearchViewController */vcSearch = [[PSSearchViewController alloc]init];
    vcSearch.title = @"Search";
    
    PSNavigationController *navControllerSearch = [[PSNavigationController alloc] initWithRootViewController:vcSearch];
    
    
    UIImage *searchUnselected = [UIImage imageNamed:@"iconSearch"];
    searchUnselected = [searchUnselected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *searchSelected = [UIImage imageNamed:@"iconSearchSelected"];
    searchSelected = [searchSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    navControllerSearch.tabBarItem = [[UITabBarItem alloc] initWithTitle:vcSearch.title
                                                                                image:searchUnselected
                                                                        selectedImage:searchSelected];
    
    [navControllerSearch.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:11.0f], NSFontAttributeName,  [UIColor colorWithHexadecimal:titleTabBarTextColor alpha:1], NSForegroundColorAttributeName,nil] forState:UIControlStateSelected];


    /* 4- Account */
    PSAccountViewController *vcMyAccount = [[PSAccountViewController alloc]init];
    vcMyAccount.title = @"My Account";
    
    PSNavigationController *navControllerAccount = [[PSNavigationController alloc] initWithRootViewController:vcMyAccount];
    
    UIImage *loginUnselected = [UIImage imageNamed:@"iconLogin"];
    loginUnselected = [loginUnselected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *loginSelected = [UIImage imageNamed:@"iconLoginSelected"];
    loginSelected = [loginSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    navControllerAccount.tabBarItem = [[UITabBarItem alloc] initWithTitle:vcMyAccount.title
                                                                  image:loginUnselected
                                                          selectedImage:loginSelected];
    
    [navControllerAccount.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:11.0f], NSFontAttributeName,  [UIColor colorWithHexadecimal:titleTabBarTextColor alpha:1], NSForegroundColorAttributeName,nil] forState:UIControlStateSelected];
    
    //Init TabBarController
    self.tabBarController = [[UITabBarController alloc]init];
    [self.tabBarController.tabBar setTranslucent:YES];
    [[UITabBar appearance] setBarTintColor:[UIColor primeSportColorTabBarController]];
    self.tabBarController.delegate = self;
    self.tabBarController.viewControllers = @[navControllerSearch,
                                              navControllerAccount];
    
    [self.view addSubview:self.tabBarController.view];
}

#pragma mark -
#pragma mark - DELEGADE METHODS (UITabBarControllerDelegate)
-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{

    if (tabBarController.tabBar.selectedItem == [tabBarController.tabBar.items objectAtIndex:1])
    {
        if ([PSSessionParameters information].isSessionActive == NO)
        {
            PSLoginViewController *vcLogin = [[PSLoginViewController alloc]init];
            vcLogin.title = @"Login";
            
            PSNavigationController *navControllerLogin = [[PSNavigationController alloc] initWithRootViewController:vcLogin];
            navControllerLogin.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentViewController:navControllerLogin
                               animated:YES
                             completion:nil];

            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    [[PSLocation GPS] updateLocation];
}

#pragma mark -
#pragma mark - PRESENT LOGIN MODAL
-(void)presentLoginToBuyTicket
{
    PSLoginViewController *vcLogin = [[PSLoginViewController alloc]initToBuyATicket];
    vcLogin.title = @"Login";
    
    PSNavigationController *navControllerLogin = [[PSNavigationController alloc] initWithRootViewController:vcLogin];
    navControllerLogin.modalPresentationStyle = UIModalPresentationPageSheet;
    
    [self presentViewController:navControllerLogin
                       animated:YES
                     completion:nil];
}

#pragma mark -
#pragma mark - START PURCHASE TICKET PROCESS
-(void)startPurchaseProcess
{
    PSShippingViewController *vcShipping = [[PSShippingViewController alloc]init];
    vcShipping.title = @"Summary";
    
    PSNavigationController *navControllerPurchase = [[PSNavigationController alloc] initWithRootViewController:vcShipping];
    navControllerPurchase.modalPresentationStyle = UIModalPresentationPageSheet;
    
    [self presentViewController:navControllerPurchase
                       animated:YES
                     completion:nil];
}

#pragma mark -
#pragma mark - PURCHASE DONE TICKET
-(void)openTicketPurchasedWithData:(NSDictionary *)data
{
    self.dictTicketInfo = data;
    [self performSelector:@selector(showTicketPurchased) withObject:nil afterDelay:1.0f];
    
}
-(void)showTicketPurchased
{
    PSFinalTicketViewController *vcFinalTicket = [[PSFinalTicketViewController alloc]initWithInfo:self.dictTicketInfo];
    vcFinalTicket.delegate = self;
    vcFinalTicket.title = @"Thank You!";
    
    PSNavigationController *navControllerFinal = [[PSNavigationController alloc] initWithRootViewController:vcFinalTicket];
    navControllerFinal.modalPresentationStyle = UIModalPresentationPageSheet;
    
    [self presentViewController:navControllerFinal
                       animated:YES
                     completion:nil];
    
    self.dictTicketInfo = [NSDictionary dictionary];
}

#pragma mark -
#pragma mark - DELEGADE METHODS (PSFinalTicketViewController)
-(void)doneButtonInFinalTapped
{
    if (self.tabBarController.tabBar.selectedItem == [self.tabBarController.tabBar.items objectAtIndex:2])
    {
        //Search Selected
        for (UINavigationController *navBAr in self.tabBarController.viewControllers)
        {
            if ([navBAr isKindOfClass:[PSNavigationController class]])
            {
                for (UIViewController *view in navBAr.viewControllers)
                {
                    if ([view isKindOfClass:[PSSearchViewController class]])
                    {
                        [navBAr popToRootViewControllerAnimated:YES];
                        [vcSearch publicCancelAction];
                    }
                }
            }
        }
    }
    else if (self.tabBarController.tabBar.selectedItem == [self.tabBarController.tabBar.items objectAtIndex:1])
    {
        //Site Links selected
        [self.navControllerSiteLinks popToRootViewControllerAnimated:YES];
    }
    else
    {
        //Home selected
        for (UINavigationController *navBAr in self.tabBarController.viewControllers)
        {
            if ([navBAr isKindOfClass:[PSNavigationController class]])
            {
                for (UIViewController *view in navBAr.viewControllers)
                {
                    if ([view isKindOfClass:[PSHomeViewController class]])
                    {
                        [navBAr popToRootViewControllerAnimated:YES];
                    }
                }
            }
        }
        
    }
}


#pragma mark -
#pragma mark - TOKEN EXPIRED & LOGOUT
-(void)goToMyAccountRoot
{
    for (UINavigationController *navBAr in self.tabBarController.viewControllers)
    {
        if ([navBAr isKindOfClass:[PSNavigationController class]])
        {
            for (UIViewController *view in navBAr.viewControllers)
            {
                if ([view isKindOfClass:[PSAccountViewController class]])
                {
                    [navBAr popToRootViewControllerAnimated:YES];
                }
            }
        }
    }
}
@end
