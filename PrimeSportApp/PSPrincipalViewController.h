//
//  PSPrincipalViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSPrincipalViewController : UIViewController

-(void)tabBarInteraction:(BOOL)interaction;
-(void)updateBadgeUpcomingEvents:(NSUInteger)totalUpcomingEvents;
-(void)tabBarSelectedItem:(NSUInteger)itemSelected;
-(void)presentLoginToBuyTicket;
-(void)startPurchaseProcess;
-(void)openTicketPurchasedWithData:(NSArray *)data;
-(void)goToMyAccountRoot;

@end
