//
//  PSTicketsPurchasedDetailViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 22/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTicketsPurchasedDetailViewController : UIViewController

- (instancetype)initWithTicketPurchasedSelected:(NSDictionary *)ticketSelected;

-(IBAction)showVenueMap;

@end
