//
//  PSLoginViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSLoginViewController.h"
#import "PSModelLogin.h"
#import "PSNewUserViewController.h"

static int const kTypePrimeSportAccount = 1;
static int const kTypeFacebookAccount   = 2;

@interface PSLoginViewController () <UITextFieldDelegate, UIAlertViewDelegate, modelLoginDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *ivLogo;
@property (weak, nonatomic) IBOutlet UIButton *loginFacebookButton;
@property (assign, nonatomic) BOOL isProcessToBuyTicket;
@property (strong, nonatomic) PSModelLogin *model;

@end

@implementation PSLoginViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelLogin alloc] init];
        self.model.delegate = self;
        
        self.isProcessToBuyTicket = NO;
    }
    return self;
}

- (instancetype)initToBuyATicket
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelLogin alloc] init];
        self.model.delegate = self;
        
        self.isProcessToBuyTicket = YES;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loginFacebookButton.layer.cornerRadius = 15;
    
    //Parallax effect
    self.ivLogo.parallaxIntensity = 15;
    
    //disable swipe gesture to back (Main Categories)
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.txtUsername.returnKeyType = UIReturnKeyNext;
    self.txtPassword.returnKeyType = UIReturnKeyDone;
    
    UIBarButtonItem *btnCloseModalLogin = [[UIBarButtonItem alloc] initWithTitle:@"Close"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(closeModalLogin)];
    btnCloseModalLogin.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = btnCloseModalLogin;
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnLoginFacebook) name:@"LoginFacebook" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (![[PSSessionParameters information].facebookToken isBlank]
        && ![[PSSessionParameters information].facebookEmail isBlank]
        && ![[PSSessionParameters information].facebookId isBlank])
    {
        [self sendLoginCredentialsWithType:kTypeFacebookAccount];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"Login dealloc");
    self.model.delegate = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoginFacebook" object:nil];
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)sendLoginCredentialsWithType:(int)loginType
{
    switch (loginType)
    {
        case kTypePrimeSportAccount:
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            [parameters setObject:[self.txtUsername.text lowercaseString] forKey:@"UserEmail"];
            [parameters setObject:self.txtPassword.text forKey:@"Password"];
            
            PSWebService *webService = [[PSWebService alloc] init];
            webService.wsMethod = kLogin;
            webService.wsParameters = parameters;
            
            [self.model postLoginCredentialsWithObject:webService];
        }
            break;
        case kTypeFacebookAccount:
        {
            if (![[PSSessionParameters information].facebookEmail isBlank] && ![[PSSessionParameters information].facebookId isBlank])
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
                self.loginFacebookButton.enabled = NO;
                
                NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                [parameters setObject:[PSSessionParameters information].facebookEmail forKey:@"UserEmail"];
                [parameters setObject:[PSSessionParameters information].facebookId forKey:@"facebookId"];
                
                PSWebService *webService = [[PSWebService alloc] init];
                webService.wsMethod = kLogin;
                webService.wsParameters = parameters;
                
                [self.model postLoginCredentialsWithObject:webService];
            }
            else
            {
                [[PSMessages getModel]showToastStatusAlertWithMessage:@"Waiting Facebook Response"
                                                  withBackgroundColor:[UIColor primeSportColorNotificationInfo]
                                                        withTextColor:[UIColor whiteColor]
                                                            withImage:@"iconStatusBar_Facebook"];
            }
        }
            break;
    }
}

-(void)sendEmailToRecoverForgotPassword:(NSString *)email
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:email forKey:@"UserEmail"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kForgotPassword;
    webService.wsParameters = parameters;
    
    [self.model postForgotPasswordWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelLogin)
-(void)modelResponseForLoginWithObject:(LGResultLogin *)result
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.loginFacebookButton.enabled = YES;
    
    if (result != nil)
    {
        NSUInteger error = (NSUInteger)result.responseCode;
        
        if (error == 0)
        {
            if (result.data.authenticationToken.length > 0 && result.data.email.length > 0)
            {
                [[PSSessionParameters information] setSessionParametersWithToken:result.data.authenticationToken
                                                                       withEmail:result.data.email];
                
                //Close
                [self closeModalLogin];
                
                //Validate if is normal login or login in purchased flow
                if (self.isProcessToBuyTicket == NO)
                {
                    PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
                    [(PSPrincipalViewController *)[delegate.window rootViewController] tabBarSelectedItem:3];
                }
                else
                {
                    [[PSMessages getModel] showAlertLoginMessageWithDelay:0.7];
                }
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:@"There is no data available"
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            if ([[result.message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:result.message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

-(void)modelResponseForForgotPasswordWithMessage:(NSString *)message
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (message != nil)
    {
        if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
        {
            [[PSMessages getModel]showAlertMessage:kSessionExpired
                                         withTitle:kInfoTitle];
        }
        else
        {
            [[PSMessages getModel] showAlertMessage:message
                                          withTitle:kInfoTitle];
        }
    }
}

#pragma mark -
#pragma mark - TEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.txtUsername)
    {
        //Selected
        self.txtUsername.backgroundColor = [UIColor colorWithHexadecimal:@"#B8D3FD" alpha:0.5];
    }
    else
    {
        //Selected
        self.txtPassword.backgroundColor = [UIColor colorWithHexadecimal:@"#B8D3FD" alpha:0.5];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.txtUsername)
    {
        //Deselect
        self.txtUsername.backgroundColor = [UIColor clearColor];
    }
    else
    {
        //Deselect
        self.txtPassword.backgroundColor = [UIColor clearColor];
    }
    
    //Remove all white spaces
    if (![textField.text isBlank])
    {
        textField.text =  [PSUtils removeAllWhiteSpacesInString:textField.text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == self.txtUsername)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else if (textField == self.txtPassword)
    {
        if ([self.txtUsername.text isBlank])
        {
            [self.txtUsername becomeFirstResponder];
        }
        else
        {
            [textField resignFirstResponder];
        }
    }
    return YES;
}

#pragma mark -
#pragma mark - HIDE KEYBOARD ON TAP
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(IBAction)btnLogin
{
    [self.view endEditing:YES];
    
    //Execute Login
    if([self.txtUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
    {
        [self.txtUsername shake];
        return;
    }
    if([PSUtils validateIsMail:self.txtUsername.text] == NO)
    {
        [[PSMessages getModel] showAlertMessage:kValidationEmailWrongFormat
                                      withTitle:kWarningTitle];
        return;
    }
    
    if([self.txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
    {
        [self.txtPassword shake];
        return;
    }
    
    //Cleaning Previous Facebook login
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [FBSession.activeSession closeAndClearTokenInformation];
        [[PSSessionParameters information] reset];
    }
    
    [self sendLoginCredentialsWithType:kTypePrimeSportAccount];
    self.txtPassword.text = [NSString string];
}

-(IBAction)btnLoginFacebook
{
    [self.view endEditing:YES];
    self.txtUsername.text = [NSString string];
    self.txtPassword.text = [NSString string];
    
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [self sendLoginCredentialsWithType:kTypeFacebookAccount];
    }
    else
    {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email"]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          
                                          PSAppDelegate *delegate = [UIApplication sharedApplication].delegate;
                                          [delegate sessionStateChanged:session state:state error:error];
                                      }];
    }
}

-(IBAction)btnForgotPassword
{
    UIAlertView *alertForgotPassword = [[UIAlertView alloc] initWithTitle:@"Enter your email address"
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Submit", nil];
    
    alertForgotPassword.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *txt = [alertForgotPassword textFieldAtIndex:0];
    txt.keyboardType = UIKeyboardTypeEmailAddress;
    [alertForgotPassword show];
}

-(IBAction)btnRegister
{
    PSNewUserViewController *vcNewUser = [[PSNewUserViewController alloc]initWithProcessPurchase:self.isProcessToBuyTicket];
    vcNewUser.title = @"Register";
    
    [self.navigationController pushViewController:vcNewUser animated:YES];
}

-(void)closeModalLogin
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - DELEGATE METHODS (UIAlertView)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *email = [alertView textFieldAtIndex:0].text;
        email = [PSUtils removeAllWhiteSpacesInString:email];
        
        if ([PSUtils validateIsMail:email] == NO)
        {
            [[PSMessages getModel]showAlertMessage:kValidationEmailWrongFormat
                                         withTitle:kWarningTitle];
        }
        else
        {
            [self sendEmailToRecoverForgotPassword:email];
        }
    }
}

@end
