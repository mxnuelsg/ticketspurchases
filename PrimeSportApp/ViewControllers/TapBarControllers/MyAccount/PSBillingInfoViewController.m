//
//  PSBillingInfoViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSBillingInfoViewController.h"
#import "PSGenericModals.h"
#import "PSModelAccount.h"

@interface PSBillingInfoViewController ()  <UITextFieldDelegate, UIActionSheetDelegate, modelAccountDelegate, modalBillingsDelegate, modalCountriesDelegate, modalStatesDelegate>

//Textfields
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtOptions;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAlias;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtFirstName;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtLastName;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtPhone;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtMobile;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress1;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress2;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCity;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtZipCode;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCountry;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtState;
@property (strong, nonatomic) UITextField *activeTextField;


//UIImageViews (icons)
@property (weak, nonatomic) IBOutlet UIImageView *icon1;
@property (weak, nonatomic) IBOutlet UIImageView *icon2;
@property (weak, nonatomic) IBOutlet UIImageView *icon3;
@property (weak, nonatomic) IBOutlet UIImageView *icon4;
@property (weak, nonatomic) IBOutlet UIImageView *icon5;
@property (weak, nonatomic) IBOutlet UIImageView *icon6;
@property (weak, nonatomic) IBOutlet UIImageView *icon7;
@property (weak, nonatomic) IBOutlet UIImageView *icon8;
@property (weak, nonatomic) IBOutlet UIImageView *icon9;
@property (weak, nonatomic) IBOutlet UIImageView *icon10;
@property (weak, nonatomic) IBOutlet UIImageView *icon11;
@property (weak, nonatomic) IBOutlet UIImageView *icon12;

//Controls
@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) UIBarButtonItem *btnEditOrSave;

//ViewControllers
@property (strong, nonatomic) PSModalBillingsViewController *vcModalBillings;
@property (strong, nonatomic) PSModalCountriesViewController *vcModalCountries;
@property (strong, nonatomic) PSModalStatesViewController *vcModalStates;


//Variables
@property (assign, nonatomic) BOOL isEditModeOn;
@property (assign, nonatomic) NSUInteger id_billing;
@property (assign, nonatomic) NSUInteger id_country;
@property (assign, nonatomic) NSUInteger id_state;

//Model
@property (strong, nonatomic) PSModelAccount *model;

@end

@implementation PSBillingInfoViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelAccount alloc] init];
        self.model.delegate = self;
        
        self.isEditModeOn = NO;
        
        self.id_billing = 0;
        self.id_country = 0;
        self.id_state   = 0;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self chargeTextfieldIcons];
    [self editModeOff];
    [self loadToolBar];
    
    self.btnEditOrSave = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                          style:UIBarButtonItemStylePlain
                                                         target:self
                                                         action:@selector(editModeOn)];
    
    self.btnEditOrSave.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = self.btnEditOrSave;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews
{
    [self.myScrollView autosize];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.translucent = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.translucent = YES;
}

- (void)dealloc
{
    NSLog(@"Billing info dealloc");
    
    self.model.delegate     = nil;
    self.myScrollView       = nil;
    self.btnEditOrSave      = nil;
    self.activeTextField    = nil;
    self.vcModalBillings    = nil;
    self.vcModalCountries   = nil;
    self.vcModalStates      = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnEditOrSave.enabled = NO;
    
    //Up scroll field
    NSDictionary *infoNotificacion = [notif userInfo];
    CGSize tamanioTeclado = [[infoNotificacion objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, tamanioTeclado.height, 0);
    [self.myScrollView setContentInset:edgeInsets];
    [self.myScrollView setScrollIndicatorInsets:edgeInsets];
    [self.myScrollView scrollRectToVisible:self.activeTextField.frame animated:YES];
}

- (void)keyboardDidHide:(NSNotification *) notif
{
    self.btnEditOrSave.enabled = YES;
    
    //Down scroll field
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    [self.myScrollView setContentInset:edgeInsets];
    [self.myScrollView setScrollIndicatorInsets:edgeInsets];
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark - TEXTFIELD ICONS
-(void)chargeTextfieldIcons
{
    self.txtAlias.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAlias"]];
    self.txtAlias.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtFirstName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtLastName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtPhone.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPhone"]];
    self.txtPhone.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtMobile.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconMobile"]];
    self.txtMobile.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress1.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress1.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress2.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress2.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCity.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCity"]];
    self.txtCity.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCountry.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCountry"]];
    self.txtCountry.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtState.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconState"]];
    self.txtState.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtZipCode.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconZipcode"]];
    self.txtZipCode.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
#pragma mark - EDIT MODE ON/OFF
-(void)editModeOn
{
    self.isEditModeOn = YES;
    
    [self.btnEditOrSave setTitle:@"Done"];
    [self.btnEditOrSave setAction:@selector(selectDoneOption)];
    
    //enable fields
    self.txtOptions.enabled     = YES;
    self.txtAlias.enabled       = YES;
    self.txtFirstName.enabled   = YES;
    self.txtLastName.enabled    = YES;
    self.txtPhone.enabled       = YES;
    self.txtMobile.enabled      = YES;
    self.txtAddress1.enabled    = YES;
    self.txtAddress2.enabled    = YES;
    self.txtZipCode.enabled     = YES;
    self.txtCountry.enabled     = YES;
    self.txtState.enabled       = YES;
    self.txtCity.enabled        = YES;
    
    //Color
    self.txtOptions.textColor   = [UIColor primeSportColorNavigationController];
    self.txtAlias.textColor     = [UIColor primeSportColorNavigationController];
    self.txtFirstName.textColor = [UIColor primeSportColorNavigationController];
    self.txtLastName.textColor  = [UIColor primeSportColorNavigationController];
    self.txtPhone.textColor     = [UIColor primeSportColorNavigationController];
    self.txtMobile.textColor    = [UIColor primeSportColorNavigationController];
    self.txtAddress1.textColor  = [UIColor primeSportColorNavigationController];
    self.txtAddress2.textColor  = [UIColor primeSportColorNavigationController];
    self.txtZipCode.textColor   = [UIColor primeSportColorNavigationController];
    self.txtCountry.textColor   = [UIColor primeSportColorNavigationController];
    self.txtState.textColor     = [UIColor primeSportColorNavigationController];
    self.txtCity.textColor      = [UIColor primeSportColorNavigationController];
    
    //hide icons
    self.icon1.hidden  = NO;
    self.icon2.hidden  = NO;
    self.icon3.hidden  = NO;
    self.icon4.hidden  = NO;
    self.icon5.hidden  = NO;
    self.icon6.hidden  = NO;
    self.icon7.hidden  = NO;
    self.icon8.hidden  = NO;
    self.icon9.hidden  = NO;
    self.icon10.hidden = NO;
    self.icon11.hidden = NO;
    self.icon12.hidden = NO;
}

-(void)editModeOff
{
    self.isEditModeOn = NO;
    
    [self.btnEditOrSave setTitle:@"Edit"];
    [self.btnEditOrSave setAction:@selector(editModeOn)];
    
    //disable fields
    self.txtOptions.enabled     = NO;
    self.txtAlias.enabled       = NO;
    self.txtFirstName.enabled   = NO;
    self.txtLastName.enabled    = NO;
    self.txtPhone.enabled       = NO;
    self.txtMobile.enabled      = NO;
    self.txtAddress1.enabled    = NO;
    self.txtAddress2.enabled    = NO;
    self.txtZipCode.enabled     = NO;
    self.txtCountry.enabled     = NO;
    self.txtState.enabled       = NO;
    self.txtCity.enabled        = NO;
    
    //Color
    self.txtOptions.textColor   = [UIColor grayColor];
    self.txtAlias.textColor     = [UIColor grayColor];
    self.txtFirstName.textColor = [UIColor grayColor];
    self.txtLastName.textColor  = [UIColor grayColor];
    self.txtPhone.textColor     = [UIColor grayColor];
    self.txtMobile.textColor    = [UIColor grayColor];
    self.txtAddress1.textColor  = [UIColor grayColor];
    self.txtAddress2.textColor  = [UIColor grayColor];
    self.txtZipCode.textColor   = [UIColor grayColor];
    self.txtCountry.textColor   = [UIColor grayColor];
    self.txtState.textColor     = [UIColor grayColor];
    self.txtCity.textColor      = [UIColor grayColor];

   
    //hide icons
    self.icon1.hidden  = YES;
    self.icon2.hidden  = YES;
    self.icon3.hidden  = YES;
    self.icon4.hidden  = YES;
    self.icon5.hidden  = YES;
    self.icon6.hidden  = YES;
    self.icon7.hidden  = YES;
    self.icon8.hidden  = YES;
    self.icon9.hidden  = YES;
    self.icon10.hidden = YES;
    self.icon11.hidden = YES;
    self.icon12.hidden = YES;
}

#pragma mark -
#pragma mark - TOOLBAR CONFIGURATION
-(void)loadToolBar
{
    //Toolbar No.1 for fields
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone  target:self action:@selector(hideKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    UISegmentedControl *segmentedNavigation = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"❮ Previous Field", @"Next Field ❯", nil]];
    segmentedNavigation.momentary = YES;
    segmentedNavigation.tintColor = [UIColor groupTableViewBackgroundColor];
    [segmentedNavigation addTarget:self action:@selector(segmentedControlHandler:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *btnSegment = [[UIBarButtonItem alloc]initWithCustomView:segmentedNavigation];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                             btnSegment,
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil  action:nil],
                             btnDone,
                             nil];
    [toolBarKeyboard sizeToFit];
    
    
    
    //Toolbar No.2 for pickers
    UIToolbar *toolBarKeyboardClean = [[UIToolbar alloc]init];
    toolBarKeyboardClean.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardClean.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnDoneClean = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneClean.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardClean.items = [NSArray arrayWithObjects:
                                    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                    btnDoneClean,
                                    nil];
    [toolBarKeyboardClean sizeToFit];
    
    
    //Toolbar No.3 for Refresh
    UIToolbar *toolBarKeyboardWithRefresh = [[UIToolbar alloc]init];
    toolBarKeyboardWithRefresh.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardWithRefresh.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Refresh" style:UIBarButtonItemStyleDone target:self action:@selector(refreshDataInputView)];
    btnRefresh.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *btnDoneRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneRefresh.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardWithRefresh.items = [NSArray arrayWithObjects:
                                    btnRefresh,
                                  [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                  btnDoneRefresh,
                                  nil];
    [toolBarKeyboardWithRefresh sizeToFit];
    
    self.txtAlias.inputAccessoryView        = toolBarKeyboard;
    self.txtFirstName.inputAccessoryView    = toolBarKeyboard;
    self.txtLastName.inputAccessoryView     = toolBarKeyboard;
    self.txtPhone.inputAccessoryView        = toolBarKeyboard;
    self.txtMobile.inputAccessoryView       = toolBarKeyboard;
    self.txtAddress1.inputAccessoryView     = toolBarKeyboard;
    self.txtAddress2.inputAccessoryView     = toolBarKeyboard;
    self.txtCity.inputAccessoryView         = toolBarKeyboardClean;
    self.txtZipCode.inputAccessoryView      = toolBarKeyboardClean;
    
    self.txtOptions.inputAccessoryView      = toolBarKeyboardWithRefresh;
    self.txtCountry.inputAccessoryView      = toolBarKeyboardWithRefresh;
    self.txtState.inputAccessoryView        = toolBarKeyboardWithRefresh;
}

- (void)segmentedControlHandler:(id)sender
{
    UISegmentedControl *mySegmented = (UISegmentedControl *)sender;
    
    switch ([mySegmented selectedSegmentIndex])
    {
        case 0:
            if (self.txtFirstName.isEditing == YES)
            {
                [self.txtAlias becomeFirstResponder];
            }
            else if (self.txtLastName.isEditing == YES)
            {
                [self.txtFirstName becomeFirstResponder];
            }
            else if (self.txtPhone.isEditing == YES)
            {
                [self.txtLastName becomeFirstResponder];
            }
            else if (self.txtMobile.isEditing == YES)
            {
                [self.txtPhone becomeFirstResponder];
            }
            else if (self.txtAddress1.isEditing == YES)
            {
                [self.txtMobile becomeFirstResponder];
            }
            else if (self.txtAddress2.isEditing == YES)
            {
                [self.txtAddress1 becomeFirstResponder];
            }
            break;
        case 1:
            if (self.txtAlias.isEditing == YES)
            {
                [self.txtFirstName becomeFirstResponder];
            }
            else if (self.txtFirstName.isEditing == YES)
            {
                [self.txtLastName becomeFirstResponder];
            }
            else if (self.txtLastName.isEditing == YES)
            {
                [self.txtPhone becomeFirstResponder];
            }
            else if (self.txtPhone.isEditing == YES)
            {
                [self.txtMobile becomeFirstResponder];
            }
            else if (self.txtMobile.isEditing == YES)
            {
                [self.txtAddress1 becomeFirstResponder];
            }
            else if (self.txtAddress1.isEditing == YES)
            {
                [self.txtAddress2 becomeFirstResponder];
            }
            break;
        default:
            break;
    }
}

-(void)hideKeyboard
{
    [self.txtOptions resignFirstResponder];
    [self.txtAlias resignFirstResponder];
    [self.txtFirstName resignFirstResponder];
    [self.txtLastName resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    [self.txtMobile resignFirstResponder];
    [self.txtAddress1 resignFirstResponder];
    [self.txtAddress2 resignFirstResponder];
    [self.txtCountry resignFirstResponder];
    [self.txtZipCode resignFirstResponder];
    [self.txtCity resignFirstResponder];
    [self.txtState resignFirstResponder];
}

-(void)refreshDataInputView
{
    if ([self.txtOptions isEditing])
    {
        [self.vcModalBillings getAllBillings];
    }
    else if ([self.txtCountry isEditing])
    {
        [self.vcModalCountries getAllCountries];
    }
    else if ([self.txtState isEditing])
    {
        [self.vcModalStates getAllStates];
    }
}

#pragma mark -
#pragma mark - ACTION BUTTONS
-(void)selectDoneOption
{
    UIActionSheet *actionSheetFilter = [[UIActionSheet alloc] initWithTitle:@"What would you like to do with this information?"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                     destructiveButtonTitle:@"Delete"
                                                          otherButtonTitles: @"Save Changes", nil];
    
    [actionSheetFilter showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getOneSpecificBilling
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithUnsignedInteger:self.id_billing] forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSpecificBillingInfo;
    webService.wsParameters = parameters;
    
    [self.model getSpecificBillingWithObject:webService];
}

-(void)deleteBillingInformationWithId:(NSUInteger)idBilling
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithUnsignedInteger:idBilling] forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kDeleteBilling;
    webService.wsParameters = parameters;
    
    [self.model getDeleteBillingInfoWithObject:webService];
}

-(void)saveUpdateBillingInformation
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];

    NSMutableDictionary *dictBillingInfo = [[NSMutableDictionary alloc] init];
    [dictBillingInfo setObject:[NSNumber numberWithUnsignedInteger:self.id_billing] forKey:@"Id"];
    [dictBillingInfo setObject:self.txtAlias.text forKey:@"NickName"];
    [dictBillingInfo setObject:self.txtAddress1.text forKey:@"Address"];
    [dictBillingInfo setObject:self.txtAddress2.text forKey:@"Address2"];
    [dictBillingInfo setObject:self.txtFirstName.text forKey:@"CardHolderFirstName"];
    [dictBillingInfo setObject:self.txtLastName.text forKey:@"CardHolderLastName"];
    [dictBillingInfo setObject:self.txtCity.text forKey:@"City"];
    [dictBillingInfo setObject:self.txtPhone.text forKey:@"Phone"];
    [dictBillingInfo setObject:self.txtMobile.text forKey:@"Mobile"];
    [dictBillingInfo setObject:self.txtZipCode.text forKey:@"ZipCode"];
    [dictBillingInfo setObject:[NSNumber numberWithUnsignedInteger:self.id_country] forKey:@"Country"];
    [dictBillingInfo setObject:[NSNumber numberWithUnsignedInteger:self.id_state] forKey:@"State"];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:dictBillingInfo forKey:@"billingInformation"];
    [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"email"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSaveOrUpdateBilling;
    webService.wsParameters = parameters;
    
    [self.model postSaveBillingInfoWithObject:webService];
}

-(void)getZipCodeInfo
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.txtZipCode.text forKey:@"zipcode"];
    [parameters setObject:[NSNumber numberWithUnsignedInteger:self.id_country] forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetZipCode;
    webService.wsParameters = parameters;
    
    [self.model getZipCodeWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForSpecificBilling:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode  = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message        = [result objectForKey:@"Message"];
        NSDictionary *dictInfo   = [result objectForKey:@"Data"];
        
        if (responseCode == 0)
        {
            if (dictInfo.allKeys.count > 0)
            {
                self.txtAlias.text      = [dictInfo valueForKey:@"Nickname"];
                self.txtFirstName.text  = [dictInfo valueForKey:@"CardHolderFirstName"];
                self.txtLastName.text   = [dictInfo valueForKey:@"CardHolderLastName"];
                self.txtPhone.text      = [dictInfo valueForKey:@"Phone"];
                self.txtMobile.text     = [dictInfo valueForKey:@"Mobile"];
                self.txtAddress1.text   = [dictInfo valueForKey:@"Address"];
                self.txtAddress2.text   = [dictInfo valueForKey:@"Address2"];
                self.txtCity.text       = [dictInfo valueForKey:@"City"];
                self.txtZipCode.text    = [dictInfo valueForKey:@"ZipCode"];
                self.txtCountry.text    = [[dictInfo valueForKey:@"CountryName"] capitalizedString];
                self.txtState.text      = [[dictInfo valueForKey:@"StateName"] capitalizedString];
                
                self.id_country = [[dictInfo valueForKey:@"Country"]integerValue];
                self.id_state   = [[dictInfo valueForKey:@"State"]integerValue];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:@"No billing info found"
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

-(void)modelResponseForDeleteBillingInfo:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode  = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message        = [result objectForKey:@"Message"];
        
        if (responseCode == 0)
        {
            [self editModeOff];
            
            // - -  reset data - -
            self.txtOptions.text   = [NSString string];
            self.txtAlias.text     = [NSString string];
            self.txtFirstName.text = [NSString string];
            self.txtLastName.text  = [NSString string];
            self.txtPhone.text     = [NSString string];
            self.txtMobile.text    = [NSString string];
            self.txtAddress1.text  = [NSString string];
            self.txtAddress2.text  = [NSString string];
            self.txtCity.text      = [NSString string];
            self.txtZipCode.text   = [NSString string];
            self.txtCountry.text   = [NSString string];
            self.txtState.text     = [NSString string];
            self.id_country        = 0;
            self.id_state          = 0;
            self.id_billing        = 0;
            // - - - - - - - - - - - -
            
            [[PSMessages getModel]showAlertForBillingDeleted];
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

-(void)modelResponseForSaveBillingInfo:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message       = [result objectForKey:@"Message"];
        
        if (responseCode == 0)
        {
            [self editModeOff];
            
            // - -  reset data - -
            self.txtOptions.text   = [NSString string];
            self.txtAlias.text     = [NSString string];
            self.txtFirstName.text = [NSString string];
            self.txtLastName.text  = [NSString string];
            self.txtPhone.text     = [NSString string];
            self.txtMobile.text    = [NSString string];
            self.txtAddress1.text  = [NSString string];
            self.txtAddress2.text  = [NSString string];
            self.txtCity.text      = [NSString string];
            self.txtZipCode.text   = [NSString string];
            self.txtCountry.text   = [NSString string];
            self.txtState.text     = [NSString string];
            self.id_country        = 0;
            self.id_state          = 0;
            self.id_billing        = 0;
            // - - - - - - - - - - - -
            
            [[PSMessages getModel]showAlertForBillingInfoSaved];
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

-(void)modelResponseForZipCode:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        self.txtState.text  = [[result valueForKey:@"StateName"] capitalizedString];
        self.txtCity.text   = [[result valueForKey:@"City"] capitalizedString];
        self.id_state       = [[result valueForKey:@"StateId"] integerValue];
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS (UIActionSheetDelegate)
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            if (self.id_billing > 0)
            {
                 [self deleteBillingInformationWithId:self.id_billing];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:kValidationBillingInfo
                                             withTitle:kWarningTitle];
            }
        }
            break;
        case 1:
        {
            if ([self.txtAlias.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationAlias
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtFirstName.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationFirstname
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtLastName.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationLastname
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtPhone.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationPhone
                                             withTitle:kWarningTitle];
            }
            else if (self.txtPhone.text.length < 10)
            {
                [[PSMessages getModel]showAlertMessage:kValidationPhoneLength
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtAddress1.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationAddress
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtCity.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationCity
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtZipCode.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtCountry.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationCountry
                                             withTitle:kWarningTitle];
            }
            else if ([self.txtState.text isBlank])
            {
                [[PSMessages getModel]showAlertMessage:kValidationState
                                             withTitle:kWarningTitle];
            }
            else
            {
                [self saveUpdateBillingInformation];
            }
            
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS MODALS
/* (UIModalBillingsViewController) */
-(void)selectedBillingInformation:(NSDictionary *)billingInformation
{
    [self.txtOptions resignFirstResponder];
    
    self.txtOptions.text = [[billingInformation valueForKey:@"Nickname"]capitalizedString];
    self.txtOptions.adjustsFontSizeToFitWidth = YES;
    self.id_billing = [[billingInformation valueForKey:@"Id"] integerValue];
    
    if (self.id_billing > 0)
    {
        [self getOneSpecificBilling];
    }
}

/* (UIModalCountriesViewController) */
-(void)selectedCountryInformation:(NSDictionary *)countryInformation
{
    [self.txtCountry resignFirstResponder];
    
    self.txtCountry.text = [[countryInformation valueForKey:@"CountryName"] capitalizedString];
    self.id_country = [[countryInformation valueForKey:@"CountryId"] integerValue];
    
    //Clean Fields
    self.txtZipCode.text = [NSString string];
    self.txtCity.text    = [NSString string];
    
    self.txtState.text = [NSString string];
    self.id_state = 0;
}

/* (UIModalStatesViewController) */
-(void)selectedStateInformation:(NSDictionary *)stateInformation
{
    [self.txtState resignFirstResponder];
    
    self.txtState.text = [[stateInformation valueForKey:@"StateName"] capitalizedString];
    self.id_state = [[stateInformation valueForKey:@"StateId"] integerValue];
    
    self.txtCity.text = [NSString string];
}

#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setActiveTextField:textField];
    
    if (textField == self.txtOptions)
    {
        self.vcModalBillings = [[PSModalBillingsViewController alloc]init];
        self.vcModalBillings.delegate = self;
        self.vcModalBillings.view.frame = CGRectMake(0, 0, 320, 190);
        self.txtOptions.inputView = self.vcModalBillings.view;
    }
    else if (textField == self.txtCountry)
    {
        self.vcModalCountries = [[PSModalCountriesViewController alloc]init];
        self.vcModalCountries.delegate = self;
        self.vcModalCountries.view.frame = CGRectMake(0, 0, 320, 190);
        self.txtCountry.inputView = self.vcModalCountries.view;
    }
    else if (textField == self.txtState)
    {
        if ([self.txtCountry.text isBlank])
        {
            [textField resignFirstResponder];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            if (self.isEditModeOn == YES)
            {
                self.vcModalStates = [[PSModalStatesViewController alloc]initWithCountryId:self.id_country];
                self.vcModalStates.delegate = self;
                self.vcModalStates.view.frame = CGRectMake(0, 0, 320, 190);
                self.txtState.inputView = self.vcModalStates.view;
            }
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setActiveTextField:nil];

    if (textField == self.txtZipCode)
    {
        if ([textField.text isBlank])
        {
            [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                         withTitle:kWarningTitle];
        }
        else if (textField.text.length < 5)
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationZipCodeLength
                                         withTitle:kWarningTitle];
        }
        else if ([self.txtCountry.text isBlank])
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            [self getZipCodeInfo];
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtZipCode)
    {
        if ([string isEqualToString:@" "] )
        {
            return NO;
        }
        
        if ([textField.text length] >= kZIP_CODE_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtPhone || textField == self.txtMobile)
    {
        if ([textField.text length] >= kPHONE_NUMBER_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtCountry || textField == self.txtState || textField == self.txtOptions)
    {
        return NO;
    }
    
    return YES;
}

@end
