//
//  PSContactInfoViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSContactInfoViewController.h"
#import "PSModelAccount.h"
#import "PSGenericModals.h"

@interface PSContactInfoViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, modelAccountDelegate, modalCountriesDelegate, modalStatesDelegate, modalCitiesDelegate>

//Textfields
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtFirstName;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtLastName;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtGender;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtBirthdate;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtEmail;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtEmailConfirm;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress1;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress2;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtPhone;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtZipCode;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCountry;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtState;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCity;
@property (strong, nonatomic) UITextField *activeTextField;

//UIImageViews (icons)
@property (weak, nonatomic) IBOutlet UIImageView *icon1;
@property (weak, nonatomic) IBOutlet UIImageView *icon2;
@property (weak, nonatomic) IBOutlet UIImageView *icon3;
@property (weak, nonatomic) IBOutlet UIImageView *icon4;
@property (weak, nonatomic) IBOutlet UIImageView *icon5;
@property (weak, nonatomic) IBOutlet UIImageView *icon6;
@property (weak, nonatomic) IBOutlet UIImageView *icon7;
@property (weak, nonatomic) IBOutlet UIImageView *icon8;
@property (weak, nonatomic) IBOutlet UIImageView *icon9;
@property (weak, nonatomic) IBOutlet UIImageView *icon10;
@property (weak, nonatomic) IBOutlet UIImageView *icon11;
@property (weak, nonatomic) IBOutlet UIImageView *icon12;
@property (weak, nonatomic) IBOutlet UIImageView *icon13;

//Controls
@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;
@property (strong, nonatomic) UIBarButtonItem *btnEditOrSave;

//ViewControllers
@property (strong, nonatomic) PSModalCountriesViewController *vcModalCountries;
@property (strong, nonatomic) PSModalStatesViewController *vcModalStates;
@property (strong, nonatomic) PSModalCitiesViewController *vcModalCities;


//Variables
@property (strong, nonatomic) NSArray *arrayGender;
@property (strong, nonatomic) NSArray *arrayIcons;

@property (assign, nonatomic) NSUInteger id_country;
@property (assign, nonatomic) NSUInteger id_state;
@property (assign, nonatomic) NSUInteger id_city;

@property (assign, nonatomic) BOOL isSubscriber;
@property (assign, nonatomic) BOOL isEditModeOn;

//Model
@property (strong, nonatomic) PSModelAccount *model;

@end

@implementation PSContactInfoViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelAccount alloc] init];
        self.model.delegate = self;
        
        self.arrayGender    = [[NSArray alloc] initWithObjects:@"Female", @"Male", nil];
        self.arrayIcons      = [[NSArray alloc] initWithObjects:@"iconFemale", @"iconMale", nil];
        self.isSubscriber   = NO;
        self.isEditModeOn   = NO;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self chargeTextfieldIcons];
    [self editModeOff];
    [self loadPickerView];
    [self getAccountInformation];
    
    self.btnEditOrSave = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                          style:UIBarButtonItemStylePlain
                                                         target:self
                                                         action:@selector(editModeOn)];
    
    self.btnEditOrSave.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = self.btnEditOrSave;
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews
{
    [self.myScrollView autosize];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.translucent = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.translucent = YES;
}

- (void)dealloc
{
    NSLog(@"Contact info dealloc");
    
    self.model.delegate     = nil;
    self.myScrollView       = nil;
    self.btnEditOrSave      = nil;
    self.arrayGender        = nil;
    self.arrayIcons         = nil;
    self.activeTextField    = nil;
    self.vcModalCountries   = nil;
    self.vcModalStates      = nil;
    self.vcModalCities      = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnEditOrSave.enabled  = NO;
    
    //Up scroll field
    NSDictionary *infoNotificacion = [notif userInfo];
    CGSize tamanioTeclado = [[infoNotificacion objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, tamanioTeclado.height, 0);
    [self.myScrollView setContentInset:edgeInsets];
    [self.myScrollView setScrollIndicatorInsets:edgeInsets];
    [self.myScrollView scrollRectToVisible:self.activeTextField.frame animated:YES];

}

- (void)keyboardDidHide: (NSNotification *) notif
{
    self.btnEditOrSave.enabled = YES;
    
    //Down scroll field
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    [self.myScrollView setContentInset:edgeInsets];
    [self.myScrollView setScrollIndicatorInsets:edgeInsets];
    [UIView commitAnimations];

}

#pragma mark -
#pragma mark - TEXTFIELD ICONS
-(void)chargeTextfieldIcons
{
    self.txtFirstName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtLastName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtGender.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconGender"]];
    self.txtGender.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtBirthdate.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconBirthday"]];
    self.txtBirthdate.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtEmail.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconMail"]];
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtEmailConfirm.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconMail"]];
    self.txtEmailConfirm.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtPhone.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPhone"]];
    self.txtPhone.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress1.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress1.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress2.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress2.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCity.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCity"]];
    self.txtCity.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCountry.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCountry"]];
    self.txtCountry.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtState.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconState"]];
    self.txtState.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtZipCode.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconZipcode"]];
    self.txtZipCode.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getAccountInformation
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"Email"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kAccountInfo;
    webService.wsParameters = parameters;
    
    [self.model getContactInfoWithObject:webService];
}

-(void)getZipCodeInfo
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.txtZipCode.text forKey:@"zipcode"];
    [parameters setObject:[NSNumber numberWithUnsignedInteger:self.id_country] forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetZipCode;
    webService.wsParameters = parameters;
    
    [self.model getZipCodeWithObject:webService];
}

-(void)saveAccountInformationChanged
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:@"Saving" andMessage:nil];
    
    //Gender
    NSUInteger genderSelected;
    
    if ([[self.txtGender.text lowercaseString] isEqualToString:@"male"])
    {
        genderSelected = 1;
    }
    else if ([[self.txtGender.text lowercaseString] isEqualToString:@"female"])
    {
        genderSelected = 2;
    }
    else
    {
        genderSelected = 0;
    }
    
    //Newsletter
    NSString *trueFalse;
    
    if (self.isSubscriber == YES)
    {
        trueFalse = @"true";
    }
    else
    {
         trueFalse = @"false";
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.txtFirstName.text forKey:@"FirstName"];
    [parameters setObject:self.txtLastName.text forKey:@"LastName"];
    [parameters setObject:[NSNumber numberWithInteger:(NSInteger)genderSelected] forKey:@"GenderId"];
    [parameters setObject:self.txtBirthdate.text forKey:@"BirthDate"];
    [parameters setObject:[self.txtEmail.text removeWhiteSpacesFromString] forKey:@"Email"];
    [parameters setObject:trueFalse forKey:@"SubscribeNewsletter"];
    [parameters setObject:self.txtAddress1.text forKey:@"Address"];
    [parameters setObject:self.txtAddress2.text forKey:@"Address2"];
    [parameters setObject:self.txtPhone.text forKey:@"Phone"];
    [parameters setObject:self.txtZipCode.text forKey:@"ZipCode"];
    [parameters setObject:[NSNumber numberWithInteger:(NSInteger)self.id_country] forKey:@"CountryId"];
    [parameters setObject:[NSNumber numberWithInteger:(NSInteger)self.id_state] forKey:@"StateId"];
    [parameters setObject:[NSNumber numberWithInteger:(NSInteger)self.id_city] forKey:@"CityId"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSaveAccount  ;
    webService.wsParameters = parameters;
    
    [self.model postSaveContactInfoWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForContactInfo:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        [self autoCompleteInfoWithDictionary:result];
    }
}

-(void)modelResponseForZipCode:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        self.txtState.text  = [[result valueForKey:@"StateName"] capitalizedString];
        self.txtCity.text   = [[result valueForKey:@"City"] capitalizedString];
        
        self.id_state = [[result valueForKey:@"StateId"] integerValue];
        self.id_city  = [[result valueForKey:@"CityId"] integerValue];
    }
}

-(void)modelResponseForSaveContactInfo:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message       = [result objectForKey:@"Message"];
        
        if (responseCode == 0)
        {
            [[PSMessages getModel] showAlertForContactInfoChanged];
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS MODALS
/* (UIModalCountriesViewController) */
-(void)selectedCountryInformation:(NSDictionary *)countryInformation
{
    [self.txtCountry resignFirstResponder];
    
    self.txtCountry.text = [[countryInformation valueForKey:@"CountryName"] capitalizedString];
    self.id_country = [[countryInformation valueForKey:@"CountryId"] integerValue];
    
    //Clean Fields
    self.txtZipCode.text = [NSString string];
    self.txtCity.text    = [NSString string];
    
    self.txtState.text = [NSString string];
    self.id_state = 0;
    
    self.txtCity.text = [NSString string];
    self.id_city = 0;
}

/* (UIModalStatesViewController) */
-(void)selectedStateInformation:(NSDictionary *)stateInformation
{
    [self.txtState resignFirstResponder];
    
    self.txtState.text = [[stateInformation valueForKey:@"StateName"] capitalizedString];
    self.id_state = [[stateInformation valueForKey:@"StateId"] integerValue];
    
    self.txtCity.text = [NSString string];
    self.id_city = 0;
}

/* (UIModalCitiesViewController) */
-(void)selectedCityInformation:(NSDictionary *)cityInformation
{
    [self.txtCity resignFirstResponder];
    
    self.txtCity.text = [[cityInformation valueForKey:@"Name"] capitalizedString];
    self.id_city = [[cityInformation valueForKey:@"CityId"] integerValue];
}

#pragma mark -
#pragma mark - FILL INFORMATION
-(void)autoCompleteInfoWithDictionary:(NSDictionary *)dictInformation
{
    NSUInteger responseCode  = [[dictInformation objectForKey:@"ResponseCode"] integerValue];
    NSString *message        = [dictInformation objectForKey:@"Message"];
    NSDictionary *dictInfo   = [dictInformation objectForKey:@"Data"];
    
    if (responseCode == 0)
    {
        if (dictInfo.allKeys.count > 0)
        {
            
            self.txtFirstName.text = [dictInfo valueForKey:@"FirstName"];
            self.txtLastName.text = [dictInfo valueForKey:@"LastName"];
            
            NSUInteger genderType = [[dictInfo valueForKey:@"GenderId"] integerValue];
            switch (genderType)
            {
                case 1:
                    self.txtGender.text = @"Male";
                    break;
                case 2:
                    self.txtGender.text = @"Female";
                    break;
                default:
                    self.txtGender.text = [NSString string];
                    break;
            }
            
            NSString *virginDate = [dictInfo valueForKey:@"BirthDate"];
            NSArray *arrayBirthDate = [virginDate componentsSeparatedByString:@" "];
            self.txtBirthdate.text = [arrayBirthDate objectAtIndex:0];
            
            self.txtEmail.text          = [dictInfo valueForKey:@"Email"];
            self.txtEmailConfirm.text   = [dictInfo valueForKey:@"Email"];
            self.txtAddress1.text       = [dictInfo valueForKey:@"Address"];
            self.txtAddress2.text       = [dictInfo valueForKey:@"Address2"];
            self.txtPhone.text          = [dictInfo valueForKey:@"Phone"];
            self.txtZipCode.text        = [dictInfo valueForKey:@"ZipCode"];
            self.txtCountry.text        = [[dictInfo valueForKey:@"CountryName"] capitalizedString];
            self.txtState.text          = [[dictInfo valueForKey:@"StateName"] capitalizedString];
            self.txtCity.text           = [[dictInfo valueForKey:@"City"] capitalizedString];
            
            self.id_country = [[dictInfo valueForKey:@"CountryId"] integerValue];
            self.id_state   = [[dictInfo valueForKey:@"StateId"] integerValue];
            self.id_city    = [[dictInfo valueForKey:@"CityId"] integerValue];
            
            BOOL isNewsletterActive = [[dictInfo valueForKey:@"SubscribeNewsletter"] boolValue];
            
            if (isNewsletterActive == YES)
            {
                self.mySwitch.on = YES;
                [self subscriptionNewsletter:self.mySwitch];
            }
        }
        else
        {
            [[PSMessages getModel]showAlertMessage:@"No contact info found"
                                         withTitle:kWarningTitle];
        }
    }
    else
    {
        if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
        {
            [[PSMessages getModel]showAlertMessage:kSessionExpired
                                         withTitle:kInfoTitle];
        }
        else
        {
            [[PSMessages getModel]showAlertMessage:message
                                         withTitle:kWarningTitle];
        }
    }
}

#pragma mark -
#pragma mark - SWITCH ACTION
- (IBAction)subscriptionNewsletter:(id)sender
{
    self.mySwitch = sender;
    
    if(self.mySwitch.isOn == YES)
    {
        self.isSubscriber = YES;
    }
    else
    {
        self.isSubscriber = NO;
    }
}

#pragma mark -
#pragma mark - EDIT MODE ON/OFF
-(void)editModeOn
{
    self.isEditModeOn = YES;
    
    [self.btnEditOrSave setTitle:@"Save"];
    [self.btnEditOrSave setAction:@selector(saveChanges)];
    
    //enable fields
    self.txtFirstName.enabled   = YES;
    self.txtLastName.enabled    = YES;
    self.txtGender.enabled      = YES;
    self.txtBirthdate.enabled   = YES;
    self.txtEmail.enabled       = YES;
    self.txtEmailConfirm.enabled = YES;
    self.txtAddress1.enabled    = YES;
    self.txtAddress2.enabled    = YES;
    self.txtPhone.enabled       = YES;
    self.txtZipCode.enabled     = YES;
    self.txtCountry.enabled     = YES;
    self.txtState.enabled       = YES;
    self.txtCity.enabled        = YES;
    
    //Color
    self.txtFirstName.textColor = [UIColor primeSportColorNavigationController];
    self.txtLastName.textColor  = [UIColor primeSportColorNavigationController];
    self.txtGender.textColor    = [UIColor primeSportColorNavigationController];
    self.txtBirthdate.textColor = [UIColor primeSportColorNavigationController];
    self.txtEmail.textColor     = [UIColor primeSportColorNavigationController];
    self.txtEmailConfirm.textColor = [UIColor primeSportColorNavigationController];
    self.txtAddress1.textColor  = [UIColor primeSportColorNavigationController];
    self.txtAddress2.textColor  = [UIColor primeSportColorNavigationController];
    self.txtPhone.textColor     = [UIColor primeSportColorNavigationController];
    self.txtZipCode.textColor   = [UIColor primeSportColorNavigationController];
    self.txtCountry.textColor   = [UIColor primeSportColorNavigationController];
    self.txtState.textColor     = [UIColor primeSportColorNavigationController];
    self.txtCity.textColor      = [UIColor primeSportColorNavigationController];
    
    //hide icons
    self.icon1.hidden  = NO;
    self.icon2.hidden  = NO;
    self.icon3.hidden  = NO;
    self.icon4.hidden  = NO;
    self.icon5.hidden  = NO;
    self.icon6.hidden  = NO;
    self.icon7.hidden  = NO;
    self.icon8.hidden  = NO;
    self.icon9.hidden  = NO;
    self.icon10.hidden = NO;
    self.icon11.hidden = NO;
    self.icon12.hidden = NO;
    self.icon13.hidden = NO;
    
    //Switch
    self.mySwitch.enabled = YES;
}

-(void)editModeOff
{
    self.isEditModeOn = NO;
    
    [self.btnEditOrSave setTitle:@"Edit"];
    [self.btnEditOrSave setAction:@selector(editModeOn)];
    
    //disable fields
    self.txtFirstName.enabled   = NO;
    self.txtLastName.enabled    = NO;
    self.txtGender.enabled      = NO;
    self.txtBirthdate.enabled   = NO;
    self.txtEmail.enabled       = NO;
    self.txtEmailConfirm.enabled = NO;
    self.txtAddress1.enabled    = NO;
    self.txtAddress2.enabled    = NO;
    self.txtPhone.enabled       = NO;
    self.txtZipCode.enabled     = NO;
    self.txtCountry.enabled     = NO;
    self.txtState.enabled       = NO;
    self.txtCity.enabled        = NO;
    
    //Color
    self.txtFirstName.textColor = [UIColor grayColor];
    self.txtLastName.textColor  = [UIColor grayColor];
    self.txtGender.textColor    = [UIColor grayColor];
    self.txtBirthdate.textColor = [UIColor grayColor];
    self.txtEmail.textColor     = [UIColor grayColor];
    self.txtEmailConfirm.textColor = [UIColor grayColor];
    self.txtAddress1.textColor  = [UIColor grayColor];
    self.txtAddress2.textColor  = [UIColor grayColor];
    self.txtPhone.textColor     = [UIColor grayColor];
    self.txtZipCode.textColor   = [UIColor grayColor];
    self.txtCountry.textColor   = [UIColor grayColor];
    self.txtState.textColor     = [UIColor grayColor];
    self.txtCity.textColor      = [UIColor grayColor];
    
    //hide icons
    self.icon1.hidden  = YES;
    self.icon2.hidden  = YES;
    self.icon3.hidden  = YES;
    self.icon4.hidden  = YES;
    self.icon5.hidden  = YES;
    self.icon6.hidden  = YES;
    self.icon7.hidden  = YES;
    self.icon8.hidden  = YES;
    self.icon9.hidden  = YES;
    self.icon10.hidden = YES;
    self.icon11.hidden = YES;
    self.icon12.hidden = YES;
    self.icon13.hidden = YES;
    
    //Switch
    self.mySwitch.enabled = NO;
}

#pragma mark -
#pragma mark - PICKER VIEW & TOOLBAR CONFIGURATION
-(void)loadPickerView
{
    //Toolbar No.1 for fields
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone  target:self action:@selector(hideKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    UISegmentedControl *segmentedNavigation = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"❮ Previous Field", @"Next Field ❯", nil]];
    segmentedNavigation.momentary = YES;
    segmentedNavigation.tintColor = [UIColor groupTableViewBackgroundColor];
    [segmentedNavigation addTarget:self action:@selector(segmentedControlHandler:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *btnSegment = [[UIBarButtonItem alloc]initWithCustomView:segmentedNavigation];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                             btnSegment,
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil  action:nil],
                             btnDone,
                             nil];
    [toolBarKeyboard sizeToFit];
    
    
    
    //Toolbar No.2 for fields
    UIToolbar *toolBarKeyboardClean = [[UIToolbar alloc]init];
    toolBarKeyboardClean.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardClean.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnDoneClean = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneClean.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardClean.items = [NSArray arrayWithObjects:
                                  [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                  btnDoneClean,
                                  nil];
    [toolBarKeyboardClean sizeToFit];
    
    //Toolbar No.3 for Refresh
    UIToolbar *toolBarKeyboardWithRefresh = [[UIToolbar alloc]init];
    toolBarKeyboardWithRefresh.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardWithRefresh.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Refresh" style:UIBarButtonItemStyleDone target:self action:@selector(refreshDataInputView)];
    btnRefresh.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *btnDoneRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneRefresh.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardWithRefresh.items = [NSArray arrayWithObjects:
                                        btnRefresh,
                                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                        btnDoneRefresh,
                                        nil];
    [toolBarKeyboardWithRefresh sizeToFit];

    
    self.txtFirstName.inputAccessoryView    = toolBarKeyboard;
    self.txtLastName.inputAccessoryView     = toolBarKeyboard;
    self.txtGender.inputAccessoryView       = toolBarKeyboard;
    self.txtBirthdate.inputAccessoryView    = toolBarKeyboard;
    self.txtEmail.inputAccessoryView        = toolBarKeyboard;
    self.txtEmailConfirm.inputAccessoryView = toolBarKeyboard;
    self.txtAddress1.inputAccessoryView     = toolBarKeyboard;
    self.txtAddress2.inputAccessoryView     = toolBarKeyboard;
    self.txtPhone.inputAccessoryView        = toolBarKeyboard;
    
    self.txtCountry.inputAccessoryView      = toolBarKeyboardWithRefresh;
    self.txtZipCode.inputAccessoryView      = toolBarKeyboardWithRefresh;
    self.txtState.inputAccessoryView        = toolBarKeyboardWithRefresh;
    self.txtCity.inputAccessoryView         = toolBarKeyboardWithRefresh;
    
    //Pickers Gender, Birthdate, Country, State & City
    /* 1 */
    UIPickerView *pickerGender = [[UIPickerView alloc]init];
    pickerGender.delegate = self;
    pickerGender.dataSource = self;
    pickerGender.backgroundColor = [UIColor whiteColor];
    pickerGender.showsSelectionIndicator = YES;
    pickerGender.multipleTouchEnabled = NO;
    pickerGender.exclusiveTouch = YES;
    
    /* 2 */
    UIDatePicker *pickerBirthdate = [[UIDatePicker alloc]init];
    pickerBirthdate.datePickerMode = UIDatePickerModeDate;
    pickerBirthdate.backgroundColor = [UIColor whiteColor];
    [pickerBirthdate addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-100];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    [comps setYear:0];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    
    pickerBirthdate.minimumDate = minDate;
    pickerBirthdate.maximumDate = maxDate;
    pickerBirthdate.date = [NSDate date];
    pickerBirthdate.multipleTouchEnabled = NO;
    pickerBirthdate.exclusiveTouch = YES;
    
    self.txtGender.inputView    = pickerGender;
    self.txtBirthdate.inputView = pickerBirthdate;
}

- (void)LabelChange:(id)sender
{
    UIDatePicker *datePicker = (UIDatePicker *)sender;

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/yyyy"];
    
    self.txtBirthdate.text = [NSString stringWithFormat:@"%@", [df stringFromDate:datePicker.date]];
}

- (void)segmentedControlHandler:(id)sender
{
    UISegmentedControl *mySegmented = (UISegmentedControl *)sender;
    
    switch ([mySegmented selectedSegmentIndex])
    {
        case 0:
            if (self.txtLastName.isEditing == YES)
            {
                [self.txtFirstName becomeFirstResponder];
            }
            else if (self.txtGender.isEditing == YES)
            {
                [self.txtLastName becomeFirstResponder];
            }
            else if (self.txtBirthdate.isEditing == YES)
            {
                [self.txtGender becomeFirstResponder];
            }
            else if (self.txtEmail.isEditing == YES)
            {
                [self.txtBirthdate becomeFirstResponder];
            }
            else if (self.txtEmailConfirm.isEditing == YES)
            {
                [self.txtEmail becomeFirstResponder];
            }
            else if (self.txtAddress1.isEditing == YES)
            {
                [self.txtEmailConfirm becomeFirstResponder];
            }
            else if (self.txtAddress2.isEditing == YES)
            {
                [self.txtAddress1 becomeFirstResponder];
            }
            else if (self.txtPhone.isEditing == YES)
            {
                [self.txtAddress2 becomeFirstResponder];
            }
            break;
        case 1:
            if (self.txtFirstName.isEditing == YES)
            {
                [self.txtLastName becomeFirstResponder];
            }
            else if (self.txtLastName.isEditing == YES)
            {
                [self.txtGender becomeFirstResponder];
            }
            else if (self.txtGender.isEditing == YES)
            {
                [self.txtBirthdate becomeFirstResponder];
            }
            else if (self.txtBirthdate.isEditing == YES)
            {
                [self.txtEmail becomeFirstResponder];
            }
            else if (self.txtEmail.isEditing == YES)
            {
                [self.txtEmailConfirm becomeFirstResponder];
            }
            else if (self.txtEmailConfirm.isEditing == YES)
            {
                [self.txtAddress1 becomeFirstResponder];
            }
            else if (self.txtAddress1.isEditing == YES)
            {
                [self.txtAddress2 becomeFirstResponder];
            }
            else if (self.txtAddress2.isEditing == YES)
            {
                [self.txtPhone becomeFirstResponder];
            }
            break;
        default:
            break;
     
    }
}

-(void)hideKeyboard
{
    [self.txtFirstName resignFirstResponder];
    [self.txtLastName resignFirstResponder];
    [self.txtGender resignFirstResponder];
    [self.txtBirthdate resignFirstResponder];
    [self.txtEmail resignFirstResponder];
    [self.txtEmailConfirm resignFirstResponder];
    [self.txtAddress1 resignFirstResponder];
    [self.txtAddress2 resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    [self.txtZipCode resignFirstResponder];
    [self.txtCountry resignFirstResponder];
    [self.txtState resignFirstResponder];
    [self.txtCity resignFirstResponder];
}

-(void)refreshDataInputView
{
    if ([self.txtCountry isEditing])
    {
        [self.vcModalCountries getAllCountries];
    }
    else if ([self.txtState isEditing])
    {
        [self.vcModalStates getAllStates];
    }
    else if ([self.txtCity isEditing])
    {
        [self.vcModalCities getAllCities];
    }
}

#pragma mark -
#pragma mark - ACTION BUTTONS
-(void)saveChanges
{
    if ([self.txtFirstName.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationFirstname
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtLastName.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationLastname
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtEmail.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationEmail
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtEmailConfirm.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationConfirmEmail
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtEmail.text isValidEmail] == NO)
    {
        [[PSMessages getModel]showAlertMessage:kValidationEmailWrongFormat
                                     withTitle:kWarningTitle];
    }
    else if (![self.txtEmail.text isEqualToString:self.txtEmailConfirm.text])
    {
        [[PSMessages getModel] showAlertMessage:kValidationDifferentEmail
                                      withTitle:kWarningTitle];
    }
    else if ([self.txtAddress1.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationAddress
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtPhone.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationPhone
                                     withTitle:kWarningTitle];
    }
    else if (self.txtPhone.text.length < 10)
    {
        [[PSMessages getModel]showAlertMessage:kValidationPhoneLength
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtZipCode.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCountry.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCountry
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtState.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationState
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCity.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCitySelection
                                     withTitle:kWarningTitle];
    }
    else
    {
        [self editModeOff];
        [self saveAccountInformationChanged];
    }
}

#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setActiveTextField:textField];
    
    if (textField == self.txtCountry)
    {
        self.vcModalCountries = [[PSModalCountriesViewController alloc]init];
        self.vcModalCountries.delegate = self;
        self.vcModalCountries.view.frame = CGRectMake(0, 0,  320, 190);
        self.txtCountry.inputView = self.vcModalCountries.view;
    }
    else if (textField == self.txtState)
    {
        if ([self.txtCountry.text isBlank])
        {
            [textField resignFirstResponder];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            if (self.isEditModeOn == YES)
            {
                self.vcModalStates = [[PSModalStatesViewController alloc]initWithCountryId:self.id_country];
                self.vcModalStates.delegate = self;
                self.vcModalStates.view.frame = CGRectMake(0, 0,  320, 190);
                self.txtState.inputView = self.vcModalStates.view;
            }
        }
    }
    else if (textField == self.txtCity)
    {
        if ([self.txtState.text isBlank])
        {
            [textField resignFirstResponder];
            [[PSMessages getModel]showAlertMessage:kValidationState
                                         withTitle:kWarningTitle];
        }
        else
        {
            self.vcModalCities = [[PSModalCitiesViewController alloc]initWithStateId:self.id_state];
            self.vcModalCities.delegate = self;
            self.vcModalCities.view.frame = CGRectMake(0, 0,  320, 190);
            self.txtCity.inputView = self.vcModalCities.view;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setActiveTextField:nil];
    
    if (textField == self.txtZipCode)
    {
        if ([textField.text isBlank])
        {
            [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                         withTitle:kWarningTitle];
        }
        else if (textField.text.length < 5)
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationZipCodeLength
                                         withTitle:kWarningTitle];
        }
        else if ([self.txtCountry.text isBlank])
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            [self getZipCodeInfo];
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtZipCode)
    {
        if ([string isEqualToString:@" "] )
        {
            
            return NO;
        }
        if ([textField.text length] >= kZIP_CODE_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtPhone)
    {
        if ([textField.text length] >= kPHONE_NUMBER_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtGender || textField == self.txtBirthdate ||textField == self.txtCountry || textField == self.txtState || textField == self.txtCity)
    {
        return NO;
    }
    
    return YES;
}

#pragma mark -
#pragma mark - DATASOURCE UIPICKERVIEW
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.arrayGender.count;
}

#pragma mark -
#pragma mark - DELEGATE UIPICKERVIEW
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = [self.arrayGender objectAtIndex:row];
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.txtGender.text = [self.arrayGender objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 200, 32)];
    label.text = [self.arrayGender objectAtIndex:row];
    label.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20];
    label.textAlignment = NSTextAlignmentLeft;
    label.backgroundColor = [UIColor clearColor];
    
    UIImage *iconRow = [UIImage imageNamed:[self.arrayIcons objectAtIndex:row]];
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:iconRow];
    icon.frame = CGRectMake(50, 0, 30, 30);
    
    UIView *pickerRowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 32)];
    [pickerRowView insertSubview:icon atIndex:0];
    [pickerRowView insertSubview:label atIndex:0];
    [pickerRowView setUserInteractionEnabled:NO];
    [pickerRowView setTag:row];
    
    return pickerRowView;
}

@end
