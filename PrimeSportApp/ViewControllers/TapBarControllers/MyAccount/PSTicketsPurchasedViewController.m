//
//  PSTicketsPurchasedViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSTicketsPurchasedViewController.h"
#import "PSTicketsPurchasedDetailViewController.h"
#import "PSModelAccount.h"

@interface PSTicketsPurchasedViewController () <UITableViewDataSource, UITableViewDelegate, modelAccountDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *mySegmented;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;

@property (strong, nonatomic) NSMutableArray *arrayData;
@property (assign, nonatomic) BOOL isPastEventsActive;
@property (assign, nonatomic) BOOL isNextPageControl;
@property (assign, nonatomic) NSUInteger pageNumber;
@property (assign, nonatomic) NSUInteger pagesTotal;
@property (assign, nonatomic) BOOL updateByRefreshControl;

@property (strong, nonatomic) PSModelAccount *model;

@end

@implementation PSTicketsPurchasedViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelAccount alloc] init];
        self.model.delegate = self;
        
        self.arrayData          = [[NSMutableArray alloc]init];
        self.isPastEventsActive = NO;
        self.isNextPageControl  = NO;
        self.updateByRefreshControl = NO;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    self.myTableView.exclusiveTouch = YES;
    [self segmentedControlHandler:self.mySegmented];
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    [self.myRefreshControl addTarget:self action:@selector(updateTableData) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Ticket purchased dealloc");
    
    self.model.delegate = nil;
    self.myCell      = nil;
    self.arrayData   = nil;
    self.myRefreshControl = nil;
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableData
{
    self.updateByRefreshControl = YES;
    [self segmentedControlHandler:self.mySegmented];
}

#pragma mark -
#pragma mark - SEGMENTED (UPCOMING | PAST EVENTS)
- (IBAction)segmentedControlHandler:(id)sender
{
    UISegmentedControl *mySegmented = (UISegmentedControl *)sender;
    
    self.isNextPageControl = NO;
    
    switch ([mySegmented selectedSegmentIndex])
    {
        case 0:
            NSLog(@"upcoming selected");
            
            self.isPastEventsActive = NO;
            self.pageNumber = 1;
            self.pagesTotal = 0;
            
            [self getUpcomingOrPastEvents];
            break;
        case 1:
            NSLog(@"past events selected");
            
            self.isPastEventsActive = YES;
            self.pageNumber = 1;
            self.pagesTotal = 0;
            
            [self getUpcomingOrPastEvents];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getUpcomingOrPastEvents
{
    self.navigationItem.hidesBackButton = YES;
    
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    }
    
    NSString *TrueOrFalse = @"";
    
    if (self.isPastEventsActive == YES)
    {
        TrueOrFalse = @"True";
    }
    else
    {
        TrueOrFalse = @"False";
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"Email"];
    [parameters setObject:TrueOrFalse forKey:@"past"];
    [parameters setObject:[NSNumber numberWithUnsignedInteger:self.pageNumber] forKey:@"page"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kTicketsPurchased;
    webService.wsParameters = parameters;
    
    [self.model getTicketsPurchasedWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForUpcomingPurchasedEvents:(TPResultTicketsPurchased *)result
{
    self.updateByRefreshControl = NO;
    [self.myRefreshControl endRefreshing];
    
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    //Clean or not collection
    if (self.isNextPageControl == NO)
    {
        self.arrayData = [NSMutableArray array];
    }
    
    if (result != nil)
    {
        NSUInteger error = (NSUInteger)result.responseCode;
        
        if (error == 0)
        {
            self.pagesTotal = (NSUInteger)result.data.totalPages;
            
            if (result.data.details.count > 0)
            {
                for (TPDetails *detail in result.data.details)
                {
                    for (TPSections *section in detail.sections)
                    {
                        for (TPTickets *ticket in section.tickets)
                        {
                            NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
                            
                            [dictList setValue:detail.name forKey:@"title"];
                            [dictList setValue:detail.venue forKey:@"venue"];
                            [dictList setValue:detail.localDate forKey:@"date"];
                            [dictList setValue:section.name forKey:@"section"];
                            [dictList setValue:ticket.row forKey:@"row"];
                            [dictList setValue:ticket.sectionWiseView forKey:@"SectionWiseView"];
                            [dictList setValue:[NSString stringWithFormat:@"%@", [[NSNumber numberWithDouble:ticket.quantity] stringValue]] forKey:@"quantity"];
                            [dictList setValue:[NSString stringWithFormat:@"%@", [[NSNumber numberWithDouble:ticket.price] stringValue]] forKey:@"price"];
                            [dictList setValue:[NSString stringWithFormat:@"%@", [[NSNumber numberWithDouble:ticket.ticketId] stringValue]] forKey:@"id"];
                            
                            [self.arrayData addObject:dictList];
                        }
                    }
                }
            }
            else
            {
                [[PSMessages getModel]showToastStatusAlertWithMessage:@"No Tickets Found"
                                                  withBackgroundColor:[UIColor primeSportColorNotificationInfo]
                                                        withTextColor:[UIColor whiteColor]];
            }
        }
        else
        {
            if ([[result.message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:result.message
                                             withTitle:kWarningTitle];
            }
        }
    }
    [self.myTableView reloadData];
    
    //Set position on top or not
    if (self.arrayData.count > 0 && self.isNextPageControl == NO)
    {
        [self.myTableView scrollToTopAnimated:YES];
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{

    return self.arrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSUpcomingPastEventsCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    //Assigning elements
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *lblVenue = (UILabel *)[cell viewWithTag:2];
    UILabel *lblDate  = (UILabel *)[cell viewWithTag:3];
    UIImageView *ivImage = (UIImageView *)[cell viewWithTag:4];
    ivImage.layer.cornerRadius = ivImage.frame.size.height/2;
    ivImage.clipsToBounds = YES;
    
    UILabel *lblSection     = (UILabel *)[cell viewWithTag:5];
    UILabel *lblRow         = (UILabel *)[cell viewWithTag:6];
    UILabel *lblQuantity    = (UILabel *)[cell viewWithTag:7];
    UILabel *lblPrice       = (UILabel *)[cell viewWithTag:8];

    lblTitle.text    = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"title"];
    lblVenue.text    = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"venue"];
    
    NSString *localDate = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"date"];
    NSArray *arrayDate = [localDate componentsSeparatedByString: @" "];
    
    if (arrayDate.count == 3)
    {
        NSString *hour  = [NSString stringWithFormat:@"%@ %@", [arrayDate objectAtIndex:1],[arrayDate objectAtIndex:2]];
        lblDate.text    = [NSString stringWithFormat:@"%@ - %@", [arrayDate objectAtIndex:0], hour];
        [lblDate boldSubstring:hour];
    }
    else
    {
        lblDate.text = localDate;
    }
    
    NSString *theSection  = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"section"];
    NSString *theRow      = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"row"];
    NSString *theQuantity = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"quantity"];
    NSString *thePrice    = [[self.arrayData objectAtIndex:indexPath.row] valueForKey:@"price"];
    
    lblSection.text  = [NSString stringWithFormat:@"Section\n%@", theSection];
    [lblSection boldSubstring:theSection];
    
    lblRow.text      = [NSString stringWithFormat:@"Row\n%@", theRow];
    [lblRow boldSubstring:theRow];
    
    lblQuantity.text = [NSString stringWithFormat:@"Quantity\n%@", theQuantity];
    [lblQuantity boldSubstring:theQuantity];
    
    lblPrice.text    = [NSString stringWithFormat:@"Price\n%@", [PSUtils moneyFormat:thePrice withFraction:YES]];
    [lblPrice boldSubstring:[PSUtils moneyFormat:thePrice withFraction:YES]];
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PSTicketsPurchasedDetailViewController *vcTicketDetail = [[PSTicketsPurchasedDetailViewController alloc]initWithTicketPurchasedSelected:[self.arrayData objectAtIndex:indexPath.row]];
    vcTicketDetail.title = @"Ticket Detail";
    
    [self.navigationController pushViewController:vcTicketDetail animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 105.0f;
}

#pragma mark -
#pragma mark - LOAD MORE DATA
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    if (endScrolling >= scrollView.contentSize.height)
    {
        self.pageNumber++;
        
        if (self.pageNumber <= self.pagesTotal)
        {
            self.isNextPageControl = YES;
            [self getUpcomingOrPastEvents];
        }
    }
}

@end
