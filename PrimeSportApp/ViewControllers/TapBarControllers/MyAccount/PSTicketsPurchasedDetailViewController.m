//
//  PSTicketsPurchasedDetailViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 22/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSTicketsPurchasedDetailViewController.h"
#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"

@interface PSTicketsPurchasedDetailViewController () <UIViewControllerTransitioningDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIView *viewTicketBody;
@property (strong, nonatomic) UIImageView *ivMapVenue;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEvent;
@property (weak, nonatomic) IBOutlet UILabel *lblVenue;
@property (weak, nonatomic) IBOutlet UILabel *lblSection;
@property (weak, nonatomic) IBOutlet UILabel *lblRow;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

//Variables
@property (copy, nonatomic) NSString *dateTime;
@property (copy, nonatomic) NSString *event;
@property (copy, nonatomic) NSString *venue;
@property (copy, nonatomic) NSString *section;
@property (copy, nonatomic) NSString *row;
@property (copy, nonatomic) NSString *quantity;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *urlVenueMap;


@end

@implementation PSTicketsPurchasedDetailViewController

- (instancetype)initWithTicketPurchasedSelected:(NSDictionary *)ticketSelected
{
    self = [super init];
    if (self)
    {
        self.dateTime    = [ticketSelected valueForKey:@"date"];
        self.event       = [ticketSelected valueForKey:@"title"];
        self.venue       = [ticketSelected valueForKey:@"venue"];
        self.section     = [ticketSelected valueForKey:@"section"];
        self.row         = [ticketSelected valueForKey:@"row"];
        self.quantity    = [ticketSelected valueForKey:@"quantity"];
        self.price       = [ticketSelected valueForKey:@"price"];
        self.urlVenueMap = [ticketSelected valueForKey:@"SectionWiseView"];
        
         self.ivMapVenue = [[UIImageView alloc]init];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myScrollView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    
    [self configTicketAppearance];
    
    NSArray *arrayDateTime = [self.dateTime componentsSeparatedByString: @" "];
    if (arrayDateTime.count == 3)
    {
        NSString *hour  = [NSString stringWithFormat:@"%@ %@", [arrayDateTime objectAtIndex:1],[arrayDateTime objectAtIndex:2]];
        self.lblTime.text   = hour;
        
         NSArray *arrayJustDate = [[arrayDateTime objectAtIndex:0] componentsSeparatedByString: @"/"];
        if (arrayJustDate.count == 3)
        {
            //day, month, year
            NSString *formatDate = [NSString stringWithFormat:@"%@/%@/%@",
                                    [arrayJustDate objectAtIndex:1] ,
                                    [arrayJustDate objectAtIndex:0],
                                    [arrayJustDate objectAtIndex:2]];
            
            self.lblDate.text = [PSUtils getDayWeekMonthDayYearFormatFromStringDate:formatDate];
        }
        else
        {
            self.lblDate.text = self.dateTime;
        }
    }
    else
    {
        self.lblDate.text = self.dateTime;
        self.lblTime.text = self.dateTime;
    }

    self.lblEvent.text      = self.event;
    self.lblVenue.text      = self.venue;
    self.lblSection.text    = [self.section capitalizedString];
    self.lblRow.text        = [self.row capitalizedString];
    self.lblQuantity.text   = self.quantity;
    self.lblPrice.text      = [PSUtils moneyFormat:self.price withFraction:YES];
    
    //Config Imageview
    self.ivMapVenue.contentMode = UIViewContentModeScaleAspectFill;
    [self.ivMapVenue setImageWithURL:[NSURL URLWithString:self.urlVenueMap]
                    placeholderImage:[UIImage imageNamed:@"backDefaultVenueMap"]];
}

-(void)viewDidLayoutSubviews
{
    [self.myScrollView autosize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"TicketDetail dealloc");
    
    self.ivMapVenue = nil;
}


#pragma mark -
#pragma mark - APPEARANCE STYLE
-(void)configTicketAppearance
{
    //Config Ticket Background
    self.viewTicketBody.parallaxIntensity = 10;
    
    //Round Corner only in top
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.viewTicketBody.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.0}].CGPath;
    
    self.viewTicketBody.layer.mask = maskLayer;
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(IBAction)showVenueMap
{
    if (self.ivMapVenue.image != nil)
    {
        TGRImageViewController *imageViewer = [[TGRImageViewController alloc] initWithImage:self.ivMapVenue.image];
        imageViewer.transitioningDelegate = self;
        
        //Roor view controller must present it
        [self.view.window.rootViewController presentViewController:imageViewer
                                                          animated:YES
                                                        completion:nil];
    }
    else
    {
        [[PSMessages getModel]showAlertMessage:@"Image not available"
                                     withTitle:@"Image Empty"];
    }
}

#pragma mark -
#pragma mark -  DELEGATE METHOD (UIViewControllerTransitioningDelegate)
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    if ([presented isKindOfClass:TGRImageViewController.class])
    {
        [UIApplication sharedApplication].statusBarHidden = YES;
        return [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:self.ivMapVenue];
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    if ([dismissed isKindOfClass:TGRImageViewController.class])
    {
        [UIApplication sharedApplication].statusBarHidden = NO;
        return [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:self.ivMapVenue];
    }
    return nil;
}

@end
