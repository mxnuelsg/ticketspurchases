//
//  PSChangePasswordViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSChangePasswordViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PSModelAccount.h"

@interface PSChangePasswordViewController () <modelAccountDelegate>

//Controls
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtNewPassword;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtConfirmNewPassword;
@property (strong, nonatomic)UIBarButtonItem *btnSave;

//Model
@property (strong, nonatomic) PSModelAccount *model;

@end

@implementation PSChangePasswordViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelAccount alloc] init];
        self.model.delegate = self;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self chargeTextfieldIcons];
    
    self.btnSave = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                    style:UIBarButtonItemStylePlain
                                                   target:self
                                                   action:@selector(saveNewPassword)];
   
    self.btnSave.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = self.btnSave;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Change password dealloc");
    
    self.model.delegate = nil;
    self.btnSave = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnSave.enabled = NO;
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    [self.myScrollView scrollToTopAnimated:YES];
    self.btnSave.enabled = YES;
}

#pragma mark -
#pragma mark - TEXTFIELD ICONS
-(void)chargeTextfieldIcons
{
    self.txtCurrentPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCurrentPassword"]];
    self.txtCurrentPassword.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtNewPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPassword"]];
    self.txtNewPassword.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtConfirmNewPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPassword"]];
    self.txtConfirmNewPassword.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)saveMyNewPassword
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:@"Changing Password" andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"Email"];
    [parameters setObject:self.txtNewPassword.text forKey:@"NewPassword"];
    [parameters setObject:self.txtCurrentPassword.text forKey:@"OldPassword"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kChangePassword;
    webService.wsParameters = parameters;
    
    [self.model postChangePasswordWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForChangePassword:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message       = [result objectForKey:@"Message"];
        
        if (responseCode == 0)
        {
            
            [[PSMessages getModel]showAlertForPasswordChanged];
            
            self.txtCurrentPassword.text    = [NSString string];
            self.txtNewPassword.text        = [NSString string];
            self.txtConfirmNewPassword.text = [NSString string];
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(void)saveNewPassword
{
    if ([self.txtCurrentPassword.text isBlank])
    {
        [[PSMessages getModel] showAlertMessage:kValidationCurrentPassword
                                      withTitle:kWarningTitle];
    }
    else if ([self.txtNewPassword.text isBlank])
    {
        [[PSMessages getModel] showAlertMessage:kValidationNewPassword
                                      withTitle:kWarningTitle];
    }
    else if (self.txtNewPassword.text.length < 6)
    {
        [[PSMessages getModel] showAlertMessage:kValidationNewPasswordLength
                                      withTitle:kWarningTitle];
    }
    else if ([self.txtConfirmNewPassword.text isBlank])
    {
        [[PSMessages getModel] showAlertMessage:kValidationConfirmNewPassword
                                      withTitle:kWarningTitle];
    }
    else if (![self.txtNewPassword.text isEqualToString:self.txtConfirmNewPassword.text])
    {
        [[PSMessages getModel] showAlertMessage:kValidationDifferentNewPasswords
                                      withTitle:kWarningTitle];
    }
    else
    {
        [self saveMyNewPassword];
    }
}

@end
