//
//  PSLoginViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSLoginViewController : UIViewController

- (instancetype)initToBuyATicket;

-(IBAction)btnLogin;
-(IBAction)btnLoginFacebook;
-(IBAction)btnForgotPassword;
-(IBAction)btnRegister;

@end
