//
//  PSTicketsPurchasedViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 16/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTicketsPurchasedViewController : UIViewController

-(IBAction)segmentedControlHandler:(id)sender;
@end
