//
//  PSNewUserViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 30/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface PSNewUserViewController : UIViewController

- (instancetype)initWithProcessPurchase:(BOOL)isProcessActive;

@end
