//
//  PSNewUserViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 30/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSNewUserViewController.h"
#import "PSModelLogin.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface PSNewUserViewController () <modelLoginDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *myScrollView;

@property (assign, nonatomic) BOOL isProcessToBuyTicket;
@property (strong, nonatomic) PSModelLogin *model;
@end

@implementation PSNewUserViewController

- (instancetype)initWithProcessPurchase:(BOOL)isProcessActive
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelLogin alloc] init];
        self.model.delegate = self;
        
        self.isProcessToBuyTicket = isProcessActive;
    }
    return self;
}

#pragma mark -
#pragma mark - CYCLE LIFE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;

    [self.myScrollView contentSizeToFit];
    [self chargeTextfieldIcons];
    [self loadNavBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"Register New User dealloc");
    self.myScrollView   = nil;
    self.model.delegate = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidHide: (NSNotification *) notif
{
    [self.myScrollView scrollToTopAnimated:YES];
}

#pragma mark -
#pragma mark - TEXTFIELD ICONS
-(void)chargeTextfieldIcons
{
    self.txtFirstName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtLastName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtEmail.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconMail"]];
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPassword"]];
    self.txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtConfirmPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPassword"]];
    self.txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)sendRegister
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:@"Creating Account" andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.txtFirstName.text forKey:@"FirstName"];
    [parameters setObject:self.txtLastName.text forKey:@"LastName"];
    [parameters setObject:self.txtEmail.text forKey:@"UserEmail"];
    [parameters setObject:self.txtEmail.text forKey:@"EmailConfirmation"];
    [parameters setObject:self.txtPassword.text forKey:@"Password"];
    [parameters setObject:self.txtConfirmPassword.text forKey:@"PasswordConfirmation"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSignUp;
    webService.wsParameters = parameters;
    
    [self.model postRegisterNewUserWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelLogin)
-(void)modelResponseForRegisterWithObject:(LGResultLogin *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger error = (NSUInteger)result.responseCode;
        
        if (error == 0)
        {
            if (result.data.authenticationToken.length > 0 && result.data.email.length > 0)
            {
                [[PSSessionParameters information] setSessionParametersWithToken:result.data.authenticationToken
                                                                       withEmail:result.data.email];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                if (self.isProcessToBuyTicket == NO)
                {
                    PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
                    [(PSPrincipalViewController *)[delegate.window rootViewController] tabBarSelectedItem:3];
                }
                else
                {
                    [[PSMessages getModel] showAlertLoginMessageWithDelay:0.7];
                }
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:@"There is no data available"
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            if ([[result.message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:result.message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    UIBarButtonItem *btnLogOut = [[UIBarButtonItem alloc] initWithTitle:@"Send"
                                                                  style:UIBarButtonItemStyleDone
                                                                 target:self
                                                                 action:@selector(submitNewRegister)];

    btnLogOut.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = btnLogOut;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(void)submitNewRegister
{
    [self.view endEditing:YES];
    
    if([self.txtFirstName.text isBlank])
    {
        [self.txtFirstName shake];
    }
    else if([self.txtLastName.text isBlank])
    {
        [self.txtLastName shake];
    }
    else if([self.txtEmail.text isBlank])
    {
        [self.txtEmail shake];
    }
    else if([self.txtEmail.text isValidEmail] == NO)
    {
        [[PSMessages getModel] showAlertMessage:kValidationEmailWrongFormat
                                      withTitle:kWarningTitle];
    }
    else if([self.txtPassword.text isBlank])
    {
        [self.txtPassword shake];
    }
    else if (self.txtPassword.text.length < 6)
    {
        [[PSMessages getModel] showAlertMessage:kValidationNewPasswordLength
                                      withTitle:kWarningTitle];
    }
    else if ([self.txtConfirmPassword.text isBlank])
    {
        [self.txtConfirmPassword shake];
    }
    else if (![self.txtPassword.text isEqualToString:self.txtConfirmPassword.text])
    {
        [[PSMessages getModel] showAlertMessage:kValidationDifferentPasswords
                                      withTitle:kWarningTitle];
    }
    else
    {
        [self sendRegister];
    }
}




#pragma mark -
#pragma mark - HIDE KEYBOARD ON TAP
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
