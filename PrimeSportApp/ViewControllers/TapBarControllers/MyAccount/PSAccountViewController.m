//
//  PSAccountViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 29/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSAccountViewController.h"
#import "PSTicketsPurchasedViewController.h"
#import "PSContactInfoViewController.h"
#import "PSBillingInfoViewController.h"
#import "PSChangePasswordViewController.h"
#import <FacebookSDK/FBProfilePictureView.h>

@interface PSAccountViewController () <UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userImage;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblName;

@end

@implementation PSAccountViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.userImage = [[FBProfilePictureView alloc]init];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //disable swipe gesture to back (Main Categories)
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    UIBarButtonItem *btnLogOut = [[UIBarButtonItem alloc] initWithTitle:@"Log Out"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(btnLogout)];
    
    btnLogOut.tintColor = [UIColor primeSportColorDoneButton];
    
    self.navigationItem.rightBarButtonItem = btnLogOut;
    
    //Set Cover 1st time
    self.coverView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cover.png"]];
    [self cleanAccountView];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([PSSessionParameters information].isSessionActive == YES)
    {
        //Set Label & Image
        if (![[PSSessionParameters information].facebookId isBlank] && ![[PSSessionParameters information].facebookEmail isBlank])
        {
            self.userImage.profileID = [PSSessionParameters information].facebookId;
            self.lblName.text = [PSSessionParameters information].facebookEmail;
        }
        else
        {
            self.userImage.profileID = nil;
            self.lblName.text = [PSSessionParameters information].sessionEmail;
        }
    }
    else
    {
        self.lblName.text = [NSString string];
    }
    
}

-(void)viewDidLayoutSubviews
{
    [self.myScrollView autosize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Account dealloc");
    
    self.userImage = nil;
}

#pragma mark -
#pragma mark - CLEAN SCREEN
-(void)cleanAccountView
{
    //Init profile image with Parallax
    self.userImage.layer.cornerRadius = self.userImage.frame.size.height/2;
    self.userImage.clipsToBounds = YES;
    self.userImage.parallaxIntensity = 15;
    
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (self.userImage.frame.size.width), (self.userImage.frame.size.height));
    borderLayer.backgroundColor = [[UIColor clearColor] CGColor];
    borderLayer.frame = borderFrame;
    borderLayer.cornerRadius  = self.userImage.frame.size.height/2;
    borderLayer.borderWidth = 2;
    borderLayer.borderColor = [[UIColor colorWithHexadecimal:@"#EEEEEE" alpha:1] CGColor];
    [self.userImage.layer addSublayer:borderLayer];
    
    //Clean labels
    self.lblName.text = @"Username";
    self.lblName.adjustsFontSizeToFitWidth = YES;
    self.lblName.borderColor =  [UIColor blackColor];
	self.lblName.borderSize = 1;
    self.lblName.parallaxIntensity = 5;
}

#pragma mark -
#pragma mark - DELEGATE METHODS (UIActionSheetDelegate)
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
            {
                [FBSession.activeSession closeAndClearTokenInformation];
            }
            
            [[PSSessionParameters information] reset];
            [self.tabBarController setSelectedIndex:0];
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(void)btnLogout
{
    [self openMoreOptions];
}

-(void)openMoreOptions
{
    UIActionSheet *actionSheetFilter = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to logout now?"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                     destructiveButtonTitle:@"Log out"
                                                          otherButtonTitles: nil, nil];
    
    [actionSheetFilter showFromTabBar:self.tabBarController.tabBar];
}

-(IBAction)openTicketsPurchased
{
    PSTicketsPurchasedViewController *vcTicketsPurchased = [[PSTicketsPurchasedViewController alloc]init];
    vcTicketsPurchased.title = @"Tickets Purchased";
    
    [self.navigationController pushViewController:vcTicketsPurchased animated:YES];
}

-(IBAction)openContactsInfo
{
    PSContactInfoViewController *vcContactInfo = [[PSContactInfoViewController alloc]init];
    vcContactInfo.title = @"Conctact Info";
    
    [self.navigationController pushViewController:vcContactInfo animated:YES];
}

-(IBAction)openBillingInfo
{
    PSBillingInfoViewController *vcBillingInfo = [[PSBillingInfoViewController alloc]init];
    vcBillingInfo.title = @"Billing Info";
    
    [self.navigationController pushViewController:vcBillingInfo animated:YES];
}

-(IBAction)openChangePassword
{
    PSChangePasswordViewController *vcChangePassword = [[PSChangePasswordViewController alloc]init];
    vcChangePassword.title = @"Change Password";
    
    [self.navigationController pushViewController:vcChangePassword animated:YES];
}

@end
