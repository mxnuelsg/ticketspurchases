//
//  PSAccountViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 29/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSAccountViewController : UIViewController

-(IBAction)openTicketsPurchased;
-(IBAction)openContactsInfo;
-(IBAction)openBillingInfo;
-(IBAction)openChangePassword;

@end
