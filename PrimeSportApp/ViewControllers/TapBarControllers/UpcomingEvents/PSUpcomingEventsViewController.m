//
//  PSUpcomingEventsViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSUpcomingEventsViewController.h"
#import "PSModelUpcomingEvents.h"
#import "PSPrincipalViewController.h"
#import "PSEventTicketsViewController.h"
#import "PSEventListViewController.h"
#import "PSEventListPerformerViewController.h"
#import "PSEventGroupViewController.h"

@interface PSUpcomingEventsViewController () <UITableViewDataSource, UITableViewDelegate, modelUpcomingEventsDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;

//Variables
@property (strong, nonatomic) NSArray *arraySiteLinks;
@property (assign, nonatomic) BOOL updateByRefreshControl;

//Model
@property (strong, nonatomic) PSModelUpcomingEvents *model;


@end

@implementation PSUpcomingEventsViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelUpcomingEvents alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    //disable swipe gesture to back (Main Categories)
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.myTableView.exclusiveTouch = YES;
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    [self.myRefreshControl addTarget:self action:@selector(updateTableWithUpcomingEvents) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
    
    [self getSiteLinks];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Upcoming Events dealloc");
    self.model.delegate      = nil;
    self.myTableView         = nil;
    self.myRefreshControl    = nil;
    self.arraySiteLinks      = nil;
    self.myCell              = nil;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getSiteLinks
{
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:@"Loading" andMessage:nil];
    }
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetSiteLinks;
    
    [self.model getSiteLinksWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelUpcomingEvents)
-(void)modelResponseForSiteLinksWithObject:(SLResultSiteLinks *)result
{
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arraySiteLinks = [NSArray array];
    
    if (result != nil)
    {
        if (result.siteLinks.count > 0)
        {
            self.arraySiteLinks = result.siteLinks;
            [self.myTableView reloadData];
            
            //get total of events & update badge
            NSUInteger total = 0;
            for (SLSiteLinks *SL in result.siteLinks)
            {
                for (SLElements *element in SL.elements)
                {
                    #pragma unused (element)
                    total++;
                }
            }
            
            PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
            [(PSPrincipalViewController *)[delegate.window rootViewController]updateBadgeUpcomingEvents:total];
            
        }
        else
        {
            [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"There is no data available.\n\nPull down to try again"]
                                         withTitle:kWarningTitle];
        }
    }
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableWithUpcomingEvents
{
    self.updateByRefreshControl = YES;
    [self getSiteLinks];
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.arraySiteLinks.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    NSUInteger index = 0;
    NSUInteger totalRows = 0;
    
    for (SLSiteLinks *element in self.arraySiteLinks)
    {
        if (index == section)
        {
            totalRows = element.elements.count;
        }
        index++;
    }

    return totalRows;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSUInteger index = 0;
    NSString *title = [NSString string];
    for (SLSiteLinks *element in self.arraySiteLinks)
    {
        if (index == section)
        {
            title = element.tabName;
        }
        index++;
    }
    
    return title;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerSectionView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,[self tableView:tableView heightForHeaderInSection:section])];
    headerSectionView.backgroundColor = [UIColor primeSportColorHeaderSection];
    
    UIView *lineSeparator         = [[UIView alloc] initWithFrame:CGRectMake(0, [self tableView:tableView heightForHeaderInSection:section], tableView.bounds.size.width, 1)];
    lineSeparator.backgroundColor = [UIColor primeSportColorLineSection];
    
    UILabel *headerSectionTitle  = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.bounds.size.width - 5, [self tableView:tableView heightForHeaderInSection:section] -2)];
    headerSectionTitle.backgroundColor = [UIColor clearColor];
    headerSectionTitle.text            = [NSString stringWithFormat:@"%@",[self tableView:tableView titleForHeaderInSection:section]];
    headerSectionTitle.numberOfLines    = 2;
    headerSectionTitle.lineBreakMode    = NSLineBreakByWordWrapping;
    headerSectionTitle.font            = [UIFont boldSystemFontOfSize:11];
    headerSectionTitle.textColor       = [UIColor whiteColor];
    
    [headerSectionView addSubview:lineSeparator];
    [headerSectionView addSubview:headerSectionTitle];
    
    return headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSUpcomingCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }
    
    //Assigning elements
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *lblVenue = (UILabel *)[cell viewWithTag:2];
    UILabel *lblDate  = (UILabel *)[cell viewWithTag:3];
    UIImageView *ivImage = (UIImageView *)[cell viewWithTag:4];
    ivImage.layer.cornerRadius = ivImage.frame.size.height/2;
    ivImage.clipsToBounds = YES;
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    NSUInteger indexSection = 0;
    NSUInteger index = 0;
    
    for (SLSiteLinks *site in self.arraySiteLinks)
    {
        if (indexSection == indexPath.section)
        {
            for (SLElements *element in site.elements)
            {
                if (index == indexPath.row)
                {
                    //Fill data cell
                    lblTitle.text = element.title;
                    lblVenue.text = element.location;
                    
                    [ivImage setImageWithURL:[NSURL URLWithString:element.imageURL]
                            placeholderImage:[UIImage imageNamed:@"iconEmptyCell.png"]];

                    NSArray *arrayDate = [element.dateRange componentsSeparatedByString: @","];
                    
                    if (arrayDate.count == 2)
                    {
                        NSString *year  = [NSString stringWithFormat:@"%@", [arrayDate objectAtIndex:1]];
                        lblDate.text    = [NSString stringWithFormat:@"%@, %@", [arrayDate objectAtIndex:0], year];
                        [lblDate boldSubstring:year];
                    }
                    else
                    {
                        lblDate.text  = element.dateRange;
                    }
                    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
                }
                index++;
            }
        }
        indexSection++;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger indexSection = 0;
    NSUInteger index = 0;
    
    for (SLSiteLinks *site in self.arraySiteLinks)
    {
        if (indexSection == indexPath.section)
        {
            for (SLElements *element in site.elements)
            {
                if (index == indexPath.row)
                {
                    if ([[element.elementType lowercaseString] isEqualToString:@"dynamicsplashpage"])
                    {
                        PSEventListViewController *vcEventList = [[PSEventListViewController alloc]initWithId:(NSUInteger)element.elementId];
                        vcEventList.title = element.title;
                        
                        [self.navigationController pushViewController:vcEventList animated:YES];
                    }
                    else if ([[element.elementType lowercaseString] isEqualToString:@"event"])
                    {
                        PSEventTicketsViewController *vcEventTicket = [[PSEventTicketsViewController alloc]initWithId:(NSUInteger)element.elementId];
                        vcEventTicket.title = @"Event Details";
                        
                        [self.navigationController pushViewController:vcEventTicket animated:YES];
                    }
                    else if ([[element.elementType lowercaseString] isEqualToString:@"performer"]
                             || [[element.elementType lowercaseString] isEqualToString:@"venue"])
                    {
                        PSEventListPerformerViewController *vcEventList = [[PSEventListPerformerViewController alloc]initWithId:(NSUInteger)element.elementId
                                                                                                                  withGroupType:[element.elementType lowercaseString]];
                        vcEventList.title = element.title;
                        
                        [self.navigationController pushViewController:vcEventList animated:YES];
                    }
                    else if ([[element.elementType lowercaseString] isEqualToString:@"splashpage"])
                    {
                        PSEventGroupViewController *vcEventGroup = [[PSEventGroupViewController alloc]initWithId:(NSUInteger)element.elementId];
                        vcEventGroup.title = element.title;
                        
                        [self.navigationController pushViewController:vcEventGroup animated:YES];
                    }
                    else
                    {
                        [[PSMessages getModel] showAlertMessage:[NSString stringWithFormat:@"This kind of event (' %@ ') doesn't has been recognized. We are going to fix it as soon as possible.", element.elementType]
                                                      withTitle:kWarningTitle];
                    }
                    NSLog(@"Type: %@", element.elementType);
                }
                index++;
            }
        }
        indexSection++;
    }

}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 30.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 80.0f;
}

@end
