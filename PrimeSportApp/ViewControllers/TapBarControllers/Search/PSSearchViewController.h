//
//  PSSearchViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSearchViewController : UIViewController
-(void)publicCancelAction;
@end
