//
//  PSSearchViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSSearchViewController.h"
#import "PSModelSearch.h"
#import "PSEventTicketsViewController.h"

@interface PSSearchViewController () <UISearchBarDelegate, UITextFieldDelegate,  UITableViewDataSource, UITableViewDelegate, modelSearchDelegate>

//Controls
@property (strong, nonatomic) UISearchBar *mySearchBar;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayResult;
@property (assign, nonatomic) NSUInteger pageNumber;
@property (assign, nonatomic) NSUInteger pagesTotal;
@property (assign, nonatomic) BOOL isKeyboardVisible;
@property (copy, nonatomic) NSString *searchText;

//Model
@property (strong, nonatomic) PSModelSearch *model;


@end

@implementation PSSearchViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
//        self.mySearchBar = [[UISearchBar alloc] init];
//        self.mySearchBar.delegate = self;
        
        self.model = [[PSModelSearch alloc] init];
        self.model.delegate = self;
        
        self.arrayResult = [NSMutableArray array];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    //disable swipe gesture to back (Main Categories)
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    self.myTableView.exclusiveTouch = YES;
    [self loadNavBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Search dealloc");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    
    self.model.delegate = nil;
    self.mySearchBar = nil;
    self.myCell      = nil;
    self.myTableView = nil;
    self.arrayResult = nil;
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.isKeyboardVisible = YES;
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    self.isKeyboardVisible = NO;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    //Add Toolbar accesory view
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Cancel"
                                                               style:UIBarButtonItemStyleDone
                                                              target:self
                                                              action:@selector(closeKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil],
                             btnDone,
                             nil];
    [toolBarKeyboard sizeToFit];
    
    self.mySearchBar = [[UISearchBar alloc] init];
    self.mySearchBar.delegate               = self;
    self.mySearchBar.autoresizingMask       = UIViewAutoresizingFlexibleWidth;
    self.mySearchBar.autocorrectionType     = UITextAutocorrectionTypeNo;
    self.mySearchBar.translucent            = NO;
    self.mySearchBar.returnKeyType          = UIReturnKeySearch;
    self.mySearchBar.autocorrectionType    = UITextAutocorrectionTypeNo;
    self.mySearchBar.inputAccessoryView     = toolBarKeyboard;
    self.mySearchBar.placeholder            = @"Artist, Team or City";

    
    /* not anymore ::::: Ahora no entra al delegado - (BOOL)textFieldShouldClear:(UITextField *)textField
    //Find the UITextField view within searchBar (outlet to UISearchBar) & assign self as delegate
    UIView *viewToGetClearButton = [self.mySearchBar.subviews objectAtIndex:0];
    for (UIView *view in viewToGetClearButton.subviews)
    {
        if ([view isKindOfClass: [UITextField class]])
        {
            UITextField *txt       = (UITextField *)view;
            txt.delegate           = self;
            txt.autocorrectionType = UITextAutocorrectionTypeNo;
            txt.inputAccessoryView = toolBarKeyboard;
            txt.returnKeyType      = UIReturnKeySearch;
            break;
        }
    }
     */
    
    self.navigationItem.titleView = self.mySearchBar;
}

-(void)closeKeyboard
{
    if (self.isKeyboardVisible == YES)
    {
        [self.mySearchBar resignFirstResponder];
    }
}

#pragma mark -
#pragma mark - EMPTY STATE
-(void)showEmptyState
{
    [self removeEmptyState];

    //Create Empty State
    PSEmptyView *emptyView = [[PSEmptyView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    emptyView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:emptyView];
}

-(void)removeEmptyState
{
    self.myTableView.hidden = NO;
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[PSEmptyView class]])
        {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getSearchEventsWithPage:(NSUInteger)page
{
    NSLog(@"Current Page: %lu", (unsigned long)self.pageNumber);
    
    [self removeEmptyState];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.searchText forKey:@"Term"];
    [parameters setObject:[NSNumber numberWithInteger:page] forKey:@"Page"];

    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSearchEvents;
    webService.wsParameters = parameters;
    
    [self.model getSearchEventsWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelSearch)
-(void)modelResponseForSearchEventsWithObject:(SEResultSearch *)result
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    if (result != nil)
    {
        if (result.events.count > 0)
        {
            [self.arrayResult addObjectsFromArray:result.events];
            [self.myTableView reloadData];
            
            self.pagesTotal = [PSUtils getTotalOfPagesFromTotalRecords:(NSUInteger)result.totalRecordsCount];
        }
        else
        {
            [self showEmptyState];
        }
    }
}

#pragma mark -
#pragma mark - SEARCH BAR DELEGATE
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    self.arrayResult = [NSMutableArray array];
    [self.myTableView reloadData];
    
    self.searchText = [NSString string];
    self.searchText = searchBar.text;
    self.pageNumber = 1;
    self.pagesTotal = 0;
    
    [self getSearchEventsWithPage:self.pageNumber];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (self.isKeyboardVisible == YES)
    {
        [searchBar resignFirstResponder];
    }

    self.arrayResult = [NSMutableArray array];
    [self.myTableView reloadData];

    searchBar.text  = [NSString string];
    self.searchText = [NSString string];
    self.pageNumber = 0;
    self.pagesTotal = 0;
    
    [self removeEmptyState];
}

#pragma mark -
#pragma mark - PUBLIC CANCEL METHOD
-(void)publicCancelAction
{
    [self searchBarCancelButtonClicked:self.mySearchBar];
}

#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:self.mySearchBar afterDelay: 0.1];
    return YES;
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    return self.arrayResult.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSSearchResultCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }
    
    //Assigning elements
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *lblVenue = (UILabel *)[cell viewWithTag:2];
    UILabel *lblDate  = (UILabel *)[cell viewWithTag:3];
    UIImageView *ivImage = (UIImageView *)[cell viewWithTag:4];
    ivImage.layer.cornerRadius = ivImage.frame.size.height/2;
    ivImage.clipsToBounds = YES;
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    NSUInteger index = 0;
    for (SEEvents *element in self.arrayResult)
    {
        if (index == indexPath.row)
        {
            //Set Title, venue & date
            lblVenue.text = [NSString stringWithFormat:@"%@, %@",[element.venueCity capitalizedString], element.venueState];
            [ivImage setImageWithURL:[NSURL URLWithString:element.imageUrl]
                    placeholderImage:[UIImage imageNamed:@"iconEmptyCell.png"]];
            

            NSArray *arrayDate = [element.localDate componentsSeparatedByString: @" "];
            
            if (arrayDate.count == 3)
            {
                NSString *hour  = [NSString stringWithFormat:@"%@ %@", [arrayDate objectAtIndex:1],[arrayDate objectAtIndex:2]];
                lblDate.text    = [NSString stringWithFormat:@"%@ - %@", [arrayDate objectAtIndex:0], hour];
                [lblDate boldSubstring:hour];
            }
            else
            {
                lblDate.text = element.localDate;
            }
            
            //Tickets Available
            MLPAccessoryBadge *accessoryBadge = [MLPAccessoryBadge new];
            accessoryBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)element.availableTickets];
            
            if ((NSUInteger)element.availableTickets > 0)
            {
                lblTitle.text = element.name;
                accessoryBadge.textColor = [UIColor blackColor];
                accessoryBadge.backgroundColor = [UIColor primeSportColorYellow];
                cell.userInteractionEnabled = YES;
                
            }
            else
            {
                lblTitle.attributedText = [PSUtils strikethroughStyleForText:element.name];
                accessoryBadge.textColor = [UIColor whiteColor];
                accessoryBadge.backgroundColor = [UIColor primeSportColorRed];
                cell.userInteractionEnabled = NO;
            }
            
            [cell setAccessoryView:accessoryBadge];
        }
        index++;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSUInteger index = 0;
    for (SEEvents *element in self.arrayResult)
    {
        if (index == indexPath.row)
        {
            PSEventTicketsViewController *vcEventTicket = [[PSEventTicketsViewController alloc]initWithId:(NSUInteger)element.eventId];
            vcEventTicket.title = @"Event Details";
            
            [self.navigationController pushViewController:vcEventTicket animated:YES];
        }
        index++;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 80.0f;
}

#pragma mark -
#pragma mark - LOAD MORE DATA
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.mySearchBar resignFirstResponder];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    if (endScrolling >= scrollView.contentSize.height)
    {
        self.pageNumber++;

        if (self.pageNumber <= self.pagesTotal)
        {
            [self getSearchEventsWithPage:self.pageNumber];
        }
    }
}

@end
