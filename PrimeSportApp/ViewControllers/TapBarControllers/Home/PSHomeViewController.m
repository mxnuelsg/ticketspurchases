//
//  PSHomeViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSHomeViewController.h"
#import "PSModelHome.h"
#import "PSSubCategoriesViewController.h"


#import "PSBorderLabel.h"

@interface PSHomeViewController () <UITableViewDataSource, UITableViewDelegate, modelHomeDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIImageView *ivEmptyState;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;

//Variables
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;
@property (strong, nonatomic) NSArray *arrayMainCategories;
@property (assign, nonatomic) BOOL updateByRefreshControl;

//Model
@property (strong, nonatomic) PSModelHome *model;

@end

@implementation PSHomeViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelHome alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    //disable swipe gesture to back (Main Categories)
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.myTableView.exclusiveTouch = YES;
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    self.myRefreshControl.tintColor = [UIColor whiteColor];
    [self.myRefreshControl addTarget:self action:@selector(updateTableWithMainCategories) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
    
    [self getMainCategories];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Home dealloc");
    self.model.delegate      = nil;
    self.myRefreshControl    = nil;
    self.myTableView         = nil;
    self.myCell              = nil;
    self.arrayMainCategories = nil;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getMainCategories
{
    self.ivEmptyState.hidden = YES;
    
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:@"Loading" andMessage:nil];
    }
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetMainCategories;
    
    [self.model getMainCategoriesWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelHome)
-(void)modelResponseForHomeWithObject:(HMResultMainCategories *)result
{
    self.updateByRefreshControl = NO;
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arrayMainCategories = [NSArray array];
    
    if (result != nil)
    {
        
        NSUInteger error = (NSUInteger)result.responseCode;
        
        if (error == 0)
        {
            if (result.data.count > 0)
            {
                //Order by 'order' key
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc]initWithKey:@"order"
                                                                          ascending:YES];
                
                self.arrayMainCategories = [result.data sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                [self.myTableView reloadData];
            }
            else
            {
                self.ivEmptyState.hidden = NO;
                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"There is no data available.\nPull down to try again"]
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            self.ivEmptyState.hidden = NO;
            
            if ([[result.message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"%@\nPull down to try again",result.message]
                                             withTitle:kWarningTitle];
            }
        }
    }
    else
    {
        self.ivEmptyState.hidden = NO;
        [[PSMessages getModel]showAlertMessage:@"An error has occurred during this data request.\nPull down to try again"
                                     withTitle:kErrorTitle];
    }
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableWithMainCategories
{
    self.updateByRefreshControl = YES;
    [self getMainCategories];
}


#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    return self.arrayMainCategories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSHomeCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }

    //Assigning elements
    UIImageView *ivBackground  = (UIImageView *)[cell viewWithTag:1];
    PSBorderLabel *lblTitle    = (PSBorderLabel *)[cell viewWithTag:2];
    lblTitle.adjustsFontSizeToFitWidth = YES;
    lblTitle.borderColor =  [UIColor blackColor];
	lblTitle.borderSize = 1;
    
    UIImage *imagePlaceholder = [UIImage imageNamed:@"backPlaceholder.png"];
    
    NSUInteger index = 0;
    for (HMData *element in self.arrayMainCategories)
    {
        if (index == indexPath.row)
        {
            //Set Title
            lblTitle.text = element.name;
            [ivBackground setImageWithURL:[NSURL URLWithString:element.imageUrl]
                         placeholderImage:imagePlaceholder];
            
            ivBackground.parallaxIntensity = 10;
        }
        index++;
    }
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSUInteger index = 0;
    for (HMData *element in self.arrayMainCategories)
    {
        if (index == indexPath.row)
        {
            PSSubCategoriesViewController *vcSubCategory  = [[PSSubCategoriesViewController alloc]initWithId:(NSUInteger)element.dataIdentifier];
            vcSubCategory.title = element.name;
            
            [self.navigationController pushViewController:vcSubCategory animated:YES];
        }
        index++;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 80.0f;
}

@end
