//
//  PSSubCategoriesViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 03/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSubCategoriesViewController : UIViewController

- (instancetype)initWithId:(NSUInteger)identifier;

@end
