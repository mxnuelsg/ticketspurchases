//
//  PSModalBillingsViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 18/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modalBillingsDelegate<NSObject>
@required
-(void)selectedBillingInformation:(NSDictionary *)billingInformation;
@end

@interface PSModalBillingsViewController : UIViewController

@property (nonatomic, assign) id <modalBillingsDelegate> delegate;

-(void)getAllBillings;

@end
