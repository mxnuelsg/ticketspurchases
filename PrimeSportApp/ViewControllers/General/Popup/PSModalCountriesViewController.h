//
//  PSModalCountriesViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 25/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modalCountriesDelegate<NSObject>
@required
-(void)selectedCountryInformation:(NSDictionary *)countryInformation;
@end

@interface PSModalCountriesViewController : UIViewController

@property (nonatomic, assign) id <modalCountriesDelegate> delegate;

- (instancetype)initWithShippingMethodId:(NSString *)methodId;
-(void)getAllCountries;

@end
