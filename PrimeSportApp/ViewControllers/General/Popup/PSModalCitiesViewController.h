//
//  PSModalCitiesViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modalCitiesDelegate <NSObject>
@required
-(void)selectedCityInformation:(NSDictionary *)cityInformation;
@end

@interface PSModalCitiesViewController : UIViewController

@property (nonatomic, assign) id <modalCitiesDelegate> delegate;

- (instancetype)initWithStateId:(NSUInteger)stateId;
-(void)getAllCities;

@end
