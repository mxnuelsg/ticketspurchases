//
//  PSModalCountriesViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 25/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModalCountriesViewController.h"
#import "PSModelPlaces.h"

@interface PSModalCountriesViewController () <modelPlacesDelegate, UITableViewDataSource, UITableViewDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayCountries;
@property (assign, nonatomic) BOOL updateByRefreshControl;
@property (assign, nonatomic) BOOL onlyShippingCountries;
@property (copy, nonatomic) NSString *shippingMethodId;

//Model
@property (strong, nonatomic) PSModelPlaces *model;

@end

@implementation PSModalCountriesViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPlaces alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
        self.onlyShippingCountries  = NO;
    }
    return self;
}

- (instancetype)initWithShippingMethodId:(NSString *)methodId
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPlaces alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
        
        self.shippingMethodId = methodId;
        self.onlyShippingCountries  = YES;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    [self.myRefreshControl addTarget:self action:@selector(updateTableWithAllCountries) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
    
    [self getAllCountries];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    self.model.delegate   = nil;
    self.arrayCountries   = nil;
    self.myRefreshControl = nil;
    NSLog(@"ModalCountries dealloc");
}


#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getAllCountries
{
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    }
    
    if (self.onlyShippingCountries == YES)
    {
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setObject:self.shippingMethodId forKey:@"ShippingMethodId"];
        
        PSWebService *webService = [[PSWebService alloc] init];
        webService.wsMethod = kShippingCountries;
        webService.wsParameters = parameters;
        
        [self.model getShippingMethodCountriesWithObject:webService];
    }
    else
    {
        PSWebService *webService = [[PSWebService alloc] init];
        webService.wsMethod = kAllCountries;
        
        [self.model getCountriesWithObject:webService];
    }
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelPlaces)
-(void)modelResponseForCountries:(NSDictionary *)result
{
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arrayCountries = [NSMutableArray array];
    
    if (result != nil)
    {
        for (id key in result)
        {
            [self.arrayCountries addObject:key];
        }
        [self.myTableView reloadData];
    }
}

-(void)modelResponseForShippingMethodCountries:(NSDictionary *)result
{
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arrayCountries = [NSMutableArray array];
    
    if (result != nil)
    {
        for (id key in result)
        {
            [self.arrayCountries addObject:key];
        }
        [self.myTableView reloadData];
    }
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableWithAllCountries
{
    self.updateByRefreshControl = YES;
    [self getAllCountries];
}

#pragma mark -
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    if (self.arrayCountries.count > 0)
    {
        return self.arrayCountries.count;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    if (self.arrayCountries.count > 0)
    {
        cell.textLabel.text = [[[self.arrayCountries objectAtIndex:indexPath.row] valueForKey:@"CountryName"] capitalizedString];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor primeSportColorCellText];
        cell.userInteractionEnabled = YES;
        
        NSString *img = [[self.arrayCountries objectAtIndex:indexPath.row] valueForKey:@"CountryShortName"];
        cell.imageView.image = [UIImage imageNamed:img];
    }
    else
    {
        cell.textLabel.text = @"- Pull to Refresh -";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor grayColor];
        cell.userInteractionEnabled = NO;
    }
    
    cell.textLabel.font =  [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.arrayCountries.count > 0)
    {
        NSDictionary *countrySelected = [self.arrayCountries objectAtIndex:indexPath.row];
        [self.delegate selectedCountryInformation:countrySelected];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

@end
