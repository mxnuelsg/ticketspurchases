//
//  PSModalStatesViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 25/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModalStatesViewController.h"
#import "PSModelPlaces.h"

@interface PSModalStatesViewController () <modelPlacesDelegate, UITableViewDataSource, UITableViewDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayStates;
@property (assign, nonatomic) BOOL updateByRefreshControl;
@property (assign, nonatomic) NSUInteger id_country;

//Model
@property (strong, nonatomic) PSModelPlaces *model;

@end

@implementation PSModalStatesViewController

- (instancetype)initWithCountryId:(NSUInteger)countryId
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPlaces alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
        self.id_country = countryId;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    [self.myRefreshControl addTarget:self action:@selector(updateTableWithAllStates) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
    
    [self getAllStates];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    self.model.delegate   = nil;
    self.arrayStates      = nil;
    self.myRefreshControl = nil;
    NSLog(@"ModalStates dealloc");
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getAllStates
{
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:(NSInteger)self.id_country] forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kStates;
    webService.wsParameters = parameters;
    
    [self.model getStatesWithObject:webService];
}


#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForStates:(NSDictionary *)result
{
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arrayStates = [NSMutableArray array];
    
    if (result != nil)
    {
        for (id key in result)
        {
            [self.arrayStates addObject:key];
        }
        [self.myTableView reloadData];
    }
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableWithAllStates
{
    self.updateByRefreshControl = YES;
    [self getAllStates];
}

#pragma mark -
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    if (self.arrayStates.count > 0)
    {
        return self.arrayStates.count;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    if (self.arrayStates.count > 0)
    {
        cell.textLabel.text = [[[self.arrayStates objectAtIndex:indexPath.row] valueForKey:@"StateName"] capitalizedString];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor primeSportColorCellText];
        cell.userInteractionEnabled = YES;
    }
    else
    {
        cell.textLabel.text = @"- Pull to Refresh -";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor grayColor];
        cell.userInteractionEnabled = NO;
    }
    
    cell.textLabel.font =  [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.arrayStates.count > 0)
    {
        NSDictionary *countrySelected = [self.arrayStates objectAtIndex:indexPath.row];
        [self.delegate selectedStateInformation:countrySelected];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}


@end
