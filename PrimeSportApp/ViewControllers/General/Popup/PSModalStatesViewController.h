//
//  PSModalStatesViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 25/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modalStatesDelegate <NSObject>
@required
-(void)selectedStateInformation:(NSDictionary *)stateInformation;
@end

@interface PSModalStatesViewController : UIViewController

@property (nonatomic, assign) id <modalStatesDelegate> delegate;

- (instancetype)initWithCountryId:(NSUInteger)countryId;
-(void)getAllStates;

@end
