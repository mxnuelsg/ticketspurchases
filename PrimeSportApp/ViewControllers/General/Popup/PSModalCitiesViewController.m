//
//  PSModalCitiesViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModalCitiesViewController.h"
#import "PSModelPlaces.h"

@interface PSModalCitiesViewController ()  <modelPlacesDelegate, UITableViewDataSource, UITableViewDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayCities;
@property (assign, nonatomic) BOOL updateByRefreshControl;
@property (assign, nonatomic) NSUInteger id_state;

//Model
@property (strong, nonatomic) PSModelPlaces *model;

@end

@implementation PSModalCitiesViewController

- (instancetype)initWithStateId:(NSUInteger)stateId
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPlaces alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
        self.id_state = stateId;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    [self.myRefreshControl addTarget:self action:@selector(updateTableWithAllCities) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
    
    [self getAllCities];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    self.model.delegate   = nil;
    self.arrayCities      = nil;
    self.myRefreshControl = nil;
    NSLog(@"ModalCities dealloc");
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getAllCities
{
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:(NSInteger)self.id_state] forKey:@"stateId"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kCities;
    webService.wsParameters = parameters;
    
    [self.model getCitiesWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForCities:(NSDictionary *)result
{
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arrayCities = [NSMutableArray array];
    
    if (result != nil)
    {
        for (id key in result)
        {
            [self.arrayCities addObject:key];
        }
        [self.myTableView reloadData];
    }
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableWithAllCities
{
    self.updateByRefreshControl = YES;
    [self getAllCities];
}

#pragma mark -
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    if (self.arrayCities.count > 0)
    {
        return self.arrayCities.count;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    if (self.arrayCities.count > 0)
    {
        cell.textLabel.text = [[[self.arrayCities objectAtIndex:indexPath.row] valueForKey:@"Name"] capitalizedString];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor primeSportColorCellText];
        cell.userInteractionEnabled = YES;
    }
    else
    {
        cell.textLabel.text = @"- Pull to Refresh -";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor grayColor];
        cell.userInteractionEnabled = NO;
    }
    
    cell.textLabel.font =  [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.arrayCities.count > 0)
    {
        NSDictionary *countrySelected = [self.arrayCities objectAtIndex:indexPath.row];
        [self.delegate selectedCityInformation:countrySelected];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}


@end
