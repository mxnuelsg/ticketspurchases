//
//  PSModalBillingsViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 18/08/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSModalBillingsViewController.h"
#import "PSModelPlaces.h"

@interface PSModalBillingsViewController () <modelPlacesDelegate, UITableViewDataSource, UITableViewDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) UIRefreshControl *myRefreshControl;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayBillings;
@property (assign, nonatomic) BOOL updateByRefreshControl;

//Model
@property (strong, nonatomic) PSModelPlaces *model;

@end

@implementation PSModalBillingsViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPlaces alloc] init];
        self.model.delegate = self;
        
        self.updateByRefreshControl = NO;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myRefreshControl = [[UIRefreshControl alloc] init];
    [self.myRefreshControl addTarget:self action:@selector(updateTableWithAllBillings) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.myRefreshControl];
    
    [self getAllBillings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    self.model.delegate = nil;
    self.arrayBillings = nil;
    self.myRefreshControl = nil;
    
    NSLog(@"ModalBillings dealloc");
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getAllBillings
{
    if (self.updateByRefreshControl == NO)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"usermail"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kBillingInfos;
    webService.wsParameters = parameters;
    
    [self.model getBillingsWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelAccount)
-(void)modelResponseForBillings:(NSDictionary *)result
{
    [self.myRefreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.arrayBillings = [NSMutableArray array];
    
    if (result != nil)
    {
        NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message       = [result objectForKey:@"Message"];
        NSArray *arrayData      = [result objectForKey:@"Data"];
        
        if (responseCode == 0)
        {
            for (NSDictionary *dict in arrayData)
            {
                [self.arrayBillings addObject:dict];
            }
            [self.myTableView reloadData];
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
                
                if ([PSTicket information].isPurchaseProcessActive == YES)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelPurhaseProcess"
                                                                        object:nil];
                }
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

#pragma mark -
#pragma mark - REFRESH TABLE
-(void)updateTableWithAllBillings
{
    self.updateByRefreshControl = YES;
    [self getAllBillings];
}

#pragma mark -
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    if (self.arrayBillings.count > 0)
    {
        return self.arrayBillings.count;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    if (self.arrayBillings.count > 0)
    {
        cell.textLabel.text = [[self.arrayBillings objectAtIndex:indexPath.row] valueForKey:@"Nickname"];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor primeSportColorCellText];
        cell.userInteractionEnabled = YES;
    }
    else
    {
        cell.textLabel.text = @"- Pull to Refresh -";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor grayColor];
        cell.userInteractionEnabled = NO;
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.arrayBillings.count > 0)
    {
        NSDictionary *billingSelected = [self.arrayBillings objectAtIndex:indexPath.row];
        [self.delegate selectedBillingInformation:billingSelected];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}


@end
