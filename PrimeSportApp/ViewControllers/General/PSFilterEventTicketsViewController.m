//
//  PSFilterEventTicketsViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 11/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSFilterEventTicketsViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "NMRangeSlider.h"

@interface PSFilterEventTicketsViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

//Controls
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet NMRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblMinimumPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMaximumPrice;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtQuantity;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;

//Variables
@property (assign, nonatomic) NSUInteger maximumQuantityToSelect;
@property (assign, nonatomic) float maximumPrice;
@property (assign, nonatomic) float minimumPrice;
@property (strong, nonatomic) NSArray *arrayQuantityOptions;

@end

@implementation PSFilterEventTicketsViewController

- (instancetype)initWithMaximumQuantityToSelect:(NSUInteger)maximunQuantity withMinimumPrice:(float)minPrice withMaximumPrice:(float)maxPrice
{
    self = [super init];
    if (self)
    {
        self.maximumQuantityToSelect = maximunQuantity;
        self.minimumPrice = minPrice;
        self.maximumPrice = maxPrice;
        
        self.arrayQuantityOptions = [NSArray array];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    [self loadNavBar];
    [self generateOptionsQuantityToPickerWithMaximumOption:self.maximumQuantityToSelect];
    [self loadToolBarForKeyboard];
    [self.myScrollView contentSizeToFit];
    
    //Set Range Slider
    [self configureRangeSlider];
    [self rangeSliderValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    self.myScrollView = nil;
    
    NSLog(@"FilterEventTickets dealloc");
}

#pragma mark -
#pragma mark - GENERATE ARRAY OPTIONS QUANTITY
-(void)generateOptionsQuantityToPickerWithMaximumOption:(NSUInteger)maximumOption
{
    NSMutableArray *arrayGenerator = [NSMutableArray array];
    for (NSUInteger index = 1; index <= maximumOption; index++)
    {
        [arrayGenerator addObject:[NSString stringWithFormat:@"%lu", (unsigned long)index]];
    }
    
    self.arrayQuantityOptions = arrayGenerator;
}

#pragma mark -
#pragma mark - CONFIG RANGE SLIDER
- (void)configureRangeSlider
{
    self.rangeSlider.minimumValue = 0;
    self.rangeSlider.maximumValue = self.maximumPrice;
    self.rangeSlider.lowerValue   = 0;
    self.rangeSlider.upperValue   = self.maximumPrice;
    self.rangeSlider.minimumRange = 0;
    self.rangeSlider.tintColor = [UIColor primeSportColorTabBarController];
    
    [self.rangeSlider addTarget:self action:@selector(rangeSliderValueChanged) forControlEvents:UIControlEventValueChanged];
}

- (void)rangeSliderValueChanged
{
    self.lblMinimumPrice.text = [PSUtils moneyFormat:[NSString stringWithFormat:@"%f", self.rangeSlider.lowerValue]
                                        withFraction:YES];
    
    self.lblMaximumPrice.text = [PSUtils moneyFormat:[NSString stringWithFormat:@"%f", self.rangeSlider.upperValue]
                                        withFraction:YES];
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(closeFilter)];
    btnClose.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = btnClose;
    
    
    
    UIBarButtonItem *btnFind = [[UIBarButtonItem alloc] initWithTitle:@"Filter"
                                                                  style:UIBarButtonItemStyleDone
                                                                 target:self
                                                                 action:@selector(findTickets)];
    
    btnFind.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = btnFind;
}


#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.txtQuantity)
    {
        [self.myScrollView scrollToTopAnimated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtQuantity)
    {
        return NO;
    }
    
    return  YES;
}

#pragma mark -
#pragma mark - PICKER VIEW & TOOLBAR CONFIGURATION
-(void)loadToolBarForKeyboard
{
    //Toolbar for fields
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done"
                                                               style:UIBarButtonItemStyleDone
                                                              target:self
                                                              action:@selector(hideKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           btnDone,
                           nil];
    [toolBarKeyboard sizeToFit];

    self.txtQuantity.inputAccessoryView  = toolBarKeyboard;
    
    //Picker for Quantity Field
    UIPickerView *pickerQuantity = [[UIPickerView alloc]init];
    pickerQuantity.delegate = self;
    pickerQuantity.dataSource = self;
    pickerQuantity.backgroundColor = [UIColor whiteColor];
    pickerQuantity.showsSelectionIndicator = YES;

    self.txtQuantity.inputView = pickerQuantity;
}

-(void)hideKeyboard
{
    [self.txtQuantity resignFirstResponder];
}

-(void)closeFilter
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)findTickets
{
    [self hideKeyboard];
    
    if ([self.txtQuantity.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationQuantity
                                     withTitle:kWarningTitle];
    }
    else
    {
        [self.delegate dataFromFilterWithPriceFrom:[NSString stringWithFormat:@"%.2f", self.rangeSlider.lowerValue]
                                       withPriceTo:[NSString stringWithFormat:@"%.2f", self.rangeSlider.upperValue]
                                      withQuantity:self.txtQuantity.text];
        [self closeFilter];
    }
}


#pragma mark -
#pragma mark - DATASOURCE UIPICKERVIEW
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.arrayQuantityOptions.count;
}

#pragma mark -
#pragma mark - DELEGATE UIPICKERVIEW
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *option = [self.arrayQuantityOptions objectAtIndex:row];

    return option;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.txtQuantity.text = [NSString stringWithFormat:@"%@",[self.arrayQuantityOptions objectAtIndex:row]];
}


@end
