//
//  PSEventTicketsViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 09/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSEventTicketsViewController : UIViewController

- (instancetype)initWithId:(NSUInteger)identifier;
-(IBAction)showFilterView;
-(IBAction)showVenueMap;
@end
