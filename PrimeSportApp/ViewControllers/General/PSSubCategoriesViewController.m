//
//  PSSubCategoriesViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 03/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSSubCategoriesViewController.h"
#import "PSEventListViewController.h"
#import "PSEventListPerformerViewController.h"
#import "PSEventGroupViewController.h"
#import "PSEventTicketsViewController.h"
#import "PSModelFlow.h"

@interface PSSubCategoriesViewController () <UITableViewDelegate, UITableViewDataSource, modelFlowDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (assign, nonatomic) NSUInteger identifierCategory;
@property (strong, nonatomic) NSArray *arraySubcategory;
@property (strong, nonatomic) PSModelFlow *model;

@end

@implementation PSSubCategoriesViewController

- (instancetype)initWithId:(NSUInteger)identifier
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelFlow alloc] init];
        self.model.delegate = self;
        
        self.identifierCategory = identifier;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    self.navigationController.navigationBar.hidden = NO;
    self.myTableView.exclusiveTouch = YES;
    [self getSubCategories];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"SubCategories dealloc");
    
    self.model.delegate     = nil;
    self.myTableView        = nil;
    self.arraySubcategory   = nil;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getSubCategories
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:self.identifierCategory] forKey:@"Id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kCategoryEventGroups;
    webService.wsParameters = parameters;
    
    [self.model getSubCategoriesWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelFlow)
-(void)modelResponseForSubCategoriesWithObject:(SCResultSubCategories *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self removeEmptyState];
    
    self.arraySubcategory = [NSArray array];
    
    if (result != nil)
    {
        
        NSUInteger error = (NSUInteger)result.responseCode;
        
        if (error == 0)
        {
            if (result.data.count > 0)
            {
                //Order by 'order' key
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc]initWithKey:@"order"
                                                                          ascending:YES];
                
                self.arraySubcategory = [result.data sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                [self.myTableView reloadData];
            }
            else
            {
                [self showEmptyState];
            }
        }
        else
        {
            if ([[result.message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:result.message
                                             withTitle:kWarningTitle];
            }
        }
    }
    else
    {
        [self showEmptyState];
    }
}

#pragma mark -
#pragma mark - EMPTY STATE
-(void)showEmptyState
{
    [self removeEmptyState];
    
    self.myTableView.hidden = YES;
    
    //Create Empty State
    PSEmptyView *emptyView = [[PSEmptyView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    emptyView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:emptyView];
}

-(void)removeEmptyState
{
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[PSEmptyView class]])
        {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    return self.arraySubcategory.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:CellIdentifier] ;
    }

    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    NSUInteger index = 0;
    for (SCData *element in self.arraySubcategory)
    {
        if (index == indexPath.row)
        {
            //Config cell
            cell.textLabel.text = element.name;
            cell.textLabel.textColor = [UIColor primeSportColorCellText];
            cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        index++;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger index = 0;
    for (SCData *element in self.arraySubcategory)
    {
        if (index == indexPath.row)
        {
            if ([[element.groupType lowercaseString] isEqualToString:@"dynamicsplashpage"])
            {
                PSEventListViewController *vcEventList = [[PSEventListViewController alloc]initWithId:(NSUInteger)element.dataIdentifier];
                vcEventList.title = element.name;
                
                [self.navigationController pushViewController:vcEventList animated:YES];
            }
            else if ([[element.groupType lowercaseString] isEqualToString:@"performer"]
                     || [[element.groupType lowercaseString] isEqualToString:@"venue"])
            {
                PSEventListPerformerViewController *vcEventList = [[PSEventListPerformerViewController alloc]initWithId:(NSUInteger)element.dataIdentifier
                                                                                                          withGroupType:[element.groupType lowercaseString]];
                vcEventList.title = element.name;
                
                [self.navigationController pushViewController:vcEventList animated:YES];
            }
            else if ([[element.groupType lowercaseString] isEqualToString:@"splashpage"])
            {
                PSEventGroupViewController *vcEventGroup = [[PSEventGroupViewController alloc]initWithId:(NSUInteger)element.dataIdentifier];
                vcEventGroup.title = element.name;
                
                [self.navigationController pushViewController:vcEventGroup animated:YES];
            }
            else if ([[element.groupType lowercaseString] isEqualToString:@"event"])
            {
                PSEventTicketsViewController *vcEventTicket = [[PSEventTicketsViewController alloc]initWithId:(NSUInteger)element.dataIdentifier];
                vcEventTicket.title = @"Event Details";
                
                [self.navigationController pushViewController:vcEventTicket animated:YES];

            }
            else
            {
                [[PSMessages getModel] showAlertMessage:[NSString stringWithFormat:@"This kind of event (' %@ ') doesn't has been recognized. We are going to fix it as soon as possible.", element.groupType]
                                              withTitle:kWarningTitle];
            }
            
            NSLog(@"Type: %@", element.groupType);
        }
        index++;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 50.0f;
}

@end
