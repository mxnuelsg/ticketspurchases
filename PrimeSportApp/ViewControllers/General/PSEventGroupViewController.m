//
//  PSEventGroupViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 04/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSEventGroupViewController.h"
#import "PSModelFlow.h"
#import "PSEventListViewController.h"
#import "PSEventListPerformerViewController.h"

@interface PSEventGroupViewController () <UITableViewDelegate, UITableViewDataSource, modelFlowDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (strong, nonatomic) NSArray *arrayEventGroup;
@property (assign, nonatomic) NSUInteger identifierEventGroup;
@property (strong, nonatomic) PSModelFlow *model;

@end

@implementation PSEventGroupViewController

- (instancetype)initWithId:(NSUInteger)identifier
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelFlow alloc] init];
        self.model.delegate = self;
        
        self.identifierEventGroup = identifier;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    self.myTableView.exclusiveTouch = YES;
    [self getSplashInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"EventGroup dealloc");
    
    self.model.delegate = nil;
    self.arrayEventGroup = nil;
    self.myTableView = nil;
}
#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getSplashInformation
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:self.identifierEventGroup] forKey:@"Id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSplash;
    webService.wsParameters = parameters;
    
    [self.model getEventGroupsWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelFlow)
-(void)modelResponseForEventGroupsWithObject:(EGResultSplash *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self removeEmptyState];
    
    self.arrayEventGroup = [NSArray array];
    
    if (result != nil)
    {
        if (result.containers.count > 0)
        {
            self.arrayEventGroup = result.containers;
            [self.myTableView reloadData];
        }
        else
        {
            [self showEmptyState];
        }
    }
    else
    {
        [self showEmptyState];
    }
}

#pragma mark -
#pragma mark - EMPTY STATE
-(void)showEmptyState
{
    [self removeEmptyState];
    
    self.myTableView.hidden = YES;
    
    //Create Empty State
    PSEmptyView *emptyView = [[PSEmptyView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    emptyView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:emptyView];
}

-(void)removeEmptyState
{
    self.myTableView.hidden = NO;
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[PSEmptyView class]])
        {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.arrayEventGroup.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    NSUInteger index = 0;
    NSUInteger numberOfevents = 0;
    for (EGContainers *element in self.arrayEventGroup)
    {
        if (index == section)
        {
            numberOfevents = element.contaiterElements.count;
        }
        index++;
    }
    
    return numberOfevents;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSUInteger index = 0;
    NSString *title = [NSString string];
    for (EGContainers *element in self.arrayEventGroup)
    {
        if (index == section)
        {
            //Config cell
            title = element.containerTitle;
        }
        index++;
    }
    return title;
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerSectionView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,[self tableView:tableView heightForHeaderInSection:section])];
    headerSectionView.backgroundColor = [UIColor primeSportColorHeaderSection];
    
    UIView *lineSeparator         = [[UIView alloc] initWithFrame:CGRectMake(0, [self tableView:tableView heightForHeaderInSection:section], tableView.bounds.size.width, 1)];
    lineSeparator.backgroundColor = [UIColor primeSportColorLineSection];
    
    UILabel *headerSectionTitle  = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.bounds.size.width - 5, [self tableView:tableView heightForHeaderInSection:section] -2)];
    headerSectionTitle.backgroundColor = [UIColor clearColor];
    headerSectionTitle.text            = [NSString stringWithFormat:@"%@",[self tableView:tableView titleForHeaderInSection:section]];
    headerSectionTitle.numberOfLines   = 2;
    headerSectionTitle.lineBreakMode   = NSLineBreakByWordWrapping;
    headerSectionTitle.font            = [UIFont boldSystemFontOfSize:11];
    headerSectionTitle.textColor       = [UIColor whiteColor];
    
    [headerSectionView addSubview:lineSeparator];
    [headerSectionView addSubview:headerSectionTitle];
    
    return headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    NSUInteger indexSection = 0;
    NSUInteger index = 0;
    for (EGContainers *element in self.arrayEventGroup)
    {
        if (indexSection == indexPath.section)
        {
            for (EGContaiterElements *item in element.contaiterElements)
            {
                if (index == indexPath.row)
                {
                    //Fill cell
                    cell.textLabel.text = item.name;
                    
                    //Config cell
                    if ([item.type isEqualToString:@"0"])
                    {
                        cell.userInteractionEnabled = NO;
                        cell.backgroundColor = [UIColor primeSportColorCellBlue];
                        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
                        cell.textLabel.numberOfLines = 2;
                        cell.textLabel.textColor = [UIColor blackColor];
                        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                    else
                    {
                        cell.userInteractionEnabled = YES;
                        cell.backgroundColor = [UIColor whiteColor];
                        cell.textLabel.font = [UIFont boldSystemFontOfSize:12];
                        cell.textLabel.numberOfLines = 2;
                        cell.textLabel.textColor = [UIColor primeSportColorCellText];
                        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    }
                }
                index++;
            }
        }
        indexSection++;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger indexSection = 0;
    NSUInteger index = 0;
    for (EGContainers *element in self.arrayEventGroup)
    {
        if (indexSection == indexPath.section)
        {
            for (EGContaiterElements *item in element.contaiterElements)
            {
                if (index == indexPath.row)
                {
                    if ([[item.type lowercaseString] isEqualToString:@"splashpage"])
                    {
                        PSEventGroupViewController *vcEventGroup = [[PSEventGroupViewController alloc]initWithId:[item.contaiterElementsIdentifier integerValue]];
                        vcEventGroup.title = item.name;
                        
                        [self.navigationController pushViewController:vcEventGroup animated:YES];
                    }
                    else if ([[item.type lowercaseString] isEqualToString:@"dynamicsplashpage"])
                    {
                        PSEventListViewController *vcEventList = [[PSEventListViewController alloc]initWithId:[item.contaiterElementsIdentifier integerValue]];
                        vcEventList.title = item.name;
                        
                        [self.navigationController pushViewController:vcEventList animated:YES];
                        
                    }
                    else if ([[item.type lowercaseString] isEqualToString:@"performer"])
                    {
                        PSEventListPerformerViewController *vcEventListPerformer = [[PSEventListPerformerViewController alloc]initWithId:[item.contaiterElementsIdentifier integerValue]
                                                                                                                           withGroupType:[item.type lowercaseString]];
                        vcEventListPerformer.title = item.name;
                        
                        [self.navigationController pushViewController:vcEventListPerformer animated:YES];
                        
                    }
                    else
                    {
                        [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"Unknown type: %@", item.type]
                                                     withTitle:kWarningTitle];
                    }
                      NSLog(@"Type: %@", item.type);
                }
                index++;
            }
        }
        indexSection++;
    }


}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 50.0f;
}



@end
