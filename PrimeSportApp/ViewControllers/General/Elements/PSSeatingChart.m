//
//  PSSeatingChart.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 01/09/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSSeatingChart.h"

@interface PSSeatingChart () <UIWebViewDelegate>

@property (copy, nonatomic) NSString *loadUrl;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@end

@implementation PSSeatingChart

- (instancetype)initWithUrl:(NSString *)url
{
    self = [super init];
    if (self)
    {
        self.loadUrl = url;
        
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.btnClose.layer.cornerRadius = 5;
    self.btnClose.hidden = YES;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    [self.myWebView  loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.loadUrl]
                                                  cachePolicy:0
                                              timeoutInterval:kWS_TIMEOUT]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"PSSeatingChart dealloc");
}


#pragma mark -
#pragma mark - ACTION BUTTONS
- (IBAction)close
{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark -
#pragma mark - WEBVIEW DELEGATE
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.btnClose.hidden = NO;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.btnClose.hidden = NO;
    
    [[PSMessages getModel]showAlertMessage:[error localizedDescription]
                                 withTitle:kErrorTitle];
}

@end
