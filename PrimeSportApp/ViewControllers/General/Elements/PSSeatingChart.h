//
//  PSSeatingChart.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 01/09/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSeatingChart : UIViewController

- (instancetype)initWithUrl:(NSString *)url;
- (IBAction)close;

@end
