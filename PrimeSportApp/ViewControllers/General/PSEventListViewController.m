//
//  PSEventListViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 04/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSEventListViewController.h"
#import "PSModelFlow.h"
#import "PSEventTicketsViewController.h"

@interface PSEventListViewController () <UITableViewDelegate, UITableViewDataSource, modelFlowDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;
@property (strong, nonatomic) NSArray *arrayList;
@property (copy, nonatomic) NSString *listDescription;
@property (assign, nonatomic) NSUInteger identifierEventList;
@property (strong, nonatomic) PSModelFlow *model;


@end

@implementation PSEventListViewController

- (instancetype)initWithId:(NSUInteger)identifier
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelFlow alloc] init];
        self.model.delegate = self;
        
        self.identifierEventList = identifier;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    self.myTableView.exclusiveTouch = YES;
    [self getDynamicSplashInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"EventList dealloc");

    self.model.delegate  = nil;
    self.arrayList   = nil;
    self.myTableView = nil;
    self.myCell      = nil;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    if (self.listDescription != nil)
    {
        UIBarButtonItem *btnMore = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnInfo.png"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(showDescription)];
        
        self.navigationItem.rightBarButtonItem = btnMore;
    }
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(void)showDescription
{
    [[PSMessages getModel]showAlertMessage:self.listDescription
                                 withTitle:@"Description"];
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getDynamicSplashInformation
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:self.identifierEventList] forKey:@"Id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kDynamicSplash;
    webService.wsParameters = parameters;
    
    [self.model getEventListDynamicSplashWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelFlow)
-(void)modelResponseForEventListWithObject:(ELDResultDynamicSplash *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self removeEmptyState];
    
    self.arrayList = [NSArray array];
    
    if (result != nil)
    {
        self.listDescription = result.Description;
        [self loadNavBar];


        if (result.containers.count > 0)
            {
                //Order by 'sort' key
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc]initWithKey:@"sort"
                                                                          ascending:YES];
                
                self.arrayList = [result.containers sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                [self.myTableView reloadData];
            }
            else
            {
                [self showEmptyState];
            }
    }
    else
    {
        [self showEmptyState];
    }
}

#pragma mark -
#pragma mark - EMPTY STATE
-(void)showEmptyState
{
    [self removeEmptyState];
    
    self.myTableView.hidden = YES;
    
    //Create Empty State
    PSEmptyView *emptyView = [[PSEmptyView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    emptyView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:emptyView];
}

-(void)removeEmptyState
{
    self.myTableView.hidden = NO;
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[PSEmptyView class]])
        {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.arrayList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    NSUInteger index = 0;
    NSUInteger numberOfevents = 0;
    for (ELDContainers *element in self.arrayList)
    {
        if (index == section)
        {
            numberOfevents = element.events.count;
        }
        index++;
    }
    
    return numberOfevents;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSUInteger index = 0;
    NSString *title = [NSString string];
    for (ELDContainers *element in self.arrayList)
    {
        if (index == section)
        {
            //Config cell
            title = element.containerTitle;
        }
        index++;
    }
    
    return title;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerSectionView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,[self tableView:tableView heightForHeaderInSection:section])];
    headerSectionView.backgroundColor = [UIColor primeSportColorHeaderSection];
    
    UIView *lineSeparator         = [[UIView alloc] initWithFrame:CGRectMake(0, [self tableView:tableView heightForHeaderInSection:section], tableView.bounds.size.width, 1)];
    lineSeparator.backgroundColor = [UIColor primeSportColorLineSection];
    
    UILabel *headerSectionTitle  = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.bounds.size.width - 5, [self tableView:tableView heightForHeaderInSection:section] -2)];
    headerSectionTitle.backgroundColor = [UIColor clearColor];
    headerSectionTitle.text            = [NSString stringWithFormat:@"%@",[self tableView:tableView titleForHeaderInSection:section]];
    headerSectionTitle.numberOfLines    = 2;
    headerSectionTitle.lineBreakMode    = NSLineBreakByWordWrapping;
    headerSectionTitle.font            = [UIFont boldSystemFontOfSize:11];
    headerSectionTitle.textColor       = [UIColor whiteColor];
    
    [headerSectionView addSubview:lineSeparator];
    [headerSectionView addSubview:headerSectionTitle];
    
    return headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSEventListCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }
        
    //Assigning elements
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *lblVenue = (UILabel *)[cell viewWithTag:2];
    UILabel *lblDate  = (UILabel *)[cell viewWithTag:3];
    
    lblVenue.adjustsFontSizeToFitWidth = YES;
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    NSUInteger indexSection = 0;
    NSUInteger index = 0;
    for (ELDContainers *element in self.arrayList)
    {
        if (indexSection == indexPath.section)
        {
            for (ELDEvents *event in element.events)
            {
                if (index == indexPath.row)
                {
                    //Fill cell
                    lblVenue.text = [NSString stringWithFormat:@"%@, %@", [event.venueCity capitalizedString], event.venueState];
                    
                    NSArray *arrayDate = [event.localDate componentsSeparatedByString: @" "];
                    
                    if (arrayDate.count == 3)
                    {
                        NSString *hour  = [NSString stringWithFormat:@"%@ %@", [arrayDate objectAtIndex:1],[arrayDate objectAtIndex:2]];
                        lblDate.text    = [NSString stringWithFormat:@"%@ - %@", [arrayDate objectAtIndex:0], hour];
                        [lblDate boldSubstring:hour];
                    }
                    else
                    {
                        lblDate.text  = event.localDate;
                    }
                    
                    //Tickets Available
                    MLPAccessoryBadge *accessoryBadge = [MLPAccessoryBadge new];
                    accessoryBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)event.availableTickets];
                    
                    if ((NSUInteger)event.availableTickets > 0)
                    {
                        lblTitle.text = event.name;
                        accessoryBadge.textColor = [UIColor blackColor];
                        accessoryBadge.backgroundColor = [UIColor primeSportColorYellow];
                        cell.userInteractionEnabled = YES;
                    }
                    else
                    {
                        lblTitle.attributedText = [PSUtils strikethroughStyleForText:event.name];
                        accessoryBadge.textColor = [UIColor whiteColor];
                        accessoryBadge.backgroundColor = [UIColor primeSportColorRed];
                        cell.userInteractionEnabled = NO;
                    }
                    
                    [cell setAccessoryView:accessoryBadge];
                }
                index++;
            }
        }
        indexSection++;
    }
    
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSUInteger indexSection = 0;
    NSUInteger index = 0;
    for (ELDContainers *element in self.arrayList)
    {
        if (indexSection == indexPath.section)
        {
            for (ELDEvents *event in element.events)
            {
                if (index == indexPath.row)
                {
                    PSEventTicketsViewController *vcEventTicket = [[PSEventTicketsViewController alloc]initWithId:(NSUInteger)event.eventId];
                    vcEventTicket.title = @"Event Details";
                    
                    [self.navigationController pushViewController:vcEventTicket animated:YES];
                    
                }
                index++;
            }
        }
        indexSection++;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 80.0f;
}

@end
