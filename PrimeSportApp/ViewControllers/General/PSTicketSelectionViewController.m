//
//  PSTicketSelectionViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 18/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSTicketSelectionViewController.h"
#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"
#import "PSModelLogin.h"

@interface PSTicketSelectionViewController () <UIScrollViewDelegate, UIViewControllerTransitioningDelegate, modelLoginDelegate>

//Outlets
@property (weak, nonatomic) IBOutlet UIScrollView *myScroll;
@property (weak, nonatomic) IBOutlet UIView *viewRow;
@property (weak, nonatomic) IBOutlet UIView *viewQuantity;
@property (weak, nonatomic) IBOutlet UIButton *btnBuyNow;

@property (weak, nonatomic) IBOutlet PSBorderLabel *lblTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblVenue;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblDateTime;


@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UILabel *lblTickets;
@property (weak, nonatomic) IBOutlet UILabel *lblSection;
@property (weak, nonatomic) IBOutlet UILabel *lblRow;
@property (weak, nonatomic) IBOutlet UILabel *lblTicketsSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;

@property (strong, nonatomic) UIImageView *ivMapVenue;

//Variables
@property (assign, nonatomic) NSInteger ticketsSelected;
@property (assign, nonatomic) NSUInteger ticketsSelectedTotal;
@property (strong, nonatomic) NSArray *arraySplitTickets;
@property (strong, nonatomic) NSDictionary *dictionaryTicketSelected;

@property (copy, nonatomic) NSString *urlMapImage;
@property (copy, nonatomic) NSString *firstTitle;
@property (copy, nonatomic) NSString *secondTitle;
@property (copy, nonatomic) NSString *thirdTitle;
@property (copy, nonatomic) NSString *section;
@property (copy, nonatomic) NSString *row;
@property (assign, nonatomic) double price;
@property (assign, nonatomic) double operation;
@property (assign, nonatomic) NSUInteger quantity;
@property (assign, nonatomic) NSUInteger ticketIdentifier;

//Model
@property (nonatomic, strong) PSModelLogin *model;

@end

@implementation PSTicketSelectionViewController

- (instancetype)initWithTicket:(ETSTickets *)ticket withTitle:(NSString *)title withVenue:(NSString *)venue withDate:(NSString *)date
{
    self = [super init];
    if (self)
    {
        
        self.model = [[PSModelLogin alloc] init];
        self.model.delegate = self;
        
        self.ivMapVenue = [[UIImageView alloc] init];
        
        self.ticketsSelected = 0;
        self.ticketsSelectedTotal = 0;
        
        self.urlMapImage       = ticket.sectionWiseView;
        self.firstTitle        = title;
        self.secondTitle       = venue;
        self.thirdTitle        = date;
        self.section           = ticket.section;
        self.row               = ticket.row;
        self.price             = ticket.price;
        self.quantity          = (NSUInteger)ticket.quantity;
        self.ticketIdentifier  = (NSUInteger)ticket.ticketId;
        
        //get & separate available quantity tickets
        if (ticket.split.length > 0)
        {
            self.arraySplitTickets = [[ticket.split componentsSeparatedByString:@","] sortedArrayWithKey:@"integerValue"
                                                                                               ascending:YES];
        }
        else
        {
            self.arraySplitTickets = [NSArray array];
        }
        
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Parallax effect
    self.lblTitle.parallaxIntensity     = 10;
    self.lblVenue.parallaxIntensity     = 5;
    self.lblDateTime.parallaxIntensity  = 2;
    
    //Rounded corner
    self.lblTotalPrice.layer.cornerRadius = 11;
    self.lblTickets.layer.cornerRadius    = 11;
    
    //Border Button
    self.btnBuyNow.layer.cornerRadius     = 12;
    
    //Config scroll
    self.myScroll.delegate = self;
    self.myScroll.scrollEnabled = YES;
    
    //Config Imageview
    self.ivMapVenue.contentMode = UIViewContentModeScaleAspectFill;

    
    //Config tickets control
    if (self.arraySplitTickets.count ==  0)
    {
        self.btnMinus.enabled = NO;
        self.btnPlus.enabled  = NO;
        self.lblTicketsSelected.text = @"0";
    }
    else
    {
        self.lblTicketsSelected.text = [self.arraySplitTickets objectAtIndex:self.ticketsSelectedTotal];
    }
    
    [self fillViewInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}

-(void)viewDidLayoutSubviews
{
    [self.myScroll autosize];
}

- (void)dealloc
{
    NSLog(@"TicketSelection dealloc");
    
    self.model.delegate           = nil;
    self.dictionaryTicketSelected = nil;
    self.arraySplitTickets        = nil;
    self.ivMapVenue               = nil;
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
    //Fill data
    self.lblSection.text = [NSString stringWithFormat:@"Section %@", self.section];
    
    self.lblRow.text = [NSString stringWithFormat:@"Row %@", self.row];
    [self.lblRow boldSubstring:self.row];
    
    self.lblTickets.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.quantity];
    self.lblTickets.adjustsFontSizeToFitWidth = YES;
    
    [self.ivMapVenue setImageWithURL:[NSURL URLWithString:self.urlMapImage]
                    placeholderImage:[UIImage imageNamed:@"backDefaultVenueMap"]];
    
    self.lblTitle.text    = self.firstTitle;
    self.lblTitle.borderColor =  [UIColor blackColor];
    self.lblTitle.borderSize = 1;
    
    self.lblVenue.text    = self.secondTitle;
    self.lblVenue.borderColor =  [UIColor blackColor];
    self.lblVenue.borderSize = 1;
    
    self.lblDateTime.text = self.thirdTitle;
    self.lblDateTime.borderColor =  [UIColor blackColor];
    self.lblDateTime.borderSize = 1;
    
    [self calculatePriceForTotalTickets:[[self.arraySplitTickets objectAtIndex:self.ticketsSelectedTotal] intValue]];
}

#pragma mark -
#pragma mark - GET TOTAL PRICE
-(void)calculatePriceForTotalTickets:(NSUInteger)noTickets
{
    self.operation = 0.00;
    self.operation = self.price * noTickets;
    
    self.lblTotalPrice.text = [PSUtils moneyFormat:[NSString stringWithFormat:@"%f", self.operation]
                                      withFraction:YES];
    self.lblTotalPrice.adjustsFontSizeToFitWidth = YES;
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(IBAction)zoomImage
{
    if (self.ivMapVenue.image != nil)
    {
        TGRImageViewController *imageViewer = [[TGRImageViewController alloc] initWithImage:self.ivMapVenue.image];
        imageViewer.transitioningDelegate = self;
        
        //Roor view controller must present it
        [self.view.window.rootViewController presentViewController:imageViewer
                                                          animated:YES
                                                        completion:nil];
    }
    else
    {
        [[PSMessages getModel]showAlertMessage:@"Image not available"
                                     withTitle:@"Image Empty"];
    }
}

-(IBAction)addTicket
{
    self.btnMinus.enabled = YES;
    self.ticketsSelected += 1;
    
    if (self.ticketsSelected == self.arraySplitTickets.count)
    {
        self.btnPlus.enabled = NO;
        self.ticketsSelected = self.arraySplitTickets.count -1;
        
        return;
    }
    self.ticketsSelectedTotal = self.ticketsSelected;
    self.lblTicketsSelected.text = [self.arraySplitTickets objectAtIndex:self.ticketsSelectedTotal];
    [self calculatePriceForTotalTickets:[self.lblTicketsSelected.text integerValue]];
}

-(IBAction)removeTicket
{
    self.btnPlus.enabled = YES;
    
    self.ticketsSelected -= 1;
    
    if (self.ticketsSelected == -1)
    {
        self.btnMinus.enabled = NO;
        self.ticketsSelected = 0;
        return;
    }
    self.ticketsSelectedTotal = self.ticketsSelected;
    self.lblTicketsSelected.text = [self.arraySplitTickets objectAtIndex:self.ticketsSelectedTotal];
    [self calculatePriceForTotalTickets:[self.lblTicketsSelected.text integerValue]];
}

- (IBAction)buyNow
{
    if ([PSSessionParameters information].isSessionActive == YES)
    {
        [self checkStatusToken];
    }
    else
    {
        PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
        [(PSPrincipalViewController *)[delegate.window rootViewController] presentLoginToBuyTicket];
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)checkStatusToken
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"userEmail"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kVerifyToken;
    webService.wsParameters = parameters;
    
    [self.model getTokenStatusWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelLogin)
-(void)modelResponseForTokenStatus:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode  = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message        = [result objectForKey:@"Message"];
        
        if (responseCode == 0)
        {
            [self buyTicket];
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

#pragma mark -
#pragma mark - START PURCHASE PROCESS
-(void)buyTicket
{
    PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    [(PSPrincipalViewController *)[delegate.window rootViewController] startPurchaseProcess];
    
    [PSTicket information].tcktEventId = [NSString stringWithFormat:@"%@", [[NSNumber numberWithInteger:self.ticketIdentifier] stringValue]];
    [PSTicket information].tcktEventTitle = self.firstTitle;
    [PSTicket information].tcktEventVenue = self.secondTitle;
    [PSTicket information].tcktEventDate  = self.thirdTitle;
    [PSTicket information].tcktEventSection = self.section;
    [PSTicket information].tcktEventRow = self.row;
    [PSTicket information].tcktEventPrice = [NSString stringWithFormat:@"%@", [[NSNumber numberWithDouble:self.price] stringValue]];
    [PSTicket information].tcktEventQuantity = self.lblTicketsSelected.text;
    [PSTicket information].tcktEventPricePerQuantity = [NSString stringWithFormat:@"%@", [[NSNumber numberWithDouble:self.operation] stringValue]];
    
}

#pragma mark -
#pragma mark -  DELEGATE METHOD (UIViewControllerTransitioningDelegate)
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    if ([presented isKindOfClass:TGRImageViewController.class])
    {
        [UIApplication sharedApplication].statusBarHidden = YES;
        return [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:self.ivMapVenue];
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    if ([dismissed isKindOfClass:TGRImageViewController.class])
    {
        [UIApplication sharedApplication].statusBarHidden = NO;
        return [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:self.ivMapVenue];
    }
    return nil;
}


@end
