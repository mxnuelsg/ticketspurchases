//
//  PSTicketSelectionViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 18/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETSTickets.h"

@interface PSTicketSelectionViewController : UIViewController

- (instancetype)initWithTicket:(ETSTickets *)ticket withTitle:(NSString *)title withVenue:(NSString *)venue withDate:(NSString *)date;

-(IBAction)zoomImage;
-(IBAction)addTicket;
-(IBAction)removeTicket;
- (IBAction)buyNow;
@end
