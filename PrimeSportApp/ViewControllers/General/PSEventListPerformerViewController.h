//
//  PSEventListPerformerViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 06/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSEventListPerformerViewController : UIViewController

- (instancetype)initWithId:(NSUInteger)identifier withGroupType:(NSString *)groupType;
@end
