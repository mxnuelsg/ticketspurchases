//
//  PSFilterEventTicketsViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 11/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol filterEventTicketsDelegate <NSObject>

@required
-(void)dataFromFilterWithPriceFrom:(NSString *)filterPriceFrom withPriceTo:(NSString *)filterPriceTo withQuantity:(NSString *)filterQuantity;
@end

@interface PSFilterEventTicketsViewController : UIViewController

@property (assign, nonatomic) id <filterEventTicketsDelegate> delegate;

- (instancetype)initWithMaximumQuantityToSelect:(NSUInteger)maximunQuantity withMinimumPrice:(float)minPrice withMaximumPrice:(float)maxPrice;

@end
