//
//  PSEventListPerformerViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 06/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSEventListPerformerViewController.h"
#import "PSModelFlow.h"
#import "PSEventTicketsViewController.h"

@interface PSEventListPerformerViewController () <UITableViewDelegate, UITableViewDataSource, modelFlowDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;
@property (strong, nonatomic) NSArray *arrayListPerformer;
@property (copy, nonatomic) NSString *listDescription;
@property (copy,nonatomic) NSString *groupType;
@property (assign, nonatomic) NSUInteger identifierEventList;
@property (strong, nonatomic) PSModelFlow *model;

@end

@implementation PSEventListPerformerViewController

- (instancetype)initWithId:(NSUInteger)identifier withGroupType:(NSString *)groupType
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelFlow alloc] init];
        self.model.delegate = self;
        
        self.identifierEventList = identifier;
        self.groupType = groupType;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Avoid last record behind transluced Tab Bar
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.myTableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);

    //Background Logo Parallax
    self.bgLogo.parallaxIntensity = 20;
    
    self.myTableView.exclusiveTouch = YES;
    [self getPerformerInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"EventListPerformer dealloc");
    
    self.model.delegate     = nil;
    self.arrayListPerformer = nil;
    self.myTableView        = nil;
    self.myCell             = nil;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    if (self.listDescription != nil)
    {
        UIBarButtonItem *btnMore = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnInfo.png"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(showDescription)];
        
        self.navigationItem.rightBarButtonItem = btnMore;
    }
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(void)showDescription
{
    [[PSMessages getModel]showAlertMessage:self.listDescription
                                 withTitle:@"Description"];
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getPerformerInformation
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:self.identifierEventList] forKey:@"Id"];
    [parameters setObject:self.groupType forKey:@"GroupType"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kEventGroup;
    webService.wsParameters = parameters;
    
    [self.model getEventListPerformerWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelFlow)
-(void)modelResponseForEventListPerformerWithObject:(ELPResultPerformer *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self removeEmptyState];
    
    self.arrayListPerformer = [NSArray array];
    
    if (result != nil)
    {
        self.listDescription = result.Description;
        [self loadNavBar];
        
        
        if (result.eventList.count > 0)
        {
            self.arrayListPerformer = result.eventList;
            [self.myTableView reloadData];
        }
        else
        {
            [self showEmptyState];
        }
    }
    else
    {
        [self showEmptyState];
    }
}

#pragma mark -
#pragma mark - EMPTY STATE
-(void)showEmptyState
{
    [self removeEmptyState];
    
    self.myTableView.hidden = YES;
    
    //Create Empty State
    PSEmptyView *emptyView = [[PSEmptyView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    emptyView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:emptyView];
}

-(void)removeEmptyState
{
    self.myTableView.hidden = NO;
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[PSEmptyView class]])
        {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
        return self.arrayListPerformer.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSEventListPerformerCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }
    
    //Assigning elements
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *lblVenue = (UILabel *)[cell viewWithTag:2];
    UILabel *lblDate  = (UILabel *)[cell viewWithTag:3];
    
    lblVenue.adjustsFontSizeToFitWidth = YES;
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    NSUInteger index = 0;
    for (ELPEventList *event in self.arrayListPerformer)
    {
        if (index == indexPath.row)
        {
            //Fill cell
            lblVenue.text = [NSString stringWithFormat:@"%@, %@", [event.venueCity capitalizedString], event.venueState];
            lblDate.text  = event.localDate;
            
            NSArray *arrayDate = [event.localDate componentsSeparatedByString: @" "];
            
            if (arrayDate.count == 3)
            {
                NSString *hour  = [NSString stringWithFormat:@"%@ %@", [arrayDate objectAtIndex:1],[arrayDate objectAtIndex:2]];
                lblDate.text    = [NSString stringWithFormat:@"%@ - %@", [arrayDate objectAtIndex:0], hour];
                [lblDate boldSubstring:hour];
            }
            else
            {
                lblDate.text  = event.localDate;
            }
            
            //Tickets Available
            MLPAccessoryBadge *accessoryBadge = [MLPAccessoryBadge new];
            accessoryBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)event.availableTickets];
            
            if ((NSUInteger)event.availableTickets > 0)
            {
                lblTitle.text = event.name;
                accessoryBadge.textColor = [UIColor blackColor];
                accessoryBadge.backgroundColor = [UIColor primeSportColorYellow];
                cell.userInteractionEnabled = YES;
            }
            else
            {
                lblTitle.attributedText = [PSUtils strikethroughStyleForText:event.name];
                accessoryBadge.textColor = [UIColor whiteColor];
                accessoryBadge.backgroundColor = [UIColor primeSportColorRed];
                cell.userInteractionEnabled = NO;
            }
            
            [cell setAccessoryView:accessoryBadge];
        }
        index++;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger index = 0;
    for (ELPEventList *event in self.arrayListPerformer)
    {
        if (index == indexPath.row)
        {
            PSEventTicketsViewController *vcEventTicket = [[PSEventTicketsViewController alloc]initWithId:(NSUInteger)event.eventId];
            vcEventTicket.title = @"Event Details";
            
            [self.navigationController pushViewController:vcEventTicket animated:YES];
            
        }
        index++;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 80.0f;
}

@end
