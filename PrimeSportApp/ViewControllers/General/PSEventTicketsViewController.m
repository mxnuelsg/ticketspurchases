//
//  PSEventTicketsViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 09/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSEventTicketsViewController.h"
#import "PSModelFlow.h"
#import "PSFilterEventTicketsViewController.h"
#import "PSTicketSelectionViewController.h"
#import "PSSeatingChart.h"
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>



@interface PSEventTicketsViewController () <UITableViewDataSource, UITableViewDelegate , UIActionSheetDelegate ,modelFlowDelegate, filterEventTicketsDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UIView *viewInformation;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblEventTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblVenueTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailableTickets;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *myCell;
@property (strong, nonatomic) UIBarButtonItem *btnOptions;

//Variables
@property (assign, nonatomic) NSUInteger identifierEvent;
@property (assign, nonatomic) NSUInteger maximumTicketsToBuy;
@property (assign, nonatomic) float maximumPrice;
@property (assign, nonatomic) float minimumPrice;
@property (assign, nonatomic) BOOL isFilterRequest;

@property(copy, nonatomic) NSString *filterPriceFrom;
@property(copy, nonatomic) NSString *filterPriceTo;
@property(copy, nonatomic) NSString *filterQuantity;
@property(copy, nonatomic) NSString *seatingChart;
@property(copy, nonatomic) NSString *categoryName;
@property(copy, nonatomic) NSString *venueName;
@property(copy, nonatomic) NSString *urlEvent;

@property (strong, nonatomic) NSArray *arrayTickets;

//Model
@property (strong, nonatomic) PSModelFlow *model;

@end

@implementation PSEventTicketsViewController

- (instancetype)initWithId:(NSUInteger)identifier
{
    self = [super init];
    if (self)
    {
        self.identifierEvent    = identifier;
        
        self.model = [[PSModelFlow alloc] init];
        self.model.delegate = self;
        
        self.maximumTicketsToBuy = 0;
        self.filterPriceFrom     = [NSString string];
        self.filterPriceTo       = [NSString string];
        self.filterQuantity      = [NSString string];
        self.isFilterRequest     = NO;
        
        self.minimumPrice = 0.0;
        self.maximumPrice = 0.0;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Parallax effect
    self.lblEventTitle.parallaxIntensity = 10;
    self.lblVenueTitle.parallaxIntensity = 5;
    self.lblDate.parallaxIntensity       = 2;
    
    self.lblAvailableTickets.layer.cornerRadius = 11;
    self.myTableView.exclusiveTouch = YES;
    
    [self loadNavBar];
    [self getEventTickets];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}

- (void)dealloc
{
    NSLog(@"EventTickets dealloc");
    self.model.delegate = nil;
    self.arrayTickets   = nil;
    self.myTableView    = nil;
    self.myCell         = nil;
    self.btnOptions     = nil;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
     self.btnOptions = [[UIBarButtonItem alloc] initWithTitle:@"Options"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(showOptions)];
    
    
    self.navigationItem.rightBarButtonItem = self.btnOptions;
}

-(void)showOptions
{
   UIActionSheet *actionSheetFilter = [[UIActionSheet alloc] initWithTitle:nil
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:@"Reset Filter"
                               otherButtonTitles:@"Filter Tickets", @"Seating Chart",@"Tweet Tickets",@"Share Tickets", nil];
    
    [actionSheetFilter showFromTabBar:self.tabBarController.tabBar];
    
    [[[actionSheetFilter valueForKey:@"_buttons"] objectAtIndex:3] setImage:[UIImage imageNamed:@"iconPostTwitter"] forState:UIControlStateNormal];
    [[[actionSheetFilter valueForKey:@"_buttons"] objectAtIndex:4] setImage:[UIImage imageNamed:@"iconPostFacebook"] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark - FILL INFORMATION
-(void)renderInformationWithEvent:(NSString *)eventName withDate:(NSString *)date withVenue:(NSString *)venue withTickets:(NSUInteger)tickets
{
    self.lblAvailableTickets.text = [NSString stringWithFormat:@"%lu", (unsigned long)tickets];
    self.lblAvailableTickets.adjustsFontSizeToFitWidth = YES;
    
    self.lblVenueTitle.text = venue;
    self.lblVenueTitle.borderColor =  [UIColor blackColor];
	self.lblVenueTitle.borderSize = 1;
    
    if (eventName.length > 0)
    {
        self.lblEventTitle.text = eventName;
        self.lblEventTitle.borderColor =  [UIColor blackColor];
        self.lblEventTitle.borderSize = 1;
    }
    
    if (date.length > 0)
    {
        self.lblDate.text = date;
        self.lblDate.borderColor =  [UIColor blackColor];
        self.lblDate.borderSize = 1;
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getEventTickets
{
    self.navigationItem.hidesBackButton = YES;
    self.btnOptions.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[NSNumber numberWithInteger:self.identifierEvent] forKey:@"EventId"];
    
    //validation
    if (self.filterPriceFrom == nil)
    {
        self.filterPriceFrom  = @"0";
    }
    if (self.filterPriceTo == nil)
    {
        self.filterPriceTo  = @"0";
    }
    if (self.filterQuantity == nil)
    {
        self.filterQuantity  = @"0";
    }

    [parameters setObject:self.filterPriceFrom forKey:@"PriceFrom"];
    [parameters setObject:self.filterPriceTo forKey:@"PriceTo"];
    [parameters setObject:self.filterQuantity forKey:@"Quantity"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kEventTicketsSearch;
    webService.wsParameters = parameters;
    
    [self.model getEventTicketsWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelFlow)
-(void)modelResponseForEventTicketsWithObject:(ETSResultEventTicketsSearch *)result
{
    self.navigationItem.hidesBackButton = NO;
    self.btnOptions.enabled = YES;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self removeEmptyState];
    
    if (result.name != nil && result.localDate != nil && result.tickets != nil && result.venue != nil)
    {
        [self renderInformationWithEvent:result.name
                                withDate:result.localDate
                               withVenue:[NSString stringWithFormat:@"%@, %@", result.venue, result.venueState]
                             withTickets:result.tickets.count];
    }
    
    self.arrayTickets = [NSArray array];
    
    if (result != nil)
    {
        self.seatingChart = result.sectionWiseView;
        self.categoryName = result.urlCategoryName;
        self.venueName    = result.venue;
        self.urlEvent     = result.eventURL;
        
        
        if (result.tickets.count > 0)
        {
            //Fill Table
            self.arrayTickets = result.tickets;
            [self.myTableView reloadData];
            
            /*
             Get maximum quantity to filter selection 
             & 
             Generate Array of Prices
            */
            NSMutableString *stringSplit = [[NSMutableString alloc]init];
            NSMutableArray *arrayPrices = [NSMutableArray array];
            
            for (ETSTickets *ticket in result.tickets)
            {
                //Split Quantity
                [stringSplit appendFormat:@"%@,", ticket.split];
                
                //Price's array
                [arrayPrices addObject:[NSString stringWithFormat:@"%f", ticket.price]];
                
            }

            NSString *finalString;
            finalString =  [NSString stringWithFormat:@"%@", stringSplit];
            
            self.maximumTicketsToBuy = [self getMaximumTicketsToFilterSelectionWithStringSplit:finalString];
            
            // Get Maximum & Minimum Price
            float maxPrice = -MAXFLOAT;
            float minPrice = MAXFLOAT;
            
            for (NSNumber *num in arrayPrices)
            {
                float x = num.floatValue;
                
                if (x < minPrice){minPrice = x;}
                if (x > maxPrice){maxPrice = x;}
            }
            
            self.minimumPrice = minPrice;
            
            if (self.isFilterRequest == NO)
            {
                self.maximumPrice = maxPrice;
            }
        }
        else
        {
            [self showEmptyState];
        }
    }
    else
    {
        [self showEmptyState];
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS (UIActionSheetDelegate)
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            //Clean
            self.filterPriceFrom = [NSString string];
            self.filterPriceTo   = [NSString string];
            self.filterQuantity  = [NSString string];
            self.isFilterRequest = NO;
            
            [self getEventTickets];
        }
            break;
        case 1:
        {
            //Filter
            [self showFilterView];
        }
            break;
        case 2:
        {
            //Map
            [self showVenueMap];
        }
            break;
        case 3:
        {
            //Twitter
            [self postInTwitter];
        }
            break;
        case 4:
        {
            //Facebook
            [self postInFacebook];
        }
            break;
        default:
            break;
    }
}

-(IBAction)showFilterView
{
    if (self.maximumTicketsToBuy > 0 && self.maximumPrice > 0.0)
    {
        PSFilterEventTicketsViewController *vcFilterView = [[PSFilterEventTicketsViewController alloc] initWithMaximumQuantityToSelect:self.maximumTicketsToBuy
                                                                                                                      withMinimumPrice:self.minimumPrice
                                                                                                                      withMaximumPrice:self.maximumPrice];
        vcFilterView.delegate = self;
        vcFilterView.title = @"Filter Tickets";
        
        PSNavigationController *navControllerFilter = [[PSNavigationController alloc] initWithRootViewController:vcFilterView];
        navControllerFilter.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [self.view.window.rootViewController presentViewController:navControllerFilter
                                                          animated:YES
                                                        completion:nil];
        
    }
    else
    {
        [[PSMessages getModel]showAlertMessage:@"This function is not available"
                                     withTitle:kWarningTitle];
    }
}

-(IBAction)showVenueMap
{
    //Remove IF EXIST previous Seating Chart
    for (UIView *view in self.view.subviews)
    {
        if ([[view nextResponder]isKindOfClass:[PSSeatingChart class]])
        {
            PSSeatingChart *venueMap = (PSSeatingChart *)[view nextResponder];
            [venueMap willMoveToParentViewController:nil];
            [venueMap.view removeFromSuperview];
            [venueMap removeFromParentViewController];
        }
    }
    
    //Open SeatingChart ViewController
    if (![self.seatingChart isBlank])
    {
        PSSeatingChart *vcChart = [[PSSeatingChart alloc] initWithUrl:self.seatingChart];
        vcChart.view.frame = CGRectMake(321, 0, vcChart.view.frame.size.width, vcChart.view.frame.size.height);
        [self addChildViewController:vcChart];
        [self.view addSubview:vcChart.view];
        [vcChart didMoveToParentViewController:self];

        //Animation In
        [UIView animateWithDuration:0.2
                         animations:^{
                             vcChart.view.frame = CGRectMake(0, 0, vcChart.view.frame.size.width, vcChart.view.frame.size.height);
                         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1
                              animations:^{}];
         }];
    }
    else
    {
        PSSeatingChart *vcChart = [[PSSeatingChart alloc] initWithUrl:@"backDefaultVenueMap"];
        vcChart.view.frame = CGRectMake(321, 0, vcChart.view.frame.size.width, vcChart.view.frame.size.height);
        [self addChildViewController:vcChart];
        [self.view addSubview:vcChart.view];
        [vcChart didMoveToParentViewController:self];
        
        //Animation In
        [UIView animateWithDuration:0.2
                         animations:^{
                             vcChart.view.frame = CGRectMake(0, 0, vcChart.view.frame.size.width, vcChart.view.frame.size.height);
                         }
                         completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.1
                              animations:^{}];
         }];
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS (PSFilterEventTicketsViewController)
-(void)dataFromFilterWithPriceFrom:(NSString *)filterPriceFrom withPriceTo:(NSString *)filterPriceTo withQuantity:(NSString *)filterQuantity
{
    //Clean
    self.filterPriceFrom = [NSString string];
    self.filterPriceTo   = [NSString string];
    self.filterQuantity  = [NSString string];
    
    //Fill
    self.filterPriceFrom = filterPriceFrom;
    self.filterPriceTo   = filterPriceTo;
    self.filterQuantity  = filterQuantity;
    self.isFilterRequest = YES;
    
    [self getEventTickets];
}

#pragma mark -
#pragma mark - GET MAXIMUM NUMBER OF TICKETS TO SELECT
-(NSUInteger)getMaximumTicketsToFilterSelectionWithStringSplit:(NSString *)stringSplit
{
    stringSplit = [stringSplit substringToIndex:[stringSplit length]-1];
    [stringSplit removeWhiteSpacesFromString];
    
    NSArray *arrayWithSeparatedValues = [stringSplit componentsSeparatedByString:@","];
    
    NSSortDescriptor *sortDescriptor  = [[NSSortDescriptor alloc] initWithKey:@"integerValue" ascending:NO];
    NSArray *sortDescriptors          = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray              = [arrayWithSeparatedValues sortedArrayUsingDescriptors:sortDescriptors];
    
    NSUInteger maximumValue = [[sortedArray objectAtIndex:0] integerValue];
    
    return maximumValue;
}

#pragma mark -
#pragma mark - EMPTY STATE
-(void)showEmptyState
{
    [self removeEmptyState];
    
    self.myTableView.hidden = YES;
    
    //Create Empty State
    PSEmptyView *emptyView = [[PSEmptyView alloc] initWithFrame:CGRectMake(0, self.viewInformation.frame.size.height, self.view.frame.size.width, 150)];
    emptyView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:emptyView];
}

-(void)removeEmptyState
{
    self.myTableView.hidden = NO;
    
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[PSEmptyView class]])
        {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    return self.arrayTickets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSEventTicketCustomCell" owner:self options:nil];
        if ([nib count] > 0)
        {
            cell = self.myCell;
        }
        else
        {
            NSLog(@"PSMessage: failed to load CustomCell nib file!");
        }
    }
    
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorSelectedCell]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    //Assigning elements
    UILabel *lblSection = (UILabel *)[cell viewWithTag:1];
    UILabel *lblRow = (UILabel *)[cell viewWithTag:2];
    UILabel *lblPrice = (UILabel *)[cell viewWithTag:3];

    
     NSUInteger index = 0;
    
    for (ETSTickets *ticket in self.arrayTickets)
    {
        if (index == indexPath.row)
        {
            //Fill cell
            lblSection.text = [NSString stringWithFormat:@"Section %@", ticket.section];
            
            lblRow.text = [NSString stringWithFormat:@"Row %@",ticket.row];
            [lblRow boldSubstring:ticket.row];
            
            lblPrice.text = [PSUtils moneyFormat:[NSString stringWithFormat:@"%f", ticket.price]
                                    withFraction:YES];
            
            //Tickets Available
            MLPAccessoryBadge *accessoryBadge = [MLPAccessoryBadge new];
            accessoryBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)ticket.quantity];
            
            if ((NSUInteger)ticket.quantity > 0)
            {
                accessoryBadge.textColor = [UIColor blackColor];
                accessoryBadge.backgroundColor = [UIColor primeSportColorYellow];
                cell.userInteractionEnabled = YES;
            }
            else
            {
                accessoryBadge.textColor = [UIColor whiteColor];
                accessoryBadge.backgroundColor = [UIColor primeSportColorRed];
                cell.userInteractionEnabled = NO;
            }
            
            [cell setAccessoryView:accessoryBadge];
        }
        index++;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger index = 0;
    
    for (ETSTickets *ticket in self.arrayTickets)
    {
        if (index == indexPath.row)
        {
            PSTicketSelectionViewController *vcTicketSelected = [[PSTicketSelectionViewController alloc]initWithTicket:ticket
                                                                                                             withTitle:self.lblEventTitle.text
                                                                                                             withVenue:self.lblVenueTitle.text
                                                                                                              withDate:self.lblDate.text];
            vcTicketSelected.title = @"Select Your Tickets";
            
            [self.navigationController pushViewController:vcTicketSelected animated:YES];
        }
        index++;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    return 49.0f;
}

#pragma mark -
#pragma mark - SOCIAL NETWORK
-(void)postInFacebook
{
    //  Create an instance of the social Sheet
    SLComposeViewController *socialController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    socialController.completionHandler = ^(SLComposeViewControllerResult result)
    {
        switch(result)
        {
            case SLComposeViewControllerResultCancelled:
                break;
            case SLComposeViewControllerResultDone:
            {
                [[PSMessages getModel] showToastStatusAlertWithMessage:@"Posted on Facebook"
                                                   withBackgroundColor:[UIColor primeSportColorFacebook]
                                                         withTextColor:[UIColor whiteColor]
                                                             withImage:@"iconStatusBar_Success"];
            }
                break;
        }
        
        //  dismiss the Sheet
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:NO completion:^{
                NSLog(@"Tweet Sheet has been dismissed.");
            }];
        });
    };
    
    /* 1 */
    NSString *Event;
    
    if ([self.lblEventTitle.text isBlank]
        || [self.lblEventTitle.text isEqualToString:@". . ."])
    {
        Event = kEmptyField;
    }
    else
    {
        Event = self.lblEventTitle.text;
    }
    
    /* 2 */
    NSString *Venue;
    
    if ([self.venueName isBlank])
    {
        Venue = kEmptyField;
    }
    else
    {
        Venue = self.venueName;
    }
    
    /* 3 */
    NSString *Date;
    
    if ([self.lblDate.text isBlank]
        || [self.lblDate.text isEqualToString:@". . ."])
    {
        Date = kEmptyField;
    }
    else
    {
        Date = self.lblDate.text;
        
        NSArray *arrayDateTime = [Date componentsSeparatedByString: @" "];
        if (arrayDateTime.count == 3)
        {
            NSArray *arrayJustDate = [[arrayDateTime objectAtIndex:0] componentsSeparatedByString: @"/"];
            if (arrayJustDate.count == 3)
            {
                //day, month, year
                NSString *formatDate = [NSString stringWithFormat:@"%@/%@/%@",
                                        [arrayJustDate objectAtIndex:1],
                                        [arrayJustDate objectAtIndex:0],
                                        [arrayJustDate objectAtIndex:2]];
                
                Date = [PSUtils getDayWeekMonthDayYearFormatFromStringDate:formatDate];
            }
            else
            {
                Date = self.lblDate.text;
            }
        }
        else
        {
            Date = self.lblDate.text;
        }
    }
    
    NSString *bodyText = [NSString stringWithFormat:@"Check out these tickets I found to see %@ at %@ on %@ from PrimeSport.com", Event, Venue, Date];
    [socialController setInitialText:bodyText];
    
    //Url
    if (![socialController addURL:[NSURL URLWithString:self.urlEvent]])
    {
        NSLog(@"Unable to add the URL!");
    }
    
    [self.view.window.rootViewController presentViewController:socialController animated:NO completion:nil];
}

-(void)postInTwitter
{
    //  Create an instance of the social Sheet
    SLComposeViewController *socialController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    socialController.completionHandler = ^(SLComposeViewControllerResult result)
    {
        switch(result)
        {
            case SLComposeViewControllerResultCancelled:
                break;
            case SLComposeViewControllerResultDone:
            {
                [[PSMessages getModel] showToastStatusAlertWithMessage:@"Posted on Twitter"
                                                   withBackgroundColor:[UIColor primeSportColorTwitter]
                                                         withTextColor:[UIColor whiteColor]
                                                             withImage:@"iconStatusBar_Success"];
            }
                break;
        }
        
        //  dismiss the Sheet
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:NO completion:^{
                NSLog(@"Tweet Sheet has been dismissed.");
            }];
        });
    };
    
    /* 1 */
    NSString *Event;
    
    if ([self.lblEventTitle.text isBlank]
        || [self.lblEventTitle.text isEqualToString:@". . ."])
    {
        Event = kEmptyField;
    }
    else
    {
        Event = self.lblEventTitle.text;
    }
    
    /* 2 */
    NSString *Venue;
    
    if ([self.venueName isBlank])
    {
        Venue = kEmptyField;
    }
    else
    {
        Venue = self.venueName;
    }
    
    
    NSString *bodyText;
    
    if ([[self.categoryName lowercaseString] isEqualToString:@"concert"] || [[self.categoryName lowercaseString] isEqualToString:@"theater"])
    {
        bodyText = [NSString stringWithFormat:@"Check out these %@ at %@ tickets I found on @PrimeSport", Event, Venue];
    }
    else
    {
        bodyText = [NSString stringWithFormat:@"Check out these %@ tickets I found on @PrimeSport", Event];
    }
    
    [socialController setInitialText:bodyText];
    
    //Url
    if (![socialController addURL:[NSURL URLWithString:self.urlEvent]])
    {
        NSLog(@"Unable to add the URL!");
    }
    
    [self.view.window.rootViewController presentViewController:socialController animated:NO completion:nil];
}

@end
