//
//  PSIntroFavorites.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 15/09/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "MYIntroductionPanel.h"
#import "PSModelFlow.h"


@interface PSIntroFavorites : MYIntroductionPanel <UITableViewDataSource, UITableViewDelegate, modelFlowDelegate>

@property (weak, nonatomic)  IBOutlet UITableView *myTableView;
@property (strong, nonatomic) PSModelFlow *model;
@property (strong, nonatomic) NSMutableArray *arrayFavorites;
@property (strong, nonatomic) NSMutableArray *arrayFavoritesSelected;

@end
