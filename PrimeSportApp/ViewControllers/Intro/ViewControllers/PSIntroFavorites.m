//
//  PSIntroFavorites.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 15/09/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSIntroFavorites.h"
#import "MYBlurIntroductionView.h"

@implementation PSIntroFavorites

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}


#pragma mark -
#pragma mark - INTERACTION METHODS
-(void)panelDidAppear
{
    [self.parentIntroductionView setEnabledGetStarted:YES];
    self.myTableView.layer.cornerRadius = 5;
    
    // Initialization code
    self.model = [[PSModelFlow alloc] init];
    self.model.delegate = self;
    
    self.arrayFavorites = [[NSMutableArray alloc] init];
    self.arrayFavoritesSelected = [[NSMutableArray alloc] init];
    
    [self getPreferredEvents];
}

-(void)panelDidDisappear
{
    [MBProgressHUD hideAllHUDsForView:self animated:YES];
    [self.parentIntroductionView setEnabledGetStarted:NO];
    
    self.model.delegate = nil;
    self.arrayFavorites = nil;
    self.arrayFavoritesSelected = nil;
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getPreferredEvents
{
    [MBProgressHUD showHUDAddedTo:self animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"One" forKey:@"name"];
    [parameters setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kPreferred;
    webService.wsParameters = parameters;
    
    [self.model getPreferredWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelFlow)
-(void)modelResponseForPreferredWithObject:(NSDictionary *)result
{
    [MBProgressHUD hideAllHUDsForView:self animated:YES];
    
    self.arrayFavorites = [NSMutableArray array];
    
    if (result != nil)
    {
        self.arrayFavorites = [result valueForKey:@"Performers"];
    }
    [self.myTableView reloadData];
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    if (self.arrayFavorites.count > 0)
    {
        return self.arrayFavorites.count;
    }
    else
    {
        return 1;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //Color for selected cell
    UIView *viewForSelectedCell = [[UIView alloc]init];
    viewForSelectedCell.layer.backgroundColor = [[UIColor primeSportColorCellBlue]CGColor];
    cell.selectedBackgroundView = viewForSelectedCell;
    
    if (self.arrayFavorites.count > 0)
    {
        // Configure the cell...
        cell.tintColor               = [UIColor primeSportColorNavigationController];
        cell.textLabel.textColor     = [UIColor primeSportColorCellText];
        cell.textLabel.font          = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.userInteractionEnabled  = YES;
        cell.textLabel.text          = [[[self.arrayFavorites objectAtIndex:indexPath.row] valueForKey:@"Name"] capitalizedString];
        
        if ([self.arrayFavoritesSelected containsObject:[self.arrayFavorites objectAtIndex:indexPath.row]])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else
    {
        // Configure the cell...
        cell.tintColor               = [UIColor primeSportColorNavigationController];
        cell.textLabel.textColor     = [UIColor darkGrayColor];
        cell.textLabel.font          = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
        cell.userInteractionEnabled  = NO;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.text          = @"- Data Not Found - ";
        
    }
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType != UITableViewCellAccessoryCheckmark)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.arrayFavoritesSelected addObject:[self.arrayFavorites objectAtIndex:indexPath.row]];
        
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.arrayFavoritesSelected removeObject:[self.arrayFavorites objectAtIndex:indexPath.row]];
    }
}

@end
