//
//  PSShippingDetailViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface PSShippingDetailViewController : UIViewController

- (IBAction)useBillingInfoSwitch:(id)sender;
-(IBAction)toolbarButtonBack;
-(IBAction)toolbarButtonNext;

@end
