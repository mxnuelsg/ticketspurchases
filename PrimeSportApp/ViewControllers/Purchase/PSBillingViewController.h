//
//  PSBillingViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 25/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSBillingViewController : UIViewController

-(IBAction)toolbarButtonBack;
-(IBAction)toolbarButtonNext;

@end
