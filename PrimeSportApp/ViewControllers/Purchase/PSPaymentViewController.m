//
//  PSPaymentViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSPaymentViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PSBillingViewController.h"

//Credit Cards Available
static int const kAMERICAN_EXPRESS  =   1;
static int const kVISA              =   2;
static int const kMASTER_CARD       =   3;
static int const kDISCOVERY_NETWORK =   4;

//Fields limit
static int const kEXPIRATION_LIMIT = 5;

static int const kNUMBER_CARD_LIMIT_4_AMERICAN_EXPRESS                  = 15;
static int const kCARD_CODE_LIMIT_4_AMERICAN_EXPRESS                    = 4;
static int const kNUMBER_CARD_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK  = 16;
static int const kCARD_CODE_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK    = 3;


@interface PSPaymentViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
{
    NSString *componentMonth;
    NSString *componentYear;
}

@property (weak, nonatomic) IBOutlet UIToolbar *myToolbar;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSection;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRow;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSubtotal;
@property (strong, nonatomic) UIBarButtonItem *btnBilling;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarNext;

@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCreditCard;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCardNumber;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCardExpiration;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCardCode;

@property (strong, nonatomic) UIPickerView *pickerCreditCards;
@property (strong, nonatomic)UIPickerView *pickerExpiration;

@property (assign, nonatomic) NSUInteger typeOfCreditCard;
@property (assign, nonatomic) BOOL isCreditCardActive;
@property (strong, nonatomic) NSArray *arrayCreditCards;
@property (strong, nonatomic) NSArray *arrayMonth;
@property (strong, nonatomic) NSArray *arrayYear;


@end

@implementation PSPaymentViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.arrayCreditCards   = [NSArray array];
        self.arrayMonth         = [NSArray array];
        self.arrayYear          = [NSArray array];
        self.isCreditCardActive = YES;
        
        componentMonth = @"00";
        componentYear  = @"00";
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lblHeaderQuantity.layer.cornerRadius = 11;
    
    [self loadNavBar];
    [self loadToolbar];
    [self fillViewInformation];
    [self loadPickerView];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

-(void)viewDidLayoutSubviews
{
    [self.myScrollView autosize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)dealloc
{
    NSLog(@"Payment dealloc");
    
    self.arrayCreditCards  = nil;
    self.arrayMonth        = nil;
    self.arrayYear         = nil;
    self.pickerCreditCards = nil;
    self.pickerExpiration  = nil;
    self.myScrollView      = nil;
    self.btnBilling        = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnBilling.enabled = NO;
    self.btnToolbarBack.enabled = NO;
    self.btnToolbarNext.enabled = NO;
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    [self.myScrollView scrollToTopAnimated:YES];
    self.btnBilling.enabled = YES;
    self.btnToolbarBack.enabled = YES;
    self.btnToolbarNext.enabled = YES;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    self.btnBilling = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(goBilling)];
    
    self.btnBilling.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = self.btnBilling;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)goBilling
{
    [self.view endEditing:YES];
    
    if ([self.txtCreditCard.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCreditCardType
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCardNumber.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCreditCardNumber
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCardExpiration.text isEqualToString:@"00/00"]
             || [componentMonth isEqualToString:@"00"] || [componentYear isEqualToString:@"00"])
    {
        [[PSMessages getModel]showAlertMessage:kValidationExpirationCreditCard
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCardCode.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCreditCardCode
                                     withTitle:kWarningTitle];
    }
    else
    {
        if (self.typeOfCreditCard == kAMERICAN_EXPRESS)
        {
            if (self.txtCardNumber.text.length != kNUMBER_CARD_LIMIT_4_AMERICAN_EXPRESS)
            {
                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"Your Credit Card Number is wrong!\n(You have %lu of %i digits)",
                                                        (unsigned long)self.txtCardNumber.text.length,
                                                        kNUMBER_CARD_LIMIT_4_AMERICAN_EXPRESS]
                                             withTitle:kWarningTitle];
                return;
            }
            
            if (self.txtCardCode.text.length != kCARD_CODE_LIMIT_4_AMERICAN_EXPRESS)
            {
                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"Your Credit Card Code is wrong!\n(You have %lu of %i digits)",
                                                        (unsigned long)self.txtCardCode.text.length,
                                                        kCARD_CODE_LIMIT_4_AMERICAN_EXPRESS]
                                             withTitle:kWarningTitle];
                return;
            }
        }
        else
        {
            if (self.txtCardNumber.text.length != kNUMBER_CARD_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK)
            {
                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"Your Credit Card Number is wrong!\n(You have %lu of %i digits)",
                                                        (unsigned long)self.txtCardNumber.text.length,
                                                        kNUMBER_CARD_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK]
                                             withTitle:kWarningTitle];
                return;
            }
            
            if (self.txtCardCode.text.length != kCARD_CODE_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK)
            {
                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"Your Credit Card Code is wrong!\n(You have %lu of %i digits)",
                                                        (unsigned long)self.txtCardCode.text.length,
                                                        kCARD_CODE_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK]
                                               withTitle:kWarningTitle];
                return;
            }
        }
        
        [PSTicket information].tcktEventCreditCardType   = self.txtCreditCard.text;
        [PSTicket information].tcktEventCreditCardNumber = self.txtCardNumber.text;
        [PSTicket information].tcktEventCreditCardExpirationDate = [self.txtCardExpiration.text removeWhiteSpacesFromString];
        [PSTicket information].tcktEventCreditCardSecurityCode = self.txtCardCode.text;
        
        //call view controller
        PSBillingViewController *vcBilling = [[PSBillingViewController alloc]init];
        vcBilling.title = @"Billing";
        
        [self.navigationController pushViewController:vcBilling animated:YES];
    }
}

#pragma mark -
#pragma mark - TOOL BAR CONFIGURATION
-(void)loadToolbar
{
    self.myToolbar.barStyle     = UIBarStyleBlackOpaque;
    self.myToolbar.barTintColor = [UIColor primeSportColorToolBar];
    [self.myToolbar sizeToFit];
}

-(IBAction)toolbarButtonBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)toolbarButtonNext
{
    [self goBilling];
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
    //Header
    self.lblHeaderSection.text = [NSString stringWithFormat:@"Section %@", [PSTicket information].tcktEventSection];
    
    self.lblHeaderRow.text     = [NSString stringWithFormat:@"Row %@", [PSTicket information].tcktEventRow];
    [self.lblHeaderRow boldSubstring:[PSTicket information].tcktEventRow];
    
    self.lblHeaderQuantity.text = [PSTicket information].tcktEventQuantity;
    self.lblHeaderQuantity.adjustsFontSizeToFitWidth = YES;
    
    self.lblHeaderPrice.text = [NSString stringWithFormat:@"Price %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPrice withFraction:YES]];
    
    self.lblHeaderSubtotal.text = [NSString stringWithFormat:@"Sub Total %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
    [self.lblHeaderSubtotal boldSubstring:[PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
    
    //Body Pickers
    self.arrayCreditCards = [[NSArray alloc] initWithObjects:
                             @"AmericanExpress",
                             @"Visa",
                             @"MasterCard",
                             @"DiscoveryNetwork",
                             nil];
    
    self.arrayMonth = [[NSArray alloc] initWithObjects:
                       @"01",
                       @"02",
                       @"03",
                       @"04",
                       @"05",
                       @"06",
                       @"07",
                       @"08",
                       @"09",
                       @"10",
                       @"11",
                       @"12",
                       nil];
    
    
    NSCalendar *calendarGregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *today                 = [NSDate date];
    NSDateComponents *components  = [calendarGregorian components:NSYearCalendarUnit fromDate:today];
    int year                      = (int)[components year];
    
    NSString *yearFormat;
    NSMutableArray *arrayYears = [NSMutableArray array];
    
    for (int index = year; index < year + 20; index++)
    {
        yearFormat = [NSString stringWithFormat:@"%i", index];
        [arrayYears addObject:yearFormat];
    }
    
    self.arrayYear = arrayYears;
    
    [self pickerView:self.pickerCreditCards didSelectRow:0 inComponent:0];
}

#pragma mark -
#pragma mark - PICKER VIEW & TOOLBAR CONFIGURATION
-(void)loadPickerView
{
    //Toolbar for fields
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done"
                                                               style:UIBarButtonItemStyleDone
                                                              target:self
                                                              action:@selector(hideKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    UISegmentedControl *segmentedNavigation = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"❮ Previous Field", @"Next Field ❯", nil]];
    segmentedNavigation.momentary = YES;
    segmentedNavigation.tintColor = [UIColor groupTableViewBackgroundColor];
    [segmentedNavigation addTarget:self action:@selector(segmentedControlHandler:) forControlEvents:UIControlEventValueChanged];
    
     UIBarButtonItem *btnSegment = [[UIBarButtonItem alloc]initWithCustomView:segmentedNavigation];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                             btnSegment,
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil],
                             btnDone,
                             nil];
    [toolBarKeyboard sizeToFit];
    
    self.txtCreditCard.inputAccessoryView      =  toolBarKeyboard;
    self.txtCardExpiration.inputAccessoryView  =  toolBarKeyboard;
    self.txtCardCode.inputAccessoryView        =  toolBarKeyboard;
    self.txtCardNumber.inputAccessoryView      = toolBarKeyboard;
    
    //Pickers
    self.pickerCreditCards = [[UIPickerView alloc]init];
    self.pickerCreditCards.delegate = self;
    self.pickerCreditCards.dataSource = self;
    self.pickerCreditCards.backgroundColor = [UIColor whiteColor];
    self.pickerCreditCards.showsSelectionIndicator = YES;
    self.pickerCreditCards.multipleTouchEnabled = NO;
    self.pickerCreditCards.exclusiveTouch = YES;

    self.pickerExpiration = [[UIPickerView alloc]init];
    self.pickerExpiration.delegate = self;
    self.pickerExpiration.dataSource = self;
    self.pickerExpiration.backgroundColor = [UIColor whiteColor];
    self.pickerExpiration.showsSelectionIndicator = YES;
    self.pickerExpiration.multipleTouchEnabled = NO;
    self.pickerExpiration.exclusiveTouch = YES;


    self.txtCreditCard.inputView     = self.pickerCreditCards;
    self.txtCardExpiration.inputView = self.pickerExpiration;
}

- (void)segmentedControlHandler:(id)sender
{
    UISegmentedControl *mySegmented = (UISegmentedControl *)sender;
    
    switch ([mySegmented selectedSegmentIndex])
    {
        case 0:
            if (self.txtCreditCard.isEditing == YES)
            {
               [self.txtCardCode becomeFirstResponder];
            }
            else if (self.txtCardNumber.isEditing == YES)
            {
                [self.txtCreditCard becomeFirstResponder];
            }
            else if (self.txtCardExpiration.isEditing == YES)
            {
                [self.txtCardNumber becomeFirstResponder];
            }
            else if (self.txtCardCode.isEditing == YES)
            {
                [self.txtCardExpiration becomeFirstResponder];
            }
            break;
        case 1:
            
            if (self.txtCreditCard.isEditing == YES)
            {
                [self.txtCardNumber becomeFirstResponder];
            }
            else if (self.txtCardNumber.isEditing == YES)
            {
                [self.txtCardExpiration becomeFirstResponder];
            }
            else if (self.txtCardExpiration.isEditing == YES)
            {
                [self.txtCardCode becomeFirstResponder];
            }
            else if (self.txtCardCode.isEditing == YES)
            {
                [self.txtCreditCard becomeFirstResponder];
            }
            break;
        default:
            break;
    }
}

-(void)hideKeyboard
{
    [self.txtCreditCard resignFirstResponder];
    [self.txtCardExpiration resignFirstResponder];
    [self.txtCardNumber resignFirstResponder];
    [self.txtCardCode resignFirstResponder];
}


#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.txtCreditCard)
    {
        self.isCreditCardActive = YES;
    }
    else
    {
       self.isCreditCardActive = NO;
    }
    
    if (textField == self.txtCardExpiration && self.isCreditCardActive == NO)
    {
        [self.pickerExpiration selectRow:0 inComponent:0 animated:YES];
        [self.pickerExpiration selectRow:0 inComponent:1 animated:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtCardNumber || textField == self.txtCardCode)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtCardNumber)
    {
        if ([string isEqualToString:@" "] )
        {
            return NO;
        }
        
        if (self.typeOfCreditCard == kAMERICAN_EXPRESS)
        {
            if ([textField.text length] >= kNUMBER_CARD_LIMIT_4_AMERICAN_EXPRESS && range.length == 0)
            {
                return NO;
            }
        }
        else
        {
            if ([textField.text length] >= kNUMBER_CARD_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK && range.length == 0)
            {
                return NO;
            }
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];

    }
    
    if (textField == self.txtCardCode)
    {
        if ([string isEqualToString:@" "] )
        {
            return NO;
        }
        
        if (self.typeOfCreditCard == kAMERICAN_EXPRESS)
        {
            if ([textField.text length] >= kCARD_CODE_LIMIT_4_AMERICAN_EXPRESS && range.length == 0)
            {
                return NO;
            }
        }
        else
        {
            if ([textField.text length] >= kCARD_CODE_LIMIT_4_VISA_MASTERCARD_DISCOVERYNETWORK && range.length == 0)
            {
                return NO;
            }
        }
    }
    
    if (textField == self.txtCardExpiration)
    {
        if ([textField.text length] >= kEXPIRATION_LIMIT && range.length == 0)
        {
            return NO;
        }
    }
    
    if (textField == self.txtCreditCard)
    {
        return NO;
    }

    return YES;
}

#pragma mark -
#pragma mark - DATASOURCE UIPICKERVIEW
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (self.isCreditCardActive == YES)
    {
        return 1;
    }
    else
    {
        return  2;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.isCreditCardActive == YES)
    {
        return self.arrayCreditCards.count;
    }
    else
    {
        if(component == 0)
        {
            return self.arrayMonth.count;
        }
        else
        {
            return self.arrayYear.count;
        }

    }
}

#pragma mark -
#pragma mark - DELEGATE UIPICKERVIEW
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;

    if (self.isCreditCardActive == YES)
    {
        title = [self.arrayCreditCards objectAtIndex:row];
    }
    else
    {
        if(component == 0)
        {
            title = [self.arrayMonth objectAtIndex:row];
        }
        else
        {
            title = [self.arrayYear objectAtIndex:row];
        }
    }
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.isCreditCardActive == YES)
    {
        self.txtCreditCard.text = [self.arrayCreditCards objectAtIndex:row];
        
        switch (row)
        {
            case 0:
                self.typeOfCreditCard = kAMERICAN_EXPRESS;
                break;
            case 1:
                self.typeOfCreditCard = kVISA;
                break;
            case 2:
                self.typeOfCreditCard = kMASTER_CARD;
                break;
            case 3:
                self.typeOfCreditCard = kDISCOVERY_NETWORK;
                break;
            default:
                self.typeOfCreditCard = 0;
                break;
        }
        
        //clean
        self.txtCardNumber.text = [NSString string];
        self.txtCardCode.text   = [NSString string];
        
        componentMonth = @"00";
        componentYear  = @"00";
        self.txtCardExpiration.text = [NSString stringWithFormat:@"%@/%@", componentMonth, componentYear];
    }
    else
    {
        if(component == 0)
        {
            componentMonth = [self.arrayMonth objectAtIndex:row];
        }
        else
        {
            componentYear = [[NSString stringWithFormat:@"%@", [self.arrayYear objectAtIndex:row]] substringWithRange:NSMakeRange(2, 2)];
        }
        
        self.txtCardExpiration.text = [NSString stringWithFormat:@"%@/%@", componentMonth, componentYear];
    }
}



@end
