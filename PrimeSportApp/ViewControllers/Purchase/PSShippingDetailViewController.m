//
//  PSShippingDetailViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 26/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSShippingDetailViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PSModelPurchaseFlow.h"
#import "PSConfirmationViewController.h"
#import "PSGenericModals.h"


@interface PSShippingDetailViewController () <UITextFieldDelegate, modelPurchaseFlowDelegate, modalCountriesDelegate, modalStatesDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UIToolbar *myToolbar;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarNext;
@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSection;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRow;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSubtotal;
@property (strong, nonatomic) UIBarButtonItem *btnCheckOut;

//ViewControllers
@property (strong, nonatomic) PSModalCountriesViewController *vcModalCountries;
@property (strong, nonatomic) PSModalStatesViewController *vcModalStates;

//Textfields
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress1;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress2;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCountry;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtState;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCity;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtZipCode;

//UIImageViews (icons)
@property (weak, nonatomic) IBOutlet UIImageView *icon1;
@property (weak, nonatomic) IBOutlet UIImageView *icon2;
@property (weak, nonatomic) IBOutlet UIImageView *icon3;
@property (weak, nonatomic) IBOutlet UIImageView *icon4;
@property (weak, nonatomic) IBOutlet UIImageView *icon5;
@property (weak, nonatomic) IBOutlet UIImageView *icon6;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayCountries;

//Model
@property (strong, nonatomic) PSModelPurchaseFlow *model;

@end

@implementation PSShippingDetailViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPurchaseFlow alloc] init];
        self.model.delegate = self;
        
        self.arrayCountries    = [NSMutableArray array];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lblHeaderQuantity.layer.cornerRadius = 11;
    [self.myScrollView contentSizeToFit];
    
    [self chargeTextfieldIcons];
    [self loadNavBar];
    [self loadToolbar];
    [self loadPickerView];
    [self fillViewInformation];
    [self lookingForPreviousData];
    [self getShippingCountries];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Shipping Detail dealloc");
    
    self.myScrollView    = nil;
    self.model.delegate  = nil;
    self.arrayCountries  = nil;
    self.btnCheckOut     = nil;
    self.vcModalCountries   = nil;
    self.vcModalStates      = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnCheckOut.enabled = NO;
    self.btnToolbarBack.enabled = NO;
    self.btnToolbarNext.enabled = NO;
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    [self.myScrollView scrollToTopAnimated:YES];
    
    self.btnCheckOut.enabled = YES;
    self.btnToolbarBack.enabled = YES;
    self.btnToolbarNext.enabled = YES;
}

#pragma mark -
#pragma mark - TEXTFIELD ICONS
-(void)chargeTextfieldIcons
{
    self.txtAddress1.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress1.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress2.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress2.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCity.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCity"]];
    self.txtCity.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCountry.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCountry"]];
    self.txtCountry.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtState.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconState"]];
    self.txtState.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtZipCode.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconZipcode"]];
    self.txtZipCode.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    self.btnCheckOut = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                        style:UIBarButtonItemStyleBordered
                                                       target:self
                                                       action:@selector(goCheckOut)];
    
    self.btnCheckOut.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = self.btnCheckOut;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)goCheckOut
{
    [self.view endEditing:YES];
    
    //validacioones
    if ([self.txtAddress1.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationAddress
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCountry.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCountry
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtState.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationState
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCity.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCitySelection
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtZipCode.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                     withTitle:kWarningTitle];
    }
    else
    {
        //Optional Field
        if ([self.txtAddress2.text isBlank])
        {
            [PSTicket information].shippingStorageAddress2 = kEmptyField;
        }
        
        PSConfirmationViewController *vcConfirmation = [[PSConfirmationViewController alloc]init];
        vcConfirmation.title = @"Confirmation";
        
        [self.navigationController pushViewController:vcConfirmation animated:YES];
    }
    
}

#pragma mark -
#pragma mark - TOOL BAR CONFIGURATION
-(void)loadToolbar
{
    self.myToolbar.barStyle     = UIBarStyleBlackOpaque;
    self.myToolbar.barTintColor = [UIColor primeSportColorToolBar];
    self.myToolbar.translucent  = NO;
    [self.myToolbar sizeToFit];
}

-(IBAction)toolbarButtonBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)toolbarButtonNext
{
    [self goCheckOut];
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
    //Header
    self.lblHeaderSection.text = [NSString stringWithFormat:@"Section %@", [PSTicket information].tcktEventSection];
    
    self.lblHeaderRow.text     = [NSString stringWithFormat:@"Row %@", [PSTicket information].tcktEventRow];
    [self.lblHeaderRow boldSubstring:[PSTicket information].tcktEventRow];
    
    self.lblHeaderQuantity.text = [PSTicket information].tcktEventQuantity;
    self.lblHeaderQuantity.adjustsFontSizeToFitWidth = YES;
    
    self.lblHeaderPrice.text = [NSString stringWithFormat:@"Price %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPrice withFraction:YES]];
    
    self.lblHeaderSubtotal.text = [NSString stringWithFormat:@"Sub Total %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
    
    [self.lblHeaderSubtotal boldSubstring:[PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity
                                                  withFraction:YES]];
}

#pragma mark -
#pragma mark - SWITCH ACTION
- (IBAction)useBillingInfoSwitch:(id)sender
{
    self.mySwitch = sender;
    
    if(self.mySwitch.isOn == YES)
    {
        NSLog(@"Switch is ON");
        [self dataByBilling];
    }
    else
    {
        NSLog(@"Switch is OFF");
        [self dataByUser];
    }
}

#pragma mark -
#pragma mark - EDIT MODE ON/OFF
-(void)dataByBilling
{
    //hide icons
    self.icon1.hidden  = YES;
    self.icon2.hidden  = YES;
    self.icon3.hidden  = YES;
    self.icon4.hidden  = YES;
    self.icon5.hidden  = YES;
    self.icon6.hidden  = YES;
    
    //Load info
    self.txtAddress1.text = [PSTicket information].tcktEventShippingInfoAddress1;
    self.txtAddress2.text = [PSTicket information].tcktEventShippingInfoAddress2;
    self.txtCity.text     = [PSTicket information].tcktEventShippingInfoCity;
    self.txtZipCode.text  = [PSTicket information].tcktEventShippingInfoZipCode;
    self.txtCountry.text  = [PSTicket information].tcktEventShippingInfoCountry;
    self.txtState.text    = [PSTicket information].tcktEventShippingInfoState;
    
    //Disable fields
    self.txtAddress1.enabled = NO;
    self.txtAddress2.enabled = NO;
    self.txtCity.enabled     = NO;
    self.txtZipCode.enabled  = NO;
    self.txtCountry.enabled  = NO;
    self.txtState.enabled    = NO;
    
    self.txtAddress1.textColor  = [UIColor grayColor];
    self.txtAddress1.textColor  = [UIColor grayColor];
    self.txtAddress2.textColor  = [UIColor grayColor];
    self.txtCity.textColor      = [UIColor grayColor];
    self.txtZipCode.textColor   = [UIColor grayColor];
    self.txtCountry.textColor   = [UIColor grayColor];
    self.txtState.textColor     = [UIColor grayColor];

    //Repeat info
    [[PSTicket information] useBillingInfo];
}

-(void)dataByUser
{
    //hide icons
    self.icon1.hidden  = NO;
    self.icon2.hidden  = NO;
    self.icon3.hidden  = NO;
    self.icon4.hidden  = NO;
    self.icon5.hidden  = NO;
    self.icon6.hidden  = NO;
    
    //Clean info
    self.txtAddress1.text = [NSString string];
    self.txtAddress2.text = [NSString string];
    self.txtCity.text     = [NSString string];
    self.txtZipCode.text  = [NSString string];
    self.txtCountry.text  = [NSString string];
    self.txtState.text    = [NSString string];
    
    //Enable fields
    self.txtAddress1.enabled = YES;
    self.txtAddress2.enabled = YES;
    self.txtCity.enabled     = YES;
    self.txtZipCode.enabled  = YES;
    self.txtCountry.enabled  = YES;
    self.txtState.enabled    = YES;
    
    self.txtAddress1.textColor  = [UIColor primeSportColorNavigationController];
    self.txtAddress1.textColor  = [UIColor primeSportColorNavigationController];
    self.txtAddress2.textColor  = [UIColor primeSportColorNavigationController];
    self.txtCity.textColor      = [UIColor primeSportColorNavigationController];
    self.txtZipCode.textColor   = [UIColor primeSportColorNavigationController];
    self.txtCountry.textColor   = [UIColor primeSportColorNavigationController];
    self.txtState.textColor     = [UIColor primeSportColorNavigationController];
    
    //clean info Repeated
    [[PSTicket information] resetShippingInfo];
}

#pragma mark -
#pragma mark - PICKER VIEW & TOOLBAR CONFIGURATION
-(void)loadPickerView
{
    //Toolbar No.1 for fields
    UIToolbar *toolBarKeyboardClean = [[UIToolbar alloc]init];
    toolBarKeyboardClean.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardClean.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnDoneClean = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneClean.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardClean.items = [NSArray arrayWithObjects:
                                  [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                  btnDoneClean,
                                  nil];
    [toolBarKeyboardClean sizeToFit];
    
    //Toolbar No.2 for Refresh
    UIToolbar *toolBarKeyboardWithRefresh = [[UIToolbar alloc]init];
    toolBarKeyboardWithRefresh.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardWithRefresh.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Refresh" style:UIBarButtonItemStyleDone target:self action:@selector(refreshDataInputView)];
    btnRefresh.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *btnDoneRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneRefresh.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardWithRefresh.items = [NSArray arrayWithObjects:
                                        btnRefresh,
                                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                        btnDoneRefresh,
                                        nil];
    [toolBarKeyboardWithRefresh sizeToFit];

    
    self.txtAddress1.inputAccessoryView     = toolBarKeyboardClean;
    self.txtAddress2.inputAccessoryView     = toolBarKeyboardClean;
    self.txtCity.inputAccessoryView         = toolBarKeyboardClean;
    self.txtZipCode.inputAccessoryView      = toolBarKeyboardClean;
    
    self.txtCountry.inputAccessoryView      = toolBarKeyboardWithRefresh;
    self.txtState.inputAccessoryView        = toolBarKeyboardWithRefresh;
}

-(void)hideKeyboard
{
    [self.txtAddress1 resignFirstResponder];
    [self.txtAddress2 resignFirstResponder];
    [self.txtCountry resignFirstResponder];
    [self.txtState resignFirstResponder];
    [self.txtCity resignFirstResponder];
    [self.txtZipCode resignFirstResponder];
}

-(void)refreshDataInputView
{
    if ([self.txtCountry isEditing])
    {
        [self.vcModalCountries getAllCountries];
    }
    else if ([self.txtState isEditing])
    {
        [self.vcModalStates getAllStates];
    }
}

#pragma mark -
#pragma mark - CHECK INFORMATION AVAILABLE
-(void)lookingForPreviousData
{
    if ([PSTicket information].isUsingBillingInfoInShipping == NO)
    {
        if (![[PSTicket information].shippingStorageAddress1 isBlank])
        {
            self.txtAddress1.text = [PSTicket information].shippingStorageAddress1;
        }
        
        if (![[PSTicket information].shippingStorageAddress2 isBlank])
        {
            self.txtAddress2.text = [PSTicket information].shippingStorageAddress2;
        }
        
        if (![[PSTicket information].shippingStorageCity isBlank])
        {
            self.txtCity.text = [PSTicket information].shippingStorageCity;
        }
        
        if (![[PSTicket information].shippingStorageZipCode isBlank])
        {
            self.txtZipCode.text = [PSTicket information].shippingStorageZipCode;
        }
        
        if (![[PSTicket information].shippingStorageCountry isBlank])
        {
            self.txtCountry.text = [PSTicket information].shippingStorageCountry;
        }
        
        if (![[PSTicket information].shippingStorageState isBlank])
        {
            self.txtState.text = [PSTicket information].shippingStorageState;
        }
    }
    else
    {
        //Automatic Switch ON
        self.mySwitch.on = YES;
        [self useBillingInfoSwitch: self.mySwitch];
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getShippingCountries
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSTicket information].tcktEventShippingMethodId forKey:@"ShippingMethodId"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kShippingCountries;
    webService.wsParameters = parameters;
    
    [self.model getShippingMethodCountriesWithObject:webService];
}

-(void)getZipCodeInfo
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.txtZipCode.text forKey:@"zipcode"];
    [parameters setObject:[PSTicket information].shippingStorageCountryId forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetZipCode;
    webService.wsParameters = parameters;
    
    [self.model getZipCodeWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelPurchaseFlow)
-(void)modelResponseForShippingMethodCountries:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        for (id key in result)
        {
            [self.arrayCountries addObject:key];
        }
        
        //Is available to use billing info?
        if (self.arrayCountries.count > 0)
        {
            NSInteger billingCountryId = [[PSTicket information].tcktEventShippingInfoCountryId integerValue];
            NSArray *arrayWithAllCountryIds = [self.arrayCountries valueForKey:@"CountryId"];
            
            if ([arrayWithAllCountryIds containsObject:[NSNumber numberWithInteger:billingCountryId]])
            {
                self.mySwitch.enabled = YES;
            }
            else
            {
                self.mySwitch.enabled = NO;
            }
        }
    }
}

-(void)modelResponseForZipCode:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        self.txtState.text  = [[result valueForKey:@"StateName"] capitalizedString];
        [PSTicket information].shippingStorageState = self.txtState.text;
        [PSTicket information].shippingStorageStateId = [result valueForKey:@"StateId"];
        
        [self textFieldDidBeginEditing:self.txtCity];
        self.txtCity.text  = [[result valueForKey:@"City"] capitalizedString];
        [self textFieldDidEndEditing:self.txtCity];
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS MODALS
/* (UIModalCountriesViewController) */
-(void)selectedCountryInformation:(NSDictionary *)countryInformation
{
    [self.txtCountry resignFirstResponder];
    
    self.txtCountry.text = [[countryInformation valueForKey:@"CountryName"] capitalizedString];
    [PSTicket information].shippingStorageCountry = self.txtCountry.text;
    [PSTicket information].shippingStorageCountryId = [countryInformation valueForKey:@"CountryId"];

    //Clean Fields
    self.txtZipCode.text = [NSString string];
    [PSTicket information].shippingStorageZipCode = self.txtZipCode.text;
    
    [self textFieldDidBeginEditing:self.txtCity];
    self.txtCity.text = [NSString string];
    [self textFieldDidEndEditing:self.txtCity];
    
    self.txtState.text = [NSString string];
    [PSTicket information].shippingStorageState = self.txtState.text;
    [PSTicket information].shippingStorageStateId = @"0";
}

/* (UIModalStatesViewController) */
-(void)selectedStateInformation:(NSDictionary *)stateInformation
{
    [self.txtState resignFirstResponder];
    
    self.txtState.text = [[stateInformation valueForKey:@"StateName"] capitalizedString];
    
    [PSTicket information].shippingStorageState = self.txtState.text;
    [PSTicket information].shippingStorageStateId = [stateInformation valueForKey:@"StateId"];
    
    [self textFieldDidBeginEditing:self.txtCity];
    self.txtCity.text = [NSString string];
    [self textFieldDidEndEditing:self.txtCity];
}

#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.txtCountry)
    {
        self.vcModalCountries = [[PSModalCountriesViewController alloc]initWithShippingMethodId:[PSTicket information].tcktEventShippingMethodId];
        self.vcModalCountries.delegate = self;
        self.vcModalCountries.view.frame = CGRectMake(0, 0, 320, 190);
        self.txtCountry.inputView = self.vcModalCountries.view;
    }
    else if (textField == self.txtState)
    {
        if ([self.txtCountry.text isBlank])
        {
            [textField resignFirstResponder];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            self.vcModalStates = [[PSModalStatesViewController alloc]initWithCountryId:[[PSTicket information].shippingStorageCountryId integerValue]];
            self.vcModalStates.delegate = self;
            self.vcModalStates.view.frame = CGRectMake(0, 0, 320, 190);
            self.txtState.inputView = self.vcModalStates.view;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.txtAddress1)
    {
        [PSTicket information].shippingStorageAddress1 = textField.text;
    }
    if (textField == self.txtAddress2)
    {
        [PSTicket information].shippingStorageAddress2 = textField.text;
    }
    if (textField == self.txtCity)
    {
        [PSTicket information].shippingStorageCity = textField.text;
    }
    
    if (textField == self.txtZipCode)
    {
        if ([textField.text isBlank])
        {
            [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                         withTitle:kWarningTitle];
        }
        else if (textField.text.length < 5)
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationZipCodeLength
                                         withTitle:kWarningTitle];
        }
        else if ([self.txtCountry.text isBlank])
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            if (textField == self.txtZipCode)
            {
                [PSTicket information].shippingStorageZipCode = textField.text;
                [self getZipCodeInfo];
            }
        }
    }

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtZipCode)
    {
        if ([string isEqualToString:@" "] )
        {
            
            return NO;
        }
        if ([textField.text length] >= kZIP_CODE_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtCountry || textField == self.txtState)
    {
        return NO;
    }
    
    return YES;
}

@end
