//
//  PSShippingViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface PSShippingViewController : UIViewController

-(IBAction)toolbarButtonNext;
-(IBAction)enterCouponCode;

@end
