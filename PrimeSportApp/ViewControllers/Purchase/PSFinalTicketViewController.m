//
//  PSFinalTicketViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 03/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSFinalTicketViewController.h"

@interface PSFinalTicketViewController () <UITableViewDataSource, UITableViewDelegate>

//* * * * * Header
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblHeaderEventTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblHeaderVenueTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblHeaderDate;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSection;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRow;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderPrice;

//* * * * * Body
@property (copy, nonatomic) NSString *dataTotal;
@property (copy, nonatomic) NSString *dataServiceFee;
@property (copy, nonatomic) NSNumber *dataDiscount;
@property (copy, nonatomic) NSNumber *dataOrderId;
@property (copy, nonatomic) NSNumber *dataShippingCost;
@property (copy, nonatomic) NSNumber *dataSubtotal;
@property (copy, nonatomic) NSNumber *dataTax;

@property (strong, nonatomic) NSArray *arrayTicket;
@property (strong, nonatomic) NSMutableDictionary *dictInfo;
@property (strong, nonatomic) NSMutableDictionary *dictKeys;
@property (strong, nonatomic) NSMutableDictionary *dictValues;
@property (assign, nonatomic) BOOL thereIsServiceFee;
@property (assign, nonatomic) BOOL thereIsDiscount;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;



@end

@implementation PSFinalTicketViewController

- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self)
    {
        self.thereIsServiceFee  = NO;
        self.thereIsDiscount    = NO;
        self.dataTotal          = [[info valueForKey:@"Total"] stringValue];
        self.dataServiceFee     = [[info valueForKey:@"ServiceFee"] stringValue];
        self.dataDiscount       = [NSNumber numberWithDouble:[[info valueForKey:@"Discount"] doubleValue]];
        self.dataOrderId        = [NSNumber numberWithLong:[[info valueForKey:@"OrderId"] longValue]];
        self.dataShippingCost   = [NSNumber numberWithLong:[[info valueForKey:@"ShippingCost"] longValue]];
        self.dataSubtotal       = [NSNumber numberWithDouble:[[info valueForKey:@"SubTotal"] doubleValue]];
        self.dataTax            = [NSNumber numberWithInt:[[info valueForKey:@"Tax"] intValue]];
        
        self.arrayTicket = [info valueForKey:@"Tickets"];
        self.dictInfo    = [NSMutableDictionary dictionary];
        
        self.dictKeys   = [NSMutableDictionary dictionary];
        self.dictValues = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Parallax effect
    self.lblHeaderEventTitle.parallaxIntensity  = 10;
    self.lblHeaderVenueTitle.parallaxIntensity  = 5;
    self.lblHeaderDate.parallaxIntensity        = 2;
    
    self.lblHeaderQuantity.layer.cornerRadius = 11;
    self.myTableView.exclusiveTouch = YES;
    
    [self loadNavBar];
    [self fillViewInformation];
    [self fillTableWithDatePrice];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"FinalTicket dealloc");
    
    self.arrayTicket = nil;
    self.dictInfo    = nil;
    self.dictValues  = nil;
    self.dictKeys    = nil;
    self.myTableView = nil;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(closeModal)];
    
    btnDone.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = btnDone;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)closeModal
{
    [self.delegate doneButtonInFinalTapped];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
    self.dictInfo =  [self.arrayTicket objectAtIndex:0];

    self.lblHeaderEventTitle.text = [NSString stringWithFormat:@"%@", [self.dictInfo valueForKey:@"EventName"]];
    self.lblHeaderEventTitle.borderColor =  [UIColor blackColor];
    self.lblHeaderEventTitle.borderSize  = 1;
    
    self.lblHeaderVenueTitle.text = [NSString stringWithFormat:@"%@", [self.dictInfo valueForKey:@"Venue"]];
    self.lblHeaderVenueTitle.borderColor =  [UIColor blackColor];
    self.lblHeaderVenueTitle.borderSize  = 1;
    
    self.lblHeaderDate.text       = [NSString stringWithFormat:@"%@", [self.dictInfo valueForKey:@"EventDate"]];
    self.lblHeaderDate.borderColor =  [UIColor blackColor];
    self.lblHeaderDate.borderSize  = 1;
    
    
    self.lblHeaderQuantity.text   = [NSString stringWithFormat:@"%@ Tickets", [self.dictInfo valueForKey:@"Quantity"]];
    self.lblHeaderSection.text    = [NSString stringWithFormat:@"Section %@", [self.dictInfo valueForKey:@"Section"]];
    self.lblHeaderRow.text        = [NSString stringWithFormat:@"Row %@", [self.dictInfo valueForKey:@"Row"]];
    [self.lblHeaderRow boldSubstring:[NSString stringWithFormat:@"%@", [self.dictInfo valueForKey:@"Row"]]];
    
    NSString *moneyPrice          = [NSString stringWithFormat:@"%@", [self.dictInfo valueForKey:@"Price"]];
    self.lblHeaderPrice.text      = [NSString stringWithFormat:@"Price %@", [PSUtils moneyFormat:moneyPrice withFraction:YES]];
}

-(void)fillTableWithDatePrice
{
    int numeration = 0;
    
    [self.dictKeys setValue:@"Order Id" forKey:[NSString stringWithFormat:@"%i", numeration]];
    [self.dictValues setValue:self.dataOrderId forKey:[NSString stringWithFormat:@"%i", numeration]];
    numeration++;
    
    [self.dictKeys setValue:@"Sub Total" forKey:[NSString stringWithFormat:@"%i", numeration]];
    [self.dictValues setValue:self.dataSubtotal forKey:[NSString stringWithFormat:@"%i", numeration]];
    numeration++;
    
    if ([self.dataServiceFee intValue] > 0)
    {
        self.thereIsServiceFee = YES;
        
        [self.dictKeys setValue:@"Service Fee" forKey:[NSString stringWithFormat:@"%i", numeration]];
        [self.dictValues setValue:self.dataServiceFee forKey:[NSString stringWithFormat:@"%i", numeration]];
        numeration++;
    }
    
    [self.dictKeys setValue:@"Shipping Cost" forKey:[NSString stringWithFormat:@"%i", numeration]];
    [self.dictValues setValue:self.dataShippingCost forKey:[NSString stringWithFormat:@"%i", numeration]];
    numeration++;

    if ([self.dataDiscount intValue] > 0)
    {
        self.thereIsDiscount = YES;
        
        [self.dictKeys setValue:@"Discount" forKey:[NSString stringWithFormat:@"%i", numeration]];
        [self.dictValues setValue:self.dataDiscount forKey:[NSString stringWithFormat:@"%i", numeration]];
        numeration++;
    }
    
    if ([self.dataTax intValue] > 0)
    {
        [self.dictKeys setValue:@"Tax" forKey:[NSString stringWithFormat:@"%i", numeration]];
        [self.dictValues setValue:self.dataTax forKey:[NSString stringWithFormat:@"%i", numeration]];
        numeration++;
    }

    [self.dictKeys setValue:@"Total" forKey:[NSString stringWithFormat:@"%i", numeration]];
    [self.dictValues setValue:self.dataTotal forKey:[NSString stringWithFormat:@"%i", numeration]];
    numeration++;
    
    [self.myTableView reloadData];
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    return self.dictKeys.allKeys.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"PRICE DETAILS";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerSectionView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,[self tableView:tableView heightForHeaderInSection:section])];
    headerSectionView.backgroundColor = [UIColor colorWithHexadecimal:@"#EDEFF1" alpha:1.0];
    
    UIView *lineSeparator         = [[UIView alloc] initWithFrame:CGRectMake(0, [self tableView:tableView heightForHeaderInSection:section], tableView.bounds.size.width, 1)];
    lineSeparator.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:0.5];
    

    UILabel *headerSectionTitle  = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.bounds.size.width - 5, [self tableView:tableView heightForHeaderInSection:section] -2)];
    headerSectionTitle.backgroundColor = [UIColor clearColor];
    headerSectionTitle.text            = [NSString stringWithFormat:@"%@",[self tableView:tableView titleForHeaderInSection:section]];
    headerSectionTitle.font            = [UIFont boldSystemFontOfSize:11.0];
    headerSectionTitle.textColor       = [UIColor lightGrayColor];
    
    [headerSectionView addSubview:lineSeparator];
    [headerSectionView addSubview:headerSectionTitle];
    
    return headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    //Fill Table Data
    
    if (indexPath.row == 0)
    {
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
        cell.detailTextLabel.text =  [NSString stringWithFormat:@"%@",[self.dictValues objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];

    }
    else
    {
        if (indexPath.row == 3)
        {
            if (self.thereIsServiceFee == NO && self.thereIsDiscount == YES)
            {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"- %@", cell.detailTextLabel.text];
                //Discount Field
            }
            else
            {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
            }
        }
        else if (indexPath.row == 4)
        {
            if (self.thereIsServiceFee == YES && self.thereIsDiscount == YES)
            {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"- %@", cell.detailTextLabel.text];
                //Discount Field
            }
            else
            {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];

            }
        }
        else
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
        }
    }
    
    //Choose Cell Style
    if ([cell.textLabel.text isEqualToString:@"Total"])
    {
        //Configuration Cells mode Bold
        cell.detailTextLabel.font       = [UIFont boldSystemFontOfSize:20];
    }
    else
    {
        //Configuration Cells mode normal
        cell.detailTextLabel.font       = [UIFont fontWithName:@"Helvetica-Light" size:20];
    }

    //Configuration Cells
    cell.textLabel.font             = [UIFont fontWithName:@"Helvetica-Light" size:18];
    cell.textLabel.textColor        = [UIColor colorWithHexadecimal:@"#9BA4AC" alpha:1.0];
    
    cell.detailTextLabel.textColor  = [UIColor primeSportColorNavigationController];
    cell.userInteractionEnabled     = NO;
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.0f;
}


@end
