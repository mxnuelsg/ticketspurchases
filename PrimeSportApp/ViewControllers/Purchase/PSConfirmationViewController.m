//
//  PSConfirmationViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 27/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSConfirmationViewController.h"
#import "PSModelPurchaseFlow.h"

@interface PSConfirmationViewController () <modelPurchaseFlowDelegate, UITableViewDataSource, UITableViewDelegate>

//* * * * * Controls
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

//* * * * * Header
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblHeaderEventTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblHeaderVenueTitle;
@property (weak, nonatomic) IBOutlet PSBorderLabel *lblHeaderDate;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSection;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRow;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderPrice;

//* * * * * Body
//Price Detail
@property (strong, nonatomic) NSMutableDictionary *dictKeys_Price;
@property (strong, nonatomic) NSMutableDictionary *dictValues_Price;

//Payment
@property (strong, nonatomic) NSMutableDictionary *dictKeys_Payment;
@property (strong, nonatomic) NSMutableDictionary *dictValues_Payment;

//Billing
@property (strong, nonatomic) NSMutableDictionary *dictKeys_Billing;
@property (strong, nonatomic) NSMutableDictionary *dictValues_Billing;

//Shipping
@property (strong, nonatomic) NSMutableDictionary *dictKeys_Shipping;
@property (strong, nonatomic) NSMutableDictionary *dictValues_Shipping;

@property (assign, nonatomic) BOOL thereIsServiceFee;
@property (assign, nonatomic) BOOL thereIsDiscount;
@property (strong, nonatomic) NSMutableDictionary *dictInfo;
@property (strong, nonatomic) PSModelPurchaseFlow *model;



@end

@implementation PSConfirmationViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPurchaseFlow alloc] init];
        self.model.delegate = self;
        
        self.dictInfo = [NSMutableDictionary dictionary];
        
        self.thereIsServiceFee = NO;
        self.thereIsDiscount   = NO;
        
        self.dictKeys_Price      = [NSMutableDictionary dictionary];
        self.dictValues_Price    = [NSMutableDictionary dictionary];
        self.dictKeys_Payment    = [NSMutableDictionary dictionary];
        self.dictValues_Payment  = [NSMutableDictionary dictionary];
        self.dictKeys_Billing    = [NSMutableDictionary dictionary];
        self.dictValues_Billing  = [NSMutableDictionary dictionary];
        self.dictKeys_Shipping   = [NSMutableDictionary dictionary];
        self.dictValues_Shipping = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Parallax effect
    self.lblHeaderEventTitle.parallaxIntensity  = 10;
    self.lblHeaderVenueTitle.parallaxIntensity  = 5;
    self.lblHeaderDate.parallaxIntensity        = 2;
    
    self.lblHeaderQuantity.layer.cornerRadius = 11;
    self.myTableView.exclusiveTouch = YES;
    
    [self loadNavBar];
    [self fillViewInformation];
    [self postGetOrderTotals];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Confirmation dealloc");
    
    self.model.delegate    = nil;
    self.dictInfo = nil;
    
    self.dictKeys_Price     = nil;
    self.dictValues_Price   = nil;
    
    self.dictKeys_Payment   = nil;
    self.dictValues_Payment = nil;
    
    self.dictKeys_Billing   = nil;
    self.dictValues_Billing = nil;
    
    self.dictKeys_Shipping   = nil;
    self.dictValues_Shipping = nil;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    UIBarButtonItem *btnSubmit = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                    style:UIBarButtonItemStyleBordered
                                                                   target:self
                                                                   action:@selector(submitOrder)];
    
    btnSubmit.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = btnSubmit;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)submitOrder
{
    [self postSubmitOrder];
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
     //* * * * * Header
    //Event
    self.lblHeaderEventTitle.text = [PSTicket information].tcktEventTitle;
    self.lblHeaderEventTitle.borderColor =  [UIColor blackColor];
    self.lblHeaderEventTitle.borderSize  = 1;
    
    self.lblHeaderVenueTitle.text = [PSTicket information].tcktEventVenue;
    self.lblHeaderVenueTitle.borderColor =  [UIColor blackColor];
    self.lblHeaderVenueTitle.borderSize  = 1;
    
    self.lblHeaderDate.text  = [PSTicket information].tcktEventDate;
    self.lblHeaderDate.borderColor =  [UIColor blackColor];
    self.lblHeaderDate.borderSize  = 1;
    
    //Section
    self.lblHeaderSection.text = [NSString stringWithFormat:@"Section %@", [PSTicket information].tcktEventSection];
    
    self.lblHeaderRow.text     = [NSString stringWithFormat:@"Row %@", [PSTicket information].tcktEventRow];
    [self.lblHeaderRow boldSubstring:[PSTicket information].tcktEventRow];
    
    self.lblHeaderQuantity.text = [NSString stringWithFormat:@"%@ Tickets", [PSTicket information].tcktEventQuantity];
    self.lblHeaderQuantity.adjustsFontSizeToFitWidth = YES;
    
    self.lblHeaderPrice.text = [NSString stringWithFormat:@"Price %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPrice withFraction:YES]];

}

-(void)fillTableWithDate
{
    //Number Key
    int numerationForPrice      = 0;
    int numerationForPayment    = 0;
    int numerationForBilling    = 0;
    int numerationForShipping   = 0;
    
    //* * * * * Body
     //Price Details
    NSNumber *total         = [NSNumber numberWithDouble:[[self.dictInfo valueForKey:@"Total"] doubleValue]];
    NSNumber *serviceFee    = [NSNumber numberWithDouble:[[self.dictInfo valueForKey:@"ServiceFee"] doubleValue]];
    NSNumber *subtotal      = [NSNumber numberWithDouble:[[self.dictInfo valueForKey:@"SubTotal"] doubleValue]];
    NSNumber *shippingCost  = [NSNumber numberWithLong:[[self.dictInfo valueForKey:@"ShippingCost"] longValue]];
    NSNumber *discount      = [NSNumber numberWithDouble:[[self.dictInfo valueForKey:@"Discount"]doubleValue ]];
    NSNumber *tax           = [NSNumber numberWithInt:[[self.dictInfo valueForKey:@"Tax"]intValue ]];
    
    [self.dictKeys_Price setValue:@"Sub Total" forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
    [self.dictValues_Price setValue:subtotal forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
    numerationForPrice++;
    
    if ([serviceFee intValue] > 0)
    {
        self.thereIsServiceFee = YES;
        
        [self.dictKeys_Price setValue:@"Service Fee" forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
        [self.dictValues_Price setValue:serviceFee forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
        numerationForPrice++;
    }
    
    [self.dictKeys_Price setValue:@"Shipping Cost" forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
    [self.dictValues_Price setValue:shippingCost forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
    numerationForPrice++;
    
    if ([discount intValue] > 0)
    {
        self.thereIsDiscount = YES;
        
        [self.dictKeys_Price setValue:@"Discount" forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
        [self.dictValues_Price setValue:discount forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
        numerationForPrice++;
    }
    
    if ([tax intValue] > 0)
    {
        [self.dictKeys_Price setValue:@"Tax" forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
        [self.dictValues_Price setValue:tax forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
        numerationForPrice++;
    }
    
    [self.dictKeys_Price setValue:@"Total" forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
    [self.dictValues_Price setValue:total forKey:[NSString stringWithFormat:@"%i", numerationForPrice]];
    numerationForPrice++;

    
    //Payment
    NSString *numberCard   = [PSTicket information].tcktEventCreditCardNumber;
    NSString *numberFormat =[numberCard substringFromIndex:[numberCard length]-4];
    
    [self.dictKeys_Payment setValue:@"Credit Card" forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    [self.dictValues_Payment setValue:[PSTicket information].tcktEventCreditCardType forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    numerationForPayment++;
    
    [self.dictKeys_Payment setValue:@"Card #" forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    [self.dictValues_Payment setValue:[NSString stringWithFormat:@"############%@", numberFormat] forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    numerationForPayment++;
    
    [self.dictKeys_Payment setValue:@"Expiration Date" forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    [self.dictValues_Payment setValue:[PSTicket information].tcktEventCreditCardExpirationDate forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    numerationForPayment++;
    
    [self.dictKeys_Payment setValue:@"Card Code" forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    [self.dictValues_Payment setValue:[PSTicket information].tcktEventCreditCardSecurityCode forKey:[NSString stringWithFormat:@"%i", numerationForPayment]];
    numerationForPayment++;
    
    
    //Billing
    [self.dictKeys_Billing setValue:@"First Name" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventHolderFirstName forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Last Name" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventHolderLastName forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Phone" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventHolderPhoneNumber forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Mobile" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventHolderMobileNumber forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Address 1" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventShippingInfoAddress1 forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Addres 2" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventShippingInfoAddress2 forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Country" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventShippingInfoCountry forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"State" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventShippingInfoState forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"City" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventShippingInfoCity forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    [self.dictKeys_Billing setValue:@"Zip Code" forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    [self.dictValues_Billing setValue:[PSTicket information].tcktEventShippingInfoZipCode forKey:[NSString stringWithFormat:@"%i", numerationForBilling]];
    numerationForBilling++;
    
    //Shipping
    [self.dictKeys_Shipping setValue:@"Method" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].tcktEventShippingMethod forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"Coupon Code" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].tcktEventCouponCode forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"Address 1" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].shippingStorageAddress1 forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"Address 2" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].shippingStorageAddress2 forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"Country" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].shippingStorageCountry forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"State" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].shippingStorageState forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"City" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].shippingStorageCity forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.dictKeys_Shipping setValue:@"Zip Code" forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    [self.dictValues_Shipping setValue:[PSTicket information].shippingStorageZipCode forKey:[NSString stringWithFormat:@"%i", numerationForShipping]];
    numerationForShipping++;
    
    [self.myTableView reloadData];
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)postGetOrderTotals
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    //Build request structure
    //Tickets
    NSMutableDictionary *dicTickets = [NSMutableDictionary dictionary];
    NSMutableArray *arrayTickets = [NSMutableArray array];
    
    [dicTickets setValue:[PSTicket information].tcktEventId forKey:@"TicketId"];
    [dicTickets setValue:[PSTicket information].tcktEventQuantity forKey:@"Quantity"];
    
    [arrayTickets addObject:dicTickets];
    
    //Billing Information
    NSMutableDictionary *dicBilling = [NSMutableDictionary dictionary];
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoStateId forKey:@"State"];
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoCountryId forKey:@"Country"];
    
    //Sendind final object
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSTicket information].tcktEventShippingMethodId forKey:@"ShippingMethodId"];
    [parameters setObject:[PSTicket information].tcktEventCouponCode forKey:@"CouponCode"];
    [parameters setObject:arrayTickets forKey:@"Tickets"];
    [parameters setObject:dicBilling forKey:@"BillingInformation"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kOrderTotals;
    webService.wsParameters = parameters;
    
    [self.model postOrderTotalsWithObject:webService];
}

-(void)postSubmitOrder
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:@"Processing" andMessage:@"Order"];
     //Build request structure
    //Tickets
    NSMutableDictionary *dicTickets = [NSMutableDictionary dictionary];
    NSMutableArray *arrayTickets = [NSMutableArray array];
    
    [dicTickets setValue:[PSTicket information].tcktEventId forKey:@"TicketId"];
    [dicTickets setValue:[PSTicket information].tcktEventQuantity forKey:@"Quantity"];
    [arrayTickets addObject:dicTickets];
    
    //Billing Information
    NSMutableDictionary *dicBilling = [NSMutableDictionary dictionary];
    
    [dicBilling setValue:[PSTicket information].tcktEventBillingSelectedId forKey:@"Id"];
    [dicBilling setValue:[PSTicket information].tcktEventBillingSelected forKey:@"Nickname"];
    [dicBilling setValue:[PSTicket information].tcktEventHolderFirstName forKey:@"CardholderFirstName"];
    [dicBilling setValue:[PSTicket information].tcktEventHolderLastName forKey:@"CardholderLastName"];
    [dicBilling setValue:[PSTicket information].tcktEventHolderPhoneNumber forKey:@"Phone"];
    
    if ([[PSTicket information].tcktEventHolderMobileNumber isEqualToString:kEmptyField])
    {
        [dicBilling setValue:@"" forKey:@"Mobile"];
    }
    else
    {
        [dicBilling setValue:[PSTicket information].tcktEventHolderMobileNumber forKey:@"Mobile"];
    }
    
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoAddress1 forKey:@"Address"];
    
    if ([[PSTicket information].tcktEventShippingInfoAddress2 isEqualToString:kEmptyField])
    {
        [dicBilling setValue:@"" forKey:@"Address2"];
    }
    else
    {
        [dicBilling setValue:[PSTicket information].tcktEventShippingInfoAddress2 forKey:@"Address2"];
    }
    
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoCountryId forKey:@"Country"];
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoStateId forKey:@"State"];
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoCity forKey:@"City"];
    [dicBilling setValue:[PSTicket information].tcktEventShippingInfoZipCode forKey:@"ZipCode"];
    
    //Shipping Address
    NSMutableDictionary *dicShipping = [NSMutableDictionary dictionary];
    
    [dicShipping setValue:[PSTicket information].shippingStorageAddress1 forKey:@"Address"];
    
    if ([[PSTicket information].shippingStorageAddress2 isEqualToString:kEmptyField])
    {
        [dicShipping setValue:@"" forKey:@"Address2"];
    }
    else
    {
        [dicShipping setValue:[PSTicket information].shippingStorageAddress2 forKey:@"Address2"];
    }
    
    [dicShipping setValue:[PSTicket information].shippingStorageCountryId forKey:@"Country"];
    [dicShipping setValue:[PSTicket information].shippingStorageStateId forKey:@"State"];
    [dicShipping setValue:[PSTicket information].shippingStorageCity forKey:@"City"];
    [dicShipping setValue:[PSTicket information].shippingStorageZipCode forKey:@"ZipCode"];

    //Sendind final object
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:arrayTickets forKey:@"Tickets"];
     [parameters setObject:[PSSessionParameters information].sessionEmail forKey:@"UserEmail"];
    [parameters setObject:[PSSessionParameters information].sessionToken forKey:@"AuthenticationToken"];
    [parameters setObject:[PSTicket information].tcktEventShippingMethodId forKey:@"ShippingMethodId"];
    
    if ([[PSTicket information].tcktEventCouponCode isEqualToString:kEmptyField])
    {
        [parameters setObject:@"" forKey:@"CouponCode"];
    }
    else
    {
       [parameters setObject:[PSTicket information].tcktEventCouponCode forKey:@"CouponCode"];
    }
    
    [parameters setObject:[PSTicket information].tcktEventCreditCardType forKey:@"CreditCardType"];
    [parameters setObject:[PSTicket information].tcktEventCreditCardNumber forKey:@"CreditCardNumber"];
    [parameters setObject:[PSTicket information].tcktEventCreditCardExpirationDate forKey:@"CreditCardExpirationDate"];
    [parameters setObject:[PSTicket information].tcktEventCreditCardSecurityCode forKey:@"CreditCardCVV"];
    [parameters setObject:dicBilling forKey:@"BillingInformation"];
    [parameters setObject:dicShipping forKey:@"ShippingAddress"];
    
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSubmitOrder;
    webService.wsParameters = parameters;
    
    [self.model postSubmitOrderWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelPurchaseFlow)
-(void)modelResponseForOrderTotals:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message       = [result objectForKey:@"Message"];
        NSArray *arrayData      = [result objectForKey:@"Data"];
        self.dictInfo           = [NSMutableDictionary dictionary];
        
        if (responseCode == 0)
        {
            if (arrayData.count > 0)
            {
                for (NSString *key in arrayData)
                {
                     [self.dictInfo setValue:[arrayData valueForKey:key] forKey:key];
                }
                [self fillTableWithDate];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:@"No Price Details data found. Please, check your info an try again."
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
                
                if ([PSTicket information].isPurchaseProcessActive == YES)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelPurhaseProcess"
                                                                        object:nil];
                }
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

-(void)modelResponseForSubmitOrder:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message       = [result objectForKey:@"Message"];
        NSArray *arrayData      = [result objectForKey:@"Data"];
        NSArray *arrayValidationErrors  = [result objectForKey:@"ValidationErrors"];
        
        if (responseCode == 0)
        {
            if (arrayData.count > 0)
            {
                PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
                [(PSPrincipalViewController *)[delegate.window rootViewController] openTicketPurchasedWithData:arrayData];
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:@"No data found. Please, check your info an try again."
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
                
                if ([PSTicket information].isPurchaseProcessActive == YES)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelPurhaseProcess"
                                                                        object:nil];
                }
            }
            else
            {
                NSMutableString *stringMessages = [[NSMutableString alloc]init];
                
                for (NSString *msg in arrayValidationErrors)
                {
                    [stringMessages appendFormat:@"✘ %@\n",msg];
                }

                [[PSMessages getModel]showAlertMessage:[NSString stringWithFormat:@"%@", stringMessages]
                                             withTitle:kWarningTitle];
            }
        }
    }
}

#pragma mark -
#pragma mark - TABLEVIEW DATASOURCE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    switch (section)
    {
        case 0:
            return self.dictKeys_Price.allKeys.count;
            break;
        case 1:
            return self.dictKeys_Payment.allKeys.count;
            break;
        case 2:
            return self.dictKeys_Billing.allKeys.count;
            break;
        case 3:
            return self.dictKeys_Shipping.allKeys.count;
            break;
        default:
            return 0;
            break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return @"PRICE DETAILS";
            break;
        case 1:
            return @"PAYMENT";
            break;
        case 2:
            return @"BILLING";
            break;
        case 3:
            return @"SHIPPING";
            break;
        default:
            return @"UNKNOWN SECTION";
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerSectionView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,[self tableView:tableView heightForHeaderInSection:section])];
    headerSectionView.backgroundColor = [UIColor colorWithHexadecimal:@"#EDEFF1" alpha:1.0];
    
    UIView *lineSeparator         = [[UIView alloc] initWithFrame:CGRectMake(0, [self tableView:tableView heightForHeaderInSection:section], tableView.bounds.size.width, 1)];
    lineSeparator.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:0.5];
    
    
    UILabel *headerSectionTitle  = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.bounds.size.width - 5, [self tableView:tableView heightForHeaderInSection:section] -2)];
    headerSectionTitle.backgroundColor = [UIColor clearColor];
    headerSectionTitle.text            = [NSString stringWithFormat:@"%@",[self tableView:tableView titleForHeaderInSection:section]];
    headerSectionTitle.font            = [UIFont boldSystemFontOfSize:11.0];
    headerSectionTitle.textColor       = [UIColor lightGrayColor];
    
    [headerSectionView addSubview:lineSeparator];
    [headerSectionView addSubview:headerSectionTitle];
    
    return headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    //Fill Table Data
    switch (indexPath.section)
    {
        case 0:
            if (indexPath.row == 2)
            {
                if (self.thereIsServiceFee == NO && self.thereIsDiscount == YES)
                {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                    cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"- %@", cell.detailTextLabel.text];
                    //Discount field
                }
                else
                {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                    cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
                }
            }
            else if (indexPath.row == 3)
            {
                if (self.thereIsServiceFee == YES && self.thereIsDiscount == YES)
                {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                    cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"- %@", cell.detailTextLabel.text];
                    //Discount field
                }
                else
                {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                    cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
                    
                }
            }
            else
            {
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
                cell.detailTextLabel.text =  [PSUtils moneyFormat:[NSString stringWithFormat:@"%@",[self.dictValues_Price objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]] withFraction:YES];
            }
            
            //Choose Cell Style
            if ([cell.textLabel.text isEqualToString:@"Total"])
            {
                //Configuration Cells mode Bold
                cell.detailTextLabel.font       = [UIFont boldSystemFontOfSize:20];
            }
            else
            {
                //Configuration Cells mode normal
                cell.detailTextLabel.font       = [UIFont fontWithName:@"Helvetica-Light" size:20];
            }
            
            //Configuration Cells
            cell.textLabel.font             = [UIFont fontWithName:@"Helvetica-Light" size:18];
            cell.textLabel.textColor        = [UIColor colorWithHexadecimal:@"#9BA4AC" alpha:1.0];
            cell.detailTextLabel.textColor  = [UIColor primeSportColorNavigationController];
            cell.userInteractionEnabled     = NO;
            break;
        case 1:
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Payment objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[self.dictValues_Payment objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            break;
        case 2:
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Billing objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[self.dictValues_Billing objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            break;
        case 3:
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.dictKeys_Shipping objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[self.dictValues_Shipping objectForKey:[NSString stringWithFormat:@"%li", (long)indexPath.row]]];
            break;
    }
    
    if (indexPath.section > 0 )
    {
        //Configuration Cells
        cell.textLabel.font             = [UIFont fontWithName:@"Helvetica-Light" size:18];
        cell.textLabel.textColor        = [UIColor colorWithHexadecimal:@"#9BA4AC" alpha:1.0];
        
        cell.detailTextLabel.font       = [UIFont fontWithName:@"Helvetica-Light" size:20];
        cell.detailTextLabel.textColor  = [UIColor primeSportColorNavigationController];
        cell.userInteractionEnabled     = NO;
    }
 
    
    return cell;
}

#pragma mark -
#pragma mark - TABLEVIEW DELEGATE

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.0f;
}


@end
