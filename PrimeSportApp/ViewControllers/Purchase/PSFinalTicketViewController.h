//
//  PSFinalTicketViewController.h
//  PrimeSportApp
//
//  Created by Manuel Salinas on 03/07/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol finalTicketDelegate <NSObject>

@required
-(void)doneButtonInFinalTapped;
@end

@interface PSFinalTicketViewController : UIViewController

@property (assign, nonatomic) id <finalTicketDelegate> delegate;

- (instancetype)initWithInfo:(NSDictionary *)info;

@end
