//
//  PSBillingViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 25/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSBillingViewController.h"
#import "PSModelPurchaseFlow.h"
#import "PSShippingDetailViewController.h"
#import "PSGenericModals.h"

@interface PSBillingViewController () <UITextFieldDelegate, modelPurchaseFlowDelegate, modalBillingsDelegate, modalCountriesDelegate, modalStatesDelegate >

//Controls
@property (weak, nonatomic) IBOutlet UIToolbar *myToolbar;
@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarNext;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSection;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRow;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSubtotal;
@property (strong, nonatomic) UIBarButtonItem *btnShipping;

//ViewControllers
@property (strong, nonatomic) PSModalBillingsViewController *vcModalBillings;
@property (strong, nonatomic) PSModalCountriesViewController *vcModalCountries;
@property (strong, nonatomic) PSModalStatesViewController *vcModalStates;

//Textfields
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtSelectPreviousBillings;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtFirstName;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtLastName;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtPhone;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtMobile;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress1;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtAddress2;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCountry;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtState;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtCity;
@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtZipCode;
@property (strong, nonatomic) UITextField *activeTextField;

//Model
@property (strong, nonatomic) PSModelPurchaseFlow *model;

@end

@implementation PSBillingViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPurchaseFlow alloc] init];
        self.model.delegate = self;
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lblHeaderQuantity.layer.cornerRadius = 11;
    
    [self chargeTextfieldIcons];
    [self loadNavBar];
    [self loadToolbar];
    [self loadPickerView];
    [self fillViewInformation];
    [self lookingForPreviousData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidLayoutSubviews
{
    [self.myScrollView autosize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Billing dealloc");
    
    self.myScrollView    = nil;
    self.model.delegate  = nil;
    self.btnShipping     = nil;
    self.activeTextField = nil;
    self.vcModalBillings = nil;
    self.vcModalCountries = nil;
    self.vcModalStates   = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnShipping.enabled = NO;
    self.btnToolbarBack.enabled = NO;
    self.btnToolbarNext.enabled = NO;
    
    //Up scroll field
    NSDictionary *infoNotificacion = [notif userInfo];
    CGSize tamanioTeclado = [[infoNotificacion objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, tamanioTeclado.height, 0);
    [[self myScrollView] setContentInset:edgeInsets];
    [[self myScrollView] setScrollIndicatorInsets:edgeInsets];
    [[self myScrollView] scrollRectToVisible:[self activeTextField].frame animated:YES];
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    self.btnShipping.enabled = YES;
    self.btnToolbarBack.enabled = YES;
    self.btnToolbarNext.enabled = YES;
    
    //Down scroll field
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
    [[self myScrollView] setContentInset:edgeInsets];
    [[self myScrollView] setScrollIndicatorInsets:edgeInsets];
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark - TEXTFIELD ICONS
-(void)chargeTextfieldIcons
{
    self.txtFirstName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtLastName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCompleteName"]];
    self.txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtPhone.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconPhone"]];
    self.txtPhone.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtMobile.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconMobile"]];
    self.txtMobile.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress1.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress1.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtAddress2.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconAddress"]];
    self.txtAddress2.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCity.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCity"]];
    self.txtCity.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtCountry.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconCountry"]];
    self.txtCountry.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtState.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconState"]];
    self.txtState.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtZipCode.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconZipcode"]];
    self.txtZipCode.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    self.btnShipping = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                        style:UIBarButtonItemStyleBordered
                                                       target:self
                                                       action:@selector(goShippingDetail)];
    
    self.btnShipping.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.rightBarButtonItem = self.btnShipping;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)goShippingDetail
{
    [self.view endEditing:YES];
    
    if ([self.txtFirstName.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationFirstname
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtLastName.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationLastname
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtPhone.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationPhone
                                     withTitle:kWarningTitle];
    }
    else if (self.txtPhone.text.length < 10)
    {
        [[PSMessages getModel]showAlertMessage:kValidationPhoneLength
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtAddress1.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationAddress
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCountry.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCountry
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtState.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationState
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtCity.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationCity
                                     withTitle:kWarningTitle];
    }
    else if ([self.txtZipCode.text isBlank])
    {
        [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                     withTitle:kWarningTitle];
    }
    else
    {
        //Optional Data
        //1 mobile
        if ([self.txtMobile.text isBlank])
        {
            [PSTicket information].tcktEventHolderMobileNumber   = kEmptyField;
        }
        else
        {
            [PSTicket information].tcktEventHolderMobileNumber   = [self.txtMobile.text removeWhiteSpacesFromString];
        }
        
        //2 second address
        if ([self.txtAddress2.text isBlank])
        {
            [PSTicket information].tcktEventShippingInfoAddress2 = kEmptyField;
        }
        else
        {
            [PSTicket information].tcktEventShippingInfoAddress2 = self.txtAddress2.text;
        }
        
        //Call view controller
        PSShippingDetailViewController *vcShippingDetail = [[PSShippingDetailViewController alloc]init];
        vcShippingDetail.title = @"Shipping";
        
        [self.navigationController pushViewController:vcShippingDetail animated:YES];
    }
}

#pragma mark -
#pragma mark - TOOL BAR CONFIGURATION
-(void)loadToolbar
{
    self.myToolbar.barStyle     = UIBarStyleBlackOpaque;
    self.myToolbar.barTintColor = [UIColor primeSportColorToolBar];
    self.myToolbar.translucent  = NO;
    [self.myToolbar sizeToFit];
}

-(IBAction)toolbarButtonBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)toolbarButtonNext
{
    [self goShippingDetail];
}

#pragma mark -
#pragma mark - CHECK INFORMATION AVAILABLE
-(void)lookingForPreviousData
{
    if (![[PSTicket information].tcktEventBillingSelected isBlank])
    {
        self.txtSelectPreviousBillings.text = [PSTicket information].tcktEventBillingSelected;
    }
    
    if (![[PSTicket information].tcktEventHolderFirstName isBlank])
    {
        self.txtFirstName.text = [PSTicket information].tcktEventHolderFirstName;
    }
    
    if (![[PSTicket information].tcktEventHolderLastName isBlank])
    {
        self.txtLastName.text = [PSTicket information].tcktEventHolderLastName;
    }
    
    if (![[PSTicket information].tcktEventHolderPhoneNumber isBlank])
    {
        self.txtPhone.text = [PSTicket information].tcktEventHolderPhoneNumber;
    }
    
    if (![[PSTicket information].tcktEventHolderMobileNumber isBlank]
        && ![[PSTicket information].tcktEventHolderMobileNumber isEqualToString:kEmptyField])
    {
        self.txtMobile.text = [PSTicket information].tcktEventHolderMobileNumber;
    }
    
    if (![[PSTicket information].tcktEventShippingInfoAddress1 isBlank])
    {
        self.txtAddress1.text = [PSTicket information].tcktEventShippingInfoAddress1;
    }
    
    if (![[PSTicket information].tcktEventShippingInfoAddress2 isBlank]
        && ![[PSTicket information].tcktEventShippingInfoAddress2 isEqualToString:kEmptyField])
    {
        self.txtAddress2.text = [PSTicket information].tcktEventShippingInfoAddress2;
    }
    
    if (![[PSTicket information].tcktEventShippingInfoCity isBlank])
    {
        self.txtCity.text = [PSTicket information].tcktEventShippingInfoCity;
    }
    
    if (![[PSTicket information].tcktEventShippingInfoZipCode isBlank])
    {
        self.txtZipCode.text = [PSTicket information].tcktEventShippingInfoZipCode;
    }
    
    if (![[PSTicket information].tcktEventShippingInfoCountry isBlank])
    {
        self.txtCountry.text = [PSTicket information].tcktEventShippingInfoCountry;
    }
    
    if (![[PSTicket information].tcktEventShippingInfoState isBlank])
    {
        self.txtState.text = [PSTicket information].tcktEventShippingInfoState;
    }
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
    //Header
    self.lblHeaderSection.text = [NSString stringWithFormat:@"Section %@", [PSTicket information].tcktEventSection];
    
    self.lblHeaderRow.text     = [NSString stringWithFormat:@"Row %@", [PSTicket information].tcktEventRow];
    [self.lblHeaderRow boldSubstring:[PSTicket information].tcktEventRow];
    
    self.lblHeaderQuantity.text = [PSTicket information].tcktEventQuantity;
    self.lblHeaderQuantity.adjustsFontSizeToFitWidth = YES;
    
    self.lblHeaderPrice.text = [NSString stringWithFormat:@"Price %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPrice withFraction:YES]];
    
    self.lblHeaderSubtotal.text = [NSString stringWithFormat:@"Sub Total %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
    [self.lblHeaderSubtotal boldSubstring:[PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
}

#pragma mark -
#pragma mark - PICKER VIEW & TOOLBAR CONFIGURATION
-(void)loadPickerView
{
    //Toolbar No.1 for fields
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone  target:self action:@selector(hideKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    UISegmentedControl *segmentedNavigation = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"❮ Previous Field", @"Next Field ❯", nil]];
    segmentedNavigation.momentary = YES;
    segmentedNavigation.tintColor = [UIColor groupTableViewBackgroundColor];
    [segmentedNavigation addTarget:self action:@selector(segmentedControlHandler:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *btnSegment = [[UIBarButtonItem alloc]initWithCustomView:segmentedNavigation];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                             btnSegment,
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil  action:nil],
                             btnDone,
                             nil];
    [toolBarKeyboard sizeToFit];
    
    //Toolbar No.2 for textfields
    UIToolbar *toolBarKeyboardClean = [[UIToolbar alloc]init];
    toolBarKeyboardClean.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardClean.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnDoneClean = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneClean.tintColor = [UIColor whiteColor];

    toolBarKeyboardClean.items = [NSArray arrayWithObjects:
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             btnDoneClean,
                             nil];
    [toolBarKeyboardClean sizeToFit];
    
    //Toolbar No.3 for Refresh
    UIToolbar *toolBarKeyboardWithRefresh = [[UIToolbar alloc]init];
    toolBarKeyboardWithRefresh.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboardWithRefresh.barTintColor = [UIColor primeSportColorNavigationController];
    
    
    UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Refresh" style:UIBarButtonItemStyleDone target:self action:@selector(refreshDataInputView)];
    btnRefresh.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *btnDoneRefresh = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    btnDoneRefresh.tintColor = [UIColor whiteColor];
    
    toolBarKeyboardWithRefresh.items = [NSArray arrayWithObjects:
                                        btnRefresh,
                                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                        btnDoneRefresh,
                                        nil];
    [toolBarKeyboardWithRefresh sizeToFit];
    
    self.txtFirstName.inputAccessoryView    = toolBarKeyboard;
    self.txtLastName.inputAccessoryView     = toolBarKeyboard;
    self.txtPhone.inputAccessoryView        = toolBarKeyboard;
    self.txtMobile.inputAccessoryView       = toolBarKeyboard;
    self.txtAddress1.inputAccessoryView     = toolBarKeyboard;
    self.txtAddress2.inputAccessoryView     = toolBarKeyboard;
    
    self.txtCity.inputAccessoryView         = toolBarKeyboardClean;
    self.txtZipCode.inputAccessoryView      = toolBarKeyboardClean;
    
    self.txtCountry.inputAccessoryView      = toolBarKeyboardWithRefresh;
    self.txtState.inputAccessoryView        = toolBarKeyboardWithRefresh;
    self.txtSelectPreviousBillings.inputAccessoryView = toolBarKeyboardWithRefresh;
}

- (void)segmentedControlHandler:(id)sender
{
    UISegmentedControl *mySegmented = (UISegmentedControl *)sender;
    
    switch ([mySegmented selectedSegmentIndex])
    {
        case 0:
            if (self.txtLastName.isEditing == YES)
            {
                [self.txtFirstName becomeFirstResponder];
            }
            else if (self.txtPhone.isEditing == YES)
            {
                [self.txtLastName becomeFirstResponder];
            }
            else if (self.txtMobile.isEditing == YES)
            {
                [self.txtPhone becomeFirstResponder];
            }
            else if (self.txtAddress1.isEditing == YES)
            {
                [self.txtMobile becomeFirstResponder];
            }
            else if (self.txtAddress2.isEditing == YES)
            {
                [self.txtAddress1 becomeFirstResponder];
            }
            break;
        case 1:
            if (self.txtFirstName.isEditing == YES)
            {
                [self.txtLastName becomeFirstResponder];
            }
            else if (self.txtLastName.isEditing == YES)
            {
                [self.txtPhone becomeFirstResponder];
            }
            else if (self.txtPhone.isEditing == YES)
            {
                [self.txtMobile becomeFirstResponder];
            }
            else if (self.txtMobile.isEditing == YES)
            {
                [self.txtAddress1 becomeFirstResponder];
            }
            else if (self.txtAddress1.isEditing == YES)
            {
                [self.txtAddress2 becomeFirstResponder];
            }
            break;
        default:
            break;
    }
}

-(void)hideKeyboard
{
    [self.txtSelectPreviousBillings resignFirstResponder];
    [self.txtFirstName resignFirstResponder];
    [self.txtLastName resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    [self.txtMobile resignFirstResponder];
    [self.txtAddress1 resignFirstResponder];
    [self.txtAddress2 resignFirstResponder];
    [self.txtCountry resignFirstResponder];
    [self.txtState resignFirstResponder];
    [self.txtCity resignFirstResponder];
    [self.txtZipCode resignFirstResponder];
}

-(void)refreshDataInputView
{
    if ([self.txtSelectPreviousBillings isEditing])
    {
        [self.vcModalBillings getAllBillings];
    }
    else if ([self.txtCountry isEditing])
    {
        [self.vcModalCountries getAllCountries];
    }
    else if ([self.txtState isEditing])
    {
        [self.vcModalStates getAllStates];
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getOneSpecificBilling
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSTicket information].tcktEventBillingSelectedId forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kSpecificBillingInfo;
    webService.wsParameters = parameters;
    
    [self.model getSpecificBillingWithObject:webService];
}

-(void)getZipCodeInfo
{
    self.navigationItem.hidesBackButton = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.txtZipCode.text forKey:@"zipcode"];
    [parameters setObject:[PSTicket information].tcktEventShippingInfoCountryId forKey:@"id"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kGetZipCode;
    webService.wsParameters = parameters;
    
    [self.model getZipCodeWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelPurchaseFlow)
-(void)modelResponseForSpecificBilling:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        NSUInteger responseCode  = [[result objectForKey:@"ResponseCode"] integerValue];
        NSString *message        = [result objectForKey:@"Message"];
        NSDictionary *dictInfo   = [result objectForKey:@"Data"];
        
        if (responseCode == 0)
        {
            if (dictInfo.allKeys.count > 0)
            {
                [self textFieldDidBeginEditing:self.txtFirstName];
                self.txtFirstName.text  = [dictInfo valueForKey:@"CardHolderFirstName"];
                [self textFieldDidEndEditing:self.txtFirstName];
                
                [self textFieldDidBeginEditing:self.txtLastName];
                self.txtLastName.text   = [dictInfo valueForKey:@"CardHolderLastName"];
                [self textFieldDidEndEditing:self.txtLastName];
                
                [self textFieldDidBeginEditing:self.txtPhone];
                self.txtPhone.text      = [dictInfo valueForKey:@"Phone"];
                [self textFieldDidEndEditing:self.txtPhone];
                
                [self textFieldDidBeginEditing:self.txtMobile];
                self.txtMobile.text     = [dictInfo valueForKey:@"Mobile"];
                [self textFieldDidEndEditing:self.txtMobile];
                
                [self textFieldDidBeginEditing:self.txtAddress1];
                self.txtAddress1.text   = [dictInfo valueForKey:@"Address"];
                [self textFieldDidEndEditing:self.txtAddress1];
                
                [self textFieldDidBeginEditing:self.txtAddress2];
                self.txtAddress2.text   = [dictInfo valueForKey:@"Address2"];
                [self textFieldDidEndEditing:self.txtAddress2];
                
                [self textFieldDidBeginEditing:self.txtCity];
                self.txtCity.text       = [dictInfo valueForKey:@"City"];
                [self textFieldDidEndEditing:self.txtCity];
                
                self.txtZipCode.text  = [dictInfo valueForKey:@"ZipCode"];
                [PSTicket information].tcktEventShippingInfoZipCode = self.txtZipCode.text ;
                
                self.txtCountry.text = [[dictInfo valueForKey:@"CountryName"] capitalizedString];
                [PSTicket information].tcktEventShippingInfoCountry = self.txtCountry.text;
                
                self.txtState.text = [[dictInfo valueForKey:@"StateName"] capitalizedString];
                [PSTicket information].tcktEventShippingInfoState = self.txtState.text;
                
                [PSTicket information].tcktEventShippingInfoCountryId   = [dictInfo valueForKey:@"Country"];
                [PSTicket information].tcktEventShippingInfoStateId     = [dictInfo valueForKey:@"State"];
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:@"No billing info found"
                                             withTitle:kWarningTitle];
            }
        }
        else
        {
            if ([[message lowercaseString] rangeOfString:@"token expired"].location != NSNotFound)
            {
                [[PSMessages getModel]showAlertMessage:kSessionExpired
                                             withTitle:kInfoTitle];
                
                if ([PSTicket information].isPurchaseProcessActive == YES)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelPurhaseProcess"
                                                                        object:nil];
                }
            }
            else
            {
                [[PSMessages getModel]showAlertMessage:message
                                             withTitle:kWarningTitle];
            }
        }
    }
}

-(void)modelResponseForZipCode:(NSDictionary *)result
{
    self.navigationItem.hidesBackButton = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        self.txtState.text  = [[result valueForKey:@"StateName"] capitalizedString];
        
        [PSTicket information].tcktEventShippingInfoState   = self.txtState.text;
        [PSTicket information].tcktEventShippingInfoStateId = [result valueForKey:@"StateId"];
        
        [self textFieldDidBeginEditing:self.txtCity];
        self.txtCity.text   = [[result valueForKey:@"City"] capitalizedString];
        [self textFieldDidEndEditing:self.txtCity];
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS 
/*UIModalBillingsViewController*/
-(void)selectedBillingInformation:(NSDictionary *)billingInformation
{
    [self.txtSelectPreviousBillings resignFirstResponder];
    
    self.txtSelectPreviousBillings.text = [[billingInformation valueForKey:@"Nickname"]capitalizedString];
    self.txtSelectPreviousBillings.adjustsFontSizeToFitWidth = YES;
    
    [PSTicket information].tcktEventBillingSelected     = self.txtSelectPreviousBillings.text;
    [PSTicket information].tcktEventBillingSelectedId = [billingInformation valueForKey:@"Id"];

    if ([[PSTicket information].tcktEventBillingSelectedId intValue] > 0)
    {
        [self getOneSpecificBilling];
    }
}

/* (UIModalCountriesViewController) */
-(void)selectedCountryInformation:(NSDictionary *)countryInformation
{
    [self.txtCountry resignFirstResponder];
    
    self.txtCountry.text = [[countryInformation valueForKey:@"CountryName"] capitalizedString];
    
    [PSTicket information].tcktEventShippingInfoCountry     = self.txtCountry.text;
    [PSTicket information].tcktEventShippingInfoCountryId   = [countryInformation valueForKey:@"CountryId"];    
    
    //Clean Fields
    self.txtZipCode.text = [NSString string];
    [PSTicket information].tcktEventShippingInfoZipCode = self.txtZipCode.text;
    
    [self textFieldDidBeginEditing:self.txtCity];
    self.txtCity.text = [NSString string];
    [self textFieldDidEndEditing:self.txtCity];
    
    self.txtState.text   = [NSString string];
    [PSTicket information].tcktEventShippingInfoState   = self.txtState.text;
    [PSTicket information].tcktEventShippingInfoStateId = @"0";
}

/* (UIModalStatesViewController) */
-(void)selectedStateInformation:(NSDictionary *)stateInformation
{
    [self.txtState resignFirstResponder];
    
    self.txtState.text = [[stateInformation valueForKey:@"StateName"] capitalizedString];
    
    [PSTicket information].tcktEventShippingInfoState   =  self.txtState.text;
    [PSTicket information].tcktEventShippingInfoStateId = [stateInformation valueForKey:@"StateId"];
    
    [self textFieldDidBeginEditing:self.txtCity];
    self.txtCity.text = [NSString string];
    [self textFieldDidEndEditing:self.txtCity];
}

#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
     [self setActiveTextField:textField];
    
    if (textField == self.txtSelectPreviousBillings)
    {
        self.vcModalBillings = [[PSModalBillingsViewController alloc]init];
        self.vcModalBillings.delegate = self;
        self.vcModalBillings.view.frame = CGRectMake(0, 0, 320, 190);
        self.txtSelectPreviousBillings.inputView = self.vcModalBillings.view;
    }
    
    if (textField == self.txtCountry)
    {
        self.vcModalCountries = [[PSModalCountriesViewController alloc]init];
        self.vcModalCountries.delegate = self;
        self.vcModalCountries.view.frame = CGRectMake(0, 0, 320, 190);
        self.txtCountry.inputView = self.vcModalCountries.view;
    }
    else if (textField == self.txtState)
    {
        if ([self.txtCountry.text isBlank])
        {
            [textField resignFirstResponder];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            self.vcModalStates = [[PSModalStatesViewController alloc]initWithCountryId:[[PSTicket information].tcktEventShippingInfoCountryId integerValue]];
            self.vcModalStates.delegate = self;
            self.vcModalStates.view.frame = CGRectMake(0, 0, 320, 190);
            self.txtState.inputView = self.vcModalStates.view;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
     [self setActiveTextField:nil];
    
    if (textField == self.txtFirstName)
    {
        [PSTicket information].tcktEventHolderFirstName = textField.text;
    }
    
    if (textField == self.txtLastName)
    {
        [PSTicket information].tcktEventHolderLastName = textField.text;
    }
    
    if (textField == self.txtPhone)
    {
        [PSTicket information].tcktEventHolderPhoneNumber = [textField.text removeWhiteSpacesFromString];
    }
    
    if (textField == self.txtMobile)
    {
        [PSTicket information].tcktEventHolderMobileNumber = [textField.text removeWhiteSpacesFromString];
    }
    
    if (textField == self.txtAddress1)
    {
        [PSTicket information].tcktEventShippingInfoAddress1 = textField.text;
    }
    
    if (textField == self.txtAddress2)
    {
        [PSTicket information].tcktEventShippingInfoAddress2 = textField.text;
    }
   
    if (textField == self.txtCity)
    {
        [PSTicket information].tcktEventShippingInfoCity = textField.text;
    }
   
    if (textField == self.txtZipCode)
    {
        if ([textField.text isBlank])
        {
            [[PSMessages getModel]showAlertMessage:kValidationZipCode
                                         withTitle:kWarningTitle];
        }
        else if (textField.text.length < 5)
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationZipCodeLength
                                         withTitle:kWarningTitle];
        }
        else if ([self.txtCountry.text isBlank])
        {
            self.txtZipCode.text = [NSString string];
            [[PSMessages getModel]showAlertMessage:kValidationCountry
                                         withTitle:kWarningTitle];
        }
        else
        {
            [PSTicket information].tcktEventShippingInfoZipCode = textField.text;
            [self getZipCodeInfo];
        }
    }

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtZipCode)
    {
        if ([string isEqualToString:@" "] )
        {
           
            return NO;
        }
        if ([textField.text length] >= kZIP_CODE_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtPhone || textField == self.txtMobile)
    {
        if ([textField.text length] >= kPHONE_NUMBER_LIMIT && range.length == 0)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.txtCountry || textField == self.txtState || textField == self.txtSelectPreviousBillings)
    {
        return NO;
    }
    
    return YES;
}


@end
