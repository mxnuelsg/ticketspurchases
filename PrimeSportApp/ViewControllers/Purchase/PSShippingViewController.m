//
//  PSShippingViewController.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 20/06/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSShippingViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PSModelPurchaseFlow.h"
#import "PSPaymentViewController.h"

static int const kCOUPON_CODE_LIMIT = 16;

@interface PSShippingViewController () <modelPurchaseFlowDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UIToolbar *myToolbar;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSection;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRow;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSubtotal;
@property (strong, nonatomic) UIBarButtonItem *btnPayment;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnToolbarNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCouponCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCouponCode;
@property (weak, nonatomic)  UITextField *txtCouponCode;

@property (weak, nonatomic) IBOutlet PSTextfieldWithoutMenu *txtShippingMethodSelected;

@property (weak, nonatomic) IBOutlet UILabel *lblSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingCost;
@property (weak, nonatomic) IBOutlet UILabel *ServiceFee;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;

@property (strong, nonatomic) UIPickerView *pickerQuantity;

//Variables
@property (strong, nonatomic) NSMutableArray *arrayShippingMethods;
@property (assign, nonatomic) double pricePerTicketsSelected;
@property (assign, nonatomic) double shippingCost;
@property (assign, nonatomic) double total;
@property (assign, nonatomic) NSUInteger ShippingId;

//Model
@property (strong, nonatomic) PSModelPurchaseFlow *model;

@end

@implementation PSShippingViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.model = [[PSModelPurchaseFlow alloc] init];
        self.model.delegate = self;
        
        self.arrayShippingMethods = [NSMutableArray array];
        self.shippingCost  = 0.0;
        self.ShippingId    = 0;
        
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    [PSTicket information].isPurchaseProcessActive = YES;
    
    self.lblHeaderQuantity.layer.cornerRadius = 11;
    [self.myScrollView contentSizeToFit];
    
    [self loadNavBar];
    [self loadToolbar];
    [self loadPickerView];
    [self fillViewInformation];
    [self calculatePrice];
    [self getShippingMethods];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelProcessAndLogOut) name:@"cancelPurhaseProcess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"Shipping/Summary dealloc");

    self.model.delegate         = nil;
    self.arrayShippingMethods   = nil;
    self.pickerQuantity         = nil;
    self.myScrollView           = nil;
    self.btnPayment             = nil;
    
    [[PSTicket information]reset];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cancelPurhaseProcess" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

#pragma mark -
#pragma mark - KEYBOARD NOTIFICATIONS
- (void)keyboardDidShow: (NSNotification *) notif
{
    self.btnPayment.enabled = NO;
    self.btnToolbarNext.enabled = NO;
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    self.btnPayment.enabled = YES;
    self.btnToolbarNext.enabled = YES;
}


#pragma mark -
#pragma mark - ACTION NOTIFICATIONS
-(void)cancelProcessAndLogOut
{
    //Logout sessions
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    [[PSSessionParameters information] reset];
    
    //Reset stack in navigationController from My Account
    PSAppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    [(PSPrincipalViewController *)[delegate.window rootViewController] goToMyAccountRoot];
    
    //Close modal
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - NAVIGATION BAR CONFIGURATION
-(void)loadNavBar
{
    UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                 style:UIBarButtonItemStyleBordered
                                                                target:self
                                                                action:@selector(cancelAndClosePurhaseProcess)];
    
    
    
    
    self.btnPayment = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                style:UIBarButtonItemStyleBordered
                                                               target:self
                                                               action:@selector(goPayment)];
    
    btnClose.tintColor = [UIColor whiteColor];
    self.btnPayment.tintColor = [UIColor primeSportColorDoneButton];
    self.navigationItem.leftBarButtonItem = btnClose;
    self.navigationItem.rightBarButtonItem = self.btnPayment;
}

-(void)cancelAndClosePurhaseProcess
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)goPayment
{
    [self.view endEditing:YES];
    
    if (self.ShippingId > 0)
    {
        if ([self.lblCouponCode.text isBlank])
        {
            [PSTicket information].tcktEventCouponCode = kEmptyField;
        }
        else
        {
            [PSTicket information].tcktEventCouponCode = self.lblCouponCode.text;
        }
        
        [PSTicket information].tcktEventShippingMethodId = [NSString stringWithFormat:@"%lu", (unsigned long)self.ShippingId];
        [PSTicket information].tcktEventShippingMethod = self.txtShippingMethodSelected.text;
        
        //call view controller
        PSPaymentViewController *vcPayment = [[PSPaymentViewController alloc]init];
        vcPayment.title = @"Payment";
        
        [self.navigationController pushViewController:vcPayment animated:YES];
    }
    else
    {
        [[PSMessages getModel]showAlertMessage:@"There is not a valid shipping method. Please, return and try again."
                                     withTitle:kWarningTitle];
    }

}

#pragma mark -
#pragma mark - TOOL BAR CONFIGURATION
-(void)loadToolbar
{
    self.myToolbar.barStyle = UIBarStyleBlackOpaque;
    self.myToolbar.barTintColor = [UIColor primeSportColorToolBar];
    [self.myToolbar sizeToFit];
}

-(IBAction)toolbarButtonNext
{
    [self goPayment];
}

#pragma mark -
#pragma mark - LOAD INFORMATION
-(void)fillViewInformation
{
    //Header
    self.lblHeaderSection.text = [NSString stringWithFormat:@"Section %@", [PSTicket information].tcktEventSection];
    
    self.lblHeaderRow.text     = [NSString stringWithFormat:@"Row %@", [PSTicket information].tcktEventRow];
    [self.lblHeaderRow boldSubstring:[PSTicket information].tcktEventRow];
    
    self.lblHeaderQuantity.text = [PSTicket information].tcktEventQuantity;
    self.lblHeaderQuantity.adjustsFontSizeToFitWidth = YES;
    
    self.lblHeaderPrice.text = [NSString stringWithFormat:@"Price %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPrice withFraction:YES]];
    
    self.lblHeaderSubtotal.text = [NSString stringWithFormat:@"Sub Total %@", [PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
    [self.lblHeaderSubtotal boldSubstring:[PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES]];
    
    //Body
    self.lblSubTotal.text        = [PSUtils moneyFormat:[PSTicket information].tcktEventPricePerQuantity withFraction:YES];
    self.pricePerTicketsSelected = [[PSTicket information].tcktEventPricePerQuantity doubleValue];
}

#pragma mark -
#pragma mark - BUTTON ACTIONS
-(IBAction)enterCouponCode
{
    if ([self.btnCouponCode.titleLabel.text isEqualToString:@"Coupon Code"])
    {
        UIAlertView *alertForgotPassword = [[UIAlertView alloc] initWithTitle:@"Coupon Code"
                                                                      message:nil
                                                                     delegate:self
                                                            cancelButtonTitle:@"Cancel"
                                                            otherButtonTitles:@"Save", nil];
        
        alertForgotPassword.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        self.txtCouponCode = [alertForgotPassword textFieldAtIndex:0];
        self.txtCouponCode.delegate = self;
        self.txtCouponCode.keyboardType = UIKeyboardTypeDefault;
        self.txtCouponCode.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        self.txtCouponCode.textAlignment = NSTextAlignmentCenter;
        
        [alertForgotPassword show];
        
    }
    else
    {
        //Transform Text
        CATransition *animation = [CATransition animation];
        animation.duration = 0.1;
        animation.type = kCATransitionFade;
        animation.subtype = kCATransitionFade;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [self.btnCouponCode.layer addAnimation:animation forKey:@"changeTextTransition"];
        [self.lblCouponCode.layer addAnimation:animation forKey:@"changeTextTransition"];
        
        [self.btnCouponCode setTitleColor:[UIColor primeSportColorNavigationController] forState:UIControlStateNormal];
        [self.btnCouponCode setTitle:@"Coupon Code" forState:UIControlStateNormal];
        self.lblCouponCode.text = [NSString string];
    }
}

#pragma mark -
#pragma mark - DELEGATE METHODS (UIAlertView)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *coupon = [[alertView textFieldAtIndex:0].text uppercaseString];
        coupon = [PSUtils removeAllWhiteSpacesInString:coupon];
        
        if ([coupon isBlank] == NO)
        {
            //Transform Text
            CATransition *animation = [CATransition animation];
            animation.duration = 0.1;
            animation.type = kCATransitionFade;
            animation.subtype = kCATransitionFade;
            animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            [self.btnCouponCode.layer addAnimation:animation forKey:@"changeTextTransition"];
            [self.lblCouponCode.layer addAnimation:animation forKey:@"changeTextTransition"];
            
            [self.btnCouponCode setTitleColor:[UIColor primeSportColorRed] forState:UIControlStateNormal];
            [self.btnCouponCode setTitle:@"Remove Coupon" forState:UIControlStateNormal];
             self.lblCouponCode.text  = coupon;
        }
    }
}

#pragma mark -
#pragma mark - EXECUTE WEB SERVICE
-(void)getShippingMethods
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES withTitle:nil andMessage:nil];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[PSTicket information].tcktEventId forKey:@"TicketIds"];
    
    PSWebService *webService = [[PSWebService alloc] init];
    webService.wsMethod = kShippingMethods;
    webService.wsParameters = parameters;
    
    [self.model getShippingMethodsWithObject:webService];
}

#pragma mark -
#pragma mark - MODEL METHODS (PSModelPurchaseFlow)
-(void)modelResponseForShippingMethods:(NSDictionary *)result
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if (result != nil)
    {
        
        for (id key in result)
        {
            [self.arrayShippingMethods addObject:key];
        }
        
        //1st option Auto-Selection
        if (self.arrayShippingMethods.count > 0)
        {
            [self pickerView:self.pickerQuantity didSelectRow:0 inComponent:0];
        }
    }
}

#pragma mark -
#pragma mark - PICKER VIEW & TOOLBAR CONFIGURATION
-(void)loadPickerView
{
    //Toolbar for fields
    UIToolbar *toolBarKeyboard = [[UIToolbar alloc]init];
    toolBarKeyboard.barStyle = UIBarStyleBlackOpaque;
    toolBarKeyboard.barTintColor = [UIColor primeSportColorNavigationController];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done"
                                                               style:UIBarButtonItemStyleDone
                                                              target:self
                                                              action:@selector(hideKeyboard)];
    btnDone.tintColor = [UIColor whiteColor];
    
    toolBarKeyboard.items = [NSArray arrayWithObjects:
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil],
                             btnDone,
                             nil];
    [toolBarKeyboard sizeToFit];
    
    self.txtShippingMethodSelected.inputAccessoryView  = toolBarKeyboard;
    
    //Picker for Quantity Field
    self.pickerQuantity = [[UIPickerView alloc]init];
    self.pickerQuantity.delegate = self;
    self.pickerQuantity.dataSource = self;
    self.pickerQuantity.backgroundColor = [UIColor whiteColor];
    self.pickerQuantity.showsSelectionIndicator = YES;
    self.pickerQuantity.multipleTouchEnabled = NO;
    self.pickerQuantity.exclusiveTouch = YES;

    
    self.txtShippingMethodSelected.inputView = self.pickerQuantity;
}

-(void)hideKeyboard
{
    [self.txtShippingMethodSelected resignFirstResponder];
}

#pragma mark -
#pragma mark - UITEXTFIELD DELEGATE
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtCouponCode)
    {
        if ([string isEqualToString:@" "] )
        {
            return NO;
        }
        
        if ([textField.text length] >= kCOUPON_CODE_LIMIT && range.length == 0)
        {
            return NO;
        }
    }
    
    if (textField == self.txtShippingMethodSelected)
    {
        return  NO;
    }
    return YES;
}

#pragma mark -
#pragma mark - DATASOURCE UIPICKERVIEW
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.arrayShippingMethods.count > 0)
    {
        return self.arrayShippingMethods.count;
    }
    else
    {
        return 1;
    }
}

#pragma mark -
#pragma mark - DELEGATE UIPICKERVIEW
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
     NSString *title;
    
    if (self.arrayShippingMethods.count > 0)
    {
        title = [[self.arrayShippingMethods objectAtIndex:row]valueForKey:@"Name"];
    }
    else
    {
        title = @"Empty Shipping Methods";
    }
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.arrayShippingMethods.count > 0)
    {
        //Reset shipping storaged info
        [[PSTicket information] resetShippingInfo];

        //Shipping Info Selected
        self.txtShippingMethodSelected.text = [NSString stringWithFormat:@"%@",[[self.arrayShippingMethods objectAtIndex:row]valueForKey:@"Name"]];
        
        self.shippingCost = [[NSString stringWithFormat:@"%@",[[self.arrayShippingMethods objectAtIndex:row]valueForKey:@"Cost"]] doubleValue];
        self.ShippingId   = [[NSString stringWithFormat:@"%@",[[self.arrayShippingMethods objectAtIndex:row]valueForKey:@"ShippingMethodId"]] integerValue];
        
        //Update Fields
        self.lblShippingCost.text = [PSUtils moneyFormat:[NSString stringWithFormat:@"%f", self.shippingCost]
                                            withFraction:YES];
        
        [self calculatePrice];
    }
}

#pragma mark -
#pragma mark - GET TOTAL PRICE
-(void)calculatePrice
{
    self.total = 0;
    self.total = self.pricePerTicketsSelected + self.shippingCost;
    
    self.lblTotal.text = [PSUtils moneyFormat:[NSString stringWithFormat:@"%f", self.total] withFraction:YES];
    self.lblTotal.adjustsFontSizeToFitWidth = YES;
}


@end
