//
//  PSAppDelegate.m
//  PrimeSportApp
//
//  Created by Manuel Salinas on 14/05/14.
//  Copyright (c) 2014 Definity First. All rights reserved.
//

#import "PSAppDelegate.h"
#import "Reachability.h"

@implementation PSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Show status bar & change text to white color
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //Enable automatic AFNetworking activity indicator
     [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    //Reachability
    Reachability *netReachability = [Reachability reachabilityForInternetConnection];
    
    netReachability.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (reachability.isReachableViaWWAN == NO)
            {
                [[PSMessages getModel]showAlertForUnReachableNetwork];
            }
        });
    };
    
    [netReachability startNotifier];
    
    //Active Session Object
    self.psSession = [[PSSessionParameters alloc]init];
    self.psTicket  = [[PSTicket alloc]init];
    
    //Facebook API: Check Session
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
    {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Handler for session state changes
                                          // This method will be called EACH time the session state changes,
                                          // also for intermediate states and NOT just when the session open
                                          
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
    }

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Handle the user leaving the app while the Facebook login dialog is being shown
    // For example: when the user presses the iOS "home" button while the login dialog is active
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession closeAndClearTokenInformation];
    [[PSSessionParameters information] reset];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    /*
        - During the Facebook login flow, your app passes control to the Facebook iOS app or Facebook in a mobile browser.
        - After authentication, your app will be called back with the session information.
     */

    //response when goes to safari
    // Note this handler block should be the exact same as the handler passed to any open calls.
    [FBSession.activeSession setStateChangeHandler:^(FBSession *session, FBSessionState state, NSError *error) {
        
        // Retrieve the app delegate
        PSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
        // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
        [appDelegate sessionStateChanged:session state:state error:error];
    }];
    
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error
{
    if (!error && state == FBSessionStateOpen)
    {
        //NSLog(@"Facebook Session opened");
        // Show the user the logged-in UI
        //        [self userLoggedIn];
        
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 // Success! Include your code to handle the results here
                 if (kLogActivated)
                 {
                     NSLog(@"user info: %@", result);
                     NSLog(@"Access Token: %@", session.accessTokenData.accessToken);
                 }
                 
                 [[PSSessionParameters information] setFacebookSessionParametersWithToken:session.accessTokenData.accessToken
                                                                                withEmail:[result objectForKey:@"email"]
                                                                           withIdentifier:[result objectForKey:@"id"]];
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginFacebook" object:self];
             }
             else
             {
                 // An error occurred, we need to handle the error
                 NSLog(@"An active access token must be used to query information about the current user.");
             }
         }];
        return;
    }
    
    /*
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed)
    {
        // If the session is closed
        // Show the user the logged-out UI
        // [self userLoggedOut];
    }
     */
    
    // Handle errors
    if (error)
    {
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES)
        {
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            
            [[PSMessages getModel]showAlertMessage:alertText
                                         withTitle:alertTitle];
        }
        else
        {
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
            {
                NSLog(@"User cancelled Facebook login");
                [[PSMessages getModel]showAlertMessage:@"We invite you to create an account with PrimeSport."
                                             withTitle:@"Facebook Login Cancelled"];
            }
            else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
            {
                alertTitle = @"Facebook Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                
                [[PSMessages getModel]showAlertMessage:alertText
                                             withTitle:alertTitle];
            }
            else
            {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                
                [[PSMessages getModel]showAlertMessage:alertText
                                             withTitle:alertTitle];
            }
        }
        
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        [[PSSessionParameters information] reset];
        
        // Show the user the logged-out UI
        //[self userLoggedOut];
    }
}

@end
